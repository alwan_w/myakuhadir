
    <div class="card-block button-list">
      <div class="modal fade modal-flex" id="default-Modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Ganti Password</h4>
            </div>

            <div class="modal-body">
              <?php if ($this->session->flashdata('pesan') != '') : ?>
                <div class="col-sm-12">
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-top: -10px;">&times;</a>
                    <strong><?= $this->session->flashdata('pesan'); ?></strong>
                  </div>
                </div>
              <?php endif; ?>
              <form method="POST" action="<?=base_url()?>dashboard/ganti-password">
                <div class="form-group">
                  <label for="exampleInputPassword1" class="form-control-label">Password Sekarang</label>
                  <input hidden="" type="text" name="referred_from" value="<?=current_url()?>">
                  <input type="password" class="form-control" name="old_password" id="exampleInputPassword1" placeholder="Masukkan Password Anda" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1" class="form-control-label">Password Baru</label>
                  <input type="password" class="form-control" name="new_password" id="exampleInputPassword1" placeholder="Masukkan Password Baru Anda" required>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Batalkan</button>
                  <button type="submit" class="btn btn-primary waves-effect waves-light ">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>