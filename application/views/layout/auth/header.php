<!DOCTYPE html>
<html lang="en">
<head>
	<title>akuhadir : tunjukkan kehadiranmu</title>
	<!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="google-signin-client_id" content="1023195713704-8vkr7sr079klis6of9gdo9han9d7u7g5.apps.googleusercontent.com">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="Absen Akuhadir.com">
	<meta name="description" content="Sistem Absensi berbasis web dari akuhadir.com">
	<meta name="theme-color" content="#2196F3" />


	<link rel="apple-touch-icon" href="<?php echo base_url('images/icons/icon-152x152.png')?>">
	

	<script type="text/javascript" src="<?php echo base_url('assets/instascan.min.js') ?>" ></script>
	<!-- Favicon icon -->
	<link rel="shortcut icon" href="<?=base_url()?>/assets/favicon.png" type="image/x-icon">
	<link rel="icon" href="<?=base_url()?>/assets/favicon.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/plugins//sweetalert/css/sweetalert.css">
	<link rel="manifest" href="<?php echo base_url('assets/manifest.json') ?>">

	<!-- Google font-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="<?=base_url()?>/theme/ltr-horizontal/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!--ico Fonts-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/assets/icon/icofont/css/icofont.css">

	<!-- Required Fremwork -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap/css/bootstrap.min.css">

	<!-- waves css -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//waves/css/waves.min.css">

	<!-- Style.css -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/assets/css/main.css">

	<!-- Responsive.css-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/assets/css/responsive.css">

	<!--color css-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/assets/css/color/color-1.min.css" id="color"/>

</head>
<body>
