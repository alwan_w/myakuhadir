<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
	<h1>Warning!!</h1>
	<p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
	<div class="iew-container">
		<ul class="iew-download">
			<li>
				<a href="http://www.google.com/chrome/">
					<img src="/theme/ltr-horizontal/assets/images/browser/chrome.png" alt="Chrome">
					<div>Chrome</div>
				</a>
			</li>
			<li>
				<a href="https://www.mozilla.org/en-US/firefox/new/">
					<img src="/theme/ltr-horizontal/assets/images/browser/firefox.png" alt="Firefox">
					<div>Firefox</div>
				</a>
			</li>
			<li>
				<a href="http://www.opera.com">
					<img src="/theme/ltr-horizontal/assets/images/browser/opera.png" alt="Opera">
					<div>Opera</div>
				</a>
			</li>
			<li>
				<a href="https://www.apple.com/safari/">
					<img src="/theme/ltr-horizontal/assets/images/browser/safari.png" alt="Safari">
					<div>Safari</div>
				</a>
			</li>
			<li>
				<a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
					<img src="/theme/ltr-horizontal/assets/images/browser/ie.png" alt="">
					<div>IE (9 & above)</div>
				</a>
			</li> 
		</ul>
	</div>
	<p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jqurey -->
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<!-- tether.js -->
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/popper.js/js/popper.min.js"></script>
<!-- waves effects.js -->
<!--
<script src="/theme/ltr-horizontal/assets/plugins//waves/js/waves.min.js"></script>
-->
<!-- Required Framework -->
<script src="<?php echo base_url() ?>theme/ltr-horizontal/assets/plugins/notification/js/bootstrap-growl.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/pages/elements.js"></script>
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/pages/preloader.js"></script>

<!--
<script type="text/javascript" src="/theme/ltr-horizontal/assets/js/common-pages.min.js"></script>
-->
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="<?php echo base_url() ?>/theme/ltr-horizontal/assets/plugins//sweetalert/js/sweetalert.js"></script>
<script src="<?php echo base_url() ?>assets/notify.js"></script>

<script>
    $(document).ready(function(){
    	var success_sound = new Audio('<?php echo base_url("assets/success.wav") ?>');
		var error_sound = new Audio('<?php echo base_url("assets/error.wav") ?>');

		swal("Selamat Datang!", "Untuk melakukan absensi, silahkan letakkan kartu anda didepan kamera :)", "info");
		let scanner = new Instascan.Scanner(
	        {
	            video: document.getElementById('preview')
	        }
	    );
	    scanner.addListener('scan', function(content) {
	        //alert('Konten QR : '+content)
	        $.ajax({
	            type : "POST",
	            url  : "<?php echo base_url('absen/insert')?>",
	            dataType : "JSON",
	            data : {content:content},
	            success: function(){
	                //notify('Selamat datang. Anda telah melakukan absen', 'success');
	                swal({
					     title: "Sukses Melakukan Absen !",
					     text: "Selamat datang semoga harimu menyenangkan :)",
					     type: "success",
					     timer: 1000
				     })
	                success_sound.play();
	            },
	            error: function(){
	            	//notify('Maaf !! Anda tidak dapat melakukan absen', 'warning');
	                swal({
					     title: "Gagal!",
					     text: "Mohon maaf anda tidak bisa absen :(",
					     type: "warning",
					     timer: 1000
				     })
	                error_sound.play();
	            }
	        });
	        return false;
	    });
	    Instascan.Camera.getCameras().then(cameras => 
	    {
	        if(cameras.length > 0){
	            scanner.start(cameras[0]);
	        } else {
	            swal("Perhatian", "Berikan applikasi ini untuk mengakses Kamera", "Warning");
	        }
	    });
	});
</script>
</body>
</html>
