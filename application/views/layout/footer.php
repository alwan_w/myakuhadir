<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="/theme/ltr-horizontal/assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="/theme/ltr-horizontal/assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="/theme/ltr-horizontal/assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="/theme/ltr-horizontal/assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="/theme/ltr-horizontal/assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->

<!-- Required Jqurey -->
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/popper.js/js/popper.min.js"></script>

<!-- Required Fremwork -->
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap/js/bootstrap.min.js"></script>

<!-- waves effects.js -->
<script src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//waves/js/waves.min.js"></script>

<!-- Scrollbar JS-->
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//jquery.nicescroll/js/jquery.nicescroll.min.js"></script>


<!--classic JS-->
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/classie/js/classie.js"></script>

<!-- notification -->
<script src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//notification/js/bootstrap-growl.min.js"></script> 

<!-- data table js -->
<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>

<!-- sparkline Chart js -->
<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jquery-sparkline/js/jquery.sparkline.js"></script>

<!-- custom js -->
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/js/main.js"></script>
<script type="text/javascript" src="<?=base_url()?>/assets/zabuto_calendar.min.js" ></script>
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/pages/issue-list.js"></script>
<script src="<?php echo base_url() ?>/theme/ltr-horizontal/assets/pages/modal.js"></script>
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/pages/elements.js"></script>
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/pages/preloader.js"></script>
<script src="<?=base_url()?>/theme/ltr-horizontal/assets/js/menu-horizontal.min.js"></script>
<script src="<?php echo base_url() ?>/theme/ltr-horizontal/assets/plugins//notification/js/bootstrap-growl.min.js"></script>

<script src="<?php echo base_url() ?>/theme/ltr-horizontal/assets/plugins//sweetalert/js/sweetalert.js"></script>
<script src="<?=base_url()?>/theme/ltr-horizontal/assets/pages/data-table.js"></script>

<!-- Date picker.js -->
<script src="<?php echo base_url() ?>/theme/ltr-horizontal/assets/plugins//datepicker/js/moment-with-locales.min.js"></script>
<script src="<?php echo base_url() ?>/theme/ltr-horizontal/assets/plugins//bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<!-- Bootstrap Datepicker js -->
<script type="text/javascript" src="<?php echo base_url() ?>/theme/ltr-horizontal/../bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url() ?>/theme/ltr-horizontal/assets/plugins//bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>
<!-- Select 2 js -->
<script src="<?php echo base_url() ?>/theme/ltr-horizontal/../bower_components/select2/js/select2.min.js"></script>

<script src="<?php echo base_url() ?>theme/ltr-horizontal/assets/plugins/notification/js/bootstrap-growl.min.js"></script>
<!-- Multi Select js -->
<script src="<?php echo base_url() ?>/theme/ltr-horizontal/../bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
<script src="<?php echo base_url() ?>/theme/ltr-horizontal/../bower_components/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>/theme/ltr-horizontal/assets/plugins/multi-select/js/jquery.quicksearch.js"></script>
<script src="<?php echo base_url() ?>assets/notify.js"></script>


<script src="<?php echo base_url('theme/ltr-horizontal/') ?>assets/plugins//form-mask/js/inputmask.js"></script>
<script src="<?php echo base_url('theme/ltr-horizontal/') ?>assets/plugins//form-mask/js/jquery.inputmask.js"></script>
<script src="<?php echo base_url('assets/prefix.js') ?>"></script>


<script type="text/javascript">
  $('#member_tlp').inputmask({ mask: "62 999 9999 9999"});
    $(function(){  
      $('.datetimepicker1').bootstrapMaterialDatePicker({
          format: 'HH:mm',
          //lang : 'id',
          clearButton: true,
          weekStart: 1,
          time: true,
          date: false
      });

  });
    $('#simpletable').on('click', '.alert-delete', function(){
         var currentrow = $(this).closest('tr')
         var id_pengguna = currentrow.find('td:eq(0)').text()
         swal({
            title: "Hapus data?",
            text: "Yakin ingin menghapus data?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function(){
            window.location.href = "<?php echo base_url('SiswaController/deleteSiswa/') ?>"+id_pengguna
        });
    })
    $('#search').click(function(){
        var bulan = ['None', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
        var start = $('#start').val()
        var end = $('#end').val()
        var status = $('#status').val();
        $('#tbody').each(function(){
            var row = $(this).row
            var dateLong = row.find('td').eq(0).text().split("--")[1]
            var statusTb = row.find('td').eq(2).text()
            var dateArr = dateLong.split(" ")
            var dateBulan = bulan.indexOf(dateArr[1])
            if(dateBulan<10)
              var dateTxt = dateArr[2] + "-0" + dateBulan + "-" + dateArr[0]
            else
              var dateTxt = dateArr[2] + "-" + dateBulan + "-" + dateArr[0]
            var show = true
            if (statusTb != status) {
              show = false
            }
            if(status == 'Semua' || status == ''){
              show = true
            }
           if (dateTxt < start || dateTxt > end) {
                show = false
            }
            if (show==true) {
                row.show()
            }
            else{
              alert('test')
                row.hide()
            }

        })
    })
</script>
<script type="text/javascript">
     $(document).ready(function() {
        $('.js-example-basic-single').select2({
            placeholder: "Select a state"
        });
    });
</script>
<script type="text/javascript">
  $('#nextMonth').click(function(){
    alert('next')
  })
</script>
<script type="text/javascript">
  $(document).ready(function(){
      var tb_siswa = $('#tbl-siswa')
      var btn = $('#addSiswa')
      var list = $('#list-siswa')
    $('#addSiswa').click(function(){
        var siswa = $('#siswa').children("option:selected").val()
        if (siswa == 'null') {
             swal('Warning !!', 'Pilih Siswa Terlebih dahulu', 'warning')
        }
        else{
           var obj = JSON.parse(siswa);
            list.append('<tr id="copy"><td><input type="text" hidden readonly value="'+obj.id+'" name="id[]">'+obj.induk+'</td><td>'+obj.name+'</td><td><span id="remove" class="btn btn-danger icofont icofont-close" ></span></td></tr>')
        }
        
        
    })
    list.on('click', '#remove', function(){
        $(this).parents('#copy').remove()
      })
  })

</script>

<script>
  $('.boxJadwal').click(function(e){
    if( $(e.target).closest('#editJD, #saveJam').length === 0 ){
        $('.boxJadwal').not(this).removeClass('aktif')
      if(!$(this).hasClass('aktif'))
      {
        $(this).addClass('aktif')
        var id = $(this).find('#idJadwal').text()
        var notif = $(this).find('#jmJudul').text() + " Aktif"
        $.ajax({
              url : "<?php echo base_url('JadwalController/updateJamMasuk')?>",
              type: "POST",
              dataType: "JSON",
              data: {id:id} ,
              success: function(data)
              {
                  alert(notif)
              },
              error: function ()
              {
                  alert('Error saving data');
              }
          });
      }
    }
      
  })
  $(document).click(function(e) {
      if( $(e.target).closest('#editJD, #saveJam').length === 0 ){
        $('#editJD').attr('readonly', 'true')
        $('#editJD').attr('class', 'notEdit')
        $('#editJD').next().hide()
      }
   
});
$('.saveJam').dblclick(function(){
    var id = $('#idJM').text();
    var jam = $('#editJD'+id).val();

    $.ajax({
        url : "<?php echo base_url('JadwalController/updateJam')?>",
        type: "POST",
        dataType: "JSON",
        data: {id:id, jam:jam} ,
        success: function(data)
        {
            alert('Jam Masuk berhasil diubah')
        },
        error: function ()
        {
            alert('Error saving data');
        }
    });
})
  $('editJD').dblclick(function(){
      $(this).removeAttr('readonly')
      $(this).attr('class', 'editable')
      $(this).next().show()
  })
</script>
<?php if ($this->session->flashdata('pesan') != '') : ?>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#default-Modal').modal('show');
    });
</script>
<?php endif;?>
<?php if ($this->session->flashdata('pesanRekap') != '') : ?>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#downloadModal').modal('show');
    });
</script>
<?php endif;?>

</body>
<?php echo $this->session->flashdata('msg'); ?>
</html>
