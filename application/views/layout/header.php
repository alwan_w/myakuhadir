<!DOCTYPE html>
<html lang="en">
<head>
    <title>Akuhadir.com</title>

    <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="<?=base_url()?>assets/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?=base_url()?>assets/favicon.png" type="image/x-icon">


    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<!--Date Picker Material Icon Css-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- iconfont -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/icon/icofont/css/icofont.css">


    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <!-- simple line icon -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/icon/simple-line-icons/css/simple-line-icons.css">

    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/../bower_components/bootstrap/css/bootstrap.min.css">
    
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/plugins//sweetalert/css/sweetalert.css">

    <!-- data table css -->
      <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/../bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/plugins//data-table/css/buttons.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/../bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">

    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/css/main.css">

    <!-- Responsive.css-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/css/responsive.css">    <!--color css-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/css/color/<?=$this->session->userdata('sess_tema')?>.css" id="color"/>

    <!-- Bootstrap Date-Picker css -->
    <link rel="stylesheet" href="<?=base_url()?>//theme/ltr-horizontal/assets/plugins//bootstrap-datepicker/css/bootstrap-datetimepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/../bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />


    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/') ?>zabuto_calendar.min.css">
    <style type="text/css">
         .my-event {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            border-radius: 2px;            
            color: #ffffff;
            border: 1px solid #1867c0;
            font-size: 12px;
            padding: 3px;
            cursor: pointer;
            margin-bottom: 1px;
            left: 4px;
            margin-right: 8px;
            position: relative;    
          }
          .my-event.with-time {
            position: absolute;
            right: 4px;
            margin-right: 0px;
          }
          .ontime {
            background-color: #1867c0;
          }
          .late {
            background-color: red;
          }
    </style>
</head>
<body class="horizontal-fixed fixed">
