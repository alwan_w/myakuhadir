<body class="horizontal-fixed fixed">
 <div class="wrapper">
    <div class="loader-bg">
    <div class="loader-bar">
    </div>
  </div>
   <!-- Navbar-->
      <header class="main-header-top hidden-print">

        <a href="index.html" class="logo"><img class="img-fluid able-logo" src="<?php echo base_url('theme/ltr-horizontal/assets/images/logo.png') ?>" alt="Theme-logo"></a>

        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button--><a href="#!" data-toggle="offcanvas" class="sidebar-toggle hidden-md-up"></a>
          <!-- Navbar Right Menu-->
          <div class="navbar-custom-menu">
            <ul class="top-nav">
                 <!-- window screen -->
                  <li class="pc-rheader-submenu">
                    <a href="#!" class="drop icon-circle" onclick="javascript:toggleFullScreen()">
                      <i class="icon-size-fullscreen"></i>
                    </a>

                  </li>
                  <!-- User Menu-->
                  <li class="dropdown">
                    <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle drop-image">
                      <span><img class="rounded-circle " src="<?=base_url()?>//theme/ltr-horizontal/assets/images/avatar-1.png" style="width:40px;" alt="User Image"></span>
                      <span>John <b>Doe</b> <i class=" icofont icofont-simple-down"></i></span>

                    </a>
                    <ul class="dropdown-menu settings-menu">
                      <li><a href="#!"><i class="icon-settings"></i> Settings</a></li>
                      <li><a href="profile.html"><i class="icon-user"></i> Profile</a></li>
                      <li><a href="message.html"><i class="icon-envelope-open"></i> My Messages</a></li>
                      <li class="p-0">
                        <div class="dropdown-divider m-0"></div>
                      </li>
                      <li><a href="lock-screen.html"><i class="icon-lock"></i> Lock Screen</a></li>
                      <li><a href="#!"><i class="icon-logout"></i> Logout</a></li>

                    </ul>
                  </li>
                </ul>

                
           <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
         </div>
         <!-- search end -->
       </div>
     </nav>
   </header>
   <aside class="main-sidebar hidden-print ">
  <section class="sidebar" id="sidebar-scroll">
    <!--horizontal Menu Starts-->
    <ul class="sidebar-menu">
      <li class="nav-level">Navigation</li>
     <!--  <li class="active treeview"><a href="#"><i class="icon-speedometer"></i><span>  Dashboard </span><i class="icon-arrow-down"></i></a>
        <ul class="treeview-menu">
          <li class="h-active"><a href="index.html"><i class="icon-arrow-right"></i><span> Dashboard 1 </span></a>
          </li>
          <li><a href="dashboard2.html"><i class="icon-arrow-right"></i><span> Dashboard 2 </span></a>
          </li>
          <li><a href="dashboard3.html"><i class="icon-arrow-right"></i><span> Dashboard 3 </span></a>
          </li>
          <li><a href="dashboard4.html"><i class="icon-arrow-right"></i><span> Dashboard 4 </span></a>
          </li>
        </ul>
      </li> -->
      <li>
        <a href="#">
          <i class="icon-notebook"></i><span> Jurnal Sebelumnya </span>
        </a>
      </li>
    </ul>
  </section>
</aside>