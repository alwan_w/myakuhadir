<?php 
$foto = $this->session->userdata('sess_foto');
if(is_null($foto) || $foto == '')
  $foto = 'default.png';
?>
 <div class="wrapper">
    <div class="loader-bg">
    <div class="loader-bar">
    </div>
  </div>
   <!-- Navbar-->
      <header class="main-header-top hidden-print">

        <a href="#" class="logo" style="text-align:left; width:auto;"><img style="max-width:auto; height:90%;" src="<?php echo base_url('assets/logos/'.$this->session->userdata('sess_logo_sekolah')) ?>" alt="Logo Sekolah"> <b style="margin-left: 5px;"><?= $this->session->userdata('sess_nama_sekolah')?></b></a>
        
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button--><a href="#!" data-toggle="offcanvas" class="sidebar-toggle hidden-md-up"></a>
          <!-- Navbar Right Menu-->
          <div class="navbar-custom-menu">
            <ul class="top-nav">
              <!--Notification Menu-->
              <!-- User Menu-->
              <li class="dropdown">
                <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle drop-image">
                  <span><img class="rounded-circle " src="<?=base_url('assets/photos/'.$foto)?>" style="width:40px;" alt="User Image"></span>
                  <span><b><?= $this->session->userdata('sess_name')?></b> <i class=" icofont icofont-simple-down"></i></span>

                </a>
                <ul class="dropdown-menu settings-menu">
                  <li><a href="<?= base_url().'dashboard/profile'?>"><i class="icon-user"></i> Profile</a></li>
                  <li class="p-0">
                    <div class="dropdown-divider m-0"></div>
                  </li>
                  <li><a href="#" data-toggle="modal" data-target="#default-Modal"><i class="icon-lock"></i> Ganti Password</a></li>
                  <li class="p-0">
                    <div class="dropdown-divider m-0"></div>
                  </li>
                  <li><a href="<?= base_url().'logout'?>"><i class="icon-logout"></i> Keluar</a></li>
                  <li class="p-0">
                    <div class="dropdown-divider m-0"></div>
                  </li>
              </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
   <!-- Side-Nav-->
   <aside class="main-sidebar hidden-print ">
    <section class="sidebar" id="sidebar-scroll">
      <!--horizontal Menu Starts-->
      <ul class="sidebar-menu">
        <li class="nav-level">Navigation</li>
        <!--li <?php if($this->uri->segment(2)==""){echo 'class="active"';}?> ><a href="<?=base_url()?>dashboard"><i class="icon-speedometer"></i><span>Dashboard</span></a>
        </li-->

        <li class="treeview <?php if($this->uri->segment(2)=="kelas" || $this->uri->segment(2)=="sekolah" || $this->uri->segment(2)=="pelajaran" || $this->uri->segment(2)=="waktu" || $this->uri->segment(2)=="siswa" || $this->uri->segment(2)=="civitas"){echo 'active';}?>"><a href="#"><i class="icon-organization"></i><span> Sekolah </span><i class="icon-arrow-down"></i></a>
          <ul class="treeview-menu">
            <li class="treeview"><a href="<?=base_url()?>dashboard/civitas"><i class="icon-arrow-right"></i><span>  Master data Guru & Karyawan </span></a>              
            </li>
            <li class="treeview"><a href="<?=base_url()?>dashboard/siswa"><i class="icon-arrow-right"></i><span>  Master data Siswa </span></a>              
            </li>
            <li class="treeview"><a href="<?=base_url()?>dashboard/kelas"><i class="icon-arrow-right"></i><span>  Master data Kelas </span></a>              
            </li>
            <li class="treeview"><a href="<?=base_url()?>dashboard/sekolah"><i class="icon-arrow-right"></i><span>  Master data Sekolah</span></a>              
            </li>
            <!--li class="treeview"><a href="<?=base_url()?>dashboard/pelajaran"><i class="icon-arrow-right"></i><span>  Master 
            data pelajaran </span></a>              
            </li-->
          </ul>
        </li>

        <!--li class="treeview <?php if($this->uri->segment(2)=="siswa" || $this->uri->segment(2)=="civitas"){echo 'active';}?>"><a href="#"><i class="icon-user-follow"></i><span> Civitas Akademik </span><i class="icon-arrow-down"></i></a>
          <ul class="treeview-menu">
            <li class="treeview"><a href="<?=base_url()?>dashboard/civitas"><i class="icon-arrow-right"></i><span>  Master data Guru & Karyawan </span></a>              
            </li>
            <li class="treeview"><a href="<?=base_url()?>dashboard/siswa"><i class="icon-arrow-right"></i><span>  Master data Siswa </span></a>              
            </li>
          </ul>
        </li-->

        <li class="treeview <?php if($this->uri->segment(2)=="jadwal" || $this->uri->segment(2)=="jadwal-sekolah" || $this->uri->segment(2)=="setting" || $this->uri->segment(2)=="jadwal-piket"){echo 'active';}?>"><a href="#"><i class="icon-grid"></i><span> Jadwal Sekolah </span><i class="icon-arrow-down"></i></a>
          <ul class="treeview-menu">
              <li class="treeview"><a href="<?=base_url()?>dashboard/jadwal"><i class="icon-arrow-right"></i><span>  Setting Jadwal Guru </span></a></li>
              <li class="treeview"><a href="<?=base_url()?>dashboard/jadwal-piket"><i class="icon-arrow-right"></i><span>  Setting Jadwal Piket </span></a></li>
              <li class="treeview"><a href="<?=base_url()?>dashboard/setting/jadwal-masuk"><i class="icon-arrow-right"></i><span>  Setting Jadwal Masuk Siswa </span></a></li>
          </ul>
        </li>

        <li class="treeview <?php if($this->uri->segment(2)=="result"){echo 'active';}?>"><a href="#"><i class="icon-user-follow"></i><span> Absensi </span><i class="icon-arrow-down"></i></a>
          <ul class="treeview-menu">
            <li class="treeview"><a href="<?= base_url()?>dashboard/result/izin-siswa"><i class="icon-arrow-right"></i><span> Izin Kehadiran Siswa </span></a></li>
            <li class="treeview"><a href="<?= base_url()?>dashboard/result/absensi/siswa"><i class="icon-arrow-right"></i><span> Hasil Absensi </span></a></li>
          </ul>
        </li>

        <li class="treeview"><a href="#"><i class="icon-camera"></i><span> Halaman Checklog </span><i class="icon-arrow-down"></i></a>
          <ul class="treeview-menu">
            <li class="treeview"><a target="_blank" href="<?= base_url()?>absen/guru"><i class="icon-arrow-right"></i><span> Guru & Karyawan </span></a></li>
            <li class="treeview"><a target="_blank" href="<?= base_url()?>absen"><i class="icon-arrow-right"></i><span> Siswa (QR Code)</span></a></li>
            <li class="treeview"><a target="_blank" href="<?= base_url()?>absen/manual"><i class="icon-arrow-right"></i><span> Siswa (Manual)</span></a></li>
          </ul>
        </li>

      </ul>
      <!--horizontal Menu Ends-->
    </section>
  </aside>

<?php $this->load->view('layout/ganti-password'); ?>