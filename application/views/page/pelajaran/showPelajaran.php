<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Edit Mata Pelajaran atau Ekstrakurikuler</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
           <form method="post" action="<?=base_url()?>dashboard/pelajaran/update/<?= $data[0]->id_mata_pelajaran ?>">
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Mata Pelajaran atau Ekstrakurikuler</label>
                <input type="text" class="form-control" name="nm_mata_pelajaran" placeholder="Matematika" required="" value="<?= $data[0]->nm_mata_pelajaran ?>">
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Mata pelajaran/Ekstrakurikuler</label>
                <select name="jenis_mata_pelajaran" class="form-control">
                  <!-- <option value="">-- Mata Pelajaran atau Ekstrakurikuler --</option>      -->
                  <option value="1">Mata Pelajaran</option>
                  <option value="2">Ekstrakurikuler</option>   
         
                </select>
              </div>

              <div class="form-group">                
                <input type="submit" class="btn btn-primary" value="Simpan">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>