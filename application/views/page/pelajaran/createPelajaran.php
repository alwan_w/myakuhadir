<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Tambah Mata Pelajaran</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
            <form method="POST" action="<?=base_url()?>dashboard/pelajaran/tambah">
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Mata Pelajaran atau Ekstrakurikuler</label>
                <input type="text" class="form-control" name="nm_mata_pelajaran" placeholder="Matematika" required="">
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Mata pelajaran/Ekstrakurikuler</label>
                <select name="jenis_mata_pelajaran" class="form-control">
                  <!-- <option value="">-- Mata Pelajaran atau Ekstrakurikuler --</option>      -->
                  <option value="1">Mata Pelajaran</option>
                  <option value="2">Ekstrakurikuler</option>            
                </select>
              </div>

              <div class="form-group">                
                <input type="submit" class="btn btn-primary" value="Simpan">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>