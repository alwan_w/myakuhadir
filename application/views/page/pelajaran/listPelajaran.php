<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Daftar Mata Pelajaran atau Ekstrakurikuler</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header"><a href="<?= base_url() ?>dashboard/pelajaran/tambah" class="btn btn-primary">Tambah Mapel/Ekstrakurikuler</a></div>
          <div class="card-block">
            <div class="data_table_main">
              <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                <thead>
                <tr>
                  <!-- <th>ID Pelajaran</th> -->
                  <th>Mata Pelajaran/Ekstrakurikuler</th>
                  <th>Jenis</th>
                  <th>Aksi</th>             
                </tr>
                </thead>
                <tfoot>
                <tr>
                  <!-- <th>ID Pelajaran</th> -->
                  <th>Mata Pelajaran/Ekstrakurikuler</th>
                  <th>Jenis</th>
                  <th>Aksi</th> 
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($data as $d) { ?>
                    <tr>                  
                      <!-- <td> <?= $d->id_mata_pelajaran ?></td> -->
                      <td><?= $d->nm_mata_pelajaran ?></td>
                      <td>
                        <?php if ($d->jenis_mata_pelajaran == "1"): ?>
                            Mata Pelajaran
                        <?php else: ?>
                            Ekstrakurikuler
                        <?php endif; ?>
                      </td>
                      <td>
                        <a href="<?= base_url() ?>dashboard/pelajaran/<?= $d->id_mata_pelajaran ?>">
                          <button type="button" class="btn btn-primary btn-icon waves-effect waves-light">
                           <i class="icofont icofont-ui-note"></i>
                          </button>
                        </a>
                        <a href="<?= base_url() ?>dashboard/pelajaran/hapus/<?= $d->id_mata_pelajaran ?>">
                          <button type="button" class="btn btn-danger btn-icon waves-effect waves-light" onclick="return confirm('Are you sure you want to delete <?php if($d->jenis_mata_pelajaran == "1") echo "Mata Pelajaran"; else echo "Ekstrakurikuler";?> <?= $d->nm_mata_pelajaran ?>?');">
                           <i class="icofont icofont-ui-delete"></i>
                          </button>
                        </a>
                      </td>
                    </tr>
                <?php } ?>                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>