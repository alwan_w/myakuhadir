<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Daftar Kelas Siswa</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header"><a href="<?= base_url () ?>dashboard/kelas-siswa/tambah" class="btn btn-primary">Tambah Siswa ke Kelas</a></div>
          <div class="card-block">
            <div class="data_table_main">
              <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                <thead>
                <tr>
                  <th>Kelas</th>
                  <th>Siswa</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                  <th>Kelas</th>
                  <th>Siswa</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($data as $d) { ?>
                    <tr>                  
                      <td> <?= $d->nm_kelas ?></td>
                      <!-- <td><?= $d->hari ?></td> -->
                      <td><?= $d->nm_pengguna ?></td>
                      <td>
                        <!-- <a href="<?= base_url() ?>dashboard/kelas-siswa/<?= $d->id_kelas_siswa ?>">
                          <button type="button" class="btn btn-primary btn-icon waves-effect waves-light">
                           <i class="icofont icofont-ui-note"></i>
                          </button>
                        </a> -->
                        <a href="<?= base_url() ?>dashboard/kelas-siswa/hapus/<?= $d->id_kelas_siswa ?>">
                          <button type="button" class="btn btn-danger btn-icon waves-effect waves-light" onclick="return confirm('Are you sure you want to this?');">
                           <i class="icofont icofont-ui-delete"></i>
                          </button>
                        </a>
                      </td>
                    </tr>
                <?php } ?>                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>