<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Tambah Siswa ke Kelas</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
            <form method="POST" action="<?=base_url()?>dashboard/kelas-siswa/tambah">

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nama Siswa</label>
                <select name="id_siswa" class="form-control">
                    <option value="">-- Pilih Nama Siswa --</option>
                  <?php foreach ($siswa as $r) { ?>
                    <option value="<?= $r->id_pengguna ?>"><?= $r->nm_pengguna ?></option>
                  <?php } ?>                  
                </select>
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Kelas</label>
                <!-- <small>Kosongi Bagian Ini Jika Memilih Mata Pelajaran</small> -->
                <select name="id_kelas" class="form-control">
                    <option value="">-- Pilih Kelas --</option>
                  <?php foreach ($kelas as $r) { ?>
                    <option value="<?= $r->id_kelas ?>"><?= $r->nm_kelas ?></option>
                  <?php } ?>                  
                </select>
                <!-- <small id="emailHelp" class="form-text text-muted">Pilih hanya jika jenis subjek Mata Pelajaran</small> -->
              </div>

              <div class="form-group">                
                <input type="submit" class="btn btn-primary" value="Simpan">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>