<!DOCTYPE html>
<html lang="en">

<head>
	<title>Akuhadir</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- Favicon icon -->
	<link rel="icon" href="<?= base_url() ?>assets/favicon.png" type="image/x-icon">

	<!-- Required Fremwork -->
	<link rel="stylesheet" type="text/css" href="theme/bower_components/bootstrap/css/bootstrap.min.css">

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/theme/ltr-horizontal/assets/icon/icofont/css/icofont.css">

	<link rel="stylesheet" href="<?= base_url() ?>/theme/ltr-horizontal/assets/css/jarallax-demo.min.css">

	<!-- waves css -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/theme/ltr-horizontal/assets/plugins//waves/css/waves.min.css">

	<!--image popup use css-->
	<link href="<?= base_url() ?>/theme/bower_components/lightbox2/css/lightbox.min.css" rel="stylesheet">

	<!-- slick.css-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/theme/bower_components/slick-carousel/css/slick.css">

	<!-- Style.css -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/theme/ltr-horizontal/assets/css/main.css">

	<!-- Style css -->
	<link rel="stylesheet" href="<?= base_url() ?>/theme/ltr-horizontal/assets/css/landingpage.min.css">

	<!-- Responsive.css-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/theme/ltr-horizontal/assets/css/responsive.css">

	<!--color css-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/theme/ltr-horizontal/assets/css/color/color-1.min.css" id="color" />

	<style>
		.car_btn {
			transition: box-shadow .1s;
			width: 120px;
			height: 120px;
			padding: 10px;
			line-height: 50px;
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
			border-radius: 5px;
			background-color: #e1e7e9;
			cursor: pointer;
			margin: 20px;
		}

		.car_btn:hover {
			background-color: #fff;
			box-shadow: 0 0 21px rgba(255, 255, 255, .3);
		}

		.car_btn img {
			float: center;
		}
	</style>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

	<!-- Nav-bar Start -->
	<nav class="navbar navbar-expand-lg navbar-light bg-inverse">
		<div class="col-sm-4 col-xs-4">
			<a href="<?= base_url() ?>">
				<img src="<?= base_url() ?>assets/favicon.png" width="50">
			</a>
		</div>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="container">
			<div class=" text-center collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Beranda</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Mitra</a>
					</li>
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger" href="#tentangKami">Tentang Kami</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#footer-sec">Hubungi Kami</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url() ?>login">Masuk</a>
					</li>
					<li class="nav-item">
						<a class="btn btn-primary btn-md waves-effect waves-light" href="<?= base_url() ?>daftar">Daftar</a>
					</li>
				</ul>
			</div>
		</div>

	</nav>
	<!--</nav>-->
	<!--</div>-->
	<!-- Nav-bar Ends -->

	<!-- Header Start -->
	<header style="background-image: url(<?= base_url() ?>/assets/images/backtoschool1.png); background-color: rgba(0,0,0,.6);  background-size: cover" id="header-bg" class="jarallax d-flex" data-jarallax='{"speed": 0.2}'>
		<!-- Menu End -->
		<div class="container">
			<div id="home-header" class="home-content">
				<div class="col-md-8 col-sm-12">
					<h1><b>Ajak <span>semua keluarga</span> ke <br>dalam kelasmu</b></h1>
					<p>Daftar sebagai..</p>
					<div class="row">'
						<div class="text-center">
							<div class="box-process">
								<button class='car_btn'>
									<img src="<?= base_url() ?>/assets/images/icons/teacher64.png">
								</button>
								<h6 style="color: white">Guru</h6>
							</div>
						</div>
						<div class="text-center">
							<div class="box-process">
								<button class='car_btn'>
									<img src="<?= base_url() ?>/assets/images/icons/mom64.png">
								</button>
								<h6 style="color: white">Orang Tua</h6>
							</div>
						</div>
						<div class="text-center">
							<div class="box-process">
								<button class='car_btn'>
									<img src="<?= base_url() ?>/assets/images/icons/student.png">
								</button>
								<h6 style="color: white">Siswa</h6>
							</div>
						</div>
						<div class="text-center">
							<div class="box-process">
								<button class='car_btn'>
									<img src="<?= base_url() ?>/assets/images/icons/admin64.png">
								</button>
								<h6 style="color: white">Admin</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- Header Ends -->

	<!--What We Do Section Start-->
	<section class="section-bg" id="tentangKami">
		<div class="container-fluid">
			<div class="row text-center">
				<div class="col-sm-12">
					<div class="container">
						<div class="title">
							<h2>Apa yang kami lakukan?</h2>
							<div class="subheading-divider"></div>
						</div>
					</div>
					<div class="col-12">
						<div class="card">
							<div class="card-header">
								<p style="padding:20px;font-size:20px;">Menghubungkan guru dengan siswa dan orangtua untuk membangun komunitas kelas yang menakjubkan</p>
							</div>
							<div class="card-block card-inner-shadow">
								<div class="row">
									<div class="col-md-12">
										<div class="card-deck-wrapper">
											<div class="card-deck">
												<div class="card card-inner-shadow global-cards">
													<img class="card-img-top" src="<?= base_url() ?>/assets/images/spirit.png">
													<div class="card-block">
														<h5>Menciptakan budaya positif</h5>
														<p class="card-text">Guru dapat mendorong siswa untuk nilai atau keterampilan apapun - apakah itu bekerja keras, bersikap baik, membantu orang lain atau hal lain.</p>
													</div>
												</div>
												<div class="card card-inner-shadow global-cards">
													<img class="card-img-top" src="<?= base_url() ?>/assets/images/active.png" alt="Card image cap">
													<div class="card-block">
														<h5>Memberikan suara pada siswa</h5>
														<p class="card-text">Siswa dapat memamerkan dan membagikan pembelajaran mereka dengan menambahkan foto dan video ke portofolio mereka sendiri.</p>
													</div>
												</div>
												<div class="card card-inner-shadow global-cards">
													<img class="card-img-top" src="<?= base_url() ?>/assets/images/class.png" alt="Card image cap">
													<div class="card-block">
														<h5>Membagikan momen dengan orang tua</h5>
														<p class="card-text">Melibatkan orang tua dengan berbagi foto dan video momen kelas yang indah.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- end of row -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--What We Do Section Ends-->

	<!--Share Story Section-->
	<section>
		<div id="share_story" class="container-fluid about-sec text-center">
			<div class="row d-flex about-sec">
				<div class="col-md-6 col-sm-12 p-0">
					<img src="<?= base_url() ?>/assets/images/share.png" class="img-fluid" alt="">
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-12 tablet-sc">
								<h2>Bagikan kisah ruang kelasmu dengan keluarga</h2><br>
								<p>Bagikan foto, video, dan pengumuman di Story Class secara instan, atau pesan pribadi dengan orang tua mana pun
								</p>
								<ul class="text-left">
									<li>
										<i class="icofont icofont-check"></i>
										<p>Orang tua dengan mudah bergabung dengan kelasmu menggunakan perangkat apa pun</p>
									</li>
									<li>
										<i class="icofont icofont-check"></i>
										<p>Terjemahkan pesan secara instan ke dalam berbagai bahasa</p>
									</li>
									<li>
										<i class="icofont icofont-check"></i>
										<p>Beri tahu keluarga saat sibuk dengan Jam-Jam Hening</p>
									</li>
								</ul>
								<div class="mob-text">
									<a href="#" class="btn btn-primary btn-md waves-effect waves-light m-r-20"> Lebih lanjut
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Share Story End Section-->

	<!--Give voice Section-->
	<section>
		<div id="give_voice" class="container-fluid about-sec text-center">
			<div class="row d-flex about-sec">
				<div class="col-md-6 col-sm-12 p-0">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-12 tablet-sc">
								<h2>Beri siswa suara dengan portofolio digital</h2><br>
								<p>Siswa dapat memamerkan pembelajaran mereka dengan menambahkan foto dan video ke portofolio digital mereka sendiri. Tersedia di Chromebook, iPad, dan komputer apa pun
								</p>
								<div class="mob-text">
									<a href="#!" class="btn btn-primary btn-md waves-effect waves-light m-r-20"> Dapatkan Portofolio
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<img src="<?= base_url() ?>/assets/images/portfolio.png" class="img-fluid" alt="">
				</div>
			</div>
		</div>
	</section>
	<!--Give Voice End Section-->

	<!--Video Section-->
	<section>
		<div id="watch_video" class="container-fluid about-sec text-center">
			<div class="row d-flex about-sec">
				<div class="col-md-6 col-sm-12 p-0">
					<img src="<?= base_url() ?>/assets/images/playvid.png" class="img-fluid" alt="">
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-12 tablet-sc">
								<h2>Satukan komunitas sekolahmu</h2><br>
								<p>Para guru, pemimpin sekolah, dan keluarga dapat bermitra di ClassDojo dan menciptakan komunitas sekolah yang luar biasa
								</p>
								<div class="mob-text">
									<a href="#!" class="btn btn-primary btn-md waves-effect waves-light m-r-20"> Tonton video
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Video End Section-->

	<!--Close Section Start-->
	<section style="background-image: url(Pics/authentication-bg.jpg);" class="section-bg">
		<div class="container-fluid">
			<div class="row text-center">
				<div class="col-sm-12">
					<div class="container">
						<div class="title">
							<h2 style="color: black">Kelas apa saja, perangkat apa pun. 100% gratis</h2>
							<div class="subheading-divider"></div>
						</div>
					</div>
					<div class="col-12">
						<div class="card">
							<div class="card-block card-inner-shadow">
								<div class="row">
									<div class="col-md-12">
										<div class="card-deck-wrapper">
											<div class="card-deck">
												<div class="card card-inner-shadow global-cards">
													<img class="card-img-top" src="<?= base_url() ?>/assets/images/android.png">
													<div class="card-block">
														<p class="card-text">Bekerja di iOS, Android, Kindle Fire, dan di komputer apa pun</p>
													</div>
												</div>
												<div class="card card-inner-shadow global-cards">
													<img class="card-img-top" src="<?= base_url() ?>/assets/images/secure.png" alt="Card image cap">
													<div class="card-block">
														<p class="card-text">Dibangun dengan privasi berdasarkan desain</p>
													</div>
												</div>
												<div class="card card-inner-shadow global-cards">
													<img class="card-img-top" src="<?= base_url() ?>/assets/images/teacher.png" alt="Card image cap">
													<div class="card-block">
														<p class="card-text">Selalu gratis untuk guru</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- end of row -->
							</div>
						</div>
					</div><br>
				</div>
				<br>
				<div class="col-sm-12"><br>
					<h2 style="color: black">Siap bergabung dengan jutaan guru lainnya?</h2><br>
					<div class="mob-text">
						<a href="#!" class="btn btn-inverse-primary btn-lg waves-effect waves-light m-r-20"> DAFTAR
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Close Section Ends-->

	<!--Footer Section Start-->
	<footer id="footer-sec" data-jarallax='{"speed": 0.2}' class="jarallax section-tb footer" style="background-image: url(<?= base_url() ?>/assets/images/bg1.png);">
		<div class="container">
			<div class="row">
				<div class="col-6 col-md-4 footer-about">
					<h3 class="footer-header">Hubungi Kami</h3>
					<p>This is Photoshop's version of Lorem impsum. Proin gra vi da nibh vel velit a uctor aliquet.</p>
					<ul class="footer_widget">
						<li class="footer_address">
							<i class="icofont icofont-location-pin"></i>
							<h5>308, Royal Square, Utran,<br /> Surat - 395006</h5>
						</li>
						<li class="footer_contact">
							<i class="icofont icofont-ui-cell-phone"></i>
							<h5>Inquiry - 8100 567 435 </h5>
						</li>
						<li class="footer-web">
							<i class="icofont icofont-globe"></i>
							<h5><a href="http://phoenixcoded.com/">www.phoenixcoded.com</a></h5>
						</li>
						<li class="footer_email">
							<i class="icofont icofont-email"></i>
							<h5><a href="mailto:phoenixcoded@gmail.com">demo@phoenixcoded.com</a></h5>
						</li>
					</ul>
				</div>
				<div class="col-6 col-md-4 footer-menu">
					<h3 class="footer-header">Temukan Kami</h3>
					<div>
						<div class="d-flex follow-sec">
							<ul>
								<li class="fb"><a href="#!"><i class="icofont icofont-social-facebook"></i>
										<h5> Facebook</h5>
									</a></li>
								<li class="twt"><a href="#!"><i class="icofont icofont-social-twitter"></i>
										<h5> Twitter</h5>
									</a></li>
								<li class="skype"><a href="#!"><i class="icofont icofont-social-skype"></i>
										<h5> Skype</h5>
									</a></li>
								<li class="l_in"><a href="#!"><i class="icofont icofont-brand-linkedin"></i>
										<h5> LinkedIn</h5>
									</a></li>
							</ul>
						</div>
					</div>
				</div>
				<!---->
				<div class="col-6 col-md-4 footer-menu">
					<h3 class="footer-header">Menu</h3>
					<div class="row">
						<div class="col-sm-6">
							<ul>
								<li><a href="#!"><i class="icofont icofont-rounded-double-right"></i>
										<h5>Portfolio</h5>
									</a></li>
								<li><a href="#!"><i class="icofont icofont-rounded-double-right"></i>
										<h5>Testinomial</h5>
									</a>
								</li>
								<li><a href="#!"><i class="icofont icofont-rounded-double-right"></i>
										<h5>Blog</h5>
									</a></li>
								<li><a href="#!"><i class="icofont icofont-rounded-double-right"></i>
										<h5>Team</h5>
									</a></li>
							</ul>
						</div>
						<div class="col-sm-6">
							<ul>
								<li><a href="#!"><i class="icofont icofont-rounded-double-right"></i>
										<h5>Pricing Plan</h5>
									</a>
								</li>
								<li><a href="#!"><i class="icofont icofont-rounded-double-right"></i>
										<h5>Contact Us</h5>
									</a></li>
								<li><a href="#!"><i class="icofont icofont-rounded-double-right"></i>
										<h5>Appoinment</h5>
									</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<div class="copyright">
		<h5 class="text-center"><a href="#!"> Copyright © 2016. All rights reserved </a></h5>
	</div>
	<!--Footer Section Ends-->

	<!-- Move to up -->
	<a class="waves-effect waves-light scrollup scrollup-icon"><i class="icofont icofont-arrow-up "></i></a>

	<!-- Required Jqurey -->
	<script type="text/javascript" src="<?= base_url() ?>/theme/bower_components/jquery/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>/theme/bower_components_/popper.js/js/popper.min.js"></script>

	<!-- light Box Jquery Start -->
	<script src="<?= base_url() ?>/theme/bower_components/lightbox2/js/lightbox-plus-jquery.min.js"></script>

	<!-- waves effects.js -->
	<script src="<?= base_url() ?>/theme/ltr-horizontal/assets/plugins//waves/js/waves.min.js"></script>

	<!-- Required Fremwork -->
	<script type="text/javascript" src="<?= base_url() ?>/theme/bower_components/bootstrap/js/bootstrap.min.js"></script>

	<script src="<?= base_url() ?>/theme/bower_components/jarallax/js/jarallax.min.js"></script>

	<!--slick JS -->
	<script src="<?= base_url() ?>/theme/bower_components/slick-carousel/js/slick.min.js"></script>

	<!-- Isotope js What We Do -->
	<script src="<?= base_url() ?>/theme/bower_components/isotope/js/jquery.isotope.js"></script>
	<script src="<?= base_url() ?>/theme/ltr-horizontal/assets/plugins//isotope/js/isotope.pkgd.min.js"></script>

	<!-- custom landing page js -->
	<script type="text/javascript" src="<?= base_url() ?>/theme/ltr-horizontal/assets/pages/landing-page.js"></script>
	<!--
<script type="text/javascript" src="theme/ltr-horizontal/assets/js/common-pages.min.js"></script>-->
	<script>
		$(window).on('load', function() {
			$('#pre-loader').fadeOut('slow');
		});
		$(function() {
			$('.selectpicker').selectpicker();
		});
	</script>
</body>

</html>