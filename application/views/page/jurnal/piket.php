            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 p-0">
                            <div class="main-header">
                                <h4>Jadwal Piket (<?=$tahunAjaran?>)</h4>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="data_table_main">
                                    <div class="card-header">
                                        <h4 class="card-header-text"><?= $teksTanggal ?></h4>
                                        <br>
                                        <h4 class="card-header-text">Halo, <?= $user[0]->nm_pengguna?></h4>
                                        <p><i>*Pilih jadwal pelajaran yang akan Anda gantikan sebagai guru piket</i></p>
                                    </div>
                                    <?php if ($this->session->flashdata('message') != ''): ?>
                                    <div class="col-sm-12">
                                        <div class="alert alert-danger alert-dismissible">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-top: -10px">&times;</a>
                                            <strong><?php echo $this->session->flashdata('message') ; ?></strong>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <div class="card-block">
                                        <div class="card">
                                         <div class="row">
                                            <div class="col-sm-12 table-responsive">
                                                <table id="demo-foo-filtering" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Jadwal</th>
                                                            <th data-breakpoints="xs">Kelas</th>
                                                            <th data-breakpoints="xs">Ketua Kelas</th>
                                                            <!-- <th data-breakpoints="xs">Status</th> -->
                                                            <th>Aksi</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($jadwal as $d):?>
                                                        <tr>
                                                            <td>
                                                                <?= $d->nm_mata_pelajaran?> (<?= $d->nama_guru ?>)
                                                                <br>Jam Ke <?= $d->periode?>
                                                                <p>
                                                                    <i class="icofont icofont-clock-time"></i>Dimulai : <?= $d->jam_mulai?>

                                                                    <br>Diakhiri : <?= $d->jam_selesai?>
                                                                </p>
                                                            </td>
                                                            <td><?= $d->nm_kelas . $d->rombel_kelas ?></td>
                                                            <td><?php if(is_null($d->nama_ketua)) echo '<i>-none-</i>'; else echo $d->nama_ketua;?></td>
                                                            <td>
                                                                <form method="POST" action="<?=base_url()?>dashboard/jurnal/hadir/<?= $d->id_jadwal_guru ?>">
                                                                    <input type="hidden" name="piket" value="TRUE">
                                                                    <input type="hidden" name="id_subjek" value="<?= $d->jenis_mata_pelajaran ?>">
                                                                    <input type="hidden" name="id_kelas" value="<?= $d->id_kelas ?>">
                                                                    <button type="submit" class="btn btn-primary btn-icon waves-effect waves-light">
                                                                        <i class="icofont icofont-bell-alt"></i>
                                                                    </button>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?> 
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>