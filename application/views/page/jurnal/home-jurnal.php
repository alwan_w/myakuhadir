<div class="content-wrapper">

        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <!-- <div class="row">
                <div class="main-header">
                    <h4>Dashboard</h4>
                </div>
            </div> -->
            <!-- 4-blocks row start -->
            <div class="row m-b-30 dashboard-header">
                <!-- <div class="col-lg-3 col-sm-6">
                    <div class="dashboard-primary bg-primary">
                        <div class="sales-primary">
                            <i class="icon-bubbles"></i>
                            <div class="f-right">
                                <h2 class="counter">4500</h2>
                                <span>Total Sales</span>
                            </div>
                        </div>
                        <div class="bg-dark-primary">
                            <p class="week-sales">LAST WEEK'S SALES</p>
                            <p class="total-sales">432</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="bg-success dashboard-success">
                        <div class="sales-success">
                            <i class="icon-speedometer"></i>
                            <div class="f-right">
                                <h2 class="counter">3521</h2>
                                <span>Total Sales</span>
                            </div>
                        </div>
                        <div class="bg-dark-success">
                            <p class="week-sales">LAST WEEK'S SALES</p>
                            <p class="total-sales ">432</p>
                        </div>
                    </div>
                </div>
                 -->
                <div class="col-lg-3 col-sm-6">
                    <div class="bg-success dashboard-success">
                        <div class="sales-facebook">
                            <i class="icon-calendar"></i>
                            <div class="f-right">
                                <h5 id="system-date"></h5>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6">
                    <div class="bg-warning dasboard-warning">
                        <div class="sales-warning">
                            <i class="icon-clock"></i>
                            <div class="f-right">
                                <h2 id= "system-clock"></h2>
                            </div>
                        </div>
                    </div>
                </div>
                       <!--  <div class="bg-dark-warning">
                            <p class="week-sales">LAST WEEK'S SALES</p>
                    </div>
                    <div class="col-sm-12 card dashboard-product">
                        <span>Products</span>
                        <h2 class="dashboard-total-products counter">37,500</h2>
                        <span class="label label-primary">Views</span>View Today
                        <div class="side-box bg-primary">
                            <i class="icon-social-soundcloud"></i>
                        </div>
                    </div>
                    <div class="col-sm-12 card dashboard-product">
                        <span>Products</span>
                        <h2 class="dashboard-total-products">$<span class="counter">30,780</span></h2>
                        <span class="label label-success">Sales</span>Reviews
                        <div class="side-box bg-success">
                            <i class="icon-bubbles"></i>
                        </div>
                    </div>
                </div> -->
            </div>
            <!-- 1-3-block row end -->

            <!-- 2-1 block start -->
            <div class="row">
                <div class="main-header">
                    <h4>Jadwal Mengajar Guru (Nama Guru)</h4>
                </div>
            </div>
            

                <!-- Detail User -->
                <!-- <div class="col-xl-4 col-lg-12">
                    <div class="card">
                        <div class="user-block-2">
                            <img class="img-fluid" src="assets/images/widget/user-1.png" alt="user-header">
                            <h5>Josephin Villa</h5>
                            <h6>Software Engineer</h6>
                        </div>
                        <div class="card-block">
                            <div class="user-block-2-activities">
                                <div class="user-block-2-active">
                                    <i class="icofont icofont-clock-time"></i> Recent Activities
                                    <label class="label label-primary">480</label>
                                </div>
                            </div>
                            <div class="user-block-2-activities">
                                <div class="user-block-2-active">
                                    <i class="icofont icofont-users"></i> Current Employees
                                    <label class="label label-primary">390</label>
                                </div>
                            </div>

                            <div class="user-block-2-activities">
                                <div class="user-block-2-active">
                                    <i class="icofont icofont-ui-user"></i> Following
                                    <label class="label label-primary">485</label>
                                </div>

                            </div>
                            <div class="user-block-2-activities">
                                <div class="user-block-2-active">
                                    <i class="icofont icofont-picture"></i> Pictures
                                    <label class="label label-primary">506</label>
                                </div>

                            </div>
                            <div class="text-center">
                                <button type="button" class="btn btn-warning waves-effect waves-light text-uppercase m-r-30">
                                    Follows
                                </button>
                                <button type="button" class="btn btn-primary waves-effect waves-light text-uppercase">
                                    Message
                                </button>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <!-- 2-1 block end -->
        </div>
        <!-- Main content ends -->



        <!-- Container-fluid ends -->
    </div>
    <script type="text/javascript">
        var tanggal     = document.getElementById('system-date'); 
        var jam         = document.getElementById('system-clock'); 

        function time() {
            tanggal.innerHTML   = "";
            jam.innerHTML       = "";

            var d   = new Date();        var year = d.getFullYear();
            var day = d.getDate();       var hour = d.getHours();

            var minutes = d.getMinutes();  var seconds = d.getSeconds();
            minutes = checkTime(minutes);
            seconds = checkTime(seconds);
  
            var month   = new Array();
              month[0]  = "Januari";         month[1]   =     "Februari";       month[2]    =   "Maret";
              month[3]  = "April";           month[4]   =     "Mei";            month[5]    =   "Juni";
              month[6]  = "Juli";            month[7]   =     "Agustus";        month[8]    =   "September";
              month[9]  = "Oktober";         month[10]  =    "November";        month[11]   =   "Desember";

            var m   = month[d.getMonth()];
  
            var weekday     = new Array(7);
              weekday[0]    =   "Minggu";        weekday[1]  = "Senin";      weekday[2] = "Selasa";
              weekday[3]    =   "Rabu";          weekday[4]  = "Kamis";      weekday[5] = "Jumat";
              weekday[6]    =   "Sabtu";

            var h = weekday[d.getDay()];
  
            tanggal.innerHTML = h+", "+day+" "+m+" "+year;
            jam.innerHTML = "Pukul "+hour + ":" + minutes + ":" + seconds;
        }


        setInterval(time, 1000);
        function checkTime(i) {
          if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
          return i;
        }
    </script>