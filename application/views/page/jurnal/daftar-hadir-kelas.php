<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card" style="margin-bottom:10px;">
                    <div class="data_table_main">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6" style="margin-bottom: 5px;">
                                    <h4 class="card-header-text"><?= humanDateFormat($tanggal_jurnal, TRUE) ?></h4>
                                    <br>
                                    <h4 class="card-header-text">Halo, <?= $user[0]->nm_pengguna?></h4>
                                    <br>
                                    <?php if(is_null($jurnal->nm_pengguna)):?>
                                    <h4 class="card-header-text">Anda Mulai Mengajar <span style="color: #4286f4"><?= $jurnal->nm_mata_pelajaran?></span> di Kelas <span style="color: #4286f4"><?= $kelas->nm_kelas . $kelas->rombel_kelas?></span> Pada Pukul <span style="color: #4286f4"><?= $jurnal->jam_hadir?></span></h4>
                                    <?php else:?>
                                    <h4 class="card-header-text">Guru Piket <span style="color: #4286f4"><?= $jurnal->nm_pengguna?></span> Mulai Mengajar <span style="color: #4286f4"><?= $jurnal->nm_mata_pelajaran?></span> di Kelas <span style="color: #4286f4"><?= $kelas->nm_kelas . $kelas->rombel_kelas?></span> Pada Pukul <span style="color: #4286f4"><?= $jurnal->jam_hadir?></span></h4>
                                    <?php endif;?>
                                </div>
                                <div class="col-sm-6 text-lg-right">
                                    <a href="<?= base_url () ?>dashboard/jurnal/isi-jurnal/<?= $id_jurnal?>" type="button" class="btn btn-primary waves-effect waves-light ">
                                        <i class="icofont icofont-paper"></i><span class="m-l-10">Jurnal Harian</span>
                                    </a>

                                    <a href="#" type="button" class="btn btn-secondary waves-effect waves-light ">
                                        <i class="icofont icofont-group-students"></i><span class="m-l-10">Daftar Siswa</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Table starts -->
                <div class="card">
                    <ul class="nav nav-tabs md-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"> <i class="icofont icofont-check-circled"></i> Sudah Absen</a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"> <i class="icofont icofont-close-circled"></i> Belum Absen</a>
                            <div class="slide"></div>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home3" role="tabpanel">
                            <div class="card-block">
                                <form  method="POST" action="<?=base_url()?>dashboard/jurnal/daftar-hadir/<?= $jurnal->id_jurnal ?>">
                                <div class="row card-header">
                                    <div class="col-sm-6">
                                        <h5 class="card-header-text">Daftar Siswa Sudah Absen Kelas <?= $kelas->nm_kelas . $kelas->rombel_kelas?></h5>
                                        <p><small>Daftar hadir berdasarkan absensi siswa di awal jam pelajaran</small></p>
                                    </div>
                                    <?php if($editable):?>
                                    <div class="col-sm-6 text-lg-right">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan Keterangan Absen Siswa</button>
                                    </div>
                                    <?php endif;?>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 table-responsive">
                                        <table class="table" data-sorting="true">
                                            <thead>
                                                <tr>
                                                    <th class="footable-asc">Nama Siswa</th>
                                                    <th>NIS Siswa</th>
                                                    <th>Keterangan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                $i = 1;
                                                foreach ($siswa as $d) { ?>
                                                    <tr>
                                                        <td><?= $d->nm_pengguna ?></td>
                                                        <td><?= $d->nomor_induk ?></td>
                                                        <td>
                                                        <?php if($editable):?>
                                                            <select class="form-control" id="exampleSelect1" style="width:80%;" name="status[<?= $d->nomor_induk ?>]" >
                                                                <option <?php if($d->kehadiran_verification == 0 || $d->kehadiran_verification == 3) echo 'value="none" selected'; else echo 'value="Sudah"';?>>Sudah Absen</option>
                                                                <option <?php if($d->kehadiran_verification == 1) echo 'value="none" selected'; else echo 'value="Sakit"';?>>Sakit</option>
                                                                <option <?php if($d->kehadiran_verification == 2) echo 'value="none" selected'; else echo 'value="Izin"';?>>Izin</option>
                                                            </select>
                                                        <?php else:?>
                                                            <?php if ($d->kehadiran_verification == "1"): ?>
                                                                <span class="badge badge-success">  Sakit</span>
                                                            <?php elseif ($d->kehadiran_verification == "2"): ?>
                                                                <span class="badge badge-warning">  Izin</span>
                                                            <?php elseif ($d->kehadiran_verification == "0"): ?>
                                                                <span class="badge" style="color:#fff; background-color: #2196F3;"> 
                                                                    Tepat Waktu
                                                                </span>
                                                            <?php else: ?>
                                                                <span class="badge badge-danger"> 
                                                                    Terlambat
                                                                </span>
                                                            <?php endif; ?>
                                                        <?php endif;?>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>        
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile3" role="tabpanel">
                                <div class="card-block">
                                    <form  method="POST" action="<?=base_url()?>dashboard/jurnal/daftar-hadir/<?= $jurnal->id_jurnal ?>">
                                    <div class="row card-header">
                                        <div class="col-sm-6">
                                            <h5 class="card-header-text">Daftar Siswa Belum Absen Kelas <?= $kelas->nm_kelas . $kelas->rombel_kelas?></h5>
                                            <p><small>Daftar hadir berdasarkan absensi siswa di awal jam pelajaran</small></p>
                                        </div>
                                        <?php if($editable):?>
                                        <div class="col-sm-6 text-lg-right">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan Keterangan Absen Siswa</button>
                                        </div>
                                        <?php endif;?>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Nama Siswa</th>
                                                        <th>NIS Siswa</th>
                                                        <th>Keterangan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    $i = 1;
                                                    foreach ($siswaTidakMasuk as $d) { ?>
                                                        <tr>
                                                            <td><?= $d->nm_pengguna ?></td>
                                                            <td><?= $d->nomor_induk ?></td>
                                                            <td>
                                                                <select class="form-control" id="exampleSelect1" style="width:80%;" <?php if($editable):?> name="status[<?= $d->nomor_induk ?>]" <?php else:?> disabled <?php endif;?>>
                                                                    <option value="none">Siswa Belum Absen</option>
                                                                    <?php if($editable):?>
                                                                    <option value="Sudah">Sudah Absen</option>
                                                                    <option value="Sakit">Sakit</option>
                                                                    <option value="Izin">Izin</option>
                                                                    <?php endif;?>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>      
                                        </div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
