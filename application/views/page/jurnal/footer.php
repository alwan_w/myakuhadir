	<!-- Required Jqurey -->
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jquery/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jquery-ui/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/popper.js/js/popper.min.js"></script>

	<!-- Required Fremwork -->
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap/js/bootstrap.min.js"></script>

	<!-- waves effects.js -->
	<script src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//waves/js/waves.min.js"></script>

	<!-- Scrollbar JS-->
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//jquery.nicescroll/js/jquery.nicescroll.min.js"></script>


	<!--classic JS-->
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/classie/js/classie.js"></script>

	<!-- notification -->
	<script src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//notification/js/bootstrap-growl.min.js"></script>

	<!--FOO Table js-->
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/footable/js/footable.min.js"></script>

	<!-- Max-Length js -->
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap-maxlength/js/bootstrap-maxlength.js"></script>

	<!-- bootstrap range picker -->
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>

	<!-- color picker -->
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/spectrum/js/spectrum.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jscolor/js/jscolor.js"></script>

	<!-- Multi Select js -->
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/multiselect/js/jquery.multi-select.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins/multi-select/js/jquery.quicksearch.js"></script>

	<!-- Tags js -->
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>

	<!-- nestable js -->
	<!-- <script src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//nestable/js/jquery.nestable.min.js"></script> -->

	<!-- data-table js -->
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//data-table/js/jszip.min.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//data-table/js/pdfmake.min.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//data-table/js/vfs_fonts.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

	<!-- custom js -->
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/js/main-dev.min.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/assets/pages/foo-table.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/assets/pages/data-table.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/pages/elements.js"></script>
	<script src="<?=base_url()?>/theme/ltr-horizontal/assets/js/menu-horizontal.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/pages/accordion.js"></script>
	<?php if ($this->session->flashdata('pesan') != '') : ?>
	<script type="text/javascript">
	    $(window).on('load',function(){
	        $('#default-Modal').modal('show');
	    });
	</script>
	<?php endif;?>

</body>
</html>