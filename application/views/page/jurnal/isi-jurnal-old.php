<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <!-- <h4>Selamat Pagi, {Guru}</h4> -->
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="<?= base_url () ?>dashboard/jurnal"><i class="icofont icofont-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><i class="icofont icofont-read-book"></i> Jurnal Guru
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="data_table_main">
                        <div class="card-header">
                            <h4 class="card-header-text">Halo, <?= $user[0]->nm_pengguna?></h4>
                            <br>
                            <h5 class="card-header-text" style="font-size: 16px">Anda Mulai Mengajar <!-- di Kelas --> <!-- <span style="color: #4286f4"><?= $kelas->nm_kelas?></span> --> Pada Pukul <span style="color: #4286f4"><?= $jurnal->jam_hadir?></span></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 grid-item">
                <div class="card">
                    <div class="card-block button-list">
                        <!-- start with icon Button -->
                        <a href="#" type="button" class="btn btn-warning waves-effect waves-light ">
                            <i class="icofont icofont-home "></i><span class="m-l-10">Informasi</span>
                        </a>
                        <a href="<?= base_url () ?>dashboard/jurnal/daftar-hadir/<?php echo $id_jurnal ?>" type="button" class="btn btn-primary waves-effect waves-light ">
                            <i class="icofont icofont-group-students"></i><span class="m-l-10">Daftar Siswa</span>
                        </a>

                        <a href="<?= base_url () ?>dashboard/jurnal/isi-jurnal-guru/<?= $id_jurnal?>" type="button" class="btn btn-primary waves-effect waves-light ">
                            <i class="icofont icofont-paper"></i><span class="m-l-10">Jurnal Harian</span>
                        </a>

                        <a href="<?= base_url () ?>dashboard/jurnal/tugas-ujian/<?= $id_jurnal?>" type="button" class="btn btn-primary waves-effect waves-light ">
                            <i class="icofont icofont-tasks"></i><span class="m-l-10">Tugas dan Ujian</span>
                        </a>

                        <form method="POST" action="<?= base_url () ?>dashboard/jurnal/kelas-selesai/<?= $id_jurnal?>">
                            <button type="submit" class="btn btn-danger waves-effect waves-light ">
                                <i class="icofont icofont-exit"></i><span class="m-l-10">Selesaikan Kelas</span>
                            </button>
                        </form>

                    </div>
                </div>
            </div>
            <!-- <div class="col-xl-4 col-lg-12 grid-item">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Informasi Kelas</h5>
                    </div>
                    <div class="user-block-2">
                        <h5>Kelas <?= $kelas->nm_kelas?></h5>
                    </div>
                    <div class="card-block">
                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-clock-time"></i> Nama Wali Kelas
                                <label class="label label-lg label-inverse-default"><?= $kelas->nm_pengguna?></label>
                            </div>
                        </div>
                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-users"></i> Nama Ketua Kelas
                                <label class="label label-lg label-inverse-default"><?= $kelas->nm_ketua_kelas?></label>
                            </div>
                        </div>

                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-ui-user"></i> Jumlah Siswa
                                <label class="label label-lg label-inverse-primary"><?= $kelas->jumlah_siswa?></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

            <div class="col-xl-4 col-lg-12 grid-item">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Informasi Guru</h5>
                    </div>
                    <div class="user-block-2">
                        <h5>Nama Lengkap <br>
                            <?= $user[0]->nm_pengguna?></h5>
                    </div>
                    <div class="card-block">
                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-clock-time"></i> Nomor Induk 
                                <label class="label label-lg label-inverse-default"><?= $user[0]->nomor_induk?></label>
                            </div>
                        </div>
                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-users"></i> Alamat
                                <label class="label label-lg label-inverse-default"><?= $user[0]->alamat?></label>
                            </div>
                        </div>

                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-ui-user"></i> Email
                                <label class="label label-lg label-inverse-primary"><?= $user[0]->email?></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>