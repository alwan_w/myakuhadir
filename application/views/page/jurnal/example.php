<div class="card-header">
                        <h5 class="card-header-text">Informasi Kelas</h5>
                    </div>
                    <div class="user-block-2">
                        <h5>Kelas <?= $kelas->nm_kelas?></h5>
                    </div>
                    <div class="card-block">
                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-clock-time"></i> Nama Wali Kelas
                                <label class="label label-lg label-inverse-default"><?= $kelas->nm_pengguna?></label>
                            </div>
                        </div>
                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-users"></i> Nama Ketua Kelas
                                <label class="label label-lg label-inverse-default"><?= $kelas->nm_ketua_kelas?></label>
                            </div>
                        </div>

                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-ui-user"></i> Jumlah Siswa
                                <label class="label label-lg label-inverse-primary"><?= $kelas->jumlah_siswa?></label>
                            </div>
                        </div>
                        <div class="m-b-10 text-center">
                            <a href="<?= base_url () ?>dashboard/jurnal/daftar-hadir/<?= $id_jurnal?>" class="btn btn-warning waves-effect waves-light text-uppercase m-r-30">Daftar Siswa</a>
                        </div>
                    </div>

<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <!-- Header Starts -->
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <!-- <h4>Selamat Pagi, {Guru}</h4> -->
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="<?= base_url () ?>dashboard/jurnal"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?= base_url () ?>dashboard/jurnal/isi-jurnal">Jurnal Guru</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-lg-12 grid-item">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Informasi Kelas</h5>
                    </div>
                    <div class="user-block-2">
                        <!-- <img class="img-fluid" src="<?=base_url()?>/theme/ltr-horizontal/assets/images/widget/user-1.png" alt="user-header"> -->
                        <h5>Kelas <?= $kelas->nm_kelas?></h5>
                        <!-- <h6>Informasi Kelas</h6> -->
                    </div>
                    <div class="card-block">
                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-clock-time"></i> Nama Wali Kelas
                                <label class="label label-lg label-inverse-default"><?= $kelas->nm_pengguna?></label>
                            </div>
                        </div>
                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-users"></i> Nama Ketua Kelas
                                <label class="label label-lg label-inverse-default"><?= $kelas->nm_ketua_kelas?></label>
                            </div>
                        </div>

                        <div class="user-block-2-activities">
                            <div class="user-block-2-active">
                                <i class="icofont icofont-ui-user"></i> Jumlah Siswa
                                <label class="label label-lg label-inverse-primary"><?= $kelas->jumlah_siswa?></label>
                            </div>

                        </div>
                        <div class="m-b-10 text-center">
                            <a href="<?= base_url () ?>dashboard/jurnal/daftar-hadir" class="btn btn-warning waves-effect waves-light text-uppercase m-r-30">Daftar Siswa</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Jurnal Kelas</h5>
                    </div>
                    <form method="POST" action="<?=base_url()?>dashboard/jurnal/isi-jurnal/<?= $id_jurnal?>">
                        <div class="card-block">
                            <div class="row advance-elements">
                                <div class="col-md-12">
                                    <label for="file" class="col-md-12  col-form-label form-control-label">Kompetensi Dasar/Indikator Pencapaian</label>
                                    <p>Materi yang diajarkan saat pertemuan tersebut</p>
                                    <textarea class="form-control max-textarea" maxlength="255" rows="4" name="kompetensi_dasar">
                                        <?= $jurnal->kompetensi_dasar?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="row advance-elements">
                                <div class="col-md-12">
                                    <label for="file" class="col-md-12  col-form-label form-control-label">Catatan Kejadian</label>
                                    <p>Tuliskan semua kejadian unik selama proses belajar mengajar</p>
                                    <textarea class="form-control max-textarea" maxlength="255" rows="4" name="catatan">
                                        <?= $jurnal->catatan?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="card card-inner-shadow">
                                <div class="card-header">
                                    <strong>Tugas atau Pekerjaan Rumah</strong>
                                    <p>Tuliskan tugas dan pekerjaan rumah yang diberikan kepada siswa saat pertemuan</p>
                                </div>
                                <div class="card-block global-cards">
                                    <div class="row advance-elements">
                                        <div class="col-md-12">
                                            <label for="exampleInputPassword1" class="form-control-label">Jenis Tugas</label>
                                            <select name="jenis_tugas" class="form-control">
                                                <option value="1">Tugas Individu</option>
                                                <option value="2">Tugas Kelompok</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row advance-elements">
                                        <div class="col-md-12">
                                            <label for="exampleInputPassword1" class="form-control-label">Deskripsi Tugas</label>
                                            <p>
                                                Tuliskan deskripsi tugas. Misalnya: Tugas mengerjakan latihan 2 pada buku pelajaran matematika
                                            </p>
                                            <textarea class="form-control max-textarea" maxlength="255" rows="4" name="deskripsi_tugas">

                                            </textarea>
                                        </div>
                                    </div>                                    
                                    <!-- <br>
                                    <div class="row advance-elements">
                                        <div class="col-md-12">
                                            <label for="exampleInputPassword1" class="form-control-label">Waktu Pengumpulan</label>
                                            <input type="datetime" name="pengumpulan_tugas" class="form-control">
                                        </div>
                                    </div>   -->                                  
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="card card-inner-shadow">
                                <div class="card-header">
                                    <strong>Rencana Ujian</strong>
                                    <p>Tuliskan rencana ujian pada pertemuan mendatang</p>
                                </div>
                                <div class="card-block global-cards">
                                    <div class="row advance-elements">
                                        <div class="col-md-12">
                                            <label for="exampleInputPassword1" class="form-control-label">Jenis Ujian</label>
                                            <select name="jenis_ujian" class="form-control">
                                                <option value="1">Ujian Harian</option>
                                                <option value="2">Ujian Tengah Semester</option>
                                                <option value="3">Ujian Akhir Semester</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row advance-elements">
                                        <div class="col-md-12">
                                            <label for="exampleInputPassword1" class="form-control-label">Catatan Untuk Ujian</label>
                                            <p>
                                                Tuliskan materi atau bab yang akan diujikan pada pertemuan berikutnya. Misalnya : Materi Aljabar 1
                                            </p>
                                            <textarea class="form-control max-textarea" maxlength="255" rows="4" name="deskripsi_ujian">

                                            </textarea>
                                        </div>
                                    </div>                                    
                                    <!-- <br>
                                    <div class="row advance-elements">
                                        <div class="col-md-12">
                                            <label for="exampleInputPassword1" class="form-control-label">Waktu Pengumpulan</label>
                                            <input type="datetime" name="pengumpulan_ujian" class="form-control">
                                        </div>
                                    </div>   -->                                  
                                </div>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="col-md-6 ">
                                <button type="submit" class="btn btn-primary btn-block waves-effect">Simpan</button>
                            </div>
                        </div>
                    </div>

<!--                     <div class="card-block">
                        <div class="row advance-elements">
                            <div class="col-sm-12 table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Jenis Tugas</th>
                                            <th>Deskripsi Tugas</th>
                                            <th>Dikumpulkan Pada</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <select name="jenis_tugas" class="form-control">
                                                    <option value="1">Tugas Individu</option>
                                                    <option value="2">Tugas Kelompok</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="deskripsi_tugas" class="form-control" placeholder="Tuliskan deskripsi tugas disini. Misalnya: Tugas mengerjakan latihan 2 pada buku pelajaran matematika">
                                            </td>
                                            <td>
                                                <input type="time" name="pengumpulan_tugas" class="form-control">
                                            </td>
                                        </tr>
                                        <tr id="newRow">
                                            <td>
                                                <select name="jenis_tugas" class="form-control">
                                                    <option value="1">Tugas Individu</option>
                                                    <option value="2">Tugas Kelompok</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="deskripsi_tugas" class="form-control" placeholder="Tuliskan deskripsi tugas disini. Misalnya: Tugas mengerjakan latihan 2 pada buku pelajaran matematika">
                                            </td>
                                            <td>
                                                <input type="time" name="pengumpulan_tugas" class="form-control">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="col-md-6">
                                    <a href="" type="button" class="btn btn-primary btn-block waves-effect">Tambah Tugas</a>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    <!--         <?php if ($jurnal->status === '2'): ?>
                                <div class="col-md-6 ">
                                    <a href="" type="button" class="btn btn-primary btn-block waves-effect">Rencana Tugas dan Ujian</a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </form>
                    </div> -->
                </div>
            </div>