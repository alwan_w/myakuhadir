            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 p-0">
                            <div class="main-header">
                                <h4>Jadwal Mengajar (<?=$tahunAjaran?>)</h4>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="data_table_main">
                                    <div class="card-header">
                                        <h4 class="card-header-text"><?= $teksTanggal ?></h4>
                                        <br>
                                        <h4 class="card-header-text">Halo, <?= $user[0]->nm_pengguna?></h4>
                                    </div>
                                    <?php if ($this->session->flashdata('message') != ''): ?>
                                    <div class="col-sm-12">
                                        <div class="alert alert-danger alert-dismissible">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-top: -10px">&times;</a>
                                            <strong><?php echo $this->session->flashdata('message') ; ?></strong>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <div class="card-block">
                                        <div class="card">
                                         <div class="row">
                                            <div class="col-sm-12 table-responsive">
                                                <table id="demo-foo-filtering" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Jadwal</th>
                                                            <th data-breakpoints="xs">Kelas</th>
                                                            <th data-breakpoints="xs">Ketua Kelas</th>
                                                            <!-- <th data-breakpoints="xs">Status</th> -->
                                                            <th>Aksi</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if(count($jadwal)<0):?>
                                                        <tr class="footable-empty">
                                                            <td colspan="4">Tidak ada jadwal, Check <a href="<?=base_url()?>dashboard/jurnal/histori-jurnal" style="font-size : 30px;">Histori Jurnal</a></td>
                                                        </tr>
                                                    <?php endif;?>
                                                    <?php foreach ($jadwal as $d):?>
                                                        <tr>
                                                            <td>
                                                                <?= $d->nm_mata_pelajaran?> Jam Ke <?= $d->jam_mulai?> - <?= $d->jam_selesai?>
                                                                <p>
                                                                    <i class="icofont icofont-clock-time"></i>Dimulai : <?= $d->jam_mulai?>

                                                                    <br>Diakhiri : <?= $d->jam_selesai?>
                                                                </p>
                                                            </td>
                                                            <td><?= $d->nm_kelas . $d->rombel_kelas ?></td>
                                                            <td><?php if(is_null($d->nama_ketua)) echo '<i>-none-</i>'; else echo $d->nama_ketua;?></td>
                                                            <td>
                                                                <form method="POST" action="<?=base_url()?>dashboard/jurnal/hadir/<?= $d->id_jadwal_guru ?>">
                                                                    <input type="hidden" name="id_subjek" value="<?= $d->jenis_mata_pelajaran ?>">
                                                                    <input type="hidden" name="id_kelas" value="<?= $d->id_kelas ?>">
                                                                    <button type="submit" class="btn btn-primary btn-icon waves-effect waves-light">
                                                                        <i class="icofont icofont-bell-alt"></i>
                                                                    </button>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?> 
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var tanggal     = document.getElementById('system-date');
        var tanggal_hari     = document.getElementById('system-date1');
        var greeting    = document.getElementById('system-greeting'); 

        function time() {
            tanggal.innerHTML   = "";
            greeting.innerHTML  = "Halo";

            var d   = new Date();        var year = d.getFullYear();
            var day = d.getDate();      

            var month   = new Array();
            month[0]  = "Januari";         month[1]   =     "Februari";       month[2]    =   "Maret";
            month[3]  = "April";           month[4]   =     "Mei";            month[5]    =   "Juni";
            month[6]  = "Juli";            month[7]   =     "Agustus";        month[8]    =   "September";
            month[9]  = "Oktober";         month[10]  =    "November";        month[11]   =   "Desember";

            var m   = month[d.getMonth()];

            var weekday     = new Array(7);
            weekday[0]    =   "Minggu";        weekday[1]  = "Senin";      weekday[2] = "Selasa";
            weekday[3]    =   "Rabu";          weekday[4]  = "Kamis";      weekday[5] = "Jumat";
            weekday[6]    =   "Sabtu";

            var h = weekday[d.getDay()];
            var jam = d.getHours();

            if(4 <= jam && jam < 10){
                var salam   = "Selamat Pagi";
            }else if(10 <= jam && jam < 14){
                var salam   = "Selamat Siang";
            }else if(14 <= jam && jam < 18){
                var salam   = "Selamat Sore";
            }else{
                var salam   = "Selamat Malam";
            }

            tanggal.innerHTML = h+", "+day+" "+m+" "+year;
            tanggal_hari.innerHTML = day+" "+m+" "+year;
            greeting.innerHTML = salam;
        }
        setInterval(time, 1000);
        function checkTime(i) {
      if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
      return i;
    }
    </script>