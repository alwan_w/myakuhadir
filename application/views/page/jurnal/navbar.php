<?php 
if(!isset($page))
  $page = '';
$piket = $kelas['piket'];
$kelas = $kelas['wali'];
$foto = $this->session->userdata('sess_foto');
if(is_null($foto) || $foto == '')
  $foto = 'default.png';
?>
<body class="horizontal-fixed fixed">
  <div class="wrapper">
    <div class="loader-bg">
      <div class="loader-bar"></div>
    </div>
    <!-- Navbar-->
    <header class="main-header-top hidden-print">
      <a href="#" class="logo" style="text-align:left; width:auto;"><img style="max-width:auto; height:90%;" src="<?php echo base_url('assets/logos/'.$this->session->userdata('sess_logo_sekolah')) ?>" alt="Logo Sekolah"> <b style="margin-left: 5px;"><?= $this->session->userdata('sess_nama_sekolah')?></b></a>

      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#!" data-toggle="offcanvas" class="sidebar-toggle hidden-md-up"></a>
        <!-- Navbar Right Menu-->
        <div class="navbar-custom-menu">
          <ul class="top-nav">
           <!-- window screen -->
            <!--li class="pc-rheader-submenu">
              <a href="#!" class="drop icon-circle" onclick="javascript:toggleFullScreen()">
                <i class="icon-size-fullscreen"></i>
              </a>
            </li-->
            <!-- User Menu-->
            <li class="dropdown">
              <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle drop-image">
                <span><img class="rounded-circle " src="<?=base_url('assets/photos/'.$foto)?>" style="width:40px;" alt="User Image"></span>
                <span><b><?= $user[0]->nm_pengguna?></b> <i class=" icofont icofont-simple-down"></i></span>
              </a>
              <ul class="dropdown-menu settings-menu">
                <li><a href="<?= base_url().'dashboard/profile'?>"><i class="icon-user"></i> Profile</a></li>
                <li class="p-0">
                  <div class="dropdown-divider m-0"></div>
                </li>
                <li><a href="#" data-toggle="modal" data-target="#default-Modal"><i class="icon-lock"></i> Ganti Password</a></li>
                <li class="p-0">
                  <div class="dropdown-divider m-0"></div>
                </li>
                <li><a href="<?= base_url().'logout'?>"><i class="icon-logout"></i> Keluar</a></li>
                <li class="p-0">
                  <div class="dropdown-divider m-0"></div>
                </li>
              </ul>
            </li>
          </ul>      
          <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
        </div>
      <!-- search end -->
      </nav>
    </header>
    <aside class="main-sidebar hidden-print ">
      <section class="sidebar" id="sidebar-scroll">
        <!--horizontal Menu Starts-->
        <ul class="sidebar-menu">
          <li class="nav-level">Navigation</li>
          <li <?php if($page == 'home') echo 'class="active"';?>>
            <a href="<?= base_url () ?>dashboard/jurnal">
              <i class="icofont icofont-home"></i><span> Home </span>
            </a>
          </li>
          <li <?php if($page == 'histori') echo 'class="active"';?>>
            <a href="<?= base_url () ?>dashboard/jurnal/histori-jurnal">
              <i class="icofont icofont-ebook"></i><span> Histori Jurnal </span>
            </a>
          </li>
          <?php if (count($kelas) != 0): ?>
          <?php foreach($kelas as $k) : ?>
          <li <?php if($page == 'absen') echo 'class="active"';?>>
            <a href="<?= base_url () ?>dashboard/jurnal/hadir-siswa/<?= $k->id_kelas ?>">
              <i class="icofont icofont-group-students"></i><span> Absen Kelas <?= $k->nm_kelas . $k->rombel_kelas ?></span>
            </a>
          </li>
          <?php endforeach;?>
          <?php endif; ?>
          <?php if(count($piket) > 0): ?>
          <?php if($piket[0]->hari == date('N')): ?>
          <li <?php if($page == 'piket') echo 'class="active"';?>>
            <a href="<?= base_url () ?>dashboard/jurnal/piket">
              <i class="icofont icofont-tasks-alt"></i><span> Guru Piket</span>
            </a>
          </li>
          <?php endif;?>
          <?php endif;?>
          <!-- -->
        </ul>
      </section>
    </aside>

<?php $this->load->view('layout/ganti-password'); ?>
