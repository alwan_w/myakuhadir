<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <!-- <h4>Selamat Pagi, {Guru}</h4> -->
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="<?= base_url () ?>dashboard/jurnal"><i class="icofont icofont-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><i class="icofont icofont-read-book"></i> Jurnal Guru
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="data_table_main">
                        <div class="card-header">
                            <h4 class="card-header-text">Halo, <?= $user[0]->nm_pengguna?></h4>
                            <br>
                            <h5 class="card-header-text" style="font-size: 16px">Anda Mulai Mengajar Pada Pukul <span style="color: #4286f4"><?= $jurnal->jam_hadir?></span></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 grid-item">
                <div class="card">
                    <div class="card-block button-list">
                        <!-- start with icon Button -->
                        <a href="<?= base_url () ?>dashboard/jurnal/isi-jurnal/<?= $id_jurnal?>" type="button" class="btn btn-primary waves-effect waves-light ">
                            <i class="icofont icofont-home "></i><span class="m-l-10">Informasi</span>
                        </a>

                        <a href="<?= base_url () ?>dashboard/jurnal/daftar-hadir/<?= $id_jurnal?>" type="button" class="btn btn-primary waves-effect waves-light ">
                            <i class="icofont icofont-group-students"></i><span class="m-l-10">Daftar Siswa</span>
                        </a>

                        <a href="<?= base_url () ?>dashboard/jurnal/isi-jurnal-guru/<?= $id_jurnal?>" type="button" class="btn btn-primary waves-effect waves-light ">
                            <i class="icofont icofont-paper"></i><span class="m-l-10">Jurnal Harian</span>
                        </a>

                        <a href="#" type="button" class="btn btn-warning waves-effect waves-light ">
                            <i class="icofont icofont-tasks"></i><span class="m-l-10">Tugas dan Ujian</span>
                        </a>

                        <form method="POST" action="<?= base_url () ?>dashboard/jurnal/kelas-selesai/<?= $id_jurnal?>">
                            <button type="submit" class="btn btn-danger waves-effect waves-light ">
                                <i class="icofont icofont-exit"></i><span class="m-l-10">Selesaikan Kelas</span>
                            </button>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Jurnal Kelas</h5>
                    </div>
                    <form method="POST" action="<?=base_url()?>dashboard/jurnal/tugas-ujian/<?= $id_jurnal?>">
                        <div class="card-block">

                            <div class="card-header">
                                <strong>Tugas atau Pekerjaan Rumah</strong>
                                <p>Tuliskan tugas dan pekerjaan rumah yang diberikan kepada siswa saat pertemuan</p>
                            </div>

                        </div>

                        <div class="card-block global-cards">
                            <div class="row advance-elements">
                                <div class="col-md-12">
                                    <label for="exampleInputPassword1" class="form-control-label">Jenis Tugas</label>
                                    <select name="jenis_tugas" class="form-control">
                                        <option value="1">Tugas Individu</option>
                                        <option value="2">Tugas Kelompok</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row advance-elements">
                                <div class="col-md-12">
                                    <label for="exampleInputPassword1" class="form-control-label">Deskripsi Tugas</label>
                                    <p>
                                        Tuliskan deskripsi tugas. Misalnya: Tugas mengerjakan latihan 2 pada buku pelajaran matematika
                                    </p>
                                    <textarea class="form-control max-textarea" maxlength="255" rows="4" name="deskripsi_tugas"><?= $jurnal->deskripsi_tugas?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-header">
                            <strong>Rencana Ujian</strong>
                            <p>Tuliskan rencana ujian pada pertemuan mendatang</p>
                        </div>
                        <div class="card-block global-cards">
                            <div class="row advance-elements">
                                <div class="col-md-12">
                                    <label for="exampleInputPassword1" class="form-control-label">Jenis Ujian</label>
                                    <select name="jenis_ujian" class="form-control">
                                        <option value="1">Ujian Harian</option>
                                        <option value="2">Ujian Tengah Semester</option>
                                        <option value="3">Ujian Akhir Semester</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row advance-elements">
                                <div class="col-md-12">
                                    <label for="exampleInputPassword1" class="form-control-label">Catatan Untuk Ujian</label>
                                    <p>
                                        Tuliskan materi atau bab yang akan diujikan pada pertemuan berikutnya. Misalnya : Materi Aljabar 1
                                    </p>
                                    <textarea class="form-control max-textarea" maxlength="255" rows="4" name="deskripsi_ujian"><?= $jurnal->deskripsi_ujian?></textarea>
                                </div>
                            </div>   
                            <div class="card-block">
                                <div class="col-md-6 ">
                                    <button type="submit" class="btn btn-primary btn-block waves-effect">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </div>