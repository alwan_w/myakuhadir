<?php if(isset($jurnal->tanggal_jurnal)) $tanggal_jurnal = $jurnal->tanggal_jurnal;
else $tanggal_jurnal = date("Y-m-d",strtotime($jurnal->created_at)); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card" style="margin-bottom:10px;">
                    <div class="data_table_main">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-sm-6" style="margin-bottom: 5px;">
                                    <h4 class="card-header-text"><?= humanDateFormat($tanggal_jurnal, TRUE) ?></h4>
                                    <br>
                                    <h4 class="card-header-text">Halo, <?= $user[0]->nm_pengguna?></h4>
                                    <br>
                                    <?php if(is_null($jurnal->nm_pengguna)):?>
                                    <h4 class="card-header-text">Anda Mulai Mengajar <span style="color: #4286f4"><?= $jurnal->nm_mata_pelajaran?></span> di Kelas <span style="color: #4286f4"><?= $kelas->nm_kelas . $kelas->rombel_kelas?></span> Pada Pukul <span style="color: #4286f4"><?= $jurnal->jam_hadir?></span></h4>
                                    <?php else:?>
                                    <h4 class="card-header-text">Guru Piket <span style="color: #4286f4"><?= $jurnal->nm_pengguna?></span> Mulai Mengajar <span style="color: #4286f4"><?= $jurnal->nm_mata_pelajaran?></span> di Kelas <span style="color: #4286f4"><?= $kelas->nm_kelas . $kelas->rombel_kelas?></span> Pada Pukul <span style="color: #4286f4"><?= $jurnal->jam_hadir?></span></h4>
                                    <?php endif;?>
                                </div>
                                <div class="col-sm-6 text-lg-right">
                                    <a href="#" type="button" class="btn btn-secondary waves-effect waves-light ">
                                        <i class="icofont icofont-paper"></i><span class="m-l-10">Jurnal Harian</span>
                                    </a>

                                    <a href="<?= base_url () ?>dashboard/jurnal/daftar-hadir/<?= $id_jurnal?>" type="button" class="btn btn-primary waves-effect waves-light ">
                                        <i class="icofont icofont-group-students"></i><span class="m-l-10">Daftar Siswa</span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Jurnal Kelas</h5>
                    </div>
                    <form method="POST" action="<?=base_url()?>dashboard/jurnal/isi-jurnal/<?= $id_jurnal?>">
                        <div class="card-block">
                            <div class="row advance-elements">
                                <div class="col-md-6">
                                    <label class="form-control-label">Materi/ Indikator Pencapaian</label>
                                    <p>Materi/ Kompetensi Dasar/ Indikator Pencapaian yang diajarkan saat pertemuan</p>
                                    <textarea class="form-control max-textarea" maxlength="255" rows="4" name="kompetensi_dasar"><?= $jurnal->kompetensi_dasar?></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-control-label">Tugas atau Pekerjaan Rumah (optional)</label>
                                    <p>
                                        Misalnya: Tugas mengerjakan latihan 2 pada buku pelajaran matematika
                                    </p>
                                    <textarea class="form-control max-textarea" maxlength="255" rows="4" name="tugas"><?= $jurnal->tugas?></textarea>
                                </div>
                            </div>
                            <div class="row" style="margin-top:15px;">
                                <div class="col-md-12">
                                    <label class="form-control-label">Catatan Kejadian (optional)</label>
                                    <p>Tuliskan semua kejadian unik selama proses belajar mengajar</p>
                                    <textarea class="form-control max-textarea" maxlength="255" rows="4" name="catatan"><?= $jurnal->catatan?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary btn-block waves-effect">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>