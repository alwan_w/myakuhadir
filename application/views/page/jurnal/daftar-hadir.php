<div class="content-wrapper">
    <div class="container-fluid">
         <!-- ROW START -->
            <div class="row">
              <div class="col-sm-12 p-0">
                <div class="main-header">
                  <h4>Halaman Absen Wali Kelas <?= $kelas->nm_kelas . $kelas->rombel_kelas?></h4>
                </div>
              </div>
            </div>
            <!-- ROW END -->
        
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Table starts -->
                <div class="card">
                    <ul class="nav nav-tabs md-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"> <i class="icofont icofont-check-circled"></i> Sudah Absen</a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"> <i class="icofont icofont-close-circled"></i> Belum Absen</a>
                            <div class="slide"></div>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home3" role="tabpanel">
                            <div class="card-block">
                                <div class="card-header">
                                    <h5 class="card-header-text">Daftar Siswa Sudah Absen Kelas <?= $kelas->nm_kelas . $kelas->rombel_kelas?>
                                            <br>
                                            Hari <?= humanDateFormat(date('Y-m-d'), 1)?>
                                            </h5>
                                    <p><small>Daftar hadir berdasarkan absensi siswa di awal jam pelajaran</small></p>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nama Siswa</th>
                                                    <th>NIS Siswa</th>
                                                    <th>Keterangan</th>
                                                    <th>Jam Hadir</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                $i = 1;
                                                foreach ($siswa as $d) { ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $i;
                                                            $i++; 
                                                            ?>
                                                        </td>
                                                        <td><?= $d->nm_pengguna ?></td>
                                                        <td><?= $d->nomor_induk ?></td>
                                                        <td>
                                                            <?php if ($d->kehadiran_verification == "1"): ?>
                                                                <span class="badge badge-warning">  Sakit</span>
                                                            <?php elseif ($d->kehadiran_verification == "2"): ?>
                                                                <span class="badge badge-warning">  Izin</span>
                                                            <?php else: ?>
                                                                <?php if (substr($d->kehadiran_timestamp,11,3) > 7): ?>
                                                                    <span class="badge badge-danger"> 
                                                                        Terlambat
                                                                    </span>
                                                                <?php else: ?>
                                                                    <span class="badge" style="color:#fff; background-color: #2196F3;"> 
                                                                        Tepat Waktu
                                                                    </span>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                        <?php if ($d->kehadiran_verification == 0 || $d->kehadiran_verification == 3): ?>
                                                            <?= substr($d->kehadiran_timestamp, 11,5) ?>
                                                            <?php else: ?>
                                                                -
                                                            <?php endif; ?>

                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>        
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="profile3" role="tabpanel">
                                <div class="card-block">
                                    <form  method="POST" action="<?=base_url()?>dashboard/jurnal/ijin-siswa/<?= $kelas->id_kelas ?>">
                                    <div class="row card-header">
                                        <div class="col-sm-6">
                                            <h5 class="card-header-text">Daftar Siswa Belum Absen Kelas <?= $kelas->nm_kelas . $kelas->rombel_kelas?>
                                            <br>
                                            Hari <?= humanDateFormat(date('Y-m-d'), 1)?>
                                            </h5>
                                            <p><small>Daftar hadir berdasarkan absensi siswa di awal jam pelajaran</small></p>
                                        </div>
                                        <div class="col-sm-6 text-lg-right">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan Keterangan Absen Siswa</button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nama Siswa</th>
                                                        <th>NIS Siswa</th>
                                                        <th>Keterangan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                    $i = 1;
                                                    foreach ($siswaTidakMasuk as $d) { ?>
                                                        <tr>
                                                            <td><?= $i++ ?></td>
                                                            <td><?= $d->nm_pengguna ?></td>
                                                            <td><?= $d->nomor_induk ?></td>
                                                            <td>
                                                                <select class="form-control" id="exampleSelect1" style="width:80%;" name="status[<?= $d->nomor_induk ?>]" >
                                                                    <option value="none">Siswa Belum Absen</option>
                                                                    <option value="Sudah">Sudah Absen</option>
                                                                    <option value="Sakit">Sakit</option>
                                                                    <option value="Izin">Izin</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>        
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>