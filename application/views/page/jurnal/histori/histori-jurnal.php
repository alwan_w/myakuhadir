<div class="content-wrapper">
  <div class="container-fluid">

    <!-- ROW START -->
    <div class="row">
      <div class="col-sm-12 p-0">
        <div class="main-header">
          <h4>Histori Jurnal Mengajar (<?=$tahunAjaran?>)</h4>
        </div>
      </div>
    </div>
    <!-- ROW END -->

    <!-- ROW START -->
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-header-text"><?= $teksTanggal ?></h4>
            <br>
            <h4 class="card-header-text">Halo, <?= $user[0]->nm_pengguna?></h4>
            <p><i>*Kotak berwarna merah jika waktu masuk lebih dari 15 menit dari waktu mulai mengajar</i></p>
          </div>
          <div class="card-block">
            <div class="data_table_main">
              <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                <thead>
                  <tr>
                    <th>Sesi</th>
                    <th>Tanggal Mengajar</th>
                    <th>Waktu Masuk</th>
                    <th>Status Jurnal</th>
                    <th>Detail</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Mapel/Kelas</th>
                    <th>Tanggal Mengajar</th>
                    <th>Waktu Masuk</th>
                    <th>Status Jurnal</th>
                    <th>Detail</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php foreach ($jurnal as $d) { ?>
                  <?php if(isset($d->tanggal_jurnal)) $tanggal_jurnal = $d->tanggal_jurnal;
                        else $tanggal_jurnal = date("Y-m-d",strtotime($d->created_at)); ?>
                    <tr>
                      <td>
                        <?= $d->nm_mata_pelajaran?> (<?= $d->nm_kelas . $d->rombel_kelas ?>)
                        <br>
                        <?= getDayName($tanggal_jurnal, FALSE, TRUE)?>, Periode : <?= $d->periode?> 
                        <br>
                        <i class="icofont icofont-clock-time"></i> <?= $d->jam_mulai?> - <?= $d->jam_selesai?>
                      </td>
                      <td>
                        <?= humanDateFormat($tanggal_jurnal);?>
                      </td>
                      <td
                        <?php if (((strtotime($d->jam_hadir) - strtotime($d->jam_mulai)) / 60) > 15):?>
                            style='background-color: #ff5252'
                        <?php endif; ?>
                      >
                        <?= $d->jam_hadir?>
                      </td>
                      <td>
                        <?php if ($d->status === '1'): ?>
                            <span class="badge badge-warning">  Belum Mengisi Jurnal</span><br>
                                                        
                        <?php elseif ($d->status === '2'): ?>
                            <span class="badge badge-danger">  Kelas Belum Selesai</span><br>

                        <?php elseif ($d->status === '3'): ?>
                            <span class="badge badge-success">  Jurnal Sudah Diisi</span><br>

                        <?php else: ?>
                            <span class="badge badge-primary">  Belum Mulai Kelas</span>
                        <?php endif; ?>
                      </td>

                          <td>
                            <a href="<?= base_url () ?>dashboard/jurnal/histori-jurnal/detail/<?php echo $d->id_jurnal ?>">
                              <button type="button" class="btn btn-info btn-icon waves-effect waves-light">
                                <i class="icofont icofont-look"></i>
                              </button>
                            </a>
                            <a href="<?= base_url () ?>dashboard/jurnal/isi-jurnal/<?php echo $d->id_jurnal ?>">
                              <button type="button" class="btn btn-primary btn-icon waves-effect waves-light">
                                <i class="icofont icofont-ui-edit"></i>
                              </button>
                            </a>
                          </td>
                        </tr>
                      <?php } ?> 
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- ROW END -->
      </div>
    </div>