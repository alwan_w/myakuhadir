<?php if(isset($jurnal[0]->tanggal_jurnal)) $tanggal_jurnal = $jurnal[0]->tanggal_jurnal;
else $tanggal_jurnal = date("Y-m-d",strtotime($jurnal[0]->created_at)); ?>
<div class="content-wrapper">
  <div class="container-fluid">

    <!-- ROW START -->
    <div class="row">
      <div class="col-sm-12">
        <div class="card" style="margin-bottom:10px;">
          <div class="data_table_main">
            <div class="card-header">
              <div class="row">
                <div class="col-sm-6">
                  <h5 class="card-header-text" style="font-size: 16px">Jurnal Kelas <span style="color: #4286f4"><?= $jurnal[0]->nm_kelas . $jurnal[0]->rombel_kelas?></span> Hari <span style="color: #4286f4"><?= humanDateFormat($tanggal_jurnal, TRUE)?></span></h5>
                  <br>
                  <h5 class="card-header-text" style="font-size: 16px">Pelajaran <span style="color: #4286f4"><?= $jurnal[0]->nm_mata_pelajaran?></span> Periode <span style="color: #4286f4"><?= $jurnal[0]->periode?></span> : <span style="color: #4286f4"><?= $jurnal[0]->jam_mulai . ' - ' . $jurnal[0]->jam_selesai?></span></h5>
                  <br>
                  <?php if(is_null($jurnal[0]->nm_pengguna)):?>
                  <h5 class="card-header-text" style="font-size: 16px">Anda Mulai Mengajar Pada Pukul <span style="color: #4286f4"><?= $jurnal[0]->jam_hadir?></span></h5>
                  <?php else:?>
                  <h5 class="card-header-text" style="font-size: 16px">Guru Piket <span style="color: #4286f4"><?= $jurnal[0]->nm_pengguna?></span> Mulai Mengajar <span style="color: #4286f4"><?= $jurnal[0]->nm_mata_pelajaran?></span> di Kelas <span style="color: #4286f4"><?= $kelas->nm_kelas . $kelas->rombel_kelas?></span> Pada Pukul <span style="color: #4286f4"><?= $jurnal[0]->jam_hadir?></span></h5>
                  <?php endif;?>
                </div>
                <div class="col-sm-6 text-lg-right">
                  <a href="#" type="button" class="btn btn-secondary waves-effect waves-light ">
                    <i class="icofont icofont-paper"></i><span class="m-l-10">Jurnal Harian</span>
                  </a>

                  <a href="<?= base_url () ?>dashboard/jurnal/histori-jurnal/daftar-hadir/<?= $jurnal[0]->id_jurnal?>" type="button" class="btn btn-primary waves-effect waves-light ">
                    <i class="icofont icofont-group-students"></i><span class="m-l-10">Daftar Siswa</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
    <!-- ROW END -->

    <!-- ROW START -->
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h5 class="card-header-text">Jurnal Kelas</h5>
          </div>
          <div class="card-block">
            <div class="row advance-elements">
              <div class="col-md-6">
                <label class="form-control-label">Materi/ Indikator Pencapaian</label>
                <p>Materi/ Kompetensi Dasar/ Indikator Pencapaian yang diajarkan saat pertemuan</p>
                <textarea class="form-control max-textarea" maxlength="255" rows="4" readonly style= "background-color: white"><?= $jurnal[0]->kompetensi_dasar?></textarea>
              </div>
              <div class="col-md-6">
                <label class="form-control-label">Tugas atau Pekerjaan Rumah (optional)</label>
                <p>
                    Misalnya: Tugas mengerjakan latihan 2 pada buku pelajaran matematika
                </p>
                <textarea class="form-control max-textarea" maxlength="255" rows="4" readonly style= "background-color: white"><?= $jurnal[0]->tugas?></textarea>
              </div>
          </div>
          <div class="row" style="margin-top:15px;">
            <div class="col-md-12">
              <label class="form-control-label">Catatan Kejadian (optional)</label>
              <p>Tuliskan semua kejadian unik selama proses belajar mengajar</p>
              <textarea class="form-control max-textarea" maxlength="255" rows="4" readonly style= "background-color: white"><?= $jurnal[0]->catatan?></textarea>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
    <!-- ROW END -->

  </div>
</div>