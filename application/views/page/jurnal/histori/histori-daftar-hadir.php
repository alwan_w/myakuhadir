<?php if(isset($jurnal[0]->tanggal_jurnal)) $tanggal_jurnal = $jurnal[0]->tanggal_jurnal;
else $tanggal_jurnal = date("Y-m-d",strtotime($jurnal[0]->created_at)); ?>
<div class="content-wrapper">
  <div class="container-fluid">

    <!-- ROW START -->
    <div class="row">
      <div class="col-sm-12">
        <div class="card" style="margin-bottom:10px;">
          <div class="data_table_main">
            <div class="card-header">
              <div class="row">
                <div class="col-sm-6">
                  <h5 class="card-header-text" style="font-size: 16px">Jurnal Kelas <span style="color: #4286f4"><?= $jurnal[0]->nm_kelas . $jurnal[0]->rombel_kelas?></span> Hari <span style="color: #4286f4"><?= humanDateFormat($tanggal_jurnal, TRUE)?></span></h5>
                  <br>
                  <h5 class="card-header-text" style="font-size: 16px">Pelajaran <span style="color: #4286f4"><?= $jurnal[0]->nm_mata_pelajaran?></span> Periode <span style="color: #4286f4"><?= $jurnal[0]->periode?></span> : <span style="color: #4286f4"><?= $jurnal[0]->jam_mulai . ' - ' . $jurnal[0]->jam_selesai?></span></h5>
                  <br>
                  <?php if(is_null($jurnal[0]->nm_pengguna)):?>
                  <h5 class="card-header-text" style="font-size: 16px">Anda Mulai Mengajar Pada Pukul <span style="color: #4286f4"><?= $jurnal[0]->jam_hadir?></span></h5>
                  <?php else:?>
                  <h5 class="card-header-text" style="font-size: 16px">Guru Piket <span style="color: #4286f4"><?= $jurnal[0]->nm_pengguna?></span> Mulai Mengajar <span style="color: #4286f4"><?= $jurnal[0]->nm_mata_pelajaran?></span> di Kelas <span style="color: #4286f4"><?= $jurnal[0]->nm_kelas . $jurnal[0]->rombel_kelas?></span> Pada Pukul <span style="color: #4286f4"><?= $jurnal[0]->jam_hadir?></span></h5>
                  <?php endif;?>
                </div>
                <div class="col-sm-6 text-lg-right">
                  <a href="<?= base_url () ?>dashboard/jurnal/histori-jurnal/detail/<?= $jurnal[0]->id_jurnal?>" type="button" class="btn btn-primary waves-effect waves-light ">
                    <i class="icofont icofont-paper"></i><span class="m-l-10">Jurnal Harian</span>
                  </a>

                  <a href="#" type="button" class="btn btn-secondary waves-effect waves-light ">
                    <i class="icofont icofont-group-students"></i><span class="m-l-10">Daftar Siswa</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
    <!-- ROW END -->

    <!-- ROW START -->
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-sm-6">
                <h5 class="card-header-text">Daftar Kehadiran Siswa Kelas <?= $jurnal[0]->nm_kelas . $jurnal[0]->rombel_kelas?></h5>
                <p><small>Daftar hadir berdasarkan absensi siswa di awal jam pelajaran</small></p>
              </div>
              <div class="col-sm-6 text-lg-right">
                <h5 class="card-header-text">Jumlah Total Siswa <?= $jumlah['total']?></h5>
                <p><small>Sakit : <?= $jumlah['sakit']?>, Izin : <?= $jumlah['izin']?>, Alpha: <?= $jumlah['alpha']?></small></p>
              </div>
            </div>
          </div>
          <div class="card-block">
            <div class="row">
              <div class="col-sm-12 table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nama Siswa</th>
                      <th>NIS Siswa</th>
                      <th>Keterangan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $i = 1;
                    foreach ($siswa as $d) { ?>
                    <tr>
                      <td><?= $i++ ?></td>
                      <td><?= $d->nm_pengguna ?></td>
                      <td><?= $d->nomor_induk ?></td>
                      <td>
                        <?php if ($d->kehadiran_verification == "1"): ?>
                        <span class="badge badge-success">  Sakit</span>
                        <?php elseif ($d->kehadiran_verification == "2"): ?>
                        <span class="badge badge-warning">  Izin</span>
                        <?php elseif ($d->kehadiran_verification == "0" || $d->kehadiran_verification == "3"): ?>
                        <span class="badge" style="color:#fff; background-color: #2196F3;">  Hadir </span>
                        <?php else:?>
                        <span class="badge badge-danger"> Tanpa Keterangan </span>
                        <?php endif; ?>
                      </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ROW END -->
  </div>
</div>
</div>