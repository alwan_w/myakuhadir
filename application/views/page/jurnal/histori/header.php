<!DOCTYPE html>
<html lang="en">

<head>
    <title>Akuhadir.com</title>
    <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<!-- Favicon icon -->
<link rel="shortcut icon" href="<?=base_url()?>//theme/ltr-horizontal/assets/images/favicon.png" type="image/x-icon">
<link rel="icon" href="<?=base_url()?>//theme/ltr-horizontal/assets/images/favicon.ico" type="image/x-icon">

<!-- Google font-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<!--Date Picker Material Icon Css-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!-- Font Awesome -->
<link href="<?=base_url()?>//theme/ltr-horizontal/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- iconfont -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/icon/icofont/css/icofont.css">

<!-- simple line icon -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/icon/simple-line-icons/css/simple-line-icons.css">

<!-- Required Fremwork -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/../bower_components/bootstrap/css/bootstrap.min.css">

<!-- Nestable CSS -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/plugins//nestable/css/jquery.nestable.min.css">

<!-- Data Table Css -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/../bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/plugins//data-table/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/../bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">

<!-- Multi Select css -->
<link rel="stylesheet" href="<?=base_url()?>//theme/ltr-horizontal/../bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css" />
<link rel="stylesheet" href="<?=base_url()?>//theme/ltr-horizontal/../bower_components/multiselect/css/multi-select.css" />

<!-- Style.css -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/css/main.css">

<!-- Responsive.css-->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/css/responsive.css">    <!--color css-->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/css/color/<?=$this->session->userdata('sess_tema')?>.css" id="color"/>

</head>