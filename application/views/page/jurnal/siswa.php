<div class="content-wrapper">
    <div class="container-fluid">
        <!-- ROW START -->
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow" style="margin-top: -20px; margin-left: -25px">
                        <li class="breadcrumb-item">
                            <a href="<?= base_url() ?>dashboard/jurnal">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <i class="icofont icofont-history"></i> Siswa
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ROW END -->

        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Table starts -->
                <div class="card">
                    <ul class="nav nav-tabs md-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"> <i class="icofont icofont-check-circled"></i> Absensi Kehadiran</a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"> <i class="icofont icofont-pencil-alt-2"></i> Aktivitas</a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"> <i class="icofont icofont-camera-alt"></i> Foto</a>
                            <div class="slide"></div>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="home3" role="tabpanel">
                            <div class="card-block">
                                <div class="card-header">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nama Siswa</th>
                                                    <th>NIS Siswa</th>
                                                    <th>Hari/Tanggal</th>
                                                    <th>Jam Hadir</th>
                                                    <th>Keterangan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="profile3" role="tabpanel">
                            <div class="card-block">
                                <div class="card-header">
                                    <div class="col-sm-12">
                                        <p>Tidak ada hasil</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="messages3" role="tabpanel">
                            <p>3. This is Photoshop's version of Lorem IpThis is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                                Aenean mas Cum sociis natoque penatibus et magnis dis.....</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
    var tanggal = document.getElementById('system-date');
    var tanggal_hari = document.getElementById('system-date1');
    var greeting = document.getElementById('system-greeting');

    function time() {
        tanggal.innerHTML = "";
        greeting.innerHTML = "Halo";

        var d = new Date();
        var year = d.getFullYear();
        var day = d.getDate();

        var month = new Array();
        month[0] = "Januari";
        month[1] = "Februari";
        month[2] = "Maret";
        month[3] = "April";
        month[4] = "Mei";
        month[5] = "Juni";
        month[6] = "Juli";
        month[7] = "Agustus";
        month[8] = "September";
        month[9] = "Oktober";
        month[10] = "November";
        month[11] = "Desember";

        var m = month[d.getMonth()];

        var weekday = new Array(7);
        weekday[0] = "Minggu";
        weekday[1] = "Senin";
        weekday[2] = "Selasa";
        weekday[3] = "Rabu";
        weekday[4] = "Kamis";
        weekday[5] = "Jumat";
        weekday[6] = "Sabtu";

        var h = weekday[d.getDay()];
        var jam = d.getHours();

        if (4 <= jam && jam < 10) {
            var salam = "Selamat Pagi";
        } else if (10 <= jam && jam < 14) {
            var salam = "Selamat Siang";
        } else if (14 <= jam && jam < 18) {
            var salam = "Selamat Sore";
        } else {
            var salam = "Selamat Malam";
        }

        tanggal.innerHTML = h + ", " + day + " " + m + " " + year;
        tanggal_hari.innerHTML = day + " " + m + " " + year;
        greeting.innerHTML = salam;
    }
    setInterval(time, 1000);

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }; // add zero in front of numbers < 10
        return i;
    }
</script>