<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Hasil Absensi (Absensi Guru & Karyawan)</h4>
              </div>
          </div>
      </div>
      <div class="row">
        
        <?php $this->load->view('page/hasilAbsensi/sidebarHasilAbsensi',array('menu' => 'civitas')); ?>

        <div class="col-xl-9 col-md-8 col-sm-12 pull-xl-3 pull-md-4 filter-bar card">
          <form action="" method="GET">
          <nav class="navbar navbar-light bg-faded m-b-30 p-10">
              <ul class="nav navbar-nav task-board-list">
                  <li class="nav-item active">
                      <a class="nav-link" href="#!">Filter: <span class="sr-only">(current)</span></a>
                  </li>

                  <li class="nav-item">
                      <a class="nav-link" href="#!" id="bydate"><i class="icofont icofont-clock-time"></i> By Date</a>
                  </li>
                  <li class="nav-item">
                    <div id="reportrange" class="f-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i> 
                    <span></span> <b class="caret"></b>
                    <input type="text" id="start" name="start" hidden="">
                    <input type="text" id="end" name="end" hidden="">
                    </div>
                  </li>
                  <!-- end of by date dropdown -->
                  <li style="margin-left: 10px" class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#!" id="bystatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="icofont icofont-chart-histogram-alt"></i><span id="status">By Status</span></a>
                      <div class="dropdown-menu" aria-labelledby="bystatus">
                          <a class="dropdown-item" href="javascript:;" onclick="setStatusAbsen('Semua')">Semua</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="javascript:;" onclick="setStatusAbsen('Belum Pulang')">Belum Pulang</a>
                          <a class="dropdown-item" href="javascript:;" onclick="setStatusAbsen('Sudah Pulang')">Sudah Pulang</a>
                      </div>
                      <input type="text" name="status" hidden="">
                  </li>
                  <!-- end of by status dropdown -->                
                  <li style="margin-left: 10px; " class="nav-item"><button style="cursor: pointer;" id="search" class="btn btn-primary">Cari</button></li>
              </ul>
          </nav>
          </form>
          <!-- end of main navbar  -->
          <div class="row">
            <style type="text/css">
              .red{
                background-color: #a51515;
                color: white;
              }
              .green{
                background-color: #07601e;
                color: white;
              }
            </style>
            <div class="col-sm-12">
              <div class="card">        
                <div class="card-header"><h5 class="card-header-text"><?php echo $tanggal; ?></h5></div>  
                <div class="card-block">
                  <div class="data_table_main">
                    <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                      <thead>
                      <tr>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>Datang</th> 
                        <th>Pulang</th>
                        <th>Status</th>
                      </tr>
                      </thead>
                      <tfoot>
                      <tr>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>Datang</th>  
                        <th>Pulang</th>
                        <th>Status</th>
                      </tr>
                      </tfoot>
                       <tbody id="tbody" >
                        <?php foreach ($read as $r) { 
                          $jam_pulang = $r['jam_pulang'];
                          if(is_null($jam_pulang)) $jam_pulang = '-';
                          //$masuk = date("H.i", strtotime($r['kehadiran_timestamp']));
                          ?>
                        <tr>
                          <td><?php echo '<i style="display:none">'.$r['tanggal'] .'</i>'.$r['tanggal_indo'] ?></td>
                          <td><?php echo $r['nm_pengguna'] ?></td>
                          <td><?= $r['jam_datang'] ?></td>
                          <td><?= $jam_pulang ?></td>
                          <td class="<?php if ($r['status']==1) {
                            echo 'red';} else{echo 'green';} ?>" >
                            <?php if ($r['status']==1) echo 'Belum Pulang'; else echo 'Sudah Pulang';?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>            
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
  </div>
</div>