<!-- Date picker.js -->
<script src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//datepicker/js/moment-with-locales.min.js"></script>
<script src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<!-- Bootstrap Datepicker js -->
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>

<!-- bootstrap range picker -->
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
<!-- <script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/assets/pages/advance-form.js"></script> -->

<script type="text/javascript">
	var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#reportrange input[name="start"]').val(start.format('YYYY-MM-DD'));
        $('#reportrange input[name="end"]').val(end.format('YYYY-MM-DD'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        "drops": "down",
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
	
    $( document ).ready(function() {
        cb(start, end)
    });
	function setStatusAbsen(status) {
		$('input[name="status"]').val(status);
        $('#status').text(status);
	}
    $(document).ready(function() {
        table = $('#simpletable').DataTable();
        table.order([0, 'desc']).draw();
    } );
</script>