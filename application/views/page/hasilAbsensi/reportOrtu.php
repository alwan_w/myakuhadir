<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Hasil Absensi (Report Orang Tua Siswa)</h4>
              </div>
          </div>
      </div>
      <div class="row">
        
        <?php $this->load->view('page/hasilAbsensi/sidebarHasilAbsensi', array('menu'=>'report')); ?>

        <div class="col-xl-9 col-md-8 col-sm-12 pull-xl-3 pull-md-4 filter-bar card">
          <form action="" method="GET">
          <nav class="navbar navbar-light bg-faded m-b-30 p-10">
              <ul class="nav navbar-nav task-board-list">
                  <li class="nav-item active">
                      <a class="nav-link" href="#!">Pilih Report: <span class="sr-only">(current)</span></a>
                  </li>

                  <li class="nav-item">
                    <div class="form-group" style="margin:0px;">
                      <select class="form-control" name="report">
                        <?php foreach($listReport as $list): ?>
                        <option value="<?= $list['id_report_ortu']?>" <?php if($list['id_report_ortu'] == $id_report_ortu) echo 'selected'?>><?= $list['nama_report']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </li>              
                  <li style="margin-left: 10px; " class="nav-item"><button style="cursor: pointer;" id="search" class="btn btn-primary">Pilih</button></li>
              </ul>
          </nav>
          </form>
          <!-- end of main navbar  -->
          <div class="row">
            <style type="text/css">
              .red{
                background-color: #a51515;
                color: white;
              }
              .yellow{
                background-color: #f5b400;
                color: white;
              }
              .green{
                background-color: #07601e;
                color: white;
              }
            </style>
            <div class="col-sm-12">
              <div class="card">        
                <div class="card-header">
                  <div class="row">
                    <div class="col-sm-6">
                      <h5 class="card-header-text"><?php echo $tanggal; ?></h5>
                    </div>
                    <div class="col-sm-6 text-lg-right">
                      <a href="<?= base_url () ?>dashboard/result/absensi/report-ortu/kirim/?>" type="button" class="btn btn-primary waves-effect waves-light ">
                          <i class="icofont icofont-email"></i><span class="m-l-10">Kirim Report Baru</span>
                      </a>
                    </div>
                  </div>
                  
                </div>  
                <div class="card-block">
                  <div class="data_table_main">
                    <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                      <thead>
                      <tr>
                        <th>Nama Siswa</th>
                        <th>Nomor Induk</th> 
                        <th>Kelas</th>
                        <th>Status Report</th>
                      </tr>
                      </thead>
                      <tfoot>
                      <tr>
                        <th>Nama Siswa</th>
                        <th>Nomor Induk</th> 
                        <th>Kelas</th>
                        <th>Status Report</th>
                      </tr>
                      </tfoot>
                       <tbody id="tbody" >
                        <?php foreach ($report as $r) { ?>
                        <tr>
                          <td><?php echo $r['nm_pengguna'] ?></td>
                          <td><?php echo $r['nomor_induk'] ?></td>
                          <td><?= $r['nm_kelas'] . $r['rombel_kelas'] ?></td>
                          <td class="<?php if ($r['status'] == 0) {
                            echo 'red';} elseif($r['status'] == 1) {echo 'yellow';} else{echo 'green';} ?>" >
                              <?php if ($r['status'] == 0)
                                echo 'Gagal Dikirim';
                              elseif($r['status'] == 1)
                                echo 'Sedang Dikirim';
                              else
                                echo 'Berhasil Dikirim';
                              ?>
                                
                            </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>            
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
  </div>
</div>