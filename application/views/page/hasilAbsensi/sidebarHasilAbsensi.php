<?php
$siswa = 0;
$report = 0;
$civitas = 0;
if(isset($menu))
{
    if($menu == 'siswa')
        $siswa++;
    elseif($menu == 'civitas')
        $civitas++;
    else
        $report++;
}
?>
<div class="col-xl-3 col-md-4 col-sm-12 push-xl-9 push-md-8">
    <div class="card sidemenu">
        <div class="card-header"><h5 class="card-header-text">Menu Hasil Absensi</h5></div>
        <div class="card-block p-t-0">       
            <ul class="basic-list">
                <li><?php if($civitas>0) echo '<i class="icofont icofont-arrow-right"></i>';?> <a href="<?= base_url() ?>dashboard/result/absensi/civitas">Absensi Guru & Karyawan</a> <?php if($civitas>0) echo '<i class="icofont icofont-arrow-left"></i>';?></li>
                <li><?php if($siswa>0) echo '<i class="icofont icofont-arrow-right"></i>';?> <a href="<?= base_url() ?>dashboard/result/absensi/siswa">Absensi Siswa</a> <?php if($siswa>0) echo '<i class="icofont icofont-arrow-left"></i>';?></li>
                <!--li><a href="<?php echo base_url() ?>dashboard/result/absensi/guru">Absensi Guru / Karyawan</a></li-->
                <li><?php if($report>0) echo '<i class="icofont icofont-arrow-right"></i>';?> <a href="<?= base_url() ?>dashboard/result/absensi/report-ortu">Report Orang Tua Siswa</a> <?php if($report>0) echo '<i class="icofont icofont-arrow-left"></i>';?></li>
                <!--li><a href="">Jurnal Kelas</a></li-->
            </ul>
        </div>
        <!-- end of card-block -->
    </div>
    <!-- end of card-block -->
</div>