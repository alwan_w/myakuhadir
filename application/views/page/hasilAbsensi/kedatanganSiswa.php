<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Hasil Absensi (Absensi Siswa)</h4>
              </div>
          </div>
      </div>
      <div class="row">
        
        <?php $this->load->view('page/hasilAbsensi/sidebarHasilAbsensi',array('menu' => 'siswa')); ?>

        <div class="col-xl-9 col-md-8 col-sm-12 pull-xl-3 pull-md-4 filter-bar card">
          <form action="" method="GET">
          <nav class="navbar navbar-light bg-faded m-b-30 p-10">
              <ul class="nav navbar-nav task-board-list">
                  <li class="nav-item active">
                      <a class="nav-link" href="#!">Filter: <span class="sr-only">(current)</span></a>
                  </li>

                  <li class="nav-item">
                      <a class="nav-link" href="#!" id="bydate"><i class="icofont icofont-clock-time"></i> By Date</a>
                  </li>
                  <li class="nav-item">
                    <div id="reportrange" class="f-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i> 
                    <span></span> <b class="caret"></b>
                    <input type="text" id="start" name="start" hidden="">
                    <input type="text" id="end" name="end" hidden="">
                    </div>
                  </li>
                  <!-- end of by date dropdown -->
                  <li style="margin-left: 10px" class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#!" id="bystatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="icofont icofont-chart-histogram-alt"></i><span id="status">By Status</span></a>
                      <div class="dropdown-menu" aria-labelledby="bystatus">
                          <a class="dropdown-item" href="javascript:;" onclick="setStatusAbsen('Semua')">Semua</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="javascript:;" onclick="setStatusAbsen('Terlambat')">Terlambat</a>
                          <a class="dropdown-item" href="javascript:;" onclick="setStatusAbsen('Tepat')">Tepat waktu</a>
                      </div>
                      <input type="text" name="status" hidden="">
                  </li>
                  <!-- end of by status dropdown -->                
                  <li style="margin-left: 10px; " class="nav-item"><button style="cursor: pointer;" id="search" class="btn btn-primary">Cari</button></li>
                  <li style="margin-left: 10px; " class="nav-item"><a href="#" style="cursor: pointer; background-color:#555; border-color:#555" class="btn btn-secondary" data-toggle="modal" data-target="#downloadModal">Download Rekap Absensi Siswa</a></li>
              </ul>
          </nav>
          </form>
          <!-- end of main navbar  -->
          <div class="row">
            <style type="text/css">
              .red{
                background-color: #a51515;
                color: white;
              }
              .green{
                background-color: #07601e;
                color: white;
              }
            </style>
            <div class="col-sm-12">
              <div class="card">        
                <div class="card-header"><h5 class="card-header-text"><?php echo $tanggal; ?></h5></div>  
                <div class="card-block">
                  <div class="data_table_main">
                    <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                      <thead>
                      <tr>
                        <th>Absen Jam</th>
                        <th>Nama Siswa</th>
                        <th>Kelas</th> 
                        <th>Status Keterlambatan</th>
                      </tr>
                      </thead>
                      <tfoot>
                      <tr>
                        <th>Waktu Absen</th>
                        <th>Nama Siswa</th>
                        <th>Kelas</th>  
                        <th>Status Keterlambatan</th>
                      </tr>
                      </tfoot>
                       <tbody id="tbody" >
                        <?php foreach ($read as $r) { 
                          $masuk = date("H.i", strtotime($r['kehadiran_timestamp']));
                          ?>
                        <tr>
                          <td><?php echo '<i style="display:none">'.$r['kehadiran_timestamp'] .'--</i>' . $r['kehadiran_tanggal'] ?></td>
                          <td><?php echo $r['nm_pengguna'] ?></td>
                          <td><?= $r['nm_kelas'] . $r['rombel_kelas'] ?></td>
                          <td class="<?php if ($masuk > $jadwal) {
                            echo 'red';} else{echo 'green';} ?>" ><?php if ($masuk > $jadwal)  { 
                              $jam = explode('.', $masuk);
                              $jdwl = explode('.', $jadwal);
                              $jam = $jam[0]*60 + $jam[1];
                              $jdwl = $jdwl[0]*60 + $jdwl[1];
                              $jm = $jam - $jdwl;
                              $mnt = $jm%60;
                              $jm = floor($jm/60);
                              if ($jm < 1) {
                                $stMnt = $mnt . ' menit';
                              }
                              else{
                                $stMnt = $jm . ' jam ';
                                if ($mnt>0)
                                  $stMnt = $stMnt . $mnt . ' menit';
                              }
                              if($jm<10)
                                $jm = '0'.$jm;
                              if($mnt<10)
                                $mnt = '0'.$mnt;
                            echo '<i style="display:none">'.$jm.$mnt.'</i> Terlambat '. $stMnt;} else{echo '<i style="display:none">0</i> Tepat';} ?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>            
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
  </div>
</div>

<div class="card-block button-list">
  <div class="modal fade modal-flex" id="downloadModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Download Excel Rekap Absensi Siswa</h4>
        </div>

        <div class="modal-body">
          <?php if ($this->session->flashdata('pesanRekap') != '') : ?>
            <div class="col-sm-12">
              <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-top: -10px;">&times;</a>
                <strong><?= $this->session->flashdata('pesanRekap'); ?></strong>
              </div>
            </div>
          <?php endif; ?>
          <form method="POST" action="<?=base_url()?>dashboard/result/download/absensi-siswa-rekap">

          <p>Pilih Bulan dan Tahun Ajaran Rekap</p>
          <div class="form-group row">
            <!-- <label for="file" class="col-md-2 col-form-label form-control-label">File input</label> -->
            <?php 
              $semester = explode(" ",getTahunAjaran());
              $tahun = explode("/",$semester[1])[0];
              $semester = $semester[0];
              $bulan = (int)date('m'); 
              $option = array(
                'Genap' => array(
                  'bulan' => array('Januari','Februari','Maret','April','Mei','Juni'),
                  'value' => array(1,2,3,4,5,6)
                ),
                'Ganjil' => array(
                  'bulan' => array('Juli','Agustus','September','Oktober','November','Desember'),
                  'value' => array(7,8,9,10,11,12)
                )
              );
            ?>
            <div class="col-md-3">
              <select class="form-control" name="semester" required>
                <option value="Ganjil" <?php if($semester == 'Ganjil') echo 'selected'?> onclick="changeOption('Ganjil')">Ganjil</option>
                <option value="Genap" <?php if($semester == 'Genap') echo 'selected'?> onclick="changeOption('Genap')">Genap</option>
              </select>
            </div>
            <div class="col-md-4">
              <select class="form-control" name="tahun" required>
                <?php for($i=2019; $i<=(int)date('Y'); $i++): ?>
                <option value="<?=$i?>" <?php if($i == $tahun) echo 'selected'?>><?=$i . '/' . ($i+1)?></option>
                <?php endfor;?>
              </select>
            </div>
            <div class="col-md-5">
              <select class="form-control" name="bulan" id="selectBulan" required>
                <option value="all">Semua Bulan</option>
                <?php for($i = 0; $i<count($option[$semester]['bulan']); $i++):?>
                  <option value="<?=$option[$semester]['value'][$i]?>" <?php if($option[$semester]['value'][$i] == $bulan) echo 'selected';?>><?=$option[$semester]['bulan'][$i]?></option>
                <?php endfor;?>
              </select>
            </div>
          </div>


        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Batalkan</button>
          <button type="submit" class="btn btn-primary waves-effect waves-light ">Download</button>
        </div>
      </form>

    </div>
  </div>
</div>

<script type="text/javascript">
  function changeOption(semester)
  {
    if(semester == 'Ganjil')
    {
      var newOptions = {
        "Semua Bulan": "all",
        "Juli": "7",
        "Agustus": "8",
        "September": "9",
        "Oktober": "10",
        "November": "11",
        "Desember": "12"
      };
    }
    else
    {
      var newOptions = {
        "Semua Bulan": "all",
        "Januari": "1",
        "Februari": "2",
        "Maret": "3",
        "April": "4",
        "Mei": "5",
        "Juni": "6"
      };
    }

    var $el = $("#selectBulan");
    $el.empty(); // remove old options
    $.each(newOptions, function(key,value) {
      $el.append($("<option></option>")
         .attr("value", value).text(key));
    });
  }

</script>