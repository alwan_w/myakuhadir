<div style="margin-top: -90px" class="content-wrapper">
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Hasil Absensi Siswa <?= $teksDate ?></h4>
              </div>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12 filter-bar">
          <div class="row">
            <style type="text/css">
              .red{
                background-color: #a51515;
                color: white;
              }
              .green{
                background-color: #07601e;
                color: white;
              }
              .blue{
                background-color: #007bff;;
                color: white;
              }
              .yellow{
                background-color: #f5b400;
                color: white;
              }
            </style>
            <div class="col-sm-12">
              <div class="card">                
                <div class="card-block">
                  <div class="row">
                    <div class="col-sm-6">
                      <p>Nama : <?php echo $nama_siswa ?></p>
                      <p>Kelas : <?php echo $kelas_siswa ?></p>  
                    </div>
                    <div class="col-sm-6 text-lg-right">
                      <p>Total Hari Efektif : <?= $rekap['hari_efektif']?></p>
                      <p>Tepat Waktu : <?= $rekap['tepat']?>, Terlambat : <?= $rekap['terlambat']?></p>
                      <p>Sakit : <?= $rekap['sakit']?>, Izin : <?= $rekap['izin']?>, Alpha : <?= $rekap['alpha']?></p>
                    </div>
                  </div>     
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="card">                
                <div class="card-block">
                  <div class="data_table_main">
                    <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap" style="width:100%;">
                      <thead>
                      <tr>
                        <th data-priority="1">Tanggal Absensi</th>
                        <th data-priority="4">Hari Absensi</th>
                        <th data-priority="3">Jam Absensi</th>
                        <th data-priority="2">Status Absensi</th>
                      </tr>
                      </thead>
                      <tfoot>
                      <tr>
                        <th>Tanggal Absensi</th>
                        <th>Hari Absensi</th>
                        <th>Jam Absensi</th>
                        <th>Status Absensi</th>
                      </tr>
                      </tfoot>
                      <tbody id="tbody" >
                        <?php foreach ($read as $r) { 
                          $tanggal = explode(' ', $r['kehadiran_timestamp']);
                          $jam = $tanggal[1];
                          $hari = getDayName($tanggal[0], FALSE, TRUE);
                          $tanggal = humanDateFormat($tanggal[0]);
                          if($r['kehadiran_verification'] != 0 && $r['kehadiran_verification'] != 3)
                            $jam = '-';
                          ?>
                        <tr>
                          <td><?php echo $tanggal ?></td>
                          <td><?php echo $hari ?></td>
                          <td><?php echo $jam ?></td>
                          <td class="<?php if ($r['kehadiran_verification'] == 0) {
                            echo 'green';} elseif($r['kehadiran_verification'] == 1 || $r['kehadiran_verification'] == 2){echo 'blue';} elseif($r['kehadiran_verification'] == 3){echo 'yellow';} else{echo 'red';}?>" ><?= $r['status_hadir']?></td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>            
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
  </div>
</div>