<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Izin Sakit Kehadiran Siswa</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <div class="filter-bar">
              <form action="" method="GET">
                <nav class="navbar navbar-light bg-faded">
                    <ul class="nav navbar-nav task-board-list">
                        <li class="nav-item active">
                            <a class="nav-link" href="#!">Filter: <span class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#!" id="bydate"><i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i> By Tanggal</a>
                        </li>
                        <li class="nav-item">
                          <div id="reportrange" class="f-right" style="background: #fff; padding: 5px 5px; border: 1px solid #ccc; border-radius: 7px; width: 100%">
                          <input class="form-control" type="date" name="tanggal" style="cursor: pointer; padding: 3px; border: none;">
                          </div>
                        </li>
                        <li style="margin-left: 10px" class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#!" id="bystatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="icofont icofont-group-students"></i><span id="kelas"> By Kelas</span></a>
                            <div class="dropdown-menu" aria-labelledby="bystatus">
                                <a class="dropdown-item" href="javascript:;" onclick="setKelas('')">Semua</a>
                                <div class="dropdown-divider"></div>
                                <?php if(isset($kelas)): ?>
                                <?php foreach($kelas as $k): ?>
                                <a class="dropdown-item" href="javascript:;" onclick="setKelas('<?= $k['nm_kelas'] ?>')"><?= $k['nm_kelas'] ?></a>
                                <?php endforeach;?>
                                <?php endif;?>
                            </div>
                            <input type="text" name="kelas" hidden="">
                        </li>
                        <li style="margin-left: 10px" class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#!" id="bystatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="icofont icofont-ui-tag"></i><span id="rombel"> By Rombel</span></a>
                            <div class="dropdown-menu" aria-labelledby="bystatus">
                                <a class="dropdown-item" href="javascript:;" onclick="setRombel('')">Semua</a>
                                <div class="dropdown-divider"></div>
                                <?php if(isset($rombel)): ?>
                                <?php foreach($rombel as $r): ?>
                                <a class="dropdown-item" href="javascript:;" onclick="setRombel('<?= $r['rombel_kelas'] ?>')"><?= $r['rombel_kelas'] ?></a>
                                <?php endforeach;?>
                                <?php endif;?>
                            </div>
                            <input type="text" name="rombel" hidden="">
                        </li>               
                        <li style="margin-left: 10px; " class="nav-item"><button style="cursor: pointer;" id="search" class="btn btn-primary">Apply</button></li>
                    </ul>
                </nav>
              </form>
            </div>
          </div>
          <ul class="nav nav-tabs md-tabs" role="tablist">
              <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#sudahAbsen" role="tab"> <i class="icofont icofont-check-circled"></i> Sudah Absen</a>
                  <div class="slide"></div>
              </li>
              <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#belumAbsen" role="tab"> <i class="icofont icofont-close-circled"></i> Belum Absen</a>
                  <div class="slide"></div>
              </li>
          </ul>
          <div class="card-block">
            <div class="data_table_main">
              <div class="tab-content">
                <div class="tab-pane active" id="sudahAbsen">
                  <h5 class="card-header-text"><?php echo 'List Siswa Sudah Absen Pada ' . $teksKelas; ?></h5>
                  <br>
                  <h5 class="card-header-text" style="padding-top: 10px; padding-bottom: 20px;"><?php echo $teksTanggal; ?></h5>
                  <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap" style ="width:100%;">
                    <thead>
                    <tr>
                      <th>Nama Siswa</th>
                      <th>Nomor Induk</th>
                      <th>Kelas</th>
                      <th>Keterangan</th>
                      <th>Waktu Absen</th>
                      <th>Aksi</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                      <th>Nama Siswa</th>
                      <th>Nomor Induk</th>
                      <th>Kelas</th>
                      <th>Keterangan</th>
                      <th>Waktu Absen</th>
                      <th></th>
                    </tr>
                    </tfoot>
                    <tbody>
                      <?php foreach ($absen as $r) { ?>
                      <tr>
                        <td><?php echo $r['nm_pengguna'] ?></td>
                        <td><?php echo $r['nomor_induk'] ?></td>
                        <td><?php if($r['nm_kelas'] == null) echo '<i>none</i>'; else echo $r['nm_kelas'] . $r['rombel_kelas']; ?></td>
                        <td>
                          <?php if ($r['kehadiran_verification'] == "1"): ?>
                              <span class="badge badge-success">  Sakit</span>
                          <?php elseif ($r['kehadiran_verification'] == "2"): ?>
                              <span class="badge badge-warning">  Izin</span>
                          <?php elseif ($r['kehadiran_verification'] == "0"): ?>
                              <span class="badge" style="color:#fff; background-color: #2196F3;">  Tepat Waktu</span>
                          <?php else: ?>
                              <span class="badge badge-danger">  Terlambat</span>
                          <?php endif; ?>
                        </td>
                        <td><?php echo substr($r['kehadiran_timestamp'], 11) ?></td>
                        <td><a href="<?= base_url("dashboard/result/izin-siswa/delete/").$r['kehadiran_id']; ?>" class="btn btn-danger" onclick="return confirm('Anda yakin menghapus Absen dari <?= $r['nm_pengguna'];?>');">Hapus</a> </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <div class="tab-pane" id="belumAbsen">
                  <form  method="POST" action="<?=base_url()?>dashboard/result/izin-siswa/update">
                  <input type="text" name="tanggal" value="<?= $tanggal?>" hidden="">
                  <div class="row">
                    <div class="col-sm-6">
                      <h5 class="card-header-text"><?php echo 'List Siswa Belum Absen Pada ' . $teksKelas; ?></h5>
                      <br>
                      <h5 class="card-header-text" style="padding-top: 10px; padding-bottom: 20px;"><?php echo $teksTanggal; ?></h5>
                    </div>
                    <div class="col-sm-6 text-right">
                      <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan Keterangan Absen Siswa</button>
                    </div>
                  </div>
                  <table id="simpletable2" class="table dt-responsive table-striped table-bordered nowrap" style ="width:100%;">
                    <thead>
                    <tr>
                      <th>Nama Siswa</th>
                      <th>Nomor Induk</th>
                      <th>Kelas</th>
                      <th>Keterangan</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                      <th>Nama Siswa</th>
                      <th>Nomor Induk</th>
                      <th>Kelas</th>
                      <th>Keterangan</th>
                    </tr>
                    </tfoot>
                    <tbody>
                      <?php foreach ($belum as $r) { ?>
                      <tr>
                        <td><?php echo $r['nm_pengguna'] ?></td>
                        <td><?php echo $r['nomor_induk'] ?></td>
                        <td><?php if($r['nm_kelas'] == null) echo '<i>none</i>'; else echo $r['nm_kelas'] . $r['rombel_kelas']; ?></td>
                        <td>
                          <select class="form-control" style="width:80%;" name="status[<?= $r['nomor_induk'] ?>]" >
                              <option value="none">Siswa Belum Absen</option>
                              <option value="Sudah">Sudah Absen</option>
                              <option value="Sakit">Sakit</option>
                              <option value="Izin">Izin</option>
                          </select>
                        </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function setKelas(kelas) {
    if(kelas != '')
    {
      $('input[name="kelas"]').val(kelas);
      $('#kelas').text(' Kelas: '+kelas);
    }
    else
      $('#kelas').text(' Kelas: Semua');
  }
  function setRombel(rombel) {
    if(rombel != '')
    {
      $('input[name="rombel"]').val(rombel);
      $('#rombel').text(' Rombel: ' + rombel);
    }
    else
      $('#rombel').text(' Rombel: Semua');
  }
</script>