<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Hasil Absensi</h4>
              </div>
          </div>
      </div>
      <div class="row">
        
        <?php $this->load->view('page/hasilAbsensi/sidebarHasilAbsensi'); ?>

        <div class="col-xl-9 col-md-8 col-sm-12 pull-xl-3 pull-md-4 filter-bar">
          <nav class="navbar navbar-light bg-faded m-b-30 p-10">
            <form method="GET" action="<?= base_url()?>/dashboard/result/absensi/guru/">
              <ul class="nav navbar-nav task-board-list">
                  <li class="nav-item active">
                      <a class="nav-link" href="javascript:;">Filter: <span class="sr-only">(current)</span></a>
                  </li>

                  <li class="nav-item">
                      <a class="nav-link" href="javascript:;" id="bydate"><i class="icofont icofont-clock-time"></i> By Date</a>
                  </li>
                  <li class="nav-item">
                    <div id="reportrange" class="f-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="glyphicon glyphicon-calendar icofont icofont-ui-calendar"></i> 
                    <span></span> <b class="caret"></b>
                    <input type="text" name="start" hidden="">
                    <input type="text" name="end" hidden="">
                    </div>
                  </li>
                  <!-- end of by date dropdown -->
                  <li style="margin-left: 10px" class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#!" id="bystatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icofont icofont-chart-histogram-alt"></i> By Status</a>
                      <div class="dropdown-menu" aria-labelledby="bystatus">
                          <a class="dropdown-item" href="javascript:;" onclick="setStatusAbsen('semua')">Semua</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="javascript:;" onclick="setStatusAbsen('terlambat')">Terlambat</a>
                          <a class="dropdown-item" href="javascript:;" onclick="setStatusAbsen('tepat')">Tepat waktu</a>
                      </div>
                      <input type="text" name="status" hidden="">
                  </li>
                  <!-- end of by status dropdown -->                
                  <li style="margin-left: 10px" class="nav-item"><button class="btn btn-primary" type="submit">Cari</button></li>
              </ul>
            </form>              
<!-- 
              <ul class="nav navbar-nav f-right">

                  <li class="nav-item">
                      <span class="m-r-15">View Mode: </span>
                      <button type="button" class="btn btn-primary waves-effect waves-light m-r-10" data-toggle="tooltip" data-placement="top" title="" data-original-title="list view">
                          <i class="icofont icofont-listine-dots"></i>
                      </button>
                      <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="grid view">
                          <i class="icofont icofont-table"></i>
                      </button>
                  </li>
              </ul> -->
          </nav>
          <nav class="navbar navbar-light bg-faded m-b-30 p-10">
            <ul class="nav navbar-nav task-board-list">
              
            </ul>
            <ul class="nav navbar-nav f-right">
              <form method="POST" action="<?php echo base_url() ?>dashboard/result/absensi/guru/upload" enctype="multipart/form-data">
                  <li class="nav-item">
                    <span class="m-r-15">Upload data absensi guru: </span>
                    <input type="file" name="userfile">
                    <button class="btn" type="submit">Upload</button>
                  </li>
              </form>              
            </ul>
          </nav>
          <!-- end of main navbar  -->
          <div class="row">

            <div class="col-sm-12">
              <div class="card">                
                <div class="card-block">
                  <div class="data_table_main">
                    <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                      <thead>
                      <tr>
                        <th>No. Akun</th>
                        <th>Nama</th>
                        <th>Tanggal</th>
                        <th>Jam</th>
                        <th>Jenis</th>
                      </tr>
                      </thead>
                      <tfoot>
                      <tr>
                        <th>No. Akun</th>
                        <th>Nama</th>
                        <th>Tanggal</th>
                        <th>Jam</th>
                        <th>Jenis</th>
                      </tr>
                      </tfoot>
                      <tbody>
                        <?php foreach ($absensi as $key) { ?>
                          <tr>
                            <td><?= $key->kode_absensi ?></td>
                            <td><?= $key->nama ?></td>
                            <td><?= date('d M Y', strtotime($key->timestamp)) ?></td>
                            <td><?= date('H:i', strtotime($key->timestamp)) ?></td>
                            <td><?= ($key->jenis == 'in')? 'Datang':'Pulang' ?></td>
                          </tr>
                        <?php } ?>                         
                      </tbody>
                    </table>
                  </div>            
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
  </div>
</div>