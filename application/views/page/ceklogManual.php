<!DOCTYPE html>
<html lang="en">
<head>
	<title>Akuhadir : tunjukkan kehadiranmu</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="Absen Akuhadir.com">
	<meta name="description" content="Sistem Absensi berbasis web dari akuhadir.com">
	<meta name="theme-color" content="#2196F3" />
	<link rel="apple-touch-icon" href="<?php echo base_url('images/icons/icon-152x152.png')?>">
	

	<script type="text/javascript" src="<?php echo base_url('assets/instascan.min.js') ?>" ></script>
	<!-- Favicon icon -->
	<link rel="shortcut icon" href="<?=base_url()?>/assets/favicon.png" type="image/x-icon">
	<link rel="icon" href="<?=base_url()?>/assets/favicon.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/plugins//sweetalert/css/sweetalert.css">
	<link rel="manifest" href="<?php echo base_url('assets/manifest.json') ?>">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link href="<?=base_url()?>/theme/ltr-horizontal/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/assets/icon/icofont/css/icofont.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/assets/plugins//waves/css/waves.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/assets/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/theme/ltr-horizontal/assets/css/color/color-1.min.css" id="color"/>

</head>
<body>

<section class="login p-fixed d-flex text-center bg-primary common-img-bg">
	<div class="container">
		<div class="row">
			<style type="text/css">
				
				@media (min-width: 768px) and (max-width: 1024px){
				  	.login-card{
						width: 100%; 
					}
					video{
						height: 250px;
					}
				  
				}
				@media(max-width: 766px){
				  	.login-card{
						width: 100%; 
					}
					video{
						height: 250px;
					}
				  
				}
				@media(max-width: 766px) and (orientation: landscape){
				  	.login-card{
						width: 70%; 
					}
				  
				}
				@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape){
				  	.login-card{
						width: 50%; 
					}
				  
				}
				@media(min-width: 1025px){
					#preview{
						width: 100%;
						height: 100%;
					}
					.login-card{
						width: 60%; 
					}
				}
			</style>
			<div class="col-sm-12">
				<div class="login-card card-block m-auto">
					<h1 class="text-center txt-primary">
						<b>Absensi Siswa</b>
					</h1>
					<h1 class="text-center txt-primary">
						Masukkan Nomor Induk Anda. Harus lengkap ya :)
					</h1>
					<form method="POST">
						<div class="form-group">
							<input class="form-control" style="width:100%; height:55px; font-size:25px;" autofocus required type="text" placeholder="Masukkan Nomor Induk disini" id="nomor" name="nomor" />
						</div>
						<div class="form-group">
							<button id="absen" type="submit" class="btn btn-lg btn-primary" style="padding: 6px 12px; cursor:pointer;">Absen!</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>/theme/ltr-horizontal/assets/plugins//sweetalert/js/sweetalert.js"></script>
<script src="<?php echo base_url() ?>assets/upup/upup.min.js"></script>

<script>
	UpUp.start({
		'cache-version': 'v2',
	  'content-url': '<?php echo site_url($this->uri->segment(1))?>',
	  'content': 'Anda butuh internet :)',
	  'service-worker-url': '/upup.sw.min.js'
	});
</script>
<script>
    $(document).ready(function(){
				
    	var success_sound = new Audio('<?php echo base_url("assets/success.wav") ?>');
		var error_sound = new Audio('<?php echo base_url("assets/error.wav") ?>');

	    $("#absen").click(function() {
	    	var nomor_induk = $("#nomor").val();  
	        $.ajax({
	            type : "POST",
	            url  : "<?php echo base_url('absen/insertManual')?>",
	            dataType : "JSON",
	            data : {nomor:nomor_induk},
	            success: function(){
	                swal({
					     title: "Sukses Melakukan Absen !",
					     text: "Selamat datang semoga harimu menyenangkan :)",
					     type: "success",
					     timer: 1000
				     })
	                success_sound.play();
	            },
	            error: function(){
	                swal({
					     title: "Gagal!",
					     text: "Mohon maaf anda tidak bisa absen :(",
					     type: "warning",
					     timer: 1000
				     })
	                error_sound.play();
	            }
	        });
	        return false;
	    });
	});
</script>



</body>
</html>