<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Tambah Jam Pelajaran</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
              <form method="post" action="<?=base_url()?>dashboard/waktu/update/<?= $data[0]->id_jam_pelajaran ?>">

               <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nama Jam Pelajaran</label>
                <input type="number" name="jam_ke" class="form-control" required="" placeholder="Misalnya : 1, 2, 3, dll" value="<?= $data[0]->jam_ke?>">
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Jam Mulai</label>
                <input type="time" name="jam_mulai" class="form-control" required="" value="<?= $data[0]->jam_mulai?>">
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Jam Selesai</label>
                <input type="time" name="jam_selesai" class="form-control" required="" value="<?= $data[0]->jam_selesai?>">
              </div>

              <div class="form-group">                
                <input type="submit" class="btn btn-primary" value="Simpan">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>