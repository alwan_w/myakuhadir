<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Daftar Jam Pelajaran</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header"><a href="<?= base_url () ?>dashboard/waktu/tambah" class="btn btn-primary">Tambah Jam</a></div>
          <div class="card-block">
            <div class="data_table_main">
              <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                <thead>
                <tr>
                  <th>Jam Ke-</th>
                  <!-- <th>Hari</th> -->
                  <th>Jam Mulai</th>
                  <th>Jam Selesai</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                  <th>Jam Ke-</th>
                  <!-- <th>Hari</th> -->
                  <th>Jam Mulai</th>
                  <th>Jam Selesai</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($data as $d) { ?>
                    <tr>                  
                      <td> <?= $d->jam_ke ?></td>
                      <!-- <td><?= $d->hari ?></td> -->
                      <td><?= $d->jam_mulai ?></td>
                      <td><?= $d->jam_selesai ?></td>
                      <td>
                        <a href="<?= base_url() ?>dashboard/waktu/<?= $d->id_jam_pelajaran ?>">
                          <button type="button" class="btn btn-primary btn-icon waves-effect waves-light">
                           <i class="icofont icofont-ui-note"></i>
                          </button>
                        </a>
                        <a href="<?= base_url() ?>dashboard/waktu/hapus/<?= $d->id_jam_pelajaran ?>">
                          <button type="button" class="btn btn-danger btn-icon waves-effect waves-light" onclick="return confirm('Are you sure you want to delete Jam Ke-<?= $d->jam_ke ?>?');">
                           <i class="icofont icofont-ui-delete"></i>
                          </button>
                        </a>
                      </td>
                    </tr>
                <?php } ?>                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>