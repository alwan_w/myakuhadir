<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Daftar Kelas (<?= $tahunAjaran?>)</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <a href="<?= base_url()?>dashboard/kelas/tambah" class="btn btn-primary">Tambah Kelas</a>
            <a href="" class="btn btn-primary" data-toggle="modal" data-target="#uploadModal">Upload Excel Data Kelas</a>
            <a href="<?=base_url()?>template/Format_Upload_Kelas.xlsx" class="btn btn-secondary" style="background-color:#555; border-color:#555">Download Format Excel Kelas</a>
          </div>
          <div class="card-block">
            <div class="data_table_main">
              <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Kelas</th>
                  <th>Wali Kelas</th>
                  <th>Ketua Kelas</th>
                  <th>Jumlah Siswa</th>
                  <th>Edit</th> 
                  <th>Add Siswa</th>                 
                </tr>
                </thead>
                <tfoot>
                <tr>
                  <th></th>
                  <th>Kelas</th>
                  <th>Wali Kelas</th>
                  <th>Ketua Kelas</th>
                  <th>Jumlah Siswa</th>
                  <th></th>
                  <th></th>
                </tr>
                </tfoot>
                <tbody>
                <?php $i = 1; foreach ($data as $d) { ?>
                    <tr>                  
                      <td> <?= $i++ ?></td>
                      <td><?= $d->nm_kelas . $d->rombel_kelas ?></td>
                      <td><?= $d->nm_wali_kelas ?></td>
                      <td><?= $d->nm_ketua_kelas ?></td>
                      <td><?= $d->jumlah_siswa ?></td>
                      <td>
                        <a href="<?= base_url() ?>dashboard/kelas/<?= $d->id_kelas ?>">
                          <button type="button" class="btn btn-primary btn-icon waves-effect waves-light">
                           <i class="icofont icofont-ui-edit"></i>
                          </button>
                        </a>
                        <!--a href="<?= base_url() ?>dashboard/kelas/hapus/<?php //echo $d->id_kelas ?>">
                          <button type="button" class="btn btn-danger btn-icon waves-effect waves-light" onclick="return confirm('Are you sure you want to delete Kelas <?= $d->nm_kelas . $d->rombel_kelas ?>?');">
                           <i class="icofont icofont-ui-delete"></i>
                          </button>
                        </a-->
                      </td>
                      <td><a href="<?= base_url() ?>dashboard/kelas/tambah-siswa/<?= $d->id_kelas ?>">
                          <button type="button" class="btn btn-success btn-icon waves-effect waves-light">
                           <i class="icofont icofont-ui-note"></i>
                          </button>
                        </a></td>
                    </tr>
                <?php } ?>                
                </tbody>
              </table>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="card-block button-list">
  <div class="modal fade modal-flex" id="uploadModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Upload Excel Data Kelas <br><?= $tahunAjaran?></h4>
        </div>

        <div class="modal-body">
          <form method="POST" action="<?=base_url()?>dashboard/kelas/upload" enctype="multipart/form-data">

          <p>Format file excel yang dapat digunakan : <a href="<?=base_url()?>template/Format_Upload_Kelas.xlsx">Download</a></p>
          <div class="form-group row">
            <!-- <label for="file" class="col-md-2 col-form-label form-control-label">File input</label> -->
            <div class="col-md-12">
              <label for="file" class="custom-file" style="width: 100%">
              <input type="file" id="file" class="form-control" name="userfile">
              </label>
            </div>
          </div>


        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Batalkan</button>
          <button type="submit" class="btn btn-primary waves-effect waves-light ">Upload</button>
        </div>
      </form>

    </div>
  </div>
</div>