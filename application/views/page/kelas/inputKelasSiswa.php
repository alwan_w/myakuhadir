<div class="content-wrapper">
  <style type="text/css">
    .readonly-inp{
      border: none;
      background-color: transparent;
    }
  </style>
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0 text-center">
              <div class="main-header">
                  <h4>Tambah Siswa di kelas <?= $data[0]->nm_kelas . $data[0]->rombel_kelas . ' ('. $data[0]->tahun_ajaran .')'?></h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
            <div class="form-group row">
                <div class="col-md-12">
                  <label for="exampleInputPassword1" class="form-control-label">Pilih Siswa</label>
                </div>
                <div class="col-8 col-md-8">
                  <select id="siswa" class="form-control js-example-basic-single" name="state">
                    <option value="null" disabled selected>--daftar siswa--</option>
                    <?php foreach ($read as $r) { ?>
                    <option value='{"induk":"<?php echo $r['nomor_induk'] ?>", "name":"<?php echo $r['nm_pengguna'] ?>", "id":"<?php echo $r['id_pengguna'] ?>"}' ><?php echo $r['nm_pengguna'] . ' (' . $r['nomor_induk'] . ')'?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-4 col-md-4">
                  <button id="addSiswa" class="btn btn-primary">+</button>
                </div>
              </div>
            <form method="post" action="<?=base_url()?>KelasController/inpKelasSiswa/<?= $data[0]->id_kelas ?>">
              <div class="form-group">                
                 <table id="tbl-siswa" class="table dt-responsive table-striped table-bordered nowrap">
                  <thead>
                    <tr>
                      <th>Nomor Induk Siswa</th>
                      <th>Nama Siswa</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="list-siswa" >
                    <?php foreach ($siswa as $s) { ?>
                    <tr>
                      <td><input type="text" hidden="" class="readonly-inp" readonly value="<?php echo $s['id_pengguna'] ?>" name="id[]"> <?php echo $s['nomor_induk'] ?> </td>
                      <input type="text" hidden="" readonly value="<?php echo $s['id_kelas_siswa'] ?>" name="idKlS[]">
                      <td>
                        <?php echo $s['nm_pengguna']; ?>
                        <?php if($s['id_pengguna'] == $data[0]->id_ketua_kelas):?>
                          <span class="badge badge-primary"> Ketua Kelas</span>
                        <?php endif;?>
                      </td>
                      <td>
                        <a href="<?php echo base_url('dashboard/kelas/pilih-ketua/'.$s['id_pengguna'].'?kelas='.$data[0]->id_kelas) ?>" class="btn btn-primary  <?php if($s['id_pengguna'] == $data[0]->id_ketua_kelas) echo 'disabled';?>">Jadikan Ketua</a>
                        <a href="<?php echo base_url('dashboard/kelas/remove-siswa/'.$s['id_pengguna'].'?kelas='.$data[0]->id_kelas) ?>" class="btn btn-danger" onclick="return confirm('Anda yakin menghapus <?= $s['nm_pengguna'];?> dari kelas <?= $data[0]->nm_kelas . $data[0]->rombel_kelas?>?');">Hapus</a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                <input type="submit" class="btn btn-primary" value="Simpan">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>