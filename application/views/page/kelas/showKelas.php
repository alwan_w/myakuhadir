<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0 text-center">
              <div class="main-header">
                  <h4>Edit Kelas</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
            <form method="post" action="<?=base_url()?>dashboard/kelas/update/<?= $data[0]->id_kelas ?>">

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1" class="form-control-label">Nama Kelas</label>
                    <input type="text" class="form-control" name="nm_kelas" placeholder="Misal: 7" required="" value="<?= $data[0]->nm_kelas?>">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1" class="form-control-label">Rombel Kelas</label>
                    <input type="text" class="form-control" name="rombel_kelas" placeholder="Misal: A" required="" value="<?= $data[0]->rombel_kelas?>">
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nama Wali Kelas</label>
                <select name="wali_kelas" class="form-control">
                    <option value="">-- Pilih Nama Guru --</option>
                  <?php foreach ($guru as $r) { ?>
                    <option value="<?= $r->id_pengguna ?>" <?php if($r->id_pengguna == $data[0]->wali_kelas) echo"selected"; ?>><?= $r->nm_pengguna ?></option>
                  <?php } ?>                  
                </select>
              </div>

              <div class="form-group">                
                <input type="submit" class="btn btn-primary" value="Simpan">
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>