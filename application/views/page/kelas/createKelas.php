<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0 text-center">
              <div class="main-header">
                  <h4>Tambah Kelas</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
            <form method="POST" action="<?=base_url()?>dashboard/kelas/tambah">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1" class="form-control-label">Nama Kelas</label>
                    <input type="text" class="form-control" name="nm_kelas" placeholder="Misal: 7" required="">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1" class="form-control-label">Rombel Kelas</label>
                    <input type="text" class="form-control" name="rombel_kelas" placeholder="Misal: A" required="">
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nama Wali Kelas</label>
                <select name="wali_kelas" class="form-control">
                    <option value="">-- Pilih Nama Guru --</option>
                  <?php foreach ($guru as $r) { ?>
                    <option value="<?= $r->id_pengguna ?>"><?= $r->nm_pengguna ?></option>
                  <?php } ?>                  
                </select>
              </div>

              <div class="form-group">                
                <input type="submit" class="btn btn-primary" value="Buat Kelas">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>