<?php 
$alamat = '<i>-kosong-</i>';
$email = $alamat;
$telepon = $alamat;
$website = $alamat;
if($sekolah['alamat_sekolah'] != '')
  $alamat = $sekolah['alamat_sekolah'];
if($sekolah['email_sekolah'] != '')
  $email = '<a href="mailto:'.$sekolah['email_sekolah'].'">'.$sekolah['email_sekolah'].'</a>';
if($sekolah['telepon_sekolah'] != '')
  $telepon = $sekolah['telepon_sekolah'];
if($sekolah['website_sekolah'] != '')
  $website = '<a href="'.$sekolah['website_sekolah'].'">'.$sekolah['website_sekolah'].'</a>';
 ?>


<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Data Informasi Sekolah</h4>
              </div>
          </div>
      </div>
      <div class="row">
        <div class="col-xl-3 col-lg-4">
          <div class="card faq-left">
            <div class="social-profile">
              <img class="img-fluid w-100" src="<?= base_url('assets/logos/'.$sekolah['logo_sekolah']) ?>" alt="Logo Sekolah">
            </div>
          </div>
        </div>
      <div class="col-xl-9 col-lg-8">
        <div class="card">          
          <div class="card-block">
            <div class="view-info">
              <div class="row">
                <div class="col-lg-12">
                  <div class="general-info">
                    <div class="row">
                      <div class="col-lg-12 col-xl-6">
                        <table class="table m-0">
                          <tbody>
                          <tr>
                              <th scope="row">Nama Sekolah</th>
                              <td><?=$sekolah['nama_sekolah']?></td>
                          </tr>
                          <tr>
                              <th scope="row">Alamat Sekolah</th>
                              <td><?=$alamat?></td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                      <!-- end of table col-lg-6 -->

                      <div class="col-lg-12 col-xl-6">
                        <table class="table">
                          <tbody>
                          <tr>
                              <th scope="row">Email Sekolah</th>
                              <td><?=$email?></td>
                          </tr>
                          <tr>
                              <th scope="row">Telepon Sekolah</th>
                              <td><?=$telepon?></td>
                          </tr>
                          <tr>
                              <th scope="row">Website Sekolah</th>
                              <td><?=$website?></td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                        <!-- end of table col-lg-6 -->
                    </div>
                      <!-- end of row -->
                  </div>
                  <!-- end of general info -->
                </div>
                <!-- end of col-lg-12 -->
              </div>
              <!-- end of row -->
            <div class="row">
              <div class="col-lg-12 text-center">
                <a class="btn btn-primary" href="<?= base_url('dashboard/sekolah/edit') ?>">Edit Informasi Sekolah</a>
              </div>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
