<?php 
 ?>
<style>
.alert p
{
 color:white;
}
</style>

<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0 text-center">
              <div class="main-header">
                  <h4>Edit Informasi Sekolah</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
            <?php if ($this->session->flashdata('pesan') != '') : ?>
                <div class="col-sm-12">
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-top: -10px;">&times;</a>
                    <p><strong><?php echo $this->session->flashdata('pesan'); ?></strong></p>
                  </div>
                </div>
              <?php endif; ?>
            <form method="POST" action="" enctype="multipart/form-data">
              <div class="form-group">
                <label class="form-control-label">Nama Sekolah</label>
                <input hidden="" type="text" name="id" value="<?=$sekolah['id_sekolah']?>">
                <input type="text" class="form-control" value="<?=$sekolah['nama_sekolah']?>" name="nama" placeholder="Nama Sekolah" required="">
              </div>

              <div class="form-group">
                <label class="form-control-label">Ganti Logo Sekolah</label>
                <input name="logo" type="file" class="form-control-file" id="fotoSekolah" multiple="" accept="image/*">
              </div>
              <div class="form-group">
                <label class="form-control-label">Alamat Sekolah</label>
                <input type="text" value="<?=$sekolah['alamat_sekolah']?>" class="form-control" name="alamat" placeholder="Alamat">
              </div>

              <div class="form-group">
                <label class="form-control-label">Email Sekolah</label>
                <input type="email" class="form-control" value="<?=$sekolah['email_sekolah']?>" name="email" placeholder="Email Sekolah">
              </div>
              <div class="form-group">
                <label class="form-control-label">Telepon Sekolah</label>
                <input type="text" class="form-control" value="<?=$sekolah['telepon_sekolah']?>" name="telepon" placeholder="No Telepon Sekolah">
              </div>
              <div class="form-group">
                <label class="form-control-label">Website Sekolah</label>
                <input value="<?=$sekolah['website_sekolah']?>" type="text" class="form-control" name="nomor" placeholder="Website Sekolah">
              </div>
              <div class="row text-center">
                <div class="col-md-12">
                  <div class="form-group">                
                    <input type="submit" class="btn btn-primary" value="Update">
                  </div>
                </div>
              </div>
            </form>
          </div>
          <!--a href="#" onclick="fun()" >Klik</a-->
        </div>
      </div>
    </div>
  </div>
</div>
