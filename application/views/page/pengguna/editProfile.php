<?php
$color = array('default', 'lightblue', 'orange', 'purple', 'red', 'green'); 
$name = array('Default', 'Light Blue', 'Orange', 'Purple', 'Red', 'Green');
 ?>
<style>
.alert p
{
 color:white;
}
</style>

<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0 text-center">
              <div class="main-header">
                  <h4>Edit Profile</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
            <?php if ($this->session->flashdata('pesan') != '') : ?>
                <div class="col-sm-12">
                  <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-top: -10px;">&times;</a>
                    <p><strong><?php echo $this->session->flashdata('pesan'); ?></strong></p>
                  </div>
                </div>
              <?php endif; ?>
            <form method="POST" action="" enctype="multipart/form-data">
              <div class="form-group">
                <label class="form-control-label">Nama</label>
                <input hidden="" type="text" name="id" value="<?=$pengguna['id_pengguna']?>">
                <input type="text" class="form-control" value="<?=$pengguna['nm_pengguna']?>" name="nama" placeholder="Nama Lengkap Anda" required="">
              </div>

              <div class="form-group">
                <label class="form-control-label">Ganti Foto Profile</label>
                <input name="foto" type="file" class="form-control-file" id="fotoProfile" multiple="" accept="image/*">
              </div>
              <div class="form-group">
                <label class="form-control-label">Alamat</label>
                <input type="text" value="<?=$pengguna['alamat']?>" class="form-control" name="alamat" placeholder="Alamat Anda">
              </div>

              <div class="form-group">
                <label class="form-control-label">Email</label>
                <input type="email" class="form-control" value="<?=$pengguna['email']?>" name="email" placeholder="Email Anda" required>
              </div>
              <div class="form-group">
                <label class="form-control-label">Nomor Telepon</label>
                <input type="text" class="form-control" value="<?=$pengguna['no_hp']?>" name="telepon" placeholder="Nomor Telepon Anda">
              </div>
              <div class="form-group">
                <label class="form-control-label">Ganti Tema Aplikasi</label>
                <select name="tema" class="form-control">
                  <?php for($i=0; $i<count($color); $i++):?>
                  <option value="<?=$color[$i]?>" <?php if($pengguna['tema_pengguna'] == $color[$i]) echo 'selected';?> onclick="changeTheme('<?=$color[$i]?>')"><?=$name[$i]?></option>
                  <?php endfor;?>
                </select>
              </div>
              <div class="row text-center">
                <div class="col-md-12">
                  <div class="form-group">                
                    <input type="submit" class="btn btn-primary" value="Update">
                  </div>
                </div>
              </div>
            </form>
          </div>
          <!--a href="#" onclick="fun()" >Klik</a-->
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function changeTheme(color) {
    var change = "<?=base_url()?>//theme/ltr-horizontal/assets/css/color/" + color + ".css";
    return $("#color").attr("href", change), !1
  }
</script>
