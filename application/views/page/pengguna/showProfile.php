<?php 
$foto = 'default.png';
$tema = 'default';
$alamat = '<i>-kosong-</i>';
$email = $alamat;
$hp = $alamat;
if($pengguna['alamat'] != '')
  $alamat = $pengguna['alamat'];
if($pengguna['email'] != '')
  $email = '<a href="mailto:'.$pengguna['email'].'">'.$pengguna['email'].'</a>';
if($pengguna['no_hp'] != '')
  $hp = $pengguna['no_hp'];
if(!is_null($pengguna['tema_pengguna']) && $pengguna['tema_pengguna'] != '')
  $tema = $pengguna['tema_pengguna'];
if(!is_null($pengguna['foto_pengguna']) && $pengguna['foto_pengguna'] != '')
  $foto = $pengguna['foto_pengguna'];
 ?>


<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Profile Anda</h4>
              </div>
          </div>
      </div>
      <div class="row">
        <div class="col-xl-3 col-lg-4">
          <div class="card faq-left">
            <div class="social-profile">
              <img class="img-fluid w-100" src="<?= base_url('assets/photos/'.$foto) ?>" alt="Logo Sekolah">
            </div>
          </div>
        </div>
      <div class="col-xl-9 col-lg-8">
        <div class="card">          
          <div class="card-block">
            <div class="view-info">
              <div class="row">
                <div class="col-lg-12">
                  <div class="general-info">
                    <div class="row">
                      <div class="col-lg-12 col-xl-6">
                        <table class="table m-0">
                          <tbody>
                          <tr>
                              <th scope="row">Nama</th>
                              <td><?=$pengguna['nm_pengguna']?></td>
                          </tr>
                          <tr>
                              <th scope="row">Alamat</th>
                              <td><?=$alamat?></td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                      <!-- end of table col-lg-6 -->

                      <div class="col-lg-12 col-xl-6">
                        <table class="table">
                          <tbody>
                          <tr>
                              <th scope="row">Email</th>
                              <td><?=$email?></td>
                          </tr>
                          <tr>
                              <th scope="row">No. Telepon</th>
                              <td><?=$hp?></td>
                          </tr>
                          <tr>
                              <th scope="row">Tema Aplikasi</th>
                              <td><?=$tema?></td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                        <!-- end of table col-lg-6 -->
                    </div>
                      <!-- end of row -->
                  </div>
                  <!-- end of general info -->
                </div>
                <!-- end of col-lg-12 -->
              </div>
              <!-- end of row -->
            <div class="row">
              <div class="col-lg-12 text-center">
                <a class="btn btn-primary" href="<?= base_url('dashboard/profile/edit') ?>">Edit Profile</a>
              </div>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
