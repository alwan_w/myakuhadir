<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0  text-center">
              <div class="main-header">
                  <h4>Buat Jadwal Guru dan Pendamping Ekstrakurikuler</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
            <form method="POST" action="<?=base_url()?>dashboard/jadwal/tambah">

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nama Guru</label>
                <select name="id_guru" class="form-control">
                    <option value="">-- Pilih Nama Guru --</option>
                  <?php foreach ($guru as $r) { ?>
                    <option value="<?= $r->id_pengguna ?>"><?= $r->nm_pengguna ?></option>
                  <?php } ?>                  
                </select>
              </div>
        
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nama Mata Pelajaran atau Ekstrakurikuler</label>
                <select name="id_mata_pelajaran" class="form-control">
                    <option value="">-- Pilih Mata Pelajaran  atau Ekstrakurikuler --</option>
                  <?php foreach ($mapel as $r) { ?>
                    <option value="<?= $r->id_mata_pelajaran ?>"><?= $r->nm_mata_pelajaran ?></option>
                  <?php } ?>                  
                </select>
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Kelas</label>
                <!-- <small>Kosongi Bagian Ini Jika Memilih Mata Pelajaran</small> -->
                <select name="id_kelas" class="form-control">
                    <option value="">-- Pilih Kelas --</option>
                  <?php foreach ($kelas as $r) { ?>
                    <option value="<?= $r->id_kelas ?>"><?= $r->nm_kelas ?></option>
                  <?php } ?>                  
                </select>
                <small id="emailHelp" class="form-text text-muted">Pilih hanya jika jenis subjek Mata Pelajaran</small>
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Hari</label>
                <select name="hari" class="form-control">
                    <option value="">-- Pilih Hari --</option>
                    <option value="1">Senin</option>                
                    <option value="2">Selasa</option>                
                    <option value="3">Rabu</option>                
                    <option value="4">Kamis</option>                
                    <option value="5">Jumat</option>                
                    <option value="6">Sabtu</option>                
                </select>
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Jam Mulai</label>
                <select name="jam_mulai" class="form-control">
                    <option value="">-- Pilih Jam Pelajaran --</option>
                  <?php foreach ($jam as $r) { ?>
                    <option value="<?= $r->id_jam_pelajaran ?>">Jam Pelajaran- <?= $r->jam_ke ?>(<?= $r->jam_mulai ?>-<?= $r->jam_selesai ?>)</option>
                  <?php } ?>                  
                </select>
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Jam Selesai
                  <small>Jika pelajaran hanya berlangsung 1 jam pelajaran, jam selesai diisi sama dengan jam mulai</small>
                </label>
                <select name="jam_selesai" class="form-control">
                    <option value="">-- Pilih Jam Pelajaran --</option>
                  <?php foreach ($jam as $r) { ?>
                    <option value="<?= $r->id_jam_pelajaran ?>">Jam Pelajaran- <?= $r->jam_ke ?>(<?= $r->jam_mulai ?>-<?= $r->jam_selesai ?>)</option>
                  <?php } ?>                  
                </select>
              </div>

              <div class="form-group">                
                <input type="submit" class="btn btn-primary" value="Simpan">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>