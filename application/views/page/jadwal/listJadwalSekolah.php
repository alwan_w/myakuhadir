<div class="content-wrapper">
  <!-- Container-fluid starts -->
  <!-- Main content starts -->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 p-0">
        <div class="main-header">
          <h4>Daftar Jadwal Sekolah (<?= $tahunAjaran?>)</h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <a href="<?= base_url () ?>dashboard/jadwal" class="btn btn-info">Lihat Jadwal Guru</a>
          </div>
          <div class="card-block">
            <div class="data_table_main">
              <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Jadwal</th>
                    <th>Tanggal Dibuat</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th></th>
                    <th>Nama Jadwal</th>
                    <th>Tanggal Dibuat</th>
                    <th>Status</th>
                    <th></th>
                  </tr>
                </tfoot>
                <tbody>
                <?php $i=1;foreach ($data as $d) { ?>
                  <tr>
                    <td><?= $i++; ?></td>                  
                    <td><?= $d->nama_jadwal?></td>
                    <td>
                      <?php 
                      if(isset($d->created_at))
                      {
                        $date = explode(" ", $d->created_at);
                        echo humanDateFormat($date[0]) . ' ' . $date[1];
                      }  
                      ?>
                    </td>
                    <td><?php if($d->status == 1) echo 'Aktif'; else echo '-';?></td>
                    <td>
                      <a href="<?= base_url() ?>dashboard/jadwal-sekolah/setAktif/<?= $d->id_jadwal_sekolah ?>" class="btn btn-primary <?php if($d->status == 1) echo 'disabled';?>">Pilih Jadwal Aktif</a>
                   </td>
                 </tr>
               <?php } ?>                        
             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
