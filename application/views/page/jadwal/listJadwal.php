<div class="content-wrapper">
  <!-- Container-fluid starts -->
  <!-- Main content starts -->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 p-0">
        <div class="main-header">
          <h4>Daftar Jadwal Guru <?='('.$tahunAjaran.')'?></h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <!-- <a href="<?= base_url () ?>dashboard/jadwal/tambah" class="btn btn-primary">Tambah Jadwal</a> -->
            <a href="" class="btn btn-primary" data-toggle="modal" data-target="#uploadModal">Upload Excel Jadwal Mata Pelajaran</a>
            <a href="<?=base_url()?>template/Format_Upload_Jadwal_Pelajaran.xlsx" class="btn btn-secondary" style="background-color:#555; border-color:#555">Download Format Excel Jadwal Mata Pelajaran</a>
            <a href="<?= base_url () ?>dashboard/jadwal-sekolah" class="btn btn-info">Ganti Jadwal Sekolah</a>
          </div>
          <div class="card-block">
            <div class="data_table_main">
              <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Guru</th>
                    <th>Mata Pelajaran</th>
                    <th>Kelas</th>
                    <th>Hari</th>
                    <th>Jam Pelajaran</th>
                    <!--th>Edit</th-->
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th></th>
                    <th>Nama Guru</th>
                    <th>Mata Pelajaran</th>
                    <th>Kelas</th>
                    <th>Hari</th>
                    <th>Jam Pelajaran</th>
                    <!--th></th-->
                  </tr>
                </tfoot>
                <tbody>
                <?php $i = 1; foreach ($data as $d) { ?>
                  <tr>    
                    <td><?= $i++;?></td>              
                    <td> <?= $d->nm_pengguna?></td>
                    <td><?= $d->nm_mata_pelajaran?></td>
                    <td><?= $d->nm_kelas . $d->rombel_kelas  ?></td>
                    <td> 
                      <?php if ($d->hari === '1'): ?>
                          Senin
                      <?php elseif ($d->hari === '2'): ?>
                          Selasa
                      <?php elseif ($d->hari === '3'): ?>
                          Rabu
                      <?php elseif ($d->hari === '4'): ?>
                          Kamis
                      <?php elseif ($d->hari === '5'): ?>
                          Jumat
                      <?php elseif ($d->hari === '6'): ?>
                          Sabtu
                      <?php else: ?>
                          Minggu
                      <?php endif; ?>
                    </td>
                    <td>
                      Periode : <?= $d->periode?> <br>
                      Dimulai Pukul   : <?= $d->jam_mulai ?> <br>
                      Berakhir Pukul  : <?= $d->jam_selesai ?> 
                    </td>
                    <!--td>
                      <a href="<?= base_url() ?>dashboard/jadwal/<?= $d->id_jadwal_guru ?>">
                        <button type="button" class="btn btn-primary btn-icon waves-effect waves-light">
                         <i class="icofont icofont-ui-edit"></i>
                       </button>
                      </a>
                      <!--a href="<?= base_url() ?>dashboard/jadwal/hapus/<?php //echo $d->id_jadwal_guru ?>">
                        <button type="button" class="btn btn-danger btn-icon waves-effect waves-light" onclick="return confirm('Are you sure you want to this jadwal?');">
                         <i class="icofont icofont-ui-delete"></i>
                        </button>
                      </a-->
                   <!--/td-->
                 </tr>
               <?php } ?>                        
             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>

<div class="card-block button-list">
  <div class="modal fade modal-flex" id="uploadModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Upload Excel Jadwal Pelajaran <br><?= $tahunAjaran?></h4>
        </div>

        <div class="modal-body">
          <form method="POST" action="<?=base_url()?>dashboard/jadwal/upload" enctype="multipart/form-data">

          
          <div class="form-group row">
            <!-- <label for="file" class="col-md-2 col-form-label form-control-label">File input</label> -->
            <div class="col-md-12">
              <label for="nama" class="custom-file" style="width: 100%">Nama Jadwal
              <input type="text" class="form-control" name="nama" required>
              </label>
            </div>
            <div class="col-md-12" style="margin-top: 15px;">
              <p>Format file excel yang dapat digunakan : <a href="<?=base_url()?>template/Format_Upload_Jadwal_Pelajaran.xlsx">Download</a></p>
              <label for="file" class="custom-file" style="width: 100%">
              <input type="file" id="file" class="form-control" name="userfile" required>
              </label>
            </div>
          </div>


        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Batalkan</button>
          <button type="submit" class="btn btn-primary waves-effect waves-light ">Upload</button>
        </div>
      </form>

    </div>
  </div>
</div>