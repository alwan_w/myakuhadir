<div class="content-wrapper">
  <!-- Container-fluid starts -->
  <!-- Main content starts -->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 p-0">
        <div class="main-header">
          <h4>Daftar Jadwal Guru Piket (<?= $tahunAjaran?>)</h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <a href="" class="btn btn-primary" data-toggle="modal" data-target="#uploadModal">Upload Excel Jadwal Guru Piket</a>
            <a href="<?=base_url()?>template/Format_Upload_Jadwal_Guru_Piket.xlsx" class="btn btn-secondary" style="background-color:#555; border-color:#555">Download Format Excel Jadwal Guru Piket</a>
          </div>
          <div class="card-block">
            <div class="data_table_main">
              <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Hari</th>
                    <th>Nama Guru</th>
                    <th>Nomor Induk</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th></th>
                    <th>Hari</th>
                    <th>Nama Guru</th>
                    <th>Nomor Induk</th>
                    <th></th>
                  </tr>
                </tfoot>
                <tbody>
                <?php $i=1;foreach ($data as $d) { ?>
                  <tr>
                    <td><?= $i++; ?></td>                  
                    <td>
                      <?php if ($d->hari === '1'): ?>
                          Senin
                      <?php elseif ($d->hari === '2'): ?>
                          Selasa
                      <?php elseif ($d->hari === '3'): ?>
                          Rabu
                      <?php elseif ($d->hari === '4'): ?>
                          Kamis
                      <?php elseif ($d->hari === '5'): ?>
                          Jumat
                      <?php elseif ($d->hari === '6'): ?>
                          Sabtu
                      <?php else: ?>
                          Minggu
                      <?php endif; ?>
                    </td>
                    <td><?= $d->nm_pengguna ?></td>
                    <td><?= $d->nomor_induk ?></td>
                    <td>
                      <a href="<?= base_url() ?>dashboard/jadwal-piket/hapus/<?= $d->id_jadwal_piket ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete Jadwal Piket dari <?= $d->nm_pengguna . ' (' . $d->nomor_induk . ')'?>?');">Hapus</a>
                   </td>
                 </tr>
               <?php } ?>                        
             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>

<div class="card-block button-list">
  <div class="modal fade modal-flex" id="uploadModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Upload Excel Jadwal Guru Piket <br><?= $tahunAjaran?></h4>
        </div>

        <div class="modal-body">
          <form method="POST" action="<?=base_url()?>dashboard/jadwal-piket/upload" enctype="multipart/form-data">

          <p>Format file excel yang dapat digunakan : <a href="<?=base_url()?>template/Format_Upload_Jadwal_Guru_Piket.xlsx">Download</a></p>
          <div class="form-group row">
            <!-- <label for="file" class="col-md-2 col-form-label form-control-label">File input</label> -->
            <div class="col-md-12">
              <label for="file" class="custom-file" style="width: 100%">
              <input type="file" id="file" class="form-control" name="userfile" required>
              </label>
            </div>
          </div>


        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Batalkan</button>
          <button type="submit" class="btn btn-primary waves-effect waves-light ">Upload</button>
        </div>
      </form>

    </div>
  </div>
</div>
