<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Buat Jadwal Masuk Siswa</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
            <form method="POST" action="<?=base_url()?>dashboard/setting/jadwal-masuk/tambah">

              <div class="form-group">
                <label for="" class="form-control-label">Judul Jadwal Masuk</label>
                <input type="text" class="form-control" maxlength="50" name="jm_judul" required placeholder="Misalnya: Reguler, Khusus Puasa, dll">
              </div>
        
              <div class="form-group">
                <label for="" class="form-control-label">Jam Masuk</label>
                <input type="time" name="jm_masuk" class="form-control" required="" value="07:00">
              </div>

              <div class="form-group">                
                <input type="submit" class="btn btn-primary" value="Simpan">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>