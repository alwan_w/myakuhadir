<style>
  .boxJadwal p{
    font-size: 15px;
    color:black;
    margin-top:10px;
  }
  .boxJadwal h4{
    font-size: 20px;
    margin-bottom:unset;
  }
  .boxJadwal{
    width: 100%;
    height: 100px;
    background-color: #dbdbdb;
    padding: 20px;
  }
  .boxJadwal:hover{
    box-shadow: 0px 0px 10px 0px;
  }
  .aktif{
    background-color: #2196F3 !important;
    color: white!important;
  }
  .aktif p{
    color: white;
  }
  .aktif .editable{
    border-bottom:2px solid white;
  }
  .editable{
    all: unset;
    border-bottom: 2px solid #2196F3;
  }
  .notEdit{
    all:unset;
  }

</style>
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 p-0">
        <div class="main-header">
          <h4>Setting Jadwal Masuk Siswa</h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <a href="<?= base_url () ?>dashboard/setting/jadwal-masuk/tambah" class="btn btn-primary">Tambah Jadwal</a>
          </div>
          <span style="display:none" id="idJM" ></span>
          <div class="card-block">
            <div class="row">
            <?php $i=1; $len=count($data); foreach ($data as $d) { ?>
              <div class="col-md-3">
                <div class="boxJadwal <?php if($d['status']==1){echo 'aktif';} ?>">
                  <span style="display:none" id="idJadwal" ><?php echo $d['id_jm'] ?></span>
                  <h4 id="jmJudul">Jadwal masuk <?php echo $d['jm_judul'] ?></h2>
                  <?php 
                    echo '<small>double click save untuk edit jam masuk</small> <br> <input onclick="getElementById('."'idJM'".').innerHTML = '.$d['id_jm'].'" id="editJD'.$d['id_jm'].'" type="text" class="editable" value="'.$d['jm_masuk'].'"><a class="saveJam" style="color:white;" href="#"> save</a>';
                  ?>
                 
                  
                </div>
              </div>
              <?php $i++;} ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
