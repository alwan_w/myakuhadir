<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0  text-center">
              <div class="main-header">
                  <h4>Tambah Guru / Karyawan</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
            <form method="POST" action="<?=base_url()?>dashboard/civitas/tambah">
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nama Guru / Karyawan</label>
                <input type="text" class="form-control" name="nama" placeholder="Nama" required="">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nomor Induk</label>
                <input type="text" class="form-control" name="nomor" placeholder="Nomor induk" required="">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Alamat</label>
                <input type="text" class="form-control" name="alamat" placeholder="Alamat">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nomor Handphone</label>
                <input type="text" class="form-control" name="phone" placeholder="No HP">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Email</label>
                <input type="email" class="form-control" name="email" placeholder="Email" required="">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" required="">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Peran</label>
                <select name="role" class="form-control">
                  <?php foreach ($role as $r) { ?>
                    <?php if($r->id_role != 1):?>
                    <option value="<?= $r->id_role ?>" <?php if($r->id_role == 3) echo 'selected';?>><?= $r->nm_role ?></option>
                    <?php endif;?>
                  <?php } ?>                  
                </select>
              </div>
              <div class="form-group">                
                <input type="submit" class="btn btn-primary" value="Simpan">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>