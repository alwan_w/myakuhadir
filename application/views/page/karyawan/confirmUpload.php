<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Konfirmasi Upload Data Excel Guru & Karyawan</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <a href="<?= base_url () ?>dashboard/civitas/tambah" class="btn btn-primary">Tambah Guru & Karyawan</a>
          </div>
          <ul class="nav nav-tabs md-tabs" role="tablist">
              <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tambahan" role="tab"> <i class="icofont icofont-ui-add"></i> Tambahan</a>
                  <div class="slide"></div>
              </li>
              <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#perubahan" role="tab"> <i class="icofont icofont-checked"></i> Perubahan</a>
                  <div class="slide"></div>
              </li>
          </ul>
          <div class="card-block">
            <div class="data_table_main">
              <div class="tab-content">
                <div class="tab-pane active" id="sudahAbsen">
                  <h5 class="card-header-text"><?php echo 'List Siswa Sudah Absen Pada ' . $teksKelas; ?></h5>
                  <br>
                  <h5 class="card-header-text" style="padding-top: 10px; padding-bottom: 20px;"><?php echo $teksTanggal; ?></h5>
                  <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap" style ="width:100%;">
                    <thead>
                    <tr>
                      <th>Nama Siswa</th>
                      <th>Nomor Induk</th>
                      <th>Kelas</th>
                      <th>Keterangan</th>
                      <th>Waktu Absen</th>
                      <th>Aksi</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                      <th>Nama Siswa</th>
                      <th>Nomor Induk</th>
                      <th>Kelas</th>
                      <th>Keterangan</th>
                      <th>Waktu Absen</th>
                      <th></th>
                    </tr>
                    </tfoot>
                    <tbody>
                      <?php foreach ($absen as $r) { ?>
                      <tr>
                        <td><?php echo $r['nm_pengguna'] ?></td>
                        <td><?php echo $r['nomor_induk'] ?></td>
                        <td><?php if($r['nm_kelas'] == null) echo '<i>none</i>'; else echo $r['nm_kelas']; ?></td>
                        <td>
                          <?php if ($r['kehadiran_verification'] == "1"): ?>
                              <span class="badge badge-warning">  Sakit</span>
                          <?php elseif ($r['kehadiran_verification'] == "2"): ?>
                              <span class="badge badge-warning">  Izin</span>
                          <?php elseif ($r['kehadiran_verification'] == "0"): ?>
                              <span class="badge badge-primary">  Tepat Waktu</span>
                          <?php else: ?>
                              <span class="badge badge-danger">  Terlambat</span>
                          <?php endif; ?>
                        </td>
                        <td><?php echo substr($r['kehadiran_timestamp'], 11) ?></td>
                        <td><a href="<?= base_url("dashboard/result/izin-siswa/delete/").$r['kehadiran_id']; ?>" class="btn btn-danger" onclick="return confirm('Anda yakin menghapus Absen dari <?= $r['nm_pengguna'];?>');">Hapus</a> </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <div class="tab-pane" id="belumAbsen">
                  <form  method="POST" action="<?=base_url()?>dashboard/result/izin-siswa/update">
                  <input type="text" name="tanggal" value="<?= $tanggal?>" hidden="">
                  <div class="row">
                    <div class="col-sm-6">
                      <h5 class="card-header-text"><?php echo 'List Siswa Belum Absen Pada ' . $teksKelas; ?></h5>
                      <br>
                      <h5 class="card-header-text" style="padding-top: 10px; padding-bottom: 20px;"><?php echo $teksTanggal; ?></h5>
                    </div>
                    <div class="col-sm-6 text-right">
                      <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan Keterangan Absen Siswa</button>
                    </div>
                  </div>
                  <table id="simpletable2" class="table dt-responsive table-striped table-bordered nowrap" style ="width:100%;">
                    <thead>
                    <tr>
                      <th>Nama Siswa</th>
                      <th>Nomor Induk</th>
                      <th>Kelas</th>
                      <th>Keterangan</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                      <th>Nama Siswa</th>
                      <th>Nomor Induk</th>
                      <th>Kelas</th>
                      <th>Keterangan</th>
                    </tr>
                    </tfoot>
                    <tbody>
                      <?php foreach ($belum as $r) { ?>
                      <tr>
                        <td><?php echo $r['nm_pengguna'] ?></td>
                        <td><?php echo $r['nomor_induk'] ?></td>
                        <td><?php if($r['nm_kelas'] == null) echo '<i>none</i>'; else echo $r['nm_kelas']; ?></td>
                        <td>
                          <select class="form-control" name="status[<?= $r['nomor_induk'] ?>]" >
                              <option value="none">Siswa Belum Absen</option>
                              <option value="Sudah">Sudah Absen</option>
                              <option value="Sakit">Sakit</option>
                              <option value="Izin">Izin</option>
                          </select>
                        </td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>