<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Daftar Guru & Karyawan</h4>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <a href="<?= base_url () ?>dashboard/civitas/tambah" class="btn btn-primary">Tambah Guru & Karyawan</a>
            <a href="" class="btn btn-primary" data-toggle="modal" data-target="#uploadModal">Upload Excel Data Guru & Karyawan</a>
            <a href="<?=base_url()?>template/Format_Upload_Civitas.xlsx" class="btn btn-secondary" style="background-color:#555; border-color:#555">Download Format Excel Guru & Karyawan</a>
            <a href="<?php echo base_url('dashboard/civitas/download-all-qr') ?>" class="btn btn-info" >Download Semua QR Code</a> 
          </div>
          <div class="card-block">
            <div class="data_table_main">
              <table id="simpletable" class="table dt-responsive table-striped table-bordered nowrap">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>Nomor Induk</th>
                  <th>Email</th>
                  <th>Peran</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                  <th></th>
                  <th>Nama</th>
                  <th>Nomor Induk</th>
                  <th>Email</th>
                  <th>Peran</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                </tfoot>
                <tbody>
                <?php $i = 1;foreach ($data as $d) { ?>
                    <tr>   
                      <td><?= $i++?></td>               
                      <td><?= $d->nm_pengguna ?></td>
                      <td><?= $d->nomor_induk ?></td>
                      <td><?= $d->email ?></td>
                      <td><?= ucfirst($d->nm_role) ?></td>
                      <td>
                        <?php if($d->status == 0)
                        echo 'Tidak Aktif';
                        else
                          echo 'Aktif';
                        ?>
                      </td>
                      <td><a href="<?= base_url() ?>dashboard/civitas/<?= $d->id_pengguna ?>" class="btn btn-primary" >Edit</a> <a href="<?php echo base_url('dashboard/civitas/qr/'.$d->id_pengguna) ?>" class="btn btn-info" >QR</a> 
                      </td>
                      <!--td>
                        <a href="<?= base_url() ?>dashboard/civitas/<?= $d->id_pengguna ?>">
                          <button type="button" class="btn btn-primary btn-icon waves-effect waves-light">
                           <i class="icofont icofont-ui-edit"></i>
                          </button>
                        </a>

                        <!--a href="<php //echo base_url() ?>dashboard/civitas/hapus/<?= $d->id_pengguna ?>">
                          <button type="button" class="btn btn-danger btn-icon waves-effect waves-light" onclick="return confirm('Are you sure you want to delete <?= ucfirst($d->nm_role) . ' ' . $d->nm_pengguna . ' (' . $d->nomor_induk . ')'?>?');">
                           <i class="icofont icofont-ui-delete"></i>
                          </button>
                        </a>
                      </td-->
                    </tr>
                <?php } ?>                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="card-block button-list">
  <div class="modal fade modal-flex" id="uploadModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Upload Excel Data Guru dan Karyawan</h4>
        </div>

        <div class="modal-body">
          <form method="POST" action="<?=base_url()?>dashboard/civitas/upload" enctype="multipart/form-data">

           <p>Format file excel yang dapat digunakan : <a href="<?=base_url()?>template/Format_Upload_Civitas.xlsx">Download</a></p>
          <div class="form-group row">
            <!-- <label for="file" class="col-md-2 col-form-label form-control-label">File input</label> -->
            <div class="col-md-12">
              <label for="file" class="custom-file" style="width: 100%">
              <input type="file" id="file" class="form-control" name="userfile">
              </label>
            </div>
          </div>


        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Batalkan</button>
          <button type="submit" class="btn btn-primary waves-effect waves-light ">Upload</button>
        </div>
      </form>

    </div>
  </div>
</div>