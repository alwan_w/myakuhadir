<?php 
  if (isset($read)) {
    foreach ($read as $r) {
      $nama = $r['nm_pengguna'];
      $nomorInduk = $r['nomor_induk'];
      $alamat = $r['alamat'];
      $phone = $r['no_hp'];
      $mail = $r['email'];
      $id = $r['id_pengguna'];
    }
  }
  else{
    $nama = '';
    $nomorInduk ='';
      $alamat = '';
      $phone = '';
      $mail = '';
      $id = '';
  }
 ?>


<div class="content-wrapper">
  <!-- Container-fluid starts -->
        <!-- Main content starts -->
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0 text-center">
              <div class="main-header">
                <?php if(isset($read)): ?>
                  <h4>Edit Siswa</h4>
                <?php else: ?>
                  <h4>Tambah Siswa</h4>
                <?php endif?>
              </div>
          </div>
      </div>
      <div class="row">
      <div class="col-sm-8 mx-auto">
        <div class="card">          
          <div class="card-block">
            <form method="POST" action="<?=base_url()?>SiswaController/store">
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nama Siswa</label>
                <input type="text" class="form-control" value="<?php echo $nama ?>" name="nama" placeholder="Nama" required="">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nomor Induk</label>
                <input hidden="" type="text" name="id" value="<?php echo $id ?>">
                <input value="<?php echo $nomorInduk ?>" type="text" class="form-control" name="nomor" placeholder="Nomor induk" required="">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Alamat</label>
                <input type="text" value="<?php echo $alamat ?>" class="form-control" name="alamat" placeholder="Alamat">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Nomor Telepon</label>
                <input type="text" class="form-control" data-prefix="+62" onkeypress="return hanyaAngka(event)" value="<?php echo $phone ?>" name="phone" placeholder="No HP">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1" class="form-control-label">Email Orang Tua <span style="font-size: 11.5px; color: purple">(digunakan untuk mengirim laporan absensi)</span></label>
                <input type="email" class="form-control" value="<?php echo $mail ?>" name="email" placeholder="Email">
              </div>
              <div class="form-group">                
                <input type="submit" class="btn btn-primary" value="Simpan">
              </div>
            </form>
          </div>
          <!--a href="#" onclick="fun()" >Klik</a-->
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function fun() {
    alert($('#member_tlp').val())
  }
  function hanyaAngka(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
  return true;
}
</script>