
<section class="login p-fixed d-flex text-center bg-primary common-img-bg">
	<!-- Container-fluid starts -->
	<div class="container-fluid">
		<div class="row">

			<div class="col-sm-12">
				<div class="login-card card-block m-auto">
					<form class="md-float-material">
						
						<h3 class="text-center txt-primary">
							Sign In ...
						</h3>
						
						<div class="row">
							
							<div class="col-sm-12 sign-in-with"><h6 class="text">Sign in You In Progress ... </h6></div>
							<div class="col-sm-4"></div>
							<div class="col-sm-4">
								
								<div style="display: none;" class="g-signin2" data-onsuccess="onSignIn"></div>
								<div class="preloader3 loader-block">
									<div class="circ1 bg-primary"></div>
									<div class="circ2 bg-primary"></div>
									<div class="circ3 bg-primary"></div>
									<div class="circ4 bg-primary"></div>
								</div>
								
							</div>
							<div class="col-sm-4"></div>
						</div>
					
					</form>
					<!-- end of form -->
				</div>
				<!-- end of login-card -->
			</div>
			<!-- end of col-sm-12 -->
		</div>
		<!-- end of row -->
	</div>
	<!-- end of container-fluid -->
</section>

<script>
  function onSignIn(googleUser) {
	var profile = googleUser.getBasicProfile();
	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	console.log('Name: ' + profile.getName());
	console.log('Image URL: ' + profile.getImageUrl());
	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
	
	var id_token = googleUser.getAuthResponse().id_token;
	
	signOut();
	
  }
  
  function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
    
    window.location='/auth/sign_in_done';
  }
</script>
