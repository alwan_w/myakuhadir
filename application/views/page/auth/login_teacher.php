<section class="login p-fixed d-flex text-center bg-primary common-img-bg">
	<!-- Container-fluid starts -->
	<div class="container-fluid">
		<div class="row">

			<div class="col-sm-12">
				<div class="login-card card-block m-auto">
					<form class="md-float-material">
						<!-- <div class="text-center">
							<img src="assets/images/logo-blue.png">
						</div> -->
						<!-- <h6 class="text-center txt-primary">
							Silahkan login menggunakan NIP dan Password anda
						</h6> -->
						<div class="md-input-wrapper">
							<input type="text" class="md-form-control" id="allownumericwithoutdecimal" />
							<label>Nomor Induk Pegawai</label>
						</div>
						<div class="md-input-wrapper">
							<input type="password" class="md-form-control" />
							<label>Password</label>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
									<label class="input-checkbox checkbox-primary">
										<input type="checkbox" id="checkbox">
										<span class="checkbox"></span>
									</label>
									<div class="captions">Remember Me</div>
								</div>
							</div>
							<div class="col-sm-6 text-right forgot-phone">
								<a href="forgot-password.html" class="f-w-600"> Forget Password?</a>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<button type="button" class="btn btn-primary btn-md btn-block waves-effect text-center">LOGIN</button>
							</div>
						</div>
						<!-- </div> -->
					</form>
					<!-- end of form -->
				</div>
				<!-- end of login-card -->
			</div>
			<!-- end of col-sm-12 -->
		</div>
		<!-- end of row -->
	</div>
	<!-- end of container-fluid -->
</section>
<script type="text/javascript">
	$("#allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
</script>