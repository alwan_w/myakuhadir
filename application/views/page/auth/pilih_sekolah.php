<!DOCTYPE html>
<html lang="en">

<head>
	<title>Akuhadir.com | Pilih Sekolah</title>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="Phoenixcoded">
	<meta name="keywords" content=", Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
	<meta name="author" content="Phoenixcoded">

	<!-- Favicon icon -->
	<link rel="shortcut icon" href="<?= base_url() ?>//theme/ltr-horizontal/assets/images/favicon.png" type="image/x-icon">
	<link rel="icon" href="<?= base_url() ?>//theme/ltr-horizontal/assets/images/favicon.ico" type="image/x-icon">

	<!-- Google font-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="<?= base_url() ?>//theme/ltr-horizontal/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!--ico Fonts-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>//theme/ltr-horizontal/assets/icon/icofont/css/icofont.css">

	<!-- Required Fremwork -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>//theme/ltr-horizontal/../bower_components/bootstrap/css/bootstrap.min.css">

	<!-- Style.css -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>//theme/ltr-horizontal/assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style.css') ?>">

	<!-- Responsive.css-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>//theme/ltr-horizontal/assets/css/responsive.css">
	<link rel="manifest" href="<?php echo base_url('assets/manifest.json') ?>">
	<!--color css-->
	<!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>//theme/ltr-horizontal/assets/css/color/color-1.min.css" id="color"/> -->

	<style>
		* {
			box-sizing: border-box;
		}

		#myInput {
			background-image: url('/css/searchicon.png');
			background-position: 10px 12px;
			background-repeat: no-repeat;
			width: 100%;
			font-size: 16px;
			padding: 12px 20px 12px 40px;
			border: 1px solid #ddd;
			margin-bottom: 12px;
		}

		#myUL {
			list-style-type: none;
			padding: 0;
			margin: 0;
		}

		#myUL li a {
			border: 1px solid #ddd;
			margin-top: -1px;
			/* Prevent double borders */
			background-color: #f6f6f6;
			padding: 12px;
			text-decoration: none;
			font-size: 12px;
			color: black;
			display: block
		}

		#myUL li a:hover:not(.header) {
			background-color: #eee;
		}
	</style>
</head>

<body>
	<section class="login p-fixed d-flex text-center bg-primary common-img-bg">
		<div class="container" style="min-height:360px;">
			<div class="col-md-12 text-center">
				<h3 class="section-heading" style="color:#000;margin-top:50px;">Cari sekolahmu:</h3>
				<div class="subheading-divider"></div>
			</div>
			<div class="row" style="color:#000;margin-top:50px;">
				<div class="col-md-12 text-center">
					<div class="col-12 col-md-8" style="margin:auto;">
						<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Ketik nama sekolah.." title="Type in a name">
						<ul id="myUL">
							<?php foreach($data as $d): ?>
							<li style="display:none;"><a href="<?= base_url('pilihsekolah?mode=' . $mode . '&sekolah=' . $d->id_sekolah)?>"><?= $d->nama_sekolah ?></a></li>
							<?php endforeach;?>
						</ul>
						<input type="hidden" name="sekolah" value="">
					</div>
				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/../bower_components/jquery/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/../bower_components/jquery-ui/js/jquery-ui.min.js"></script>
	<!-- tether.js -->
	<script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/../bower_components/popper.js/js/popper.min.js"></script>
	<!-- waves effects.js -->
	<script src="<?= base_url() ?>//theme/ltr-horizontal/assets/plugins//waves/js/waves.min.js"></script>
	<!-- Required Framework -->
	<script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/../bower_components/bootstrap/js/bootstrap.min.js"></script>
	<!-- Custom js -->
	<script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/assets/pages/elements.js"></script>
	<!-- <script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/assets/js/common-pages.min.js"></script> -->
	<script src="<?php echo base_url() ?>assets/upup/upup.min.js"></script>

	<script>
		UpUp.start({
			'cache-version': 'v2',
			'content-url': '<?php echo site_url($this->uri->segment(1)) ?>',
			'content': 'Anda butuh internet :)',
			'service-worker-url': '/upup.sw.min.js'
		});
	</script>

	<script>
		function myFunction() {
			var input, filter, ul, li, a, i, txtValue;
			input = document.getElementById("myInput");
			filter = input.value.toUpperCase();
			ul = document.getElementById("myUL");
			li = ul.getElementsByTagName("li");
			limit = 0;
			for (i = 0; i < li.length; i++) {
				a = li[i].getElementsByTagName("a")[0];
				txtValue = a.textContent || a.innerText;
				if(input.value=="")
					li[i].style.display = "none";
				else if (txtValue.toUpperCase().indexOf(filter) > -1) {
					li[i].style.display = "";
					limit++;
				} else {
					li[i].style.display = "none";
				}
				/*if(limit>4)
					break;*/
			}
		}
	</script>

</body>

</html>