<!DOCTYPE html>
<html lang="en">

<head>
	<title>Akuhadir.com - Pilih Mode Pengguna</title>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="Phoenixcoded">
	<meta name="keywords" content=", Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
	<meta name="author" content="Phoenixcoded">

	<!-- Favicon icon -->
	<link rel="shortcut icon" href="<?= base_url() ?>//theme/ltr-horizontal/assets/images/favicon.png" type="image/x-icon">
	<link rel="icon" href="<?= base_url() ?>//theme/ltr-horizontal/assets/images/favicon.ico" type="image/x-icon">

	<!-- Google font-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="<?= base_url() ?>//theme/ltr-horizontal/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!--ico Fonts-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>//theme/ltr-horizontal/assets/icon/icofont/css/icofont.css">

	<!-- Required Fremwork -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>//theme/ltr-horizontal/../bower_components/bootstrap/css/bootstrap.min.css">

	<!-- Style.css -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>//theme/ltr-horizontal/assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style.css') ?>">

	<!-- Responsive.css-->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>//theme/ltr-horizontal/assets/css/responsive.css">
	<link rel="manifest" href="<?php echo base_url('assets/manifest.json') ?>">
	<!--color css-->
	<!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>//theme/ltr-horizontal/assets/css/color/color-1.min.css" id="color"/> -->
	<style>
		.btn {
			cursor: pointer;
		}
	</style>

</head>

<body>
	<section class="login p-fixed d-flex text-center bg-primary common-img-bg">
		<div class="container" style="min-height:360px;">
			<div class="col-md-12 text-center">
				<h3 class="section-heading" style="color:#000;margin-top:50px;">Pilih mode ke Akuhadir sebagai:</h3>
				<div class="subheading-divider"></div>
			</div>
			<div class="row" style="color:#000;margin-top:50px;">
				<div class="col-6 col-md-4">
					<div>
						<a href="<?= base_url('pilihsekolah?mode=siswa') ?>">
							<img style="width:100px;margin-bottom:20px;" src="<?php echo base_url('assets/images/icons/student.svg') ?>">
						</a>
					</div>
					<div>
						<a href="<?= base_url('pilihsekolah?mode=siswa') ?>">
							<button type="button" class="btn" style="background:#01AFEF;">Siswa</button>
						</a>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div>
						<a href="<?= base_url('registersekolah') ?>">
							<img style="width:100px;margin-bottom:20px;" src="<?php echo base_url('assets/images/icons/professor.svg') ?>">
						</a>
					</div>
					<div>
						<a href="<?= base_url('registersekolah') ?>">
							<button type="button" class="btn" style="background:#FDC300;">Kepala Sekolah</button>
						</a>
					</div>
				</div>
				<div class="col-6 col-md-4">
					<div>
						<a href="<?= base_url('pilihsekolah?mode=guru') ?>">
							<img style="width:100px;margin-bottom:20px;" src="<?php echo base_url('assets/images/icons/parent.svg') ?>">
						</a>
					</div>
					<div>
						<a href="<?= base_url('pilihsekolah?mode=guru') ?>">
							<button type="button" class="btn" style="background:#ACD968;">Guru</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/../bower_components/jquery/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/../bower_components/jquery-ui/js/jquery-ui.min.js"></script>
	<!-- tether.js -->
	<script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/../bower_components/popper.js/js/popper.min.js"></script>
	<!-- waves effects.js -->
	<script src="<?= base_url() ?>//theme/ltr-horizontal/assets/plugins//waves/js/waves.min.js"></script>
	<!-- Required Framework -->
	<script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/../bower_components/bootstrap/js/bootstrap.min.js"></script>
	<!-- Custom js -->
	<script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/assets/pages/elements.js"></script>
	<!-- <script type="text/javascript" src="<?= base_url() ?>//theme/ltr-horizontal/assets/js/common-pages.min.js"></script> -->
	<script src="<?php echo base_url() ?>assets/upup/upup.min.js"></script>

	<script>
		UpUp.start({
			'cache-version': 'v2',
			'content-url': '<?php echo site_url($this->uri->segment(1)) ?>',
			'content': 'Anda butuh internet :)',
			'service-worker-url': '/upup.sw.min.js'
		});
	</script>

</body>

</html>