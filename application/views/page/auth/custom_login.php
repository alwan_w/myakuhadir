<!DOCTYPE html>
<html lang="en">
<head>
	<title>Akuhadir.com | Masuk</title>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="description" content="Phoenixcoded">
	<meta name="keywords"
		  content=", Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
	<meta name="author" content="Phoenixcoded">

	<!-- Favicon icon -->
	<link rel="shortcut icon" href="<?=base_url()?>//theme/ltr-horizontal/assets/images/favicon.png" type="image/x-icon">
	<link rel="icon" href="<?=base_url()?>//theme/ltr-horizontal/assets/images/favicon.ico" type="image/x-icon">

	<!-- Google font-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="<?=base_url()?>//theme/ltr-horizontal/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!--ico Fonts-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/icon/icofont/css/icofont.css">

	<!-- Required Fremwork -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/../bower_components/bootstrap/css/bootstrap.min.css">

	<!-- Style.css -->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/css/main.css">

	<!-- Responsive.css-->
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/css/responsive.css">
	<link rel="manifest" href="<?php echo base_url('assets/manifest.json') ?>">
	<!--color css-->
	<!-- <link rel="stylesheet" type="text/css" href="<?=base_url()?>//theme/ltr-horizontal/assets/css/color/color-1.min.css" id="color"/> -->

	<meta name="google-signin-client_id" content="503366119834-h9k6g3d4hgpefe4ke2epe8pua7iml7oc.apps.googleusercontent.com">
	<style>
		.btn-theme {
		  color: #fff;
		  background-color: #3a74d1;
		  border-color: #3a74d1;
		  margin: 4px;
		  transition: .2s ease-in;
		}
		.btn-theme:hover,
		.btn-theme:focus,
		.btn-theme:active,
		.btn-theme.active,
		.open .dropdown-toggle.btn-theme {
		  color: #fff;
		  background-color: #00b3fe;
		  border-color: #00b3fe;
		}
		.btn-google {
			padding: 8px 12px;
			margin-bottom: 0;
			font-size: 14px;
			box-shadow:0 2px 4px 0 rgba(0,0,0,.25);
			font-family: 'Raleway', sans-serif;
			font-weight: normal;
			background-color:white;
			border-color:#757575;
			color:#757575;
		}
		.btn-google:hover {
			background-color:white;
			color:#757575;
			border-color:#4285f4;
		}
	</style>

</head>
<body>
<section class="login p-fixed d-flex text-center bg-primary common-img-bg">
	<!-- Container-fluid starts -->
	<div class="container-fluid">
		<div class="row">

			<div class="col-sm-12">
				<div class="login-card card-block m-auto">
					<form class="md-float-material" action="" method="POST">
						<h3 class="text-center txt-primary">
							Silahkan masuk menggunakan Email anda
						</h3>
						<?php if ($this->session->flashdata('message') != ''): ?>
                            <div class="col-sm-12">
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-top: -10px">&times;</a>
                                    <strong><?php echo $this->session->flashdata('message') ; ?></strong>
                                </div>
                            </div>
                        <?php endif; ?>
						<!-- <label class="label label-lg label-danger">Large Label</label> -->
						<div class="md-input-wrapper">
							<input type="text" name="user" class="md-form-control" required />
							<label>Email Anda</label>
						</div>
						<div class="md-input-wrapper">
							<input type="password" name="password" class="md-form-control" required />
							<label>Password</label>
						</div>
						<!-- <div class="row">
							<div class="col-sm-6 col-xs-12">
								<div class="rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
									<label class="input-checkbox checkbox-primary">
										<input type="checkbox" id="checkbox">
										<span class="checkbox"></span>
									</label>
									<div class="captions">Remember Me</div>

								</div>
							</div>
							<div class="col-sm-6 col-xs-12 forgot-phone text-right">
								<a href="forgot-password.html" class="text-right f-w-600"> Forget Password?</a>
							</div>
						</div> -->
						<div class="row">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
									Masuk
								</button>
								<a href="<?php echo $googleLoginURL; ?>">
									<button type="button" class="btn btn-theme btn-google">
										<img width="20px" src="<?php echo base_url('assets/images/G-logo.png')?>"><b> Login with Google</b>
									</button>
								</a>
							</div>
						</div>
						<!-- <div class="card-footer"> -->
						<!-- <div class="col-sm-12 col-xs-12 text-center">
							<span class="text-muted">Don't have an account?</span>
							<a href="register2.html" class="f-w-600 p-l-5">Sign up Now</a>
						</div> -->

						<!-- </div> -->
					</form>
					<!-- end of form -->
				</div>
				<!-- end of login-card -->
			</div>
			<!-- end of col-sm-12 -->
		</div>
		<!-- end of row -->
	</div>
	<!-- end of container-fluid -->
</section>

<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 9]>
<div class="ie-warning">
	<h1>Warning!!</h1>
	<p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
	<div class="iew-container">
		<ul class="iew-download">
			<li>
				<a href="http://www.google.com/chrome/">
					<img src="assets/images/browser/chrome.png" alt="Chrome">
					<div>Chrome</div>
				</a>
			</li>
			<li>
				<a href="https://www.mozilla.org/en-US/firefox/new/">
					<img src="assets/images/browser/firefox.png" alt="Firefox">
					<div>Firefox</div>
				</a>
			</li>
			<li>
				<a href="http://www.opera.com">
					<img src="assets/images/browser/opera.png" alt="Opera">
					<div>Opera</div>
				</a>
			</li>
			<li>
				<a href="https://www.apple.com/safari/">
					<img src="assets/images/browser/safari.png" alt="Safari">
					<div>Safari</div>
				</a>
			</li>
			<li>
				<a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
					<img src="assets/images/browser/ie.png" alt="">
					<div>IE (9 & above)</div>
				</a>
			</li>
		</ul>
	</div>
	<p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jqurey -->
<script type="text/javascript" src="<?=base_url()?>//theme/ltr-horizontal/../bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>//theme/ltr-horizontal/../bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<!-- tether.js -->
<script type="text/javascript" src="<?=base_url()?>//theme/ltr-horizontal/../bower_components/popper.js/js/popper.min.js"></script>
<!-- waves effects.js -->
<script src="<?=base_url()?>//theme/ltr-horizontal/assets/plugins//waves/js/waves.min.js"></script>
<!-- Required Framework -->
<script type="text/javascript" src="<?=base_url()?>//theme/ltr-horizontal/../bower_components/bootstrap/js/bootstrap.min.js"></script>
<!-- Custom js -->
<script type="text/javascript" src="<?=base_url()?>//theme/ltr-horizontal/assets/pages/elements.js"></script>
<!-- <script type="text/javascript" src="<?=base_url()?>//theme/ltr-horizontal/assets/js/common-pages.min.js"></script> -->
<script src="<?php echo base_url() ?>assets/upup/upup.min.js"></script>

<script>
    UpUp.start({
		'cache-version': 'v2',
	  'content-url': '<?php echo site_url($this->uri->segment(1))?>',
	  'content': 'Anda butuh internet :)',
	  'service-worker-url': '/upup.sw.min.js'
    });
  </script>

</body>
</html>