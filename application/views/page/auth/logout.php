
<section class="login p-fixed d-flex text-center bg-primary common-img-bg">
	<!-- Container-fluid starts -->
	<div class="container-fluid">
		<div class="row">

			<div class="col-sm-12">
				<div class="login-card card-block m-auto">
					<form class="md-float-material">
						
						<h3 class="text-center txt-primary">
							Sign Out ? 
						</h3>
						
						<div class="row">
							
							<div class="col-sm-12 sign-in-with"><h6 class="text">Sign out Now</h6></div>
							<div class="col-sm-12">
								<div class="social-login text-center">
									<div style="display: none;" class="g-signin2" data-onsuccess="onSignIn"></div>
									<a class="btn btn-danger" onclick="signOut();">Sign out</a>
								</div>
							</div>
						</div>
					
					</form>
					<!-- end of form -->
				</div>
				<!-- end of login-card -->
			</div>
			<!-- end of col-sm-12 -->
		</div>
		<!-- end of row -->
	</div>
	<!-- end of container-fluid -->
</section>

<script>
  function onSignIn(googleUser) {
	var profile = googleUser.getBasicProfile();
	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	console.log('Name: ' + profile.getName());
	console.log('Image URL: ' + profile.getImageUrl());
	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
	
	var id_token = googleUser.getAuthResponse().id_token;
	
	
  }
  function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
    
    window.location='/auth/sign_out';
  }
</script>
