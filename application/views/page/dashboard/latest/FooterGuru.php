<!-- variable -->
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script src="https://unpkg.com/highcharts/highcharts.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vuetify@1.x/dist/vuetify.js"></script>
<!-- vue-highcharts should be load after Highcharts -->
<script src="https://unpkg.com/vue-highcharts/dist/vue-highcharts.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="<?php echo base_url() ?>assets/vuejs/chartComponent/vue-rangedate-picker.min.js"></script>
<script type="text/javascript">
	var BASEURL = "<?= base_url()?>";
	Vue.use(VueHighcharts);
	Vue.use(VueRangedatePicker)
</script>

<script src="<?php echo base_url() ?>assets/vuejs/chartComponent/pie-chart-guru-waktu.vue.js"></script>
<script src="<?php echo base_url() ?>assets/vuejs/chartComponent/bar-chart.vue.js"></script>
<script src="<?php echo base_url() ?>assets/vuejs/chartComponent/piechart-legend.vue.js"></script>
<script src="<?php echo base_url() ?>assets/vuejs/chartComponent/line-chart.vue.js"></script>
<script src="<?php echo base_url() ?>assets/vuejs/latest/DashboardGuru.vue.js"></script>