<div class="content-wrapper" id="guruapp">
<v-app>
  <v-content>
    <div class="container-fluid">
      <div class="contaner">
        <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Dashboard</h4><small> Data Kehadiran Guru</small>                  
                  <p class="text-muted m-b-20"><i class="icofont icofont-tags"></i><span>&nbsp;Dashboard ini akan menampilkan data kehadiran guru yang bersumber dari fingerprint dan data jurnal kelas.</span></p>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="" style="padding: 16px;">
            <a href="<?php echo base_url() ?>dashboard/?type=siswa" style="vertical-align: middle;" class="btn btn-primary"><i class="icon-arrow-left-circle"></i> Ke Dashboard Siswa</a>
          </div>
          <div class="ml-auto mr-3" style=";">         
            <div style="display: inline-block;">
              <strong>Filter</strong> &nbsp;
            </div>
            <div style="display: inline-block;">
              <vue-rangedate-picker @selected="onDateSelected" i18n="ID" righttoleft="true" :initRange="selectedDate"></vue-rangedate-picker>
            </div>            
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">              
              <div class="card-block" style="border-bottom: 1px #bdc3c7 solid;">                
                <div class="float-left">
                  <h5><i class="icofont icofont-ui-home" style="color: #6c7a89"></i>&nbsp;&nbsp;Ringkasan Kehadiran Guru</h5>
                </div>
                <div class="float-right ml-auto mr-3">                  
                  <small class="badge badge-pill badge-primary">Periode Data: <strong>{{datestart | indonesianDate}} - {{dateend | indonesianDate}}</strong></small>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3" style="border-bottom: 1px solid #bdc3c7">
                  <div class="card-block" style="border-right: 1px solid #bdc3c7;">
                    <div style="text-align: center;">
                      <h1 class="display-3">{{topBar.kehadiran}} <small style="font-size: 25%">kali</small></h1>
                      <p>Jumlah Kehadiran</p>
                    </div>                    
                  </div>
                </div>
                <div class="col-md-3" style="border-bottom: 1px solid #bdc3c7">
                  <div class="card-block" style="border-right: 1px solid #bdc3c7">
                    <div style="text-align: center;">
                      <h1 class="display-3">{{topBar.keterlambatan}} <small style="font-size: 25%">kali</small></h1>
                      <p><i class="icon-clock"></i> Jumlah keterlambatan</p>
                    </div>                    
                  </div>
                </div>
                <div class="col-md-3" style="border-bottom: 1px solid #bdc3c7; border-right: 1px solid #bdc3c7">
                  <div class="row">
                    <div class="col-sm-6" style="border-right: 1px solid #bdc3c7">
                      <div class="card-block">
                        <div style="text-align: center; padding-top: 40px">
                          <h1 class="display-5">{{ringkasanPerorang.min}} <small style="font-size: 55%">kali</small></h1>
                          <p>Keterlambatan Min 5</p>
                        </div>                    
                      </div>
                    </div>
                    <div class="col-sm-6" style="border-right: 1px solid #bdc3c7">
                      <div class="card-block">
                        <div style="text-align: center; padding-top: 40px">
                          <h1 class="display-5">{{ringkasanPerorang.plus}} <small style="font-size: 55%">kali</small></h1>
                          <p>Keterlambatan Plus 5</p>
                        </div>                    
                      </div>
                    </div>
                  </div>                  
                </div>
                <div class="col-md-3" style="border-bottom: 1px solid #bdc3c7">
                  <div class="card-block" style="border-right: 1px solid #bdc3c7">
                    <div style="text-align: center;">
                      <h1 class="display-3">{{topBar.jumlahGuru}} <small style="font-size: 25%">orang</small></h1>
                      <p><i class="icon-people"></i> Jumlah guru/karyawan</p>
                    </div>                    
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">                  
                  <div class="card-block">                    
                    <small>Update terakhir: <strong>{{topBar.updateData | indonesianDate}}</strong> </small>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="row">
                <div class="col-md-4">
                  <div class="card-block" style="border-right: 1px solid #bdc3c7">
                    <h6 class="sub-title">Status Kehadiran guru</h6>
                    <pie-chart-legend :series="pieKeterlambatan" title=""></pie-chart-legend>
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="card-block">
                    <h6 class="sub-title">Jumlah Keterlambatan Guru Per Hari</h6>
                    <line-chart :series="keterlambatanHarian" title=""></line-chart>
                  </div>
                </div>
              </div>
            </div>            
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-block" style="border-bottom: 1px solid #bdc3c7">
                <h5><i class="icon-user-following" style="color: #6c7a89"></i> Keterlambatan guru per orang</h5>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="card-block">
                        <h6 style="text-align: center;">
                          Keterlambatan
                        </h6>                        
                      </div>
                      <div class="row">
                        <div class="col-md-6" style="border-right: 1px solid #bdc3c7">
                          <div class="card-block">
                            <h1>{{ringkasanPerorang.min}}</h1>
                            <p>MIN 5</p>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="card-block">
                            <h1>{{ringkasanPerorang.plus}}</h1>
                            <p>PLUS 5</p>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="card-block" style="text-align: center; background-color: #1b8bf9; color: white">
                            <div>
                              Total Keterlambatan : {{topBar.keterlambatan}}
                            </div>                        
                          </div>
                        </div>
                      </div>
                    </div>                                      
                  </div>
                  <div class="card-block">                    
                    <h6 class="sub-title">Presentase jenis keterlambatan</h6>
                    <pie-chart-legend :series="pieJenisTerlambat" title=""></pie-chart-legend>
                  </div>
                </div>
                <div class="col-md-8">
                  <div style="margin-top: 15px">
                  <h6 class="sub-title">Jumlah Keterlambatan Guru Per Orang</h6>
                  <template>
                    <v-data-table
                      :headers="headers"
                      :items="keterlambatanPerorang"
                      class="elevation-1"
                      :pagination.sync="pagination"
                    >
                      <template v-slot:items="props">
                        <td>{{ props.index+1 }}</td>
                        <td class="text-xs-right">{{ props.item.kode_absensi }}</td>
                        <td @click="detailTerlambatHandle(props.item.kode_absensi, datestart, dateend)" style="color: rgb(27, 139, 249); cursor: pointer;">{{ props.item.nama }}</td>
                        <td class="">{{ props.item.mins }} <small style="font-size: 85%">kali</small></td>
                        <td class="">{{ props.item.pluss }} <small style="font-size: 85%">kali</small></td>
                        <td>{{ props.item.count }} <small style="font-size: 85%">kali</small></td>
                      </template>
                    </v-data-table>
                  </template>
                  </div>  
                </div>
              </div>
            </div>
          </div>
        </div>

        <v-layout row justify-center>
          <v-dialog v-model="detailKeterlambatanPerorang.dialog" scrollable max-width="400px">
            <v-card>
              <v-card-title>Detail Keterlambatan : {{detailKeterlambatanPerorang.nama}}</v-card-title>
              <v-divider></v-divider>
              <div style="padding: 10px">
                <span style="background-color: #e840405e; width: 15px; height: 15px; display: inline-block;"></span> <small>Terlambat lebih dari 5 menit</small>
              </div>
              <v-card-text style="height: 300px;">
                <table class="table">
                  <tr>
                    <th width="20px">No</th>
                    <th>Tanggal</th>
                    <th>Jam</th>
                  </tr>
                  <tr v-for="(i, idx) in detailKeterlambatanPerorang.data" :style="{backgroundColor: (i.jenis == 'max' ? '#e840405e' : 'transparent' )}">
                    <td>{{idx + 1}}</td>
                    <td>{{i.tanggal}}</td>
                    <td>{{i.waktu}}</td>
                  </tr>
                </table>
              </v-card-text>
              <v-divider></v-divider>
              <v-card-actions>
                <v-btn color="blue darken-1" flat @click="detailKeterlambatanPerorang.dialog = false">Tutup</v-btn>
              </v-card-actions>
            </v-card>
          </v-dialog>
        </v-layout>

        <div class="row">
          <div class="col-lg-8 col-md-8">
            <div class="card">
              <div class="card-block" style="border-bottom: 1px solid #bdc3c7">
                <h5><i class="icon-docs" style="color: #6c7a89"></i> Data Jurnal kelas</h5>                
              </div>
              <div class="card-block">                
                <div style="margin-top: 10px; margin-bottom: 30px">
                  <div class="row">
                    <div class="col-sm-9">
                      <template>
                        Kelas: <label v-for="k in listKelas" style="cursor: pointer;" :class="{ 'bg-primary' : active.listKelas == k.id_kelas  ,'label': true,  'label-inverse-primary': true}" @click="filterKelas(newStart, end, k.id_kelas)">{{k.nm_kelas}}</label>
                      </template>
                    </div>
                    <div class="col-sm-3">
                      <ul>
                        <li><span style="display: inline-block; height: 10px; width: 10px; background-color: red"></span> <small>Terlambat</small></li>
                        <li><span style="display: inline-block; height: 10px; width: 10px; background-color: blue"></span> <small>Tepat waktu</small></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="row">                    
                  <div class="col-md-12">
                    <div class="tab-content tabs">
                        <div class="tab-pane active" id="home1" role="tabpanel">
                         
                          <template>
                             <v-layout>
                              <v-flex
                                sm4
                                xs12
                                class="text-sm-left text-xs-center"
                              >
                                <v-btn @click="$refs.calendar.prev()">
                                  <v-icon
                                    dark
                                    left
                                  >
                                    keyboard_arrow_left
                                  </v-icon>
                                  Prev
                                </v-btn>
                              </v-flex>
                              <v-flex
                                sm4
                                xs12
                                class="text-xs-center"
                              >                                
                               <v-chip color="primary" text-color="white">Bulan: {{start | bulan}}</v-chip>
                              </v-flex>
                              <v-flex
                                sm4
                                xs12
                                class="text-sm-right text-xs-center"
                              >
                                <v-btn @click="$refs.calendar.next()">
                                  Next
                                  <v-icon
                                    right
                                    dark
                                  >
                                    keyboard_arrow_right
                                  </v-icon>
                                </v-btn>
                              </v-flex>
                            </v-layout>
                            <v-layout wrap>
                             <v-flex>
                              <v-sheet height="500">
                                <v-calendar
                                  :now="today"
                                  :value="today"
                                  color="primary"
                                  ref="calendar"
                                  v-model="start"
                                  :weekdays="[1, 2, 3, 4, 5, 6, 0]"
                                >
                                  <template v-slot:day="{ date }">
                                    <template v-for="event in eventsMap[date]">
                                      <v-menu
                                        :key="event.title"
                                        v-model="event.open"
                                        full-width
                                        offset-x
                                      >
                                        <template v-slot:activator="{ on }">
                                          <div
                                            v-if="!event.time"
                                            v-ripple
                                            :class="{'my-event': true,  'ontime': !event.terlambat, 'late': event.terlambat}"
                                            v-on="on"
                                            
                                          >{{event.guru}}</div>
                                        </template>
                                        <v-card
                                          color="grey lighten-4"
                                          min-width="350px"
                                          flat
                                        >
                                          <v-toolbar
                                            v-if="!event.terlambat"
                                            color="primary"
                                            dark
                                          >                                              
                                            <v-toolbar-title>{{event.mapel}} (Kelas: {{event.kelas}})</v-toolbar-title>
                                            <v-spacer></v-spacer>                                             
                                          </v-toolbar>
                                          <v-toolbar
                                            v-if="event.terlambat"
                                            color="red"
                                            dark
                                          >                                              
                                            <v-toolbar-title>{{event.mapel}} (Kelas: {{event.kelas}})</v-toolbar-title>
                                            <v-spacer></v-spacer>                                             
                                          </v-toolbar>
                                          <v-card-title primary-title>
                                            <table>
                                              <tr>
                                                <th width="200" align="right">Guru</th>
                                                <td width="400">{{event.guru}}</td>
                                              </tr>
                                              <tr>
                                                <th  align="right">Jam Memulai Pelajaran</th>
                                                <td :style="{'color': 'red'}" v-if="event.terlambat">{{event.hadir}}</td>
                                                <td v-else>{{event.hadir}}</td>
                                              </tr>
                                              <tr>
                                                <th align="right">Jam Mapel</th>
                                                <td>{{event.mulai}} - {{event.selesai}}</td>
                                              </tr>
                                              <tr>
                                                <th align="right">Kompetensi dasar</th>
                                                <td>{{event.kompetensi}}</td>
                                              </tr>
                                              <tr>
                                                <th align="right">Catatan</th>
                                                <td>{{event.catatan}}</td>
                                              </tr>
                                            </table>
                                          </v-card-title>
                                          <v-card-actions>
                                            <v-btn
                                              flat
                                              color="secondary"
                                            >
                                              Cancel
                                            </v-btn>
                                          </v-card-actions>
                                        </v-card>
                                      </v-menu>
                                    </template>
                                  </template>
                                </v-calendar>
                              </v-sheet>
                            </v-flex>
                          </v-layout>
                          </template>
                           
                        </div>
                    </div>
                  </div>                    
                </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card">
                <div class="card-block">                  
                  <div style="border-bottom: solid 1px grey; padding-bottom: 10px; margin-bottom: 10px">
                    <small class="badge badge-pill badge-primary">Periode: <strong>{{newStart | indonesianDate}} - {{end | indonesianDate}}</strong></small>
                  </div>
                  <span>Keterlambatan guru seluruh kelas</span>
                  <p><small>daftar guru yang terlambat memasuki kelas lebih dari 10 menit</small></p>
                  <template>
                    <v-data-table
                      :headers="jurnalKeterlambatanPerorang.headers"
                      :items="jurnalKeterlambatanPerorang.keterlambatanPerorang"
                      class="elevation-1"
                      :pagination.sync="pagination"
                    >
                      <template v-slot:items="props">
                        <td>{{ props.index+1 }}</td>                        
                        <td>{{ props.item.nm_pengguna }}</td>
                        <td>{{ props.item.count }} <small style="font-size: 85%">kali</small></td>
                      </template>
                    </v-data-table>
                  </template>
                </div>
              </div>              
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">              
              <template>
                <v-expansion-panel>
                  <v-expansion-panel-content                      
                  >
                    <template v-slot:header>
                      <div class="btn btn-primary">Buat Catatan</div>
                    </template>
                    <v-card>
                      <v-card-text>
                        <div class="row">
                          <div class="col-lg-4">
                            <form @submit.prevent="createNotes" method="POST">
                              <v-textarea
                                outline
                                name="input-7-1"
                                label="Catatan"
                                v-model="notes.txtNotes"
                                required
                                hint="Buat catatan disini"
                              ></v-textarea>                            
                              <div class="float-left">
                                <button class="btn btn-primary">Simpan</button>
                              </div>
                            </form>                            
                            <div class="float-right">
                              <small>Tanggal: <strong>{{now}}</strong></small>
                            </div>
                          </div>
                          <div class="col-lg-8">
                             <v-data-table
                                :headers="notes.headers"
                                :items="notes.catatan"
                                class="elevation-1"                                
                              >
                                <template v-slot:items="props">
                                  <td width="180">{{ props.item.tanggal | indonesianDate}}</td>                        
                                  <td>{{ props.item.notes.substr(0, 100) }} [...]</td>
                                  <td><a href="javascript:;" @click="handlerNote(props.item.id)">Lihat</a></td>
                                </template>
                              </v-data-table>
                          </div>
                        </div>
                      </v-card-text>
                    </v-card>
                  </v-expansion-panel-content>
                </v-expansion-panel>
              </template>              
            </div>
          </div>

        </div>
      </div>

      <div class="text-xs-center">
        <v-dialog
          v-model="notes.dialog"
          width="500"
        >
          <v-card>
            <v-card-title
              class="headline grey lighten-2"
              primary-title
            >              
              <small>Catatan tanggal {{notes.selectedDate | indonesianDate}}</small>
            </v-card-title>

            <v-card-text>
              <v-textarea
                outline
                name="input-7-1"
                label="Catatan"
                v-model="notes.selectedNotes"
                required
                hint="Buat catatan disini"
              ></v-textarea>
              <button class="btn btn-primary" @click="updateNote(notes.selectedId)">Ubah</button>
              <button class="btn" @click="deleteNote(notes.selectedId)">Hapus</button>
            </v-card-text>

            <v-divider></v-divider>

            <v-card-actions>
              <v-spacer></v-spacer>
              <v-btn
                color="primary"
                flat
                @click="notes.dialog = false"
              >
                Tutup
              </v-btn>
            </v-card-actions>
          </v-card>
        </v-dialog>
      </div>

      <!-- KEHADIRAN GURU START HERE -->      
      <div class="row m-b-30 dashboard-header">
  
      </div>

    </div>
    <!-- Container-fluid ends -->
  </v-content>
</v-app>
</div>