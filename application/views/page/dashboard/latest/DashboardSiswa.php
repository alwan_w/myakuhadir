<div class="content-wrapper" id="siswaapp">
<v-app>
  <v-content>
    <div class="container-fluid">
      <div class="contaner">
        <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Dashboard</h4><small> Data Kehadiran Siswa</small>                  
                  <p class="text-muted m-b-20"><i class="icofont icofont-tags"></i><span>&nbsp;Dashboard ini akan menampilkan data kehadiran siswa yang bersumber dari aplikasi scanner barcode.</span></p>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="" style="padding: 16px;">
            <a href="<?php echo base_url() ?>dashboard" style="vertical-align: middle;" class="btn btn-primary"><i class="icon-arrow-left-circle"></i> ke Dashboard Guru</a>
          </div>
          <div class="ml-auto mr-3" style=";">         
            <div style="display: inline-block;">
              <strong>Filter</strong> &nbsp;
            </div>
            <div style="display: inline-block;">
              <vue-rangedate-picker @selected="onDateSelected" i18n="ID" righttoleft="true"></vue-rangedate-picker>
            </div>            
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-block">
                <div style="display: inline-block;">                  
                  <span>Filter kelas: &nbsp;</span>
                </div>
                <div style="display: inline-block;">
                  <template>
                    <label v-for="k in listKelas" style="cursor: pointer; margin-right: 5px" :class="{ 'bg-primary' : active.listKelas == k.id_kelas,  'btn-sm': true, 'btn-outline-primary': true, 'btn': true}" @click="filterKelas(datestart, dateend, k.id_kelas)">{{k.nm_kelas}}</label>
                  </template> 
                </div>                
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card">              
              <div class="card-block" style="border-bottom: 1px #bdc3c7 solid;">
                <div class="float-left">
                  <h5><i class="icofont icofont-ui-home" style="color: #6c7a89"></i>&nbsp;&nbsp;Ringkasan Kehadiran Siswa</h5>
                </div>
                <div class="float-right ml-auto mr-3">                  
                  <small class="badge badge-pill badge-primary">Periode Data: <strong>{{datestart | indonesianDate}} - {{dateend | indonesianDate}}</strong></small>                  
                </div>
              </div>
              <div class="row">
                <div class="col-md-3" style="border-bottom: 1px solid #bdc3c7">
                  <div class="card-block" style="border: 1px solid #bdc3c7; background-color: #1b8bf9; color: white">
                    <div style="text-align: center;">
                      <h1 class="display-3">{{topBar.presentase}} <small style="font-size: 25%">%</small></h1>
                      <p style="color: white">Presentase kehadiran</p>
                    </div>                    
                  </div>
                </div>
                <div class="col-md-3" style="border-bottom: 1px solid #bdc3c7">
                  <div class="card-block" style="border-right: 1px solid #bdc3c7">
                    <div style="text-align: center;">
                      <h1 class="display-3">{{topBar.kehadiran}} <small style="font-size: 25%">kali</small></h1>
                      <p><i class="icon-clock"></i> Jumlah Kehadiran</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-3" style="border-bottom: 1px solid #bdc3c7; border-right: 1px solid #bdc3c7">
                  <div class="card-block">
                    <div style="text-align: center;">
                      <h1 class="display-3">{{topBar.ketidakhadiran}} <small style="font-size: 25%">kali</small></h1>
                      <p><i class="icon-clock"></i> Jumlah ketidakhadiran</p>
                    </div>
                  </div>                  
                </div>
                <div class="col-md-3" style="border-bottom: 1px solid #bdc3c7">
                  <div class="card-block" style="border-right: 1px solid #bdc3c7">
                    <div style="text-align: center;">
                      <h1 class="display-3">{{topBar.jumlahSiswa}} <small style="font-size: 25%">orang</small></h1>
                      <p><i class="icon-people"></i> Jumlah siswa</p>
                    </div>                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="row">
                <div class="col-md-4">
                  <div class="card-block">
                    <h6 class="sub-title">Status kehadiran siswa</h6>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="card-block" style="text-align: center; background-color: #1b8bf9; color: white">
                            <div>
                              Total Keterlambatan : {{topBar.keterlambatan}}
                            </div>                        
                          </div>
                        </div>
                      </div>
                    <pie-chart-legend :series="pieKeterlambatan" title=""></pie-chart-legend>
                  </div>
                </div>
                <div class="col-md-8">
                  <div class="card-block">
                    <h6 class="sub-title">Jumlah Keterlambatan Siswa Per Hari</h6>
                    <line-chart :series="keterlambatanHarian" title=""></line-chart>
                  </div>
                </div>
              </div>
            </div>  
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">              
              <div class="row">
                <div class="col-md-3 col-lg-3">
                  <div class="card-block">
                    <h6 class="sub-title">Presentase status tidak hadir</h6>
                    <pie-chart-legend :series="pieKehadiran" title=""></pie-chart-legend>
                  </div>                  
                </div>
                <div class="col-md-9 col-lg-9">
                  <div class="card-block">
                    <h6 class="sub-title">Ketidakhadiran Siswa Per Hari</h6>                    
                    <line-chart :series="alasanTidakHadirHarian" title=""></line-chart>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-block">
                <div style="margin-top: 15px">
                  <h6 class="sub-title">Jumlah Keterlambatan Siswa Per Orang</h6>
                  <template>
                    <v-data-table
                      :headers="keterlambatanPerorang.headers"
                      :items="keterlambatanPerorang.keterlambatanPerorang"
                      class="elevation-1"
                      :pagination.sync="pagination"
                    >
                      <template v-slot:items="props">
                        <td>{{ props.index+1 }}</td>
                        <td>{{ props.item.nomor_induk }}</td>
                        <td @click="detailTerlambatHandle(props.item.nomor_induk, datestart, dateend)" style="color: rgb(27, 139, 249); cursor: pointer;">{{ props.item.nm_pengguna }}</td>
                        <td>{{ props.item.nm_kelas }}</td>                        
                        <td>{{ props.item.count }} <small style="font-size: 85%">kali</small></td>
                      </template>
                    </v-data-table>
                  </template>
                </div>  
              </div>
            </div>
          </div>
        </div>

        <v-layout row justify-center>
          <v-dialog v-model="detailKeterlambatanPerorang.dialog" scrollable max-width="400px">
            <v-card>
              <v-card-title>Detail Keterlambatan : {{detailKeterlambatanPerorang.nama}}</v-card-title>
              <v-divider></v-divider>
              <v-card-text style="height: 300px;">
                <table class="table">
                  <tr>
                    <th width="20px">No</th>
                    <th>Tanggal</th>
                    <th>Jam</th>
                  </tr>
                  <tr v-for="(i, idx) in detailKeterlambatanPerorang.data">
                    <td>{{idx + 1}}</td>
                    <td>{{i.tanggal}}</td>
                    <td>{{i.waktu}}</td>
                  </tr>
                </table>
              </v-card-text>
              <v-divider></v-divider>
              <v-card-actions>
                <v-btn color="blue darken-1" flat @click="detailKeterlambatanPerorang.dialog = false">Tutup</v-btn>
              </v-card-actions>
            </v-card>
          </v-dialog>
        </v-layout>

      </div>      

      <!-- KEHADIRAN GURU START HERE -->      
      <div class="row m-b-30 dashboard-header">
  
      </div>

    </div>
    <!-- Container-fluid ends -->
  </v-content>
</v-app>
</div>