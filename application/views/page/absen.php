<div class="content-wrapper">
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12 p-0">
              <div class="main-header">
                  <h4>Dashboard</h4>
              </div>
          </div>
      </div>
      <div class="card">
        <div class="card-header"><h5 class="card-header-text">Sweetalert (Dialogs)</h5></div>
        <div class="card-block button-list">
            <button type="button" class="btn btn-flat flat-primary sweet-1 m-b-10"
                    onclick="_gaq.push(['_trackEvent', 'example', 'try', 'sweet-1']);">Basic</button>

            <button type="button" class="btn btn-flat flat-success alert-success-msg m-b-10"
                    onclick="_gaq.push(['_trackEvent', 'example', 'try', 'alert-success']);">Success</button>

            <button type="button" class="btn btn-flat flat-warning alert-confirm m-b-10"
                    onclick="_gaq.push(['_trackEvent', 'example', 'try', 'alert-confirm']);">Confirm</button>

            <button type="button" class="btn btn-flat flat-danger alert-success-cancel m-b-10"
                    onclick="_gaq.push(['_trackEvent', 'example', 'try', 'alert-success-cancel']);">Success or Cancel</button>

            <button type="button" class="btn btn-flat flat-primary alert-prompt m-b-10"
                    onclick="_gaq.push(['_trackEvent', 'example', 'try', 'alert-prompt']);">Prompt</button>

            <button type="button" class="btn btn-flat flat-success    alert-ajax m-b-10"
                    onclick="_gaq.push(['_trackEvent', 'example', 'try', 'alert-ajax']);">Ajax</button>

        </div>
    </div>

  </div>
  <!-- Container-fluid ends -->
</div>
</div>