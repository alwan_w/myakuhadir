<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<title>Cross-Browser QRCode generator for Javascript</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
<script type="text/javascript" src="<?=base_url()?>/theme/ltr-horizontal/../bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/qrcode.min.js') ?>"></script>
</head>
<body>
<a href="dataURL" target="_blank" download="image.png">Download</a>
<div id="qrcode" style="width:100px; height:100px; margin-top:15px;"></div>
<script type="text/javascript">
	
	var qrcode = new QRCode(document.getElementById("qrcode"), {
		width : 200,
		height : 200
	});
	qrcode.makeCode('ass')
	

</script>


</body>