<!DOCTYPE html>
<html>
  <head>
    <title>Absen Siswa</title>
    <script type="text/javascript" src="<?php echo base_url('assets/instascan.min.js') ?>" ></script>
    <style type="text/css">
      #preview{
        width: 100%;
      }
    </style>
  </head>
  <body>
    <video id="preview"></video>
    <script>
        let scanner = new Instascan.Scanner(
            {
                video: document.getElementById('preview')
            }
        );
        scanner.addListener('scan', function(content) {
            alert('Konten QR : '+content)
            //window.open("<?php //echo base_url('absen/ceklog/') ?>"+content, "_blank");
        });
        Instascan.Camera.getCameras().then(cameras => 
        {
            if(cameras.length > 0){
                scanner.start(cameras[0]);
            } else {
                console.error("Please enable Camera!");
            }
        });
    </script>

 </body>
</html>