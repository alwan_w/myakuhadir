<?php 

class absen_model extends CI_Model
{
	public function getTanggalEfektif($id_sekolah, $month, $year)
	{
		$this->db->select('DATE(kehadiran_timestamp) as tanggal, COUNT(kehadiran_id) as jumlah');
		$this->db->from('kehadiran');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where('MONTH(kehadiran_timestamp)', $month);
		$this->db->where('YEAR(kehadiran_timestamp)', $year);
		$this->db->group_by('DATE(kehadiran_timestamp)');
		return $this->db->get()->result_array();
	}

	public function updateToken($update, $where)
	{
		$this->db->update('tb_token', $update, $where);
		return $where['token_id'];
	}
	public function getToken($all = false)
	{
		$this->db->select('*');
		$this->db->from('tb_token');
		$this->db->join('report_ortu', 'tb_token.id_report_ortu = report_ortu.id_report_ortu');
		$this->db->join('pengguna_sekolah', 'tb_token.nomor_induk = pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna', 'pengguna_sekolah.id_pengguna = pengguna.id_pengguna');
		$this->db->where('tb_token.status', 1);
		$this->db->where('tb_token.tanggal_exp >', date('Y-m-d'));
		$this->db->order_by('tb_token.token_id', 'ASC');
		$this->db->order_by('tb_token.tanggal_exp', 'ASC');
		$token = $this->db->get()->result_array();
		if(!$all)
		{
			if(count($token)>0)
				$token = $token[0];
		}
		return $token;
	}

	public function getJamMasuk()
	{
		return $this->db->query("SELECT * FROM tb_jadwal_masuk WHERE status = 1");
	}
	public function read($id_sekolah)
	{
		$this->db->select('*');
		$this->db->from('kehadiran');
		$this->db->join('pengguna_sekolah', 'kehadiran.kehadiran_siswa = pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('kelas_siswa', 'pengguna.id_pengguna = kelas_siswa.id_siswa', 'left');
		$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas', 'left');
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		return $this->db->get();
		//return $this->db->query("SELECT * FROM kehadiran, pengguna WHERE kehadiran.kehadiran_siswa = pengguna.nomor_induk ");
	}

	public function getKehadiranByMonth($id_sekolah, $bulan, $tahun, $semester, $rekap = TRUE)
	{
		$this->db->select('kehadiran_timestamp, kehadiran_verification, pengguna.nm_pengguna, pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna_sekolah', 'kehadiran.kehadiran_siswa = pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		//$this->db->join('kelas_siswa', 'pengguna.id_pengguna = kelas_siswa.id_siswa', 'left');
		//$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas', 'left');

		$this->db->where('kehadiran.id_sekolah', $id_sekolah);
		if($semester == 'Ganjil')
		{
			$this->db->where('YEAR(kehadiran_timestamp)' , $tahun);
			if($bulan == 'all')
		      	$this->db->where('MONTH(kehadiran_timestamp) >=' , 7);
		    else
		    	$this->db->where('MONTH(kehadiran_timestamp)' , $bulan);
		}
        else
        {
        	$this->db->where('YEAR(kehadiran_timestamp)' , ($tahun+1));
        	if($bulan == 'all')
	        	$this->db->where('MONTH(kehadiran_timestamp) <=' , 6);
	        else
	        	$this->db->where('MONTH(kehadiran_timestamp)' , $bulan);
        }
		
		//$query = $this->db->get('kehadiran');
		$order = 'DESC';
		if($rekap)
			$order = 'ASC';
		$this->db->order_by('kehadiran_timestamp', $order);
		return $this->db->get('kehadiran');
		 
		//return $query->result();
	}
	public function getKehadiranByDate($id_sekolah, $start = null, $end = null, $rekap = FALSE)
	{
		$this->db->select('kehadiran_timestamp, kehadiran_verification, kelas.nm_kelas, pengguna.nm_pengguna, pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna_sekolah', 'kehadiran.kehadiran_siswa = pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('kelas_siswa', 'pengguna.id_pengguna = kelas_siswa.id_siswa', 'left');
		$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas', 'left');

		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		if($start)
            $this->db->where('kehadiran_timestamp >=' , $start);
        if($end)
 			$this->db->where('kehadiran_timestamp <=' , $end); 
		//$query = $this->db->get('kehadiran');
		$order = 'DESC';
		if($rekap)
			$order = 'ASC';
		$this->db->order_by('kehadiran_timestamp', $order);
		return $this->db->get('kehadiran');
		 
		//return $query->result();
	}

	public function getKehadiranToken($id_sekolah, $tahunAjaran, $nomor_induk, $month = null, $year = null)
	{
		$this->db->select('*');
		$this->db->from('kehadiran');
		$this->db->join('pengguna_sekolah', 'kehadiran.kehadiran_siswa = pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('kelas_siswa', 'pengguna.id_pengguna = kelas_siswa.id_siswa', 'left');
		$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas', 'left');
		$this->db->where('pengguna_sekolah.nomor_induk', $nomor_induk);
		$this->db->where('kelas.id_sekolah', $id_sekolah);
		$this->db->where('kelas.tahun_ajaran', $tahunAjaran);
		if(!is_null($year))
			$this->db->where('YEAR(kehadiran_timestamp)' , $year);
		if(!is_null($month))
			$this->db->where('MONTH(kehadiran_timestamp)' , $month);
		return $this->db->get()->result_array();
	}

	public function getKehadiranGuru($id_sekolah, $start = null, $end = null, $status = null)
	{
		$this->db->select('pengguna.nm_pengguna, kehadiran_guru.tanggal, kehadiran_guru.status, kehadiran_guru.jam_datang, kehadiran_guru.jam_pulang');
		$this->db->join('pengguna', 'kehadiran_guru.id_pengguna = pengguna.id_pengguna');
		//$this->db->join('pengguna_sekolah', "pengguna.id_pengguna = pengguna_sekolah.id_pengguna  AND kehadiran_guru.id_sekolah = pengguna_sekolah.id_sekolah", 'left');
		//$this->db->join('role', 'pengguna_sekolah.id_role = role.id_role');
		
		if(!is_null($end))
			$tahunAjaran = getTahunAjaran($end);
		elseif(!is_null($start))
			$tahunAjaran = getTahunAjaran($start);
		else
			$tahunAjaran = getTahunAjaran();
		
		$this->db->where('kehadiran_guru.id_sekolah', $id_sekolah);
		if($start == $end)
			$this->db->where('kehadiran_guru.tanggal', $start);
		else
		{
			if($start)
			{
				if(is_null($end))
		            $this->db->where('kehadiran_guru.tanggal' , $start);
		        else
		        	$this->db->where('kehadiran_guru.tanggal >=' , $start);
			}
	        if($end)
	        {
	        	if(is_null($start))
	 				$this->db->where('kehadiran_guru.tanggal' , $end);
	 			else
	 				$this->db->where('kehadiran_guru.tanggal <=' , $end); 
	        }
		}
		
		$this->db->order_by('kehadiran_guru.tanggal', 'DESC');
		$this->db->order_by('kehadiran_guru.jam_pulang', 'DESC');
		if(!is_null($status))
			$this->db->where('status', $status);
		return $this->db->get('kehadiran_guru');
	}

	//----------------------------------------------------------------------------//
	public function getKehadiranSiswa($id_sekolah, $start = null, $end = null, $status = null)
	{
		$this->db->select('*');
		$this->db->join('pengguna_sekolah', 'kehadiran.kehadiran_siswa = pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('kelas_siswa', 'pengguna.id_pengguna = kelas_siswa.id_siswa', 'left');
		$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas', 'left');

		if(!is_null($end))
			$tahunAjaran = getTahunAjaran($end);
		elseif(!is_null($start))
			$tahunAjaran = getTahunAjaran($start);
		else
			$tahunAjaran = getTahunAjaran();
		
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('kelas.tahun_ajaran', $tahunAjaran);
		if($start == $end)
			$this->db->where('date(kehadiran_timestamp)', $start);
		else
		{
			if($start)
			{
				if(is_null($end))
		            $this->db->where('date(kehadiran_timestamp)' , $start);
		        else
		        	$this->db->where('date(kehadiran_timestamp) >=' , $start);
			}
	        if($end)
	        {
	        	if(is_null($start))
	 				$this->db->where('date(kehadiran_timestamp)' , $end);
	 			else
	 				$this->db->where('date(kehadiran_timestamp) <=' , $end); 
	        }
		}
		
		if(!is_null($status))
			$this->db->where('kehadiran_verification', $status);
		//$query = $this->db->get('kehadiran');
		return $this->db->get('kehadiran');
		 
		//return $query->result();
	}
	//-------------------------------------------------------------------------//

	public function getTelat($id)
	{
		$this->db->select('*');
		$this->db->where('kehadiran.kehadiran_siswa', $id);
		$this->db->where('kehadiran.kehadiran_verification', '3');
		$this->db->from('kehadiran');
		$this->db->join('pengguna_sekolah', 'kehadiran.kehadiran_siswa = pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		return $this->db->get();
		//return $this->db->query("SELECT * FROM kehadiran, pengguna WHERE kehadiran.kehadiran_siswa = pengguna.nomor_induk AND kehadiran.kehadiran_siswa = '$id' AND kehadiran.kehadiran_verification = 3 "); //status kehadiran 3  = terlambat
	}
	
	public function readWhere($today, $siswa)
	{
		$this->db->select('*');
		$this->db->where("kehadiran_timestamp LIKE '".$today."%'");
		$this->db->where('kehadiran_siswa', $siswa[0]);
		$this->db->from('kehadiran');
		return $this->db->get();
		//return $this->db->query("SELECT * FROM kehadiran WHERE kehadiran_timestamp LIKE '$id%' AND kehadiran_siswa = '$siswa'");
	}
	public function getKehadiranById($id)
	{
		$this->db->select('*');
		$this->db->where('kehadiran.kehadiran_siswa', $id);
		$this->db->from('kehadiran');
		$this->db->join('pengguna_sekolah', 'kehadiran.kehadiran_siswa = pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('kelas_siswa', 'pengguna.id_pengguna = kelas_siswa.id_siswa', 'left');
		$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas', 'left');
		return $this->db->get();
		//return $this->db->query("SELECT * FROM kehadiran, pengguna WHERE kehadiran_siswa = '$id' AND kehadiran.kehadiran_siswa = pengguna.nomor_induk");
	}

	public function updateKehadiranGuru($update, $where)
	{
		return $this->db->update('kehadiran_guru', $update, $where);
	}
	public function simpanKehadiranGuru($data)
	{
		return $this->db->insert('kehadiran_guru', $data);
	}
	public function checkKehadiranGuru($id_sekolah, $id_pengguna, $date)
	{
		$this->db->select('*');
		$this->db->from('kehadiran_guru');
		$this->db->where('tanggal', $date);
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where('id_pengguna', $id_pengguna);
		return $this->db->get()->result_array();
	}
	public function readWhereGuru($id, $old = false)
	{
		$this->db->select('*');
		$this->db->where('pengguna_sekolah.id_role > 1');
		$this->db->where('pengguna_sekolah.nomor_induk', $id[0]);
		if($old)
			$this->db->where('pengguna_sekolah.id_sekolah', $id[1]);
		else
		{
			$this->db->where('pengguna_sekolah.id_pengguna_sekolah', $id[2]);
			$this->db->where('pengguna_sekolah.id_pengguna', $id[1]);
		}
		$this->db->from('pengguna_sekolah');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');

		return $this->db->get()->result_array();
		//return $this->db->query("SELECT * FROM pengguna WHERE nomor_induk = '$id' AND id_role ='1'");
	}
	public function readWhereSiswa($id, $old = false)
	{
		$this->db->select('*');
		$this->db->where('pengguna_sekolah.id_role', 1);
		$this->db->where('pengguna_sekolah.nomor_induk', $id[0]);
		if($old)
			$this->db->where('pengguna_sekolah.id_sekolah', $id[1]);
		else
		{
			$this->db->where('pengguna_sekolah.id_pengguna_sekolah', $id[2]);
			$this->db->where('pengguna_sekolah.id_pengguna', $id[1]);
		}
		$this->db->from('pengguna_sekolah');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');

		return $this->db->get();
		//return $this->db->query("SELECT * FROM pengguna WHERE nomor_induk = '$id' AND id_role ='1'");
	}
	public function simpanKehadiran($data)
	{
		return $this->db->insert('kehadiran', $data);
	}
	public function readToken($token)
	{
		$this->db->select('*');
		$this->db->from('tb_token');
		$this->db->join('report_ortu', 'report_ortu.id_report_ortu = tb_token.id_report_ortu', 'left');
		$this->db->where('token_code', $token);
		$this->db->order_by('tanggal_exp', 'DESC');
		return $this->db->get()->result_array();
	}
	public function insertToken($data)
	{
		return $this->db->insert('tb_token', $data);
	}
	
    public function dataSiswa()
    {
        $this->db->select('nomor_induk, pengguna.id_pengguna');
        $this->db->from('pengguna');
        $this->db->join('pengguna_sekolah', 'pengguna.id_pengguna=pengguna_sekolah.id_pengguna', 'left');
        $this->db->where('pengguna_sekolah.id_sekolah=1');
        $this->db->where('id_role=1');
        return $this->db->get();
    }
    
    public function dataHadir()
    {
        $this->db->select('*');
        $this->db->from('kehadiran');
        $this->db->where('id_sekolah = 1');
		$this->db->order_by('kehadiran_timestamp', 'DESC');
        return $this->db->get();
    }
}
?>