<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Model {

	public function insert () {
		$data = array(
	        'created_at' => date('Y-m-d H:i:s'),
	        'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->insert('kelas', $data);
		return $this->db->insert_id();
	}

	public function addJadwalPiket($data)
	{
		$this->db->insert('jadwal_piket', $data);
		return $this->db->insert_id();
	}

	public function deleteJadwalPiket($where)
	{
		return $this->db->delete('jadwal_piket', $where);
	}

	public function getListJadwalPiket($id_sekolah, $tahunAjaran)
	{
		$this->db->select('*');
		$this->db->from('jadwal_piket');
		$this->db->join('pengguna', 'pengguna.id_pengguna = jadwal_piket.id_guru');
		$this->db->join('pengguna_sekolah', 'pengguna_sekolah.id_pengguna = pengguna.id_pengguna');
		$this->db->where('jadwal_piket.id_sekolah', $id_sekolah);
		$this->db->where('jadwal_piket.tahun_ajaran', $tahunAjaran);
		$this->db->order_by('hari', 'ASC');
		return $this->db->get()->result();
	}

	public function getJadwalSekolah($id_sekolah, $tahunAjaran, $id_jadwal_sekolah)
	{
		$this->db->select('*');
		$this->db->from('jadwal_sekolah');
		$this->db->where('id_jadwal_sekolah', $id_jadwal_sekolah);
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where('tahun_ajaran', $tahunAjaran);
		return $this->db->get()->result();
	}

	public function getListJadwalSekolah($id_sekolah, $tahunAjaran)
	{
		$this->db->select('*');
		$this->db->from('jadwal_sekolah');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where('tahun_ajaran', $tahunAjaran);
		return $this->db->get()->result();
	}

	public function setStausJadwalSekolah($id_sekolah, $tahunAjaran, $id_jadwal_sekolah)
	{
		$this->db->update('jadwal_sekolah', array('status' => 0), array('id_sekolah' => $id_sekolah, 'tahun_ajaran'=> $tahunAjaran));
		$this->db->update('jadwal_sekolah', array('status' => 1), array('id_jadwal_sekolah' => $id_jadwal_sekolah));
	}

	public function addJadwalGuru($insert)
	{
		$this->db->insert('jadwal_guru', $insert);
		return $this->db->insert_id();
	}

	public function checkKelas($id_sekolah, $tahunAjaran, $nm_kelas, $rombel_kelas)
	{
		$this->db->select('id_kelas');
		$this->db->from('kelas');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where('tahun_ajaran', $tahunAjaran);
		$this->db->where('nm_kelas', $nm_kelas);
		$this->db->where('rombel_kelas', $rombel_kelas);
		$check = $this->db->get()->result_array();
		if(count($check)>0)
			return $check[0]['id_kelas'];
		else
		{
			$date = date('Y-m-d H:i:s');
			$insert = array(
				'id_sekolah' => $id_sekolah,
				'tahun_ajaran' => $tahunAjaran,
				'nm_kelas' => $nm_kelas,
				'rombel_kelas' => $rombel_kelas,
				'jumlah_siswa' => 0,
				'created_at' => $date,
				'updated_at' => $date
			);
			$this->db->insert('kelas', $insert);
			return $this->db->insert_id();
		}

	}

	public function addJadwalSekolah($id_sekolah, $tahunAjaran, $nama_jadwal)
	{
		$date = date('Y-m-d H:i:s');
		$insert = array(
			'id_sekolah' => $id_sekolah,
			'tahun_ajaran' => $tahunAjaran,
			'nama_jadwal' => $nama_jadwal,
			'status' => 0,
			'created_at' => $date,
			'updated_at' => $date
		);
		if($insert['nama_jadwal'] == '')
			$insert['nama_jadwal'] = 'Tanpa Nama';
		$this->db->insert('jadwal_sekolah', $insert);
		return $this->db->insert_id();
	}

	public function addMapel($id_sekolah, $nm_mata_pelajaran)
	{
		$date = date('Y-m-d H:i:s');
		$insert = array(
			'id_sekolah' => $id_sekolah,
			'nm_mata_pelajaran' => $nm_mata_pelajaran,
			'jenis_mata_pelajaran' => 1,
			'created_at' => $date,
			'updated_at' => $date
		);
		$this->db->insert('mata_pelajaran', $insert);
		return $this->db->insert_id();

	}

	public function getAllMapel($id_sekolah)
	{
		$this->db->select('id_mata_pelajaran, nm_mata_pelajaran');
		$this->db->from('mata_pelajaran');
		$this->db->where('id_sekolah', $id_sekolah);
		return $this->db->get()->result_array();
	}

	public function checkGuru($id_sekolah, $guru)
	{
		$this->db->select('id_pengguna');
		$this->db->from('pengguna_sekolah');
		$this->db->where('pengguna_sekolah.nomor_induk', $guru['nomor_induk']);
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$check = $this->db->get()->result_array();
		if(count($check)>0)
			return $check[0]['id_pengguna'];
		else
		{
			$date = date('Y-m-d H:i:s');
			if(is_null($guru['nama']))
				$guru['nama'] = 'Tanpa Nama';
			$insert = array(
				'nm_pengguna' => $guru['nama'],
				'created_at' => $date,
				'updated_at' => $date
			);
			$this->db->insert('pengguna', $insert);
			$id_pengguna = $this->db->insert_id();
			$insert = array(
				'id_sekolah' => $id_sekolah,
				'id_pengguna' => $id_pengguna,
				'id_role' => 3,
				'nomor_induk' => $guru['nomor_induk'],
				'created_at' => $date,
				'updated_at' => $date
			);
			return $id_pengguna;
		}
	}

	public function getList () {
		$query = $this->db->get('kelas');
		return $query->result();
	}

	public function delete ($id) {
		$this->db->where('id_kelas', $id);
		$this->db->delete('kelas');
	}

	public function getKaryawanById ($id) {
		$this->db->where('id_kelas', $id);
		$this->db->from('kelas');
		return $this->db->get()->result();
	}

	public function update ($id, $kelas) {		
		$this->db->set('nm_kelas', $kelas);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id_kelas', $id);
		$this->db->update('kelas');
	}
}