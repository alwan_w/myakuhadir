<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JadwalGuru extends CI_Model {

	public function insert ($jadwal) {
		$data = array(
		        'id_guru'	 => $jadwal['id_guru'],
		        'id_kelas'	 => $jadwal['id_kelas'],
		        'hari'		 => $jadwal['hari'],
		        'jam_mulai'		=> $jadwal['jam_mulai'],
		        'jam_selesai'	=> $jadwal['jam_selesai'],
		        'id_mata_pelajaran'	=> $jadwal['id_mata_pelajaran'],
		        'id_sekolah' => $jadwal['id_sekolah'],
		        'created_at' => date('Y-m-d H:i:s'),
		        'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->insert('jadwal_guru', $data);
	}
	public function getGuru($id_sekolah){
		$this->db->select('*');
		$this->db->from('pengguna_sekolah');
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('pengguna_sekolah.id_role >=', 3);
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('role', 'role.id_role = pengguna_sekolah.id_role');
		return $this->db->get()->result();
	}

	public function getJadwalById($id){
		$this->db->select('*');
		$this->db->from('jadwal_guru');
		$this->db->where('jadwal_guru.id_jadwal_guru', $id);
		return $this->db->get()->result();
	}

	public function getMapel($id_sekolah){
		$this->db->select('*');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->from('mata_pelajaran');
		return $this->db->get()->result();
	}

	public function getKelas($id_sekolah){
		$this->db->select('*');
		$this->db->where('kelas.id_sekolah', $id_sekolah);
		$this->db->from('kelas');
		return $this->db->get()->result();
	}

	public function getJam($id_sekolah){
		$this->db->select('*');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->from('jam_pelajaran');
		return $this->db->get()->result();
	}

	public function getList ($id_sekolah, $tahunAjaran = NULL) {
		$this->db->select('*');
		$this->db->from('jadwal_guru');
		$this->db->join('jadwal_sekolah', 'jadwal_sekolah.id_jadwal_sekolah = jadwal_guru.id_jadwal_sekolah');
		$this->db->join('pengguna', 'jadwal_guru.id_guru = pengguna.id_pengguna');
		$this->db->join('kelas', 'jadwal_guru.id_kelas = kelas.id_kelas');
		$this->db->join('mata_pelajaran', 'jadwal_guru.id_mata_pelajaran = mata_pelajaran.id_mata_pelajaran');
		$this->db->where('jadwal_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('jadwal_sekolah.tahun_ajaran', $tahunAjaran);
		$this->db->where('jadwal_sekolah.status', 1);
		// $this->db->join('jam_pelajaran', 'jadwal_guru.id_jam_pelajaran = jam_pelajaran.id_jam_pelajaran');
		$query = $this->db->get();
		return $query->result();
	}

	public function delete ($id) {
		$this->db->where('id_kelas', $id);
		$this->db->delete('kelas');
	}

	public function getKaryawanById ($id) {
		$this->db->where('id_kelas', $id);
		$this->db->from('kelas');
		return $this->db->get()->result();
	}

	public function update ($id, $jadwal) {		
		$this->db->set('id_guru', $jadwal['id_guru']);
		$this->db->set('id_kelas', $jadwal['id_kelas']);
		$this->db->set('hari', $jadwal['hari']);
		$this->db->set('jam_mulai', $jadwal['jam_mulai']);
		$this->db->set('jam_selesai', $jadwal['jam_selesai']);
		$this->db->set('id_mata_pelajaran', $jadwal['id_mata_pelajaran']);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id_jadwal_guru', $id);
		$this->db->update('jadwal_guru');
	}
}