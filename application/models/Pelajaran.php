<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelajaran extends CI_Model {

	public function insert ($data) {
		$data = array(
		        'nm_mata_pelajaran' => $data['nm_mata_pelajaran'],
		        'jenis_mata_pelajaran' => $data['jenis_mata_pelajaran'],
		        'id_sekolah'	=> $data['id_sekolah'],
		        'created_at' => date('Y-m-d H:i:s'),
		        'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->insert('mata_pelajaran', $data);
	}

	public function getList ($id_sekolah) {
		$this->db->select('*');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->from('mata_pelajaran');
		return $this->db->get()->result();
	}

	public function delete ($id) {
		$this->db->where('id_mata_pelajaran', $id);
		$this->db->delete('mata_pelajaran');
	}

	public function getPelajaranById ($id) {
		$this->db->where('id_mata_pelajaran', $id);
		$this->db->from('mata_pelajaran');
		return $this->db->get()->result();
	}

	public function update ($id, $data) {		
		$this->db->set('nm_mata_pelajaran', $data['nm_mata_pelajaran']);
		$this->db->set('jenis_mata_pelajaran', $data['jenis_mata_pelajaran']);
		$this->db->set('id_sekolah', $data['id_sekolah']);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id_mata_pelajaran', $id);
		$this->db->update('mata_pelajaran');
	}
}