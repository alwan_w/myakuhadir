<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OAuth_model extends CI_Model {

    public function checkGoogleUser($data = array())
    {
        $this->db->select('id');
        $this->db->from('google_user');
        $this->db->where(array('oauth_provider'=>$data['oauth_provider'],'oauth_uid'=>$data['oauth_uid']));
        $query = $this->db->get();
        $check = $query->num_rows();
        if($check > 0)
        {
            $result = $query->row_array();
		    $data['id_pengguna'] = $this->checkPenggunaGoogle($data);
		    $data['updated_at'] = date("Y-m-d H:i:s");
            $update = $this->db->update('google_user',$data,array('id'=>$result['id']));
            $userID = $result['id'];
        }
        else
        {
		    $data['id_pengguna'] = $this->checkPenggunaGoogle($data);
		    $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at']= date("Y-m-d H:i:s");
            $insert = $this->db->insert('google_user',$data);
            $userID = $this->db->insert_id();
        }
        return $userID?$userID:false;
    }

    private function checkPenggunaGoogle($data)
    {
    	$cek = $this->db->get_where('pengguna', array('email' => $data['email']));
	    $cek = $cek->result_array();
	    if(count($cek)>0)
	    	return $cek[0]['id_pengguna'];
	    else
	    {
	    	$user = array(
	    		'nm_pengguna' => $data['first_name'] . " " . $data['last_name'],
	    		'alamat' => '',
	    		'no_hp' => '',
	    		'email' => $data['email'],
	    		'password' => '',
	    		'created_at' => date("Y-m-d H:i:s"),
	    		'updated_at' => date("Y-m-d H:i:s")
	    	);
	    	$this->db->insert('pengguna', $user);
	    	return $this->db->insert_id();
	    }
    }

    public function getUserWhere($where)
    {
        $res=$this->db->get_where('pengguna', $where);
		return $res->row();
    }


}



/* End of file M_google.php */

/* Location: ./application/models/M_google.php */