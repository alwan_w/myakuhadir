<?php 
/**
 * 
 */
class siswa extends CI_Model
{
	public function nonAktifAll($id_sekolah)
	{
		$update = array('status' => 0);
		$where = array(
			'id_sekolah' => $id_sekolah,
			'id_role' => 1
		);
		return $this->db->update('pengguna_sekolah', $update, $where);
	}
	public function addExcel($id_sekolah, $data, $new = FALSE)
	{
		$pengguna = array(
			'nm_pengguna' => $data['nm_pengguna'],
			'updated_at' => $data['date']
		);
		if(isset($data['password']) && !is_null($data['password']))
			$pengguna['password'] = md5($data['password']);
		if(!is_null($data['no_hp']))
			$pengguna['no_hp'] = $data['no_hp'];
		if(!is_null($data['alamat']))
			$pengguna['alamat'] = $data['alamat'];
		
		$penggunaSekolah = array(
			'id_role' => 1,
			'status' => 1,
			'updated_at' => $data['date']
		);
		if($new)
		{
			if(!isset($pengguna['password']))
				$pengguna['password'] = md5($data['nomor_induk']);
			$pengguna['email'] = $data['email'];
			$pengguna['created_at'] = $data['date'];
			$this->db->insert('pengguna', $pengguna);

			$penggunaSekolah['id_pengguna'] = $this->db->insert_id();
			$penggunaSekolah['id_sekolah'] = $id_sekolah;
			$penggunaSekolah['nomor_induk'] = $data['nomor_induk'];
			$penggunaSekolah['created_at'] = $data['date'];
			$this->db->insert('pengguna_sekolah', $penggunaSekolah);

			return $penggunaSekolah['id_pengguna'];
		}
		else
		{
			if($data['type'] == 'email')
				$penggunaSekolah['nomor_induk'] = $data['nomor_induk'];
			else
				$pengguna['email'] = $data['email'];

			$where = array(
				'id_pengguna' => $data['id_pengguna']
			);
			$this->db->update('pengguna', $pengguna, $where);
			$where['id_sekolah'] = $id_sekolah;
			$this->db->update('pengguna_sekolah', $penggunaSekolah, $where);

			return $data['id_pengguna'];
		}
	}
	public function getOwnedPengguna($id_sekolah, $emailPengguna, $nomorIndukPengguna)
	{
		$this->db->select('*');
		$this->db->from('pengguna');
		$this->db->join('pengguna_sekolah', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where_in('pengguna_sekolah.nomor_induk', $nomorIndukPengguna);
		/*if(count($emailPengguna)>0)
		{
			$this->db->where_in('pengguna.email', $emailPengguna);
			$this->db->or_where_in('pengguna_sekolah.nomor_induk', $nomorIndukPengguna);
		}*/
		return $this->db->get();
	}

	public function deleteSiswa($id)
	{
		return $this->db->query("DELETE FROM pengguna WHERE id_pengguna ='$id' ");
	}
	public function getSiswaAndKelas($id_sekolah, $tahunAjaran, $rekap = FALSE, $urutKelas = FALSE)
	{
		$this->db->select('*');
		$this->db->from('pengguna_sekolah');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('kelas_siswa', "pengguna.id_pengguna = kelas_siswa.id_siswa  AND kelas_siswa.tahun_ajaran = '".$tahunAjaran."'", 'left');
		$this->db->join('kelas', "kelas_siswa.id_kelas = kelas.id_kelas", 'left');
		/*$this->db->join('kelas_siswa', "pengguna.id_pengguna = kelas_siswa.id_siswa", 'left');
		$this->db->join('kelas', "kelas_siswa.id_kelas = kelas.id_kelas  AND kelas.tahun_ajaran = '".$tahunAjaran."'", 'left');*/
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('pengguna_sekolah.id_role', '1');
		
		//$this->db->where('kelas.tahun_ajaran', $tahunAjaran);
		if($urutKelas)
		{
			$this->db->order_by('kelas.nm_kelas', 'ASC');
			$this->db->order_by('kelas.rombel_kelas', 'ASC');
		}
		$this->db->order_by('pengguna.nm_pengguna','ASC');
		if($urutKelas)
			$this->db->order_by('kelas.nm_kelas', 'ASC');
		if($rekap)
		{
			$this->db->where('kelas.tahun_ajaran', $tahunAjaran);
			$this->db->order_by('pengguna_sekolah.nomor_induk', 'ASC');
		}
		else
			$this->db->where('pengguna_sekolah.status', '1');
		return $this->db->get();
	}
	public function readSiswa($id_sekolah)
	{
		$this->db->select('*');
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('pengguna_sekolah.id_role', '1');
		$this->db->from('pengguna_sekolah');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		return $this->db->get();
		//return $this->db->query("SELECT * FROM `pengguna_sekolah` LEFT JOIN `pengguna` ON `pengguna_sekolah`.`id_pengguna` = `pengguna`.`id_pengguna` WHERE `pengguna_sekolah`.`id_sekolah` = '$id_sekolah' AND `pengguna_sekolah`.`id_role` = '1'");
	}
	public function readSiswaWhere($id, $id_sekolah)
	{
		$this->db->select('*');
		$this->db->where('pengguna_sekolah.id_pengguna', $id);
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('pengguna_sekolah.id_role', '1');
		$this->db->from('pengguna_sekolah');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna', 'left');
		$this->db->join('kelas_siswa', 'pengguna.id_pengguna = kelas_siswa.id_siswa', 'left');
		$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas', 'left');
		return $this->db->get();
	}
	public function getSiswaByKelas($kelas)
	{
		$this->db->select('*');
		$this->db->where('kelas_siswa.id_kelas', $kelas);
		$this->db->where('kelas_siswa.id_siswa = pengguna.id_pengguna');
		$this->db->from('kelas_siswa');
		$this->db->join('pengguna', 'pengguna.id_pengguna = kelas_siswa.id_siswa');
		$this->db->join('pengguna_sekolah', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->order_by('nm_pengguna', 'a');
		return $this->db->get();
		//return $this->db->query("SELECT * FROM kelas_siswa, pengguna WHERE kelas_siswa.id_siswa = pengguna.id_pengguna AND kelas_siswa.id_kelas = '$kelas' ");
	}
	public function getIdSiswaByKelas($kelas)
	{
		return array_column($this->db->query("SELECT id_siswa FROM kelas_siswa WHERE kelas_siswa.id_kelas = '$kelas' ")->result_array(), 'id_siswa');
	}
	public function getSiswaNoKelas()
	{
		return $this->db->query("SELECT * FROM pengguna WHERE id_role = '1' AND kelas_siswa = 0 ");
	}
	public function insKelasSiswa($data)
	{
		
		return $this->db->replace('kelas_siswa', $data);
	}
	public function deleteFromKelas($id_siswa, $kelas)
	{
		$date = date('Y-m-d H:i:s');
		$update = array(
			'jumlah_siswa' => ($kelas['jumlah_siswa']-1),
			'updated_at' => $date
		);
		$this->db->update('kelas', $update, array('id_kelas' => $kelas['id_kelas']));
		return $this->db->delete('kelas_siswa', array('id_siswa' => $id_siswa, 'id_kelas' => $kelas['id_kelas']));
	}
	public function insert ($data) {
		 date_default_timezone_set("Asia/Jakarta"); 
		 if ($data['id']=='') {
		 	$dataSend = array(
		        'nm_pengguna' => $data['nama'],
		        'alamat' => $data['alamat'],
		        'no_hp' => $data['phone'],
		        'email' => $data['email'],
		        'created_at' => date('Y-m-d H:i:s'),
		        'updated_at' => date('Y-m-d H:i:s')
			);

			$this->db->insert('pengguna', $dataSend);
			$id_pengguna = $this->db->insert_id();
			$data = array(
				'id_pengguna' => $id_pengguna,
				'id_role' => $data['role'],
				'id_sekolah' => $data['sekolah'],
		        'nomor_induk' => $data['nomor'],
		        'status' => 1,
				'created_at' => date('Y-m-d H:i:s'),
		        'updated_at' => date('Y-m-d H:i:s')
			);
			$this->db->insert('pengguna_sekolah', $data);
		 }
		 else{			 	
			$dataSend = array(
			        'nm_pengguna' => $data['nama'],
			        'alamat' => $data['alamat'],
			        'no_hp' => $data['phone'],
			        'email' => $data['email'],
			        'updated_at' => date('Y-m-d H:i:s')
			);
			$this->db->where('id_pengguna', $data['id']);
			$this->db->update('pengguna', $dataSend);

			$datasend = array(
		        'nomor_induk' => $data['nomor'],
		        'updated_at' => date('Y-m-d H:i:s')
			);
			$this->db->where('id_pengguna', $data['id']);
			$this->db->where('id_sekolah', $data['sekolah']);
			$this->db->update('pengguna_sekolah', $datasend);
		 }
	}
    public function dataSiswa()
    {
        $this->db->select('nomor_induk, pengguna.id_pengguna');
        $this->db->from('pengguna');
        $this->db->join('pengguna_sekolah', 'pengguna.id_pengguna=pengguna_sekolah.id_pengguna', 'left');
        $this->db->where('pengguna_sekolah.id_sekolah=1');
        $this->db->where('id_role=1');
        return $this->db->get();
    }
    
    public function dataHadir()
    {
        $this->db->select('*');
        $this->db->from('kehadiran');
        $this->db->where('id_sekolah = 1');
		$this->db->order_by('kehadiran_timestamp', 'DESC');
        return $this->db->get();
    }
    
    public function dataKelas()
    {
        $this->db->select('*');
        $this->db->from('kelas');
        $this->db->where('id_sekolah=1');
        $this->db->order_by('id_kelas', 'ASC');
        return $this->db->get();
    }
    
    public function dataKelasSiswa()
    {
        $this->db->select('*');
        $this->db->from('kelas_siswa');
        return $this->db->get();
    }
    
    public function dataPengguna()
    {
        $this->db->select('*');
        $this->db->from('pengguna');
        return $this->db->get();
    }
    
    public function dataPenggunaSekolah()
    {
        $this->db->select('*');
        $this->db->from('pengguna_sekolah');
        return $this->db->get();
    }
    
    public function jurnalGuru()
    {
        $this->db->select('*');
        $this->db->from('jurnal_guru_ekstra');
        return $this->db->get();
    }
}
?>