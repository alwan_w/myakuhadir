<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JamPelajaran extends CI_Model {

	public function updOther($id)
	{
		return $this->db->query("UPDATE tb_jadwal_masuk SET status = 0 WHERE id_jm NOT IN ('$id')");
	}
	public function updateJam($data, $id)
	{
		$this->db->where('id_jm', $id);
		return $this->db->update('tb_jadwal_masuk', $data);
	}
	public function getJamMasuk($id_sekolah)
	{
		$this->db->select('*');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->from('tb_jadwal_masuk');
		return $this->db->get();
	}

	public function insertJadwalMasuk($data)
	{
		$data = array(
	        'jm_masuk' => $data['jm_masuk'],
	        'jm_judul' => $data['jm_judul'],
	        'status' => 0,
	        'id_sekolah' => $data['id_sekolah']
		);

		$this->db->insert('tb_jadwal_masuk', $data);
	}

	public function insert ($data) {
		$data = array(
		        'jam_ke' => $data['jam_ke'],
		        'jam_mulai' => $data['jam_mulai'],
		        'jam_selesai' => $data['jam_selesai'],
		        'id_sekolah' => $data['id_sekolah'],
		        'created_at' => date('Y-m-d H:i:s'),
		        'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->insert('jam_pelajaran', $data);
	}

	public function getList ($id_sekolah) {
		$this->db->select('*');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->from('jam_pelajaran');
		return $this->db->get()->result();
	}

	public function delete ($id) {
		$this->db->where('id_jam_pelajaran', $id);
		$this->db->delete('jam_pelajaran');
	}

	public function getWaktuById ($id) {
		$this->db->where('id_jam_pelajaran', $id);
		$this->db->from('jam_pelajaran');
		return $this->db->get()->result();
	}

	public function update ($id, $data) {		
		$this->db->set('jam_ke', $data['jam_ke']);
		$this->db->set('jam_mulai', $data['jam_mulai']);
		$this->db->set('jam_selesai', $data['jam_selesai']);
		$this->db->set('id_sekolah', $data['id_sekolah']);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id_jam_pelajaran', $id);
		$this->db->update('jam_pelajaran');
	}
}