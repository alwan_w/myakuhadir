<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Model {

	public function pilihKetuaKelas($id_ketua_kelas, $id_kelas)
	{
		$date = date('Y-m-d H:i:s');
		$this->db->update('kelas', array('id_ketua_kelas' => $id_ketua_kelas, 'updated_at' => $date), array('id_kelas' => $id_kelas));
		return $id_kelas;
	}
	public function checkKelasSiswa($data)
	{
		$this->db->select('id_kelas_siswa');
		$this->db->from('kelas_siswa');
		$this->db->where('id_kelas', $data['id_kelas']);
		$this->db->where('id_siswa', $data['id_siswa']);
		return $this->db->get();
	}

	public function checkPengguna($pengguna, $wali = FALSE)
	{
		$this->db->select('pengguna.id_pengguna, pengguna_sekolah.id_pengguna_sekolah');
		$this->db->from('pengguna_sekolah');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna', 'left');
		$this->db->where('id_sekolah', $pengguna['id_sekolah']);
		$this->db->where('nomor_induk', $pengguna['nomor_induk']);
		$check = $this->db->get()->result_array();

		if(count($check)>0)
		{
			$update = array('status' => 1, 'updated_at' => $pengguna['date']);
			$where = array(
				'id_pengguna_sekolah' => $check[0]['id_pengguna_sekolah'], 
				'id_sekolah' => $pengguna['id_sekolah']
			);
			$this->db->update('pengguna_sekolah', $update, $where);
			return $check[0]['id_pengguna'];
		}
		else
		{
			$data = array(
				'nm_pengguna' => $pengguna['nm_pengguna'],
				'created_at' => $pengguna['date'],
				'updated_at' => $pengguna['date']
			);
			$this->db->insert('pengguna', $data);
			$id = $this->db->insert_id();

			$data = array(
				'id_pengguna' => $id,
				'nomor_induk' => $pengguna['nomor_induk'],
				'id_sekolah' => $pengguna['id_sekolah'],
				'id_role' => 1,
				'status' => 1,
				'created_at' => $pengguna['date'],
				'updated_at' => $pengguna['date']
			);
			if($wali)
				$data['id_role'] = 3;
			$this->db->insert('pengguna_sekolah', $data);

			return $id;
		}
	}
	public function addKelasSiswa($data)
	{
		$this->db->insert('kelas_siswa', $data);
		return $this->db->insert_id();
	}
	public function emptyKelas($id_kelas, $tahunAjaran)
	{
		$where = array(
			'id_kelas' => $id_kelas,
			'tahun_ajaran' => $tahunAjaran
		);
		return $this->db->delete('kelas_siswa', $where);
	}
	public function excelKelas($data, $where = null)
	{
		if(is_null($where))
		{
			$this->db->insert('kelas', $data);
			return $this->db->insert_id();
		}
		else
		{
			$this->db->update('kelas', $data, $where);
			return $where['id_kelas'];
		}
	}
	public function checkKelas($id_sekolah, $tahunAjaran, $kelas) 
	{
		$this->db->select('id_kelas, jumlah_siswa');
		$this->db->from('kelas');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where('tahun_ajaran', $tahunAjaran);
		$this->db->where('nm_kelas', $kelas['nm_kelas']);
		$this->db->where('rombel_kelas', $kelas['rombel_kelas']);
		return $this->db->get();
	}

	public function insert($data) {
		$this->db->insert('kelas', $data);
		return $this->db->insert_id();
	}

	public function getGuru($id_sekolah){
		$this->db->select('*');
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('pengguna_sekolah.id_role', 3);
		$this->db->from('pengguna_sekolah');
		$this->db->join('pengguna', 'pengguna_sekolah.id_pengguna = pengguna.id_pengguna','left');
		return $this->db->get()->result();
	}
	public function getWaliKelas($id)
	{
		return $this->db->query("SELECT * FROM pengguna, kelas WHERE kelas.wali_kelas = pengguna.id_pengguna AND kelas.id_kelas = '$id' ");
	}
	public function getKelasByGuru($id)
	{
		return $this->db->query("SELECT * FROM kelas WHERE wali_kelas = '$id' ");
	}
	public function getListKelas ($id_sekolah, $tahunAjaran) {
		$this->db->select('nm_kelas, rombel_kelas, jumlah_siswa, id_kelas, ketua.nm_pengguna as nm_ketua_kelas, wali.nm_pengguna as nm_wali_kelas');
		$this->db->where('kelas.id_sekolah', $id_sekolah);
		$this->db->where('kelas.tahun_ajaran', $tahunAjaran);
		$this->db->from('kelas');
		$this->db->join('pengguna as wali', 'kelas.wali_kelas = wali.id_pengguna','left');
		$this->db->join('pengguna as ketua', 'kelas.id_ketua_kelas = ketua.id_pengguna','left');
		$this->db->order_by('kelas.nm_kelas', 'ASC');
		$this->db->order_by('kelas.rombel_kelas', 'ASC');
		return $this->db->get()->result();
	}

	public function deleteKelas ($id) {
		$this->db->where('id_kelas', $id);
		$this->db->delete('kelas');
		$this->db->delete('kelas_siswa', array('id_kelas' => $id));
	}

	public function getKelasById ($id) {
		$this->db->where('id_kelas', $id);
		$this->db->from('kelas');
		return $this->db->get();
	}

	public function update ($data, $where) 
	{	
		return $this->db->update('kelas', $data, $where);
	}
}