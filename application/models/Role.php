<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Model {

	public function insert ($data) {
		$data = array(
		        'nm_mata_pelajaran' => $data,
		        'created_at' => date('Y-m-d H:i:s'),
		        'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->insert('mata_pelajaran', $data);
	}

	public function getList () {
		$query = $this->db->get('role');
		return $query->result();
	}

	public function delete ($id) {
		$this->db->where('id_mata_pelajaran', $id);
		$this->db->delete('mata_pelajaran');
	}

	public function getPelajaranById ($id) {
		$this->db->where('id_mata_pelajaran', $id);
		$this->db->from('mata_pelajaran');
		return $this->db->get()->result();
	}

	public function update ($id, $data) {		
		$this->db->set('nm_mata_pelajaran', $data);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id_mata_pelajaran', $id);
		$this->db->update('mata_pelajaran');
	}
}