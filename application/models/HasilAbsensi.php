<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HasilAbsensi extends CI_Model {

	public function addTokenReport($data)
	{
		$this->db->insert('tb_token', $data);
		return $this->db->insert_id();
	}

	public function addReportOrtu($data)
	{
		$this->db->insert('report_ortu', $data);
		return $this->db->insert_id();
	}

	public function getReportOrtu($id_sekolah, $tahunAjaran, $id_report_ortu)
	{
		$this->db->select('report_ortu.waktu_report, pengguna.nm_pengguna, pengguna_sekolah.nomor_induk, kelas.nm_kelas, kelas.rombel_kelas, tb_token.status');
		$this->db->from('tb_token');
		$this->db->join('report_ortu', 'report_ortu.id_report_ortu = tb_token.id_report_ortu');
		$this->db->join('pengguna_sekolah', 'tb_token.nomor_induk = pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna', 'pengguna_sekolah.id_pengguna = pengguna.id_pengguna');
		$this->db->join('kelas_siswa', 'kelas_siswa.id_siswa = pengguna.id_pengguna');
		$this->db->join('kelas', 'kelas.id_kelas = kelas_siswa.id_kelas');
		$this->db->where('report_ortu.id_report_ortu', $id_report_ortu);
		$this->db->where('kelas.id_sekolah', $id_sekolah);
		$this->db->where('kelas.tahun_ajaran', $tahunAjaran);
		return $this->db->get()->result_array();
	}

	public function getListReportOrtu($id_sekolah, $tahunAjaran)
	{
		$this->db->select('*');
		$this->db->from('report_ortu');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where('tahun_ajaran', $tahunAjaran);
		$this->db->order_by('waktu_report', 'DESC');
		return $this->db->get()->result_array();
	}

	public function insert () {
		$data = array(
		        
		        'created_at' => date('Y-m-d H:i:s'),
		        'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->insert('kelas', $data);
	}

	public function getList ($start, $end, $status, $id_sekolah) {
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where('timestamp >=', $start);
		$this->db->where('timestamp <=', $end);
		if ($status != null) {
			if ($status == 'tepat') {
				$this->db->where('DATE_FORMAT(timestamp,"%H:%i") <=', '07:00');
			} elseif($status == 'terlambat') {
				$this->db->where('DATE_FORMAT(timestamp,"%H:%i") >', '07:00');
				$this->db->where('jenis', 'in');
			}
		}
		$query = $this->db->get('absensi_guru');
		return $query->result();
	}

	public function getDistinctDay ($start, $end) {
		$this->db->select('DISTINCT DATE(`timestamp`)');
		$this->db->where('timestamp >=', $start);
		$this->db->where('timestamp <=', $end);
		$query = $this->db->get('absensi_guru');
		return $query->result();
	}

	public function getPercentageTime ($start, $end, $startTime, $endTime) {
		$query1 = $this->db->where('timestamp >=', $start)
			->where('timestamp <=', $end)
			->where('DATE_FORMAT(timestamp,"%H:%i") >', $startTime)
			->where('DATE_FORMAT(timestamp,"%H:%i") <=', $endTime)
			->where('jenis', 'in');
		$query1 = $this->db->get('absensi_guru');
		$data1 = $query1->num_rows();
		$query2 = $this->db->where('timestamp >=', $start)
			->where('timestamp <=', $end)			
			->where('jenis', 'in');
		$query2 = $this->db->get('absensi_guru');
		$data2 = $query2->num_rows();
		return $data1;
	}

	public function getTimeRange ($start, $end, $status) {
		$query = $this->db->query(
			'SELECT count(*) as count, concat( sec_to_time(time_to_sec(timestamp)- time_to_sec(timestamp)%(15*60) + (15*60))) AS timekey
			FROM absensi_guru
			WHERE jenis = "'.$status.'"
			AND timestamp >= "'.$start.'"
			AND timestamp <= "'.$end.'"
			GROUP BY timekey;'
		)->result();
		return $query;
	}

	public function getMonthlyLate ($year) {	
			return $this->db->query(
				"SELECT count(*) as count, DATE_FORMAT(`timestamp`, '%Y-%m-01') as month 
				FROM absensi_guru 
				WHERE DATE_FORMAT(timestamp,'%H:%i') > '07:00' 
				AND jenis = 'in'
				AND DATE_FORMAT(timestamp,'%Y') = DATE_FORMAT('".$year."','%Y')
				GROUP BY DATE_FORMAT(`timestamp`, '%Y-%m-01')"
			)->result();
	}

	public function getPeopleLate ($start, $end) {
		$min5 = 'SELECT count(*) as mins, kode_absensi FROM absensi_guru 
			WHERE timestamp >= "'.$start.'"
			AND timestamp <= "'.$end.'"
			AND DATE_FORMAT(timestamp,"%H:%i") > "07:00" 
			AND DATE_FORMAT(timestamp,"%H:%i") < "07:06"
			AND jenis = "in"
			GROUP BY kode_absensi';
		$plus5 = 'SELECT count(*) as pluss, kode_absensi FROM absensi_guru 
			WHERE timestamp >= "'.$start.'"
			AND timestamp <= "'.$end.'"
			AND DATE_FORMAT(timestamp,"%H:%i") > "07:05"			
			AND jenis = "in"
			GROUP BY kode_absensi';

		// $guru = 'SELECT DISTINCT nama, kode_absensi FROM absensi_guru 
		// 	WHERE timestamp >= "'.$start.'"
		// 	AND timestamp <= "'.$end.'"';

		return $query = $this->db->select('IFNULL(mins,0) as mins, IFNULL(pluss,0) as pluss, (IFNULL(mins,0)+IFNULL(pluss,0)) as count, nama, absensi_guru.kode_absensi')
			->from('absensi_guru')
			->where('timestamp >=', $start)
			->where('timestamp <=', $end)
			// ->where('DATE_FORMAT(timestamp,"%H:%i") >', '07:00')
			->where('jenis', 'in')
			// ->join('('.$guru.') guru', 'guru.kode_absensi=absensi_guru.kode_absensi', 'left')
			->join('('.$min5.') min5', 'min5.kode_absensi=absensi_guru.kode_absensi', 'left')
			->join('('.$plus5.') plus5', 'plus5.kode_absensi=absensi_guru.kode_absensi', 'left')			
			->order_by('count', 'DESC')
			->group_by('nama')
			->get()->result();
	}

	public function getDailyLate ($start, $end) {
		return $query = $this->db->select('count(*) as count, DATE_FORMAT(`timestamp`, "%Y-%m-%d") as day')
			->where('timestamp >=', $start)
			->where('timestamp <=', $end)
			->where('DATE_FORMAT(timestamp,"%H:%i") >', '07:00')
			->where('jenis', 'in')			
			->group_by('DATE_FORMAT(`timestamp`, "%Y-%m-%d")')
			->get('absensi_guru')->result();	
	}

	public function insert_batch ($data) {		
		foreach ($data as $in) {
			$dataInsert = array(
		        'nama' => $in['nama'],
		        'kode_absensi' => $in['kode_absensi'],
		        'jenis' => $in['jenis'],
		        'timestamp' => $in['timestamp'],
		        'id_sekolah' => $in['id_sekolah'],
		        'min' => (strtotime(date('H:i:s', strtotime($in['timestamp']))) > strtotime('7:00') && strtotime(date('H:i:s', strtotime($in['timestamp']))) < strtotime('7:06'))? 1:0,
		        'max' => (strtotime(date('H:i:s', strtotime($in['timestamp']))) > strtotime('7:05'))? 1:0,
		        'created_at' => date('Y-m-d H:i:s'),
		        'updated_at' => date('Y-m-d H:i:s')
			);
			if ($this->checkIfAbsenExist($in['kode_absensi'], date('Y-m-d', strtotime($in['timestamp'])), 'in')) {
				$this->db->insert('absensi_guru', $dataInsert);
			}
		}
		return true;		
	}

	public function checkIfAbsenExist ($kode, $time, $jenis) {
		$this->db->where('kode_absensi', $kode);
		$this->db->where('DATE_FORMAT(timestamp,"%Y-%m-%d")', $time);
		$this->db->where('jenis', $jenis);
		$this->db->from('absensi_guru');
		$num = $this->db->get()->num_rows();
		if ($num > 0) {
			return False;
		} else {
			return True;
		}
	}

	public function getListSiswa ($start, $end) {
		$this->db->where('kehadiran_timestamp >=', $start);
		$this->db->where('kehadiran_timestamp <=', $end);		
		$this->db->where('DATE_FORMAT(kehadiran_timestamp,"%H:%i") >', '07:05');
		$query = $this->db->get('kehadiran');
		return $query->result();
	}

	public function getDistinctDaySiswa ($start, $end) {
		$this->db->select('DISTINCT DATE(`kehadiran_timestamp`)');
		$this->db->where('kehadiran_timestamp >=', $start);
		$this->db->where('kehadiran_timestamp <=', $end);
		$query = $this->db->get('kehadiran');
		return $query->result();
	}

	public function getSiswa ($start, $end) {
		$this->db->where('kehadiran_timestamp >=', $start);
		$this->db->where('kehadiran_timestamp <=', $end);		
		$query = $this->db->get('kehadiran');
		return $query->result();
	}


	public function getDistinctDaySiswaPerKelas ($start, $end, $kelas) {
		$this->db->select('DISTINCT DATE(`kehadiran_timestamp`)');
		$this->db->where('kehadiran_timestamp >=', $start);
		$this->db->where('kehadiran_timestamp <=', $end);
		$this->db->where('kelas_siswa.id_kelas', $kelas);
		$this->db->join('pengguna', 'pengguna.nomor_induk=kehadiran.kehadiran_siswa');
		$this->db->join('kelas_siswa', 'pengguna.id_pengguna=kelas_siswa.id_siswa');
		$query = $this->db->get('kehadiran');
		return $query->result();
	}

	public function getSiswaPerKelas ($start, $end, $kelas) {
		$this->db->where('kehadiran_timestamp >=', $start);
		$this->db->where('kehadiran_timestamp <=', $end);
		$this->db->where('kelas_siswa.id_kelas', $kelas);
		$this->db->join('pengguna', 'pengguna.nomor_induk=kehadiran.kehadiran_siswa');
		$this->db->join('kelas_siswa', 'pengguna.id_pengguna=kelas_siswa.id_siswa');
		$query = $this->db->get('kehadiran');
		return $query->result();
	}

	public function getDailyLateSiswaPerKelas ($start, $end, $kelas) {
		return $query = $this->db->select('count(*) as count, DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d") as day')
			->where('kehadiran_timestamp >=', $start)
			->where('kehadiran_timestamp <=', $end)
			->where('DATE_FORMAT(kehadiran_timestamp,"%H:%i") >', '07:05')
			->where('kelas_siswa.id_kelas', $kelas)
			->join('pengguna', 'pengguna.nomor_induk=kehadiran.kehadiran_siswa')
			->join('kelas_siswa', 'pengguna.id_pengguna=kelas_siswa.id_siswa')
			->group_by('DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d")')
			->get('kehadiran')->result();	
	}

	public function getDailyLateSiswa ($start, $end) {
		return $query = $this->db->select('count(*) as count, DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d") as day')
			->where('kehadiran_timestamp >=', $start)
			->where('kehadiran_timestamp <=', $end)
			->where('DATE_FORMAT(kehadiran_timestamp,"%H:%i") >', '07:05')
			->group_by('DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d")')
			->get('kehadiran')->result();	
	}

	public function getPercentageTimeSiswa ($start, $end, $startTime, $endTime) {
		$query1 = $this->db->where('kehadiran_timestamp >=', $start)
			->where('kehadiran_timestamp <=', $end)
			->where('DATE_FORMAT(kehadiran_timestamp,"%H:%i") >', $startTime)
			->where('DATE_FORMAT(kehadiran_timestamp,"%H:%i") <=', $endTime);
		$query1 = $this->db->get('kehadiran');
		$data1 = $query1->num_rows();		
		return $data1;
	}

	public function getPeopleLateSiswa ($start, $end) {		
		return $query = $this->db->select('count(*) as count, nm_pengguna, nomor_induk, nm_kelas')
			->from('kehadiran')
			->where('kehadiran_timestamp >=', $start)
			->where('kehadiran_timestamp <=', $end)
			->where('DATE_FORMAT(kehadiran_timestamp,"%H:%i") >', '07:05')
			->join('pengguna', 'pengguna.nomor_induk=kehadiran.kehadiran_siswa')
			->join('kelas_siswa', 'kelas_siswa.id_siswa=pengguna.id_pengguna', 'left')
			->join('kelas', 'kelas.id_kelas=kelas_siswa.id_kelas', 'left')
			->order_by('count', 'DESC')
			->group_by('nm_pengguna')			
			->get()->result();
	}

	public function getPeopleLateSiswaByKelas ($start, $end, $kelas) {
		return $query = $this->db->select('IFNULL(COUNT(*), 0) as count, nm_pengguna, nomor_induk, nm_kelas')
			->from('kehadiran')			
			->where('kehadiran_timestamp >=', $start)
			->where('kehadiran_timestamp <=', $end)
			->where('DATE_FORMAT(kehadiran_timestamp,"%H:%i") >', '07:05')
			->where('kelas_siswa.id_kelas', $kelas)
			->join('pengguna', 'pengguna.nomor_induk=kehadiran.kehadiran_siswa', 'right')
			->join('kelas_siswa', 'kelas_siswa.id_siswa=pengguna.id_pengguna', 'left')
			->join('kelas', 'kelas.id_kelas=kelas_siswa.id_kelas', 'left')
			->order_by('count', 'DESC')
			->group_by('nm_pengguna')			
			->get()->result();
	}

	public function getTerlambatPerKelas ($start, $end) {
		return $query = $this->db->select('count(*) as count, nm_kelas')
			->from('kehadiran')
			->where('kehadiran_timestamp >=', $start)
			->where('kehadiran_timestamp <=', $end)
			->where('DATE_FORMAT(kehadiran_timestamp,"%H:%i") >', '07:05')
			->join('pengguna', 'pengguna.nomor_induk=kehadiran.kehadiran_siswa')
			->join('kelas_siswa', 'kelas_siswa.id_siswa=pengguna.id_pengguna', 'left')
			->join('kelas', 'kelas.id_kelas=kelas_siswa.id_kelas', 'left')
			->group_by('nm_kelas')			
			->get()->result();	
	}

	public function getKehadiranKelas ($start, $end) {
		// return $query = $this->db->select('count(*) as count, nm_kelas')
		// 	->from('kehadiran')
		// 	->where('kehadiran_timestamp >=', $start)
		// 	->where('kehadiran_timestamp <=', $end)			
		// 	->join('pengguna', 'pengguna.nomor_induk=kehadiran.kehadiran_siswa')
		// 	->join('kelas', 'kelas.id_kelas=pengguna.kelas_siswa', 'left')			
		// 	->group_by('nm_kelas')			
		// 	->get()->result();
		$sql1 = "
			SELECT count(*) FROM pengguna
			JOIN kelas_siswa on kelas_siswa.id_siswa=pengguna.id_pengguna
			WHERE kelas_siswa.id_kelas = kelas.id_kelas			
		";
		$sql2 = "
			SELECT count(DISTINCT DATE(kehadiran_timestamp)) 
			FROM kehadiran
			WHERE kehadiran_timestamp >= '".$start."'
			AND kehadiran_timestamp <= '".$end."'
		";
		$sql = "
			SELECT ((".$sql1.") * (".$sql2.")) - count(*) as count, nm_kelas, id_kelas FROM kehadiran
			JOIN pengguna ON pengguna.nomor_induk=kehadiran.kehadiran_siswa
			JOIN kelas_siswa on kelas_siswa.id_siswa=pengguna.id_pengguna
			LEFT JOIN kelas ON kelas.id_kelas=kelas_siswa.id_kelas
			WHERE kehadiran_timestamp >= ?
			AND kehadiran_timestamp <= ?
			GROUP BY nm_kelas, id_kelas
		";
		return $this->db->query($sql, array($start, $end))->result();
	}

	public function jurnal ($start, $end, $kelas) {
		return $query = $this->db->select('nm_mata_pelajaran as mapel, nm_pengguna, jadwal_guru.*, jurnal_guru_ekstra.*, DATE_FORMAT(jurnal_guru_ekstra.created_at, "%Y-%m-%d") as day')
			->from('jurnal_guru_ekstra')
			->where('jurnal_guru_ekstra.created_at >=', $start)
			->where('jurnal_guru_ekstra.created_at <=', $end)
			->where('jurnal_guru_ekstra.id_kelas', $kelas)			
			->join('jadwal_guru', 'jadwal_guru.id_jadwal_guru=jurnal_guru_ekstra.id_jadwal_guru')			
			->join('pengguna', 'pengguna.id_pengguna=jurnal_guru_ekstra.id_guru')
			->join('mata_pelajaran', 'mata_pelajaran.id_mata_pelajaran=jurnal_guru_ekstra.id_subjek')
			->order_by('jurnal_guru_ekstra.created_at', 'ASC')
			->order_by('jadwal_guru.hari', 'ASC')
			->get()->result();	
	}

	public function getJurnalLate ($start, $end, $kelas) {
		return $query = $this->db->select('sum(jadwal_guru.jam_mulai + INTERVAL 15 MINUTE < jurnal_guru_ekstra.jam_hadir) as count')
			->from('jurnal_guru_ekstra')
			->where('jurnal_guru_ekstra.created_at >=', $start)
			->where('jurnal_guru_ekstra.created_at <=', $end)
			->where('jurnal_guru_ekstra.id_kelas', $kelas)			
			->join('jadwal_guru', 'jadwal_guru.id_jadwal_guru=jurnal_guru_ekstra.id_jadwal_guru')
			->get()->result();
	}

	public function getJurnalLateByPeriod ($start, $end, $kelas) {
		return $query = $this->db->select('sum(jadwal_guru.jam_mulai + INTERVAL 15 MINUTE < jurnal_guru_ekstra.jam_hadir) as count, jadwal_guru.jam_mulai, jadwal_guru.jam_selesai')
			->from('jurnal_guru_ekstra')
			->where('jurnal_guru_ekstra.created_at >=', $start)
			->where('jurnal_guru_ekstra.created_at <=', $end)
			->where('jurnal_guru_ekstra.id_kelas', $kelas)			
			->join('jadwal_guru', 'jadwal_guru.id_jadwal_guru=jurnal_guru_ekstra.id_jadwal_guru')
			->group_by('jadwal_guru.jam_mulai')
			->get()->result();	
	}

	public function getJurnalLateByPerson ($start, $end) {
		return $query = $this->db->select('sum(jadwal_guru.jam_mulai + INTERVAL 11 MINUTE < jurnal_guru_ekstra.jam_hadir) as count, pengguna.nm_pengguna, jurnal_guru_ekstra.id_guru')
			->from('jurnal_guru_ekstra')
			->where('jurnal_guru_ekstra.created_at >=', $start)
			->where('jurnal_guru_ekstra.created_at <=', $end)			
			->join('jadwal_guru', 'jadwal_guru.id_jadwal_guru=jurnal_guru_ekstra.id_jadwal_guru')
			->join('pengguna', 'pengguna.id_pengguna=jurnal_guru_ekstra.id_guru')
			->group_by('pengguna.nm_pengguna')
			->get()->result();	
	}

	public function getJurnalLateByPerson_new ($start, $end) {
		return $this->db->query("SELECT sum(jadwal_guru.jam_mulai + INTERVAL 11 MINUTE < jurnal_guru_ekstra.jam_hadir) as count, jurnal_guru_ekstra.id_guru, nm_pengguna FROM jurnal_guru_ekstra JOIN jadwal_guru ON jadwal_guru.id_jadwal_guru=jurnal_guru_ekstra.id_jadwal_guru JOIN pengguna ON pengguna.id_pengguna=jurnal_guru_ekstra.id_guru WHERE jurnal_guru_ekstra.created_at >= '".$start."' AND jurnal_guru_ekstra.created_at <= '".$end."' GROUP BY jurnal_guru_ekstra.id_guru ORDER BY count DESC")->result();
	}
}