<?php
class M_DataDashboard extends CI_Model
{
        public function dataSiswa()
    {
        $this->db->select('nomor_induk, pengguna.id_pengguna');
        $this->db->from('pengguna');
        $this->db->join('pengguna_sekolah', 'pengguna.id_pengguna=pengguna_sekolah.id_pengguna', 'left');
        $this->db->where('pengguna_sekolah.id_sekolah=1');
        $this->db->where('id_role=1');
        return $this->db->get();
    }
    
    public function dataHadir()
    {
        $this->db->select('*');
        $this->db->from('kehadiran');
        $this->db->where('id_sekolah = 1');
		$this->db->order_by('kehadiran_timestamp', 'DESC');
        return $this->db->get();
    }
    
    public function dataKelas()
    {
        $this->db->select('*');
        $this->db->from('kelas');
        $this->db->where('id_sekolah=1');
        $this->db->order_by('id_kelas', 'ASC');
        return $this->db->get();
    }
    
    public function dataKelasSiswa()
    {
        $this->db->select('*');
        $this->db->from('kelas_siswa');
        return $this->db->get();
    }
    
    public function dataPengguna()
    {
        $this->db->select('*');
        $this->db->from('pengguna');
        return $this->db->get();
    }
    
    public function dataPenggunaSekolah()
    {
        $this->db->select('*');
        $this->db->from('pengguna_sekolah');
        return $this->db->get();
    }
    
    public function jurnalGuru()
    {
        $this->db->select('*');
        $this->db->from('jurnal_guru_ekstra');
        return $this->db->get();
    }
}
?>