<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurnal extends CI_Model {

	public function getAllKelas($id_sekolah, $tahunAjaran, $andRombel = FALSE)
	{
		$this->db->distinct();
		$this->db->select('nm_kelas');
		$this->db->from('kelas');
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where('tahun_ajaran', $tahunAjaran);
		if($andRombel)
		{
			$kelas = $this->db->get()->result_array();

			$this->db->distinct();
			$this->db->select('rombel_kelas');
			$this->db->from('kelas');
			$this->db->where('id_sekolah', $id_sekolah);
			$this->db->where('tahun_ajaran', $tahunAjaran);

			$rombel = $this->db->get()->result_array();

			return array('kelas' => $kelas, 'rombel' => $rombel);
		}
		else
			return $this->db->get()->result_array();

	}
	public function delete($table, $where){
        $res = $this->db->delete($table, $where); // Kode ini digunakan untuk menghapus record yang sudah ada
        return $res;
    }

    public function getKetuaWali($id_kelas)
    {
    	$this->db->select('wali.nm_pengguna as nama_wali, ketua.nm_pengguna as nama_ketua');
    	$this->db->from('kelas');
    	$this->db->join('pengguna as wali', 'kelas.wali_kelas = wali.id_pengguna');
    	$this->db->join('pengguna as ketua', 'kelas.id_ketua_kelas = ketua.id_pengguna');
    	$this->db->where('id_kelas', $id_kelas);

    	return $this->db->get()->result_array();
    }

    public function getJadwalPiket($id_sekolah, $id_guru, $tahunAjaran, $tanggal)
    {
    	date_default_timezone_set('Asia/Jakarta');
		//$minggu_ke = date('W');
		$hari = date('N', strtotime($tanggal));
		$this->db->select('ketua.nm_pengguna as nama_ketua, guru.nm_pengguna as nama_guru, nm_mata_pelajaran, jam_mulai, jam_selesai, nm_kelas, rombel_kelas, jenis_mata_pelajaran, kelas.id_kelas, jadwal_guru.id_jadwal_guru, jadwal_guru.periode, jurnal_guru_ekstra.id_jurnal');
		$this->db->from('jadwal_guru');
		$this->db->join('jadwal_sekolah', 'jadwal_sekolah.id_jadwal_sekolah = jadwal_guru.id_jadwal_sekolah');
		$this->db->join('mata_pelajaran', 'mata_pelajaran.id_mata_pelajaran = jadwal_guru.id_mata_pelajaran');
		$this->db->join('kelas', 'jadwal_guru.id_kelas = kelas.id_kelas');
		//$this->db->join('pengguna as wali', 'kelas.wali_kelas = wali.id_pengguna', 'left');
    	$this->db->join('pengguna as ketua', 'kelas.id_ketua_kelas = ketua.id_pengguna', 'left');
    	$this->db->join('pengguna as guru', 'jadwal_guru.id_guru = guru.id_pengguna', 'left');
    	$this->db->join("jurnal_guru_ekstra", "jurnal_guru_ekstra.id_jadwal_guru = jadwal_guru.id_jadwal_guru AND jurnal_guru_ekstra.tanggal_jurnal = '" . $tanggal . "'", 'left');
		//$this->db->where("jadwal_guru.id_jadwal_guru NOT IN (SELECT id_jadwal_guru from jurnal_guru_ekstra where id_guru = '$guru' AND hari = '$hari' AND minggu_ke = '$minggu_ke')");
		$this->db->where('jadwal_guru.id_sekolah', $id_sekolah);
		$this->db->where('jadwal_guru.tahun_ajaran', $tahunAjaran);
		$this->db->where('jadwal_sekolah.status', 1);
		$this->db->where('jadwal_guru.hari', $hari);
		$this->db->where('jadwal_guru.id_guru !=', $id_guru);
		$this->db->where('jurnal_guru_ekstra.id_jurnal', NULL);
		
		
		$query = $this->db->get();
		//$query 	= $this->db->query("select * from jadwal_guru join mata_pelajaran on mata_pelajaran.id_mata_pelajaran = jadwal_guru.id_mata_pelajaran join kelas on kelas.id_kelas = jadwal_guru.id_kelas WHERE id_jadwal_guru NOT IN (SELECT id_jadwal_guru from jurnal_guru_ekstra where id_guru = '$guru' AND hari = '$hari' and minggu_ke = '$minggu_ke') AND hari = '$hari' AND id_guru = '$guru'");
		return $query->result();
    }

	public function getListJadwal($guru, $id_sekolah, $tahunAjaran, $tanggal = null) {
		date_default_timezone_set('Asia/Jakarta');
		$minggu_ke = date('W');
		$hari = date('N');
		$this->db->select('ketua.nm_pengguna as nama_ketua, nm_mata_pelajaran, jam_mulai, jam_selesai, nm_kelas, rombel_kelas, jadwal_guru.periode, jenis_mata_pelajaran, kelas.id_kelas, id_jadwal_guru');
		$this->db->from('jadwal_guru');
		$this->db->join('jadwal_sekolah', 'jadwal_sekolah.id_jadwal_sekolah = jadwal_guru.id_jadwal_sekolah');
		$this->db->join('mata_pelajaran', 'mata_pelajaran.id_mata_pelajaran = jadwal_guru.id_mata_pelajaran');
		$this->db->join('kelas', 'jadwal_guru.id_kelas = kelas.id_kelas');
		//$this->db->join('pengguna as wali', 'kelas.wali_kelas = wali.id_pengguna', 'left');
    	$this->db->join('pengguna as ketua', 'kelas.id_ketua_kelas = ketua.id_pengguna', 'left');
    	$this->db->where("jadwal_guru.id_jadwal_guru NOT IN (SELECT id_jadwal_guru from jurnal_guru_ekstra where tanggal_jurnal = '$tanggal')");
		//$this->db->where("jadwal_guru.id_jadwal_guru NOT IN (SELECT id_jadwal_guru from jurnal_guru_ekstra where id_guru = '$guru' AND hari = '$hari' AND minggu_ke = '$minggu_ke')");
		$this->db->where('jadwal_guru.id_sekolah', $id_sekolah);
		$this->db->where('jadwal_guru.tahun_ajaran', $tahunAjaran);
		$this->db->where('jadwal_sekolah.status', 1);
		$this->db->where('jadwal_guru.hari', $hari);
		$this->db->where('jadwal_guru.id_guru', $guru);
		
		
		$query = $this->db->get();
		//$query 	= $this->db->query("select * from jadwal_guru join mata_pelajaran on mata_pelajaran.id_mata_pelajaran = jadwal_guru.id_mata_pelajaran join kelas on kelas.id_kelas = jadwal_guru.id_kelas WHERE id_jadwal_guru NOT IN (SELECT id_jadwal_guru from jurnal_guru_ekstra where id_guru = '$guru' AND hari = '$hari' and minggu_ke = '$minggu_ke') AND hari = '$hari' AND id_guru = '$guru'");
		return $query->result();
	}

	public function getListJadwalGuru($guru,$tanggal = null) {
		date_default_timezone_set('Asia/Jakarta');
		// $minggu_ke 	= date('W');
		// $tanggal 	= date('N');
		// $query 	= $this->db->query("SELECT * from jurnal_guru_ekstra  join mata_pwhere id_guru = '$guru' AND hari = '$tanggal' and minggu_ke = '$minggu_ke'");
		// return $query->result();
	}

	public function cekLog ($data) 
	{
		$this->db->insert('jurnal_guru_ekstra', $data);
		return $this->db->insert_id();
	}

	public function inputJurnal($id_jurnal,$data)
	{
		date_default_timezone_set('Asia/Jakarta');
		$status = 1;
		if(!is_null($data['kompetensi_dasar']) || !is_null($data['catatan']) || !is_null($data['tugas']))
			$status = 3;
		$data = array(
			'kompetensi_dasar' => $data['kompetensi_dasar'],
			'catatan' => $data['catatan'],
			'tugas' => $data['tugas'],
			'status' => $status,
		    'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->where('id_jurnal', $id_jurnal);
		$this->db->update('jurnal_guru_ekstra', $data);
	}

	public function exitKelas($id_jurnal){
		date_default_timezone_set('Asia/Jakarta');
		$data 	= array(
			'status'		=> '3',
			'updated_at'	=> date('Y-m-d H:i:s'),
			'jam_keluar'	=> date('H:i:s')
		);
		$this->db->where('id_jurnal', $id_jurnal);
		$this->db->update('jurnal_guru_ekstra', $data);
	}

	public function cekJadwalGuru($id_jadwal_guru){
		$this->db->select('id_jadwal_guru, id_guru');
		$this->db->where('id_jadwal_guru', $id_jadwal_guru);
		$this->db->from('jadwal_guru');
		return $this->db->get()->result_array();	
	}

	public function cekJurnal($id_jadwal_guru, $tanggal){
		$this->db->select('id_jurnal');
		$this->db->where('id_jadwal_guru', $id_jadwal_guru);
		$this->db->where('tanggal_jurnal', $tanggal);
		$this->db->from('jurnal_guru_ekstra');
		return $this->db->get()->result_array();	
	}

	public function cekWaliKelas($id_guru, $id_sekolah, $tahunAjaran, $piket = FALSE)
	{
		$this->db->select('*');
		$this->db->from('kelas');
		$this->db->where('wali_kelas', $id_guru);
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where('tahun_ajaran', $tahunAjaran);
		$wali = $this->db->get()->result();
		// $this->db->where('status <','3');
		if($piket)
		{
			$this->db->select('*');
			$this->db->from('jadwal_piket');
			$this->db->where('id_guru', $id_guru);
			$this->db->where('tahun_ajaran', $tahunAjaran);
			$this->db->where('hari', date('N'));
			$piket = $this->db->get()->result();
			return array('wali' => $wali, 'piket' => $piket);
		}
		else
			return $wali;	
	}

	public function inputUjian($id_jurnal, $data)
	{
		date_default_timezone_set('Asia/Jakarta');
		$data 		= array(
			'id_jurnal'				=>	$id_jurnal,
			'jenis_ujian'			=>	$data['jenis_ujian'],
			'deskripsi_ujian'		=>	$data['deskripsi_ujian'],
			'status'				=> 	'0',
		    'created_at' 			=> date('Y-m-d H:i:s'),
		    'updated_at' 			=> date('Y-m-d H:i:s')
		);
		$this->db->insert('ujian_siswa', $data);
	}

	public function inputTugas($id_jurnal, $data)
	{
		date_default_timezone_set('Asia/Jakarta');
		$data 		= array(
			'id_jurnal'				=>	$id_jurnal,
			'jenis_tugas'			=>	$data['jenis_tugas'],
			'deskripsi_tugas'		=>	$data['deskripsi_tugas'],
			'status'				=> 	'0',
		    'created_at' 			=> 	date('Y-m-d H:i:s'),
		    'updated_at' 			=> 	date('Y-m-d H:i:s')
		);
		$this->db->insert('tugas_siswa', $data);
	}

	public function getKelasBySiswa ($id_siswa, $id_sekolah)
	{
		$this->db->select('*');
		$this->db->where('kelas_siswa.id_siswa', $id_siswa);
		$this->db->where('kelas.id_sekolah', $id_sekolah);
		$this->db->from('kelas_siswa');
		$this->db->join('kelas', 'kelas.id_kelas = kelas_siswa.id_kelas');
		$this->db->join('pengguna', 'kelas.wali_kelas = pengguna.id_pengguna');
		return $this->db->get()->row();
	}

	public function getKelasById ($id) {
		$this->db->where('id_kelas', $id);
		$this->db->from('kelas');
		$this->db->join('pengguna', 'kelas.wali_kelas = pengguna.id_pengguna');
		return $this->db->get()->row();
	}

	public function getJurnal( $result, $ids, $tahunAjaran = null){
		if($result == null){
			$this->db->select('*');
			$this->db->from('jurnal_guru_ekstra');
			$this->db->join('jadwal_guru', 'jurnal_guru_ekstra.id_jadwal_guru = jadwal_guru.id_jadwal_guru', 'left');
			$this->db->join('mata_pelajaran', 'jadwal_guru.id_mata_pelajaran = mata_pelajaran.id_mata_pelajaran', 'left');
			$this->db->join('pengguna', 'jurnal_guru_ekstra.id_guru_piket = pengguna.id_pengguna', 'left');
			$this->db->where('jurnal_guru_ekstra.id_jurnal', $ids);
			/*
			$this->db->join('tugas_siswa', 'tugas_siswa.id_jurnal = jurnal_guru_ekstra.id_jurnal','left');
			$this->db->join('ujian_siswa', 'ujian_siswa.id_jurnal = jurnal_guru_ekstra.id_jurnal','left');*/
			
			return $this->db->get()->row();			
		}
		elseif($result == 1)
		{
			$this->db->select('jurnal_guru_ekstra.id_jurnal, mata_pelajaran.nm_mata_pelajaran, kelas.nm_kelas, kelas.rombel_kelas, jadwal_guru.hari, jadwal_guru.jam_mulai, jadwal_guru.jam_selesai, jurnal_guru_ekstra.jam_hadir, jadwal_guru.periode, jurnal_guru_ekstra.created_at, jurnal_guru_ekstra.tanggal_jurnal, jurnal_guru_ekstra.status');
			$this->db->from('jurnal_guru_ekstra');
			$this->db->join('jadwal_guru','jurnal_guru_ekstra.id_jadwal_guru = jadwal_guru.id_jadwal_guru');
			$this->db->join('pengguna', 'jurnal_guru_ekstra.id_guru = pengguna.id_pengguna');
			$this->db->join('kelas', 'jurnal_guru_ekstra.id_kelas = kelas.id_kelas');
			$this->db->join('mata_pelajaran', 'jadwal_guru.id_mata_pelajaran = mata_pelajaran.id_mata_pelajaran');
			$this->db->where('jurnal_guru_ekstra.id_guru',$ids );
			$this->db->where('jadwal_guru.tahun_ajaran',$tahunAjaran );
			return $this->db->get()->result();
		}
		else
		{
			$this->db->select('jurnal_guru_ekstra.id_jurnal, mata_pelajaran.nm_mata_pelajaran, jurnal_guru_ekstra.id_kelas, kelas.nm_kelas, kelas.rombel_kelas, jadwal_guru.hari, jadwal_guru.jam_mulai, jadwal_guru.jam_selesai, jurnal_guru_ekstra.jam_hadir, jurnal_guru_ekstra.created_at, jurnal_guru_ekstra.tanggal_jurnal, jurnal_guru_ekstra.status, jadwal_guru.periode, jurnal_guru_ekstra.kompetensi_dasar, jurnal_guru_ekstra.catatan, jurnal_guru_ekstra.tugas, pengguna.nm_pengguna');
			$this->db->from('jurnal_guru_ekstra');
			$this->db->join('jadwal_guru','jurnal_guru_ekstra.id_jadwal_guru = jadwal_guru.id_jadwal_guru');
			$this->db->join('pengguna', 'jurnal_guru_ekstra.id_guru_piket = pengguna.id_pengguna', 'left');
			$this->db->join('kelas', 'jurnal_guru_ekstra.id_kelas = kelas.id_kelas');
			$this->db->join('mata_pelajaran', 'jadwal_guru.id_mata_pelajaran = mata_pelajaran.id_mata_pelajaran');
			$this->db->where('jurnal_guru_ekstra.id_jurnal',$ids );
			return $this->db->get()->result();
		}
	}

	public function getSiswaAbsen($id_sekolah, $tanggal, $tahunAjaran, $kelas = null, $rombel = null, $status = null)
	{
		$this->db->select('*');
		$this->db->from('kehadiran');
		$this->db->join('pengguna_sekolah', 'kehadiran.kehadiran_siswa = pengguna_sekolah.nomor_induk');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('kelas_siswa', 'pengguna.id_pengguna = kelas_siswa.id_siswa', 'left');
		$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas', 'left');

		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('date(kehadiran_timestamp)', $tanggal);
		$this->db->where('kelas.tahun_ajaran', $tahunAjaran);
		if(!is_null($kelas))
			$this->db->where('nm_kelas', $kelas);

		if(!is_null($rombel))
			$this->db->where('rombel_kelas', $rombel);

		if(!is_null($status))
			$this->db->where('kehadiran_verification', $status);
		return $this->db->get();
	}

	public function getSiswaBelumAbsen($id_sekolah, $tahunAjaran, $nisAbsen = array(), $kelas = null, $rombel = null)
	{
		$this->db->select('*');
		$this->db->from('pengguna_sekolah');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('kelas_siswa', 'pengguna.id_pengguna = kelas_siswa.id_siswa', 'left');
		$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas', 'left');

		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('pengguna_sekolah.id_role', 1);
		$this->db->where('kelas.tahun_ajaran', $tahunAjaran);
		if(count($nisAbsen)>0)
			$this->db->where_not_in('pengguna_sekolah.nomor_induk', $nisAbsen);
		if(!is_null($kelas))
			$this->db->where('nm_kelas', $kelas);

		if(!is_null($rombel))
			$this->db->where('rombel_kelas', $rombel);

		return $this->db->get();
	}
	
	public function showKehadiranSiswa($id_kelas, $date){
		$this->db->select('*');
		$this->db->from('kelas_siswa');
		$this->db->join('pengguna', 'kelas_siswa.id_siswa = pengguna.id_pengguna');
		$this->db->join('pengguna_sekolah', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas');
		$this->db->join('kehadiran', 'kehadiran.kehadiran_siswa = pengguna_sekolah.nomor_induk','left');
		$this->db->where('kelas_siswa.id_kelas', $id_kelas);
		$this->db->where('kelas.id_sekolah = pengguna_sekolah.id_sekolah');
		$this->db->like('kehadiran_timestamp', $date, 'after');
		
		return $this->db->get()->result();
	}

	public function getKehadiranJurnalAll($data)
	{
		$this->db->select('kehadiran_jurnal.id_kehadiran_jurnal, kehadiran_jurnal.id_jurnal, kehadiran_jurnal.id_kelas, kehadiran_jurnal.id_kehadiran, kehadiran_jurnal.kehadiran_verification, kehadiran_jurnal.tanggal_kehadiran, kehadiran_jurnal.jam_kehadiran, kehadiran.kehadiran_siswa, kehadiran.kehadiran_verification as absen_real, kehadiran.kehadiran_timestamp, kehadiran.id_sekolah, kehadiran.id_pengguna_pengabsen');
		$this->db->from('kehadiran_jurnal');
		$this->db->join('kehadiran', 'kehadiran_jurnal.id_kehadiran = kehadiran.kehadiran_id');
		$this->db->where('id_jurnal', $data['id_jurnal']);
		$check = $this->db->get()->result();
		$type = 'jurnal';
		if(count($check) < 1)
		{
			$this->db->select('kehadiran_jurnal.id_kehadiran_jurnal, kehadiran_jurnal.id_jurnal, kehadiran_jurnal.id_kelas, kehadiran_jurnal.id_kehadiran, kehadiran_jurnal.kehadiran_verification, kehadiran_jurnal.tanggal_kehadiran, kehadiran_jurnal.jam_kehadiran, kehadiran.kehadiran_siswa, kehadiran.kehadiran_verification as absen_real, kehadiran.kehadiran_timestamp, kehadiran.id_sekolah, kehadiran.id_pengguna_pengabsen');
			$this->db->from('kehadiran_jurnal');
			$this->db->join('kehadiran', 'kehadiran_jurnal.id_kehadiran = kehadiran.kehadiran_id');
			$this->db->where('id_jurnal !=', $data['id_jurnal']);
			$this->db->where('tanggal_kehadiran', $data['tanggal']);
			$this->db->where('id_kelas', $data['id_kelas']);
			$this->db->where('jam_kehadiran <', $data['jam_kehadiran']);
			$this->db->order_by('jam_kehadiran', 'DESC');
			$check = $this->db->get()->result();
			$type = 'kehadiran';
		}
		return array('kehadiranJurnal' => $check, 'type' => $type);
	}

	public function getKehadiran($id_sekolah, $nomor_induk, $tanggal)
	{
		$this->db->select('*');
		$this->db->from('kehadiran');
		$this->db->where('kehadiran_siswa', $nomor_induk);
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->where('date(kehadiran_timestamp)' , $tanggal);
		return $this->db->get()->result_array();
	}

	public function getKehadiranKelas($id_sekolah, $id_kelas, $tanggal)
	{
		$this->db->select('*');
		$this->db->from('kelas_siswa');
		$this->db->join('pengguna', 'kelas_siswa.id_siswa = pengguna.id_pengguna', 'left');
		$this->db->join('pengguna_sekolah', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna', 'left');
		$this->db->join('kehadiran', "pengguna_sekolah.nomor_induk = kehadiran.kehadiran_siswa AND date(kehadiran.kehadiran_timestamp) = '".$tanggal."'", 'left');
		$this->db->where('kelas_siswa.id_kelas', $id_kelas);
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->order_by('pengguna.nm_pengguna', 'ASC');
		return $this->db->get()->result();
	}

	public function getKehadiranJurnal($id_jurnal, $id_kehadiran, $tanggal)
	{
		$this->db->select('*');
		$this->db->from('kehadiran_jurnal');
		$this->db->where('id_jurnal', $id_jurnal);
		$this->db->where('id_kehadiran', $id_kehadiran);
		$this->db->where('tanggal_kehadiran' , $tanggal);
		return $this->db->get()->result();
	}

	public function updateKehadiranJurnal($data, $baru = FALSE)
	{
		if($baru)
		{
			$this->db->insert('kehadiran_jurnal', $data);
			return $this->db->insert_id();
		}
		else
		{
			$this->db->update('kehadiran_jurnal', $data['update'], $data['where']);
			return $data['where']['id_kehadiran_jurnal'];
		}
	}

	public function addKehadiranJurnal($data, $baru = FALSE)
	{
		if($baru)
		{
			$temp = $data['kehadiran'];
			$this->db->insert('kehadiran', $temp);
			$data = $data['kehadiranJurnal'];
			$data['id_kehadiran'] = $this->db->insert_id();
		}
		$this->db->insert('kehadiran_jurnal', $data);
		return $this->db->insert_id();
	}

	public function siswaTidakAbsen($id_kelas, $nomor_induk){
		date_default_timezone_set("Asia/Jakarta"); 
        $now = date('Y-m-d');
        $this->db->select('*');
		$this->db->where('kelas_siswa.id_kelas', $id_kelas);
		if(count($nomor_induk)>0)
			$this->db->where_not_in('pengguna_sekolah.nomor_induk', $nomor_induk);
		$this->db->from('pengguna_sekolah');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('kelas_siswa', 'kelas_siswa.id_siswa = pengguna.id_pengguna');
		$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas');
		return $this->db->get()->result();
		//$query = $this->db->query("SELECT * FROM pengguna as p, kelas_siswa as ks, kelas as kl WHERE ks.id_kelas = '$id_kelas' AND p.id_pengguna=ks.id_siswa AND ks.id_kelas = kl.id_kelas AND p.nomor_induk NOT IN (SELECT kehadiran_siswa FROM kehadiran WHERE kehadiran_timestamp LIKE '$now%' )");
		//return $query->result();
	}

	public function siswaAbsen($siswa){
		$this->db->insert('kehadiran', $siswa);
		return $this->db->insert_id();
	}
}