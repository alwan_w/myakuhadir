<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelasSiswa extends CI_Model {

	public function insert ($jadwal) {
		$data = array(
		        'id_kelas'	 => $jadwal['id_kelas'],
		        'id_siswa'	 => $jadwal['id_siswa'],
		        'created_at' => date('Y-m-d H:i:s'),
		        'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->insert('kelas_siswa', $data);
	}
	public function getSiswa($id_sekolah){
		$this->db->select('*');
		$this->db->from('pengguna_sekolah');
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('pengguna_sekolah.id_role', 1);
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('role', 'role.id_role = pengguna_sekolah.id_role');
		return $this->db->get()->result();
	}

	public function getList () {
		$this->db->select('*');
		$this->db->from('kelas_siswa');
		$this->db->join('pengguna', 'kelas_siswa.id_siswa = pengguna.id_pengguna');
		$this->db->join('kelas', 'kelas_siswa.id_kelas = kelas.id_kelas');
		// $this->db->join('mata_pelajaran', 'jadwal_guru.id_mata_pelajaran = mata_pelajaran.id_mata_pelajaran');
		// $this->db->join('jam_pelajaran', 'jadwal_guru.id_jam_pelajaran = jam_pelajaran.id_jam_pelajaran');
		$query = $this->db->get();
		return $query->result();
	}

	public function delete ($id) {
		$this->db->where('id_kelas_siswa', $id);
		$this->db->delete('kelas_siswa');
	}

	public function getKaryawanById ($id) {
		$this->db->where('id_kelas', $id);
		$this->db->from('kelas');
		return $this->db->get()->result();
	}

	public function update ($id, $kelas) {		
		$this->db->set('nm_kelas', $kelas);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id_kelas', $id);
		$this->db->update('kelas');
	}
}