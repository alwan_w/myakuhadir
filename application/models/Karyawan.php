<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Model {

	public function addExcel($id_sekolah, $data, $new = FALSE)
	{
		$pengguna = array(
			'nm_pengguna' => $data['nm_pengguna'],
			'updated_at' => $data['date']
		);
		if(!is_null($data['password']))
			$pengguna['password'] = md5($data['password']);
		if(!is_null($data['no_hp']))
			$pengguna['no_hp'] = $data['no_hp'];
		if(!is_null($data['alamat']))
			$pengguna['alamat'] = $data['alamat'];
		
		$penggunaSekolah = array(
			'id_role' => 3,
			//'status' => 1,
			'updated_at' => $data['date']
		);
		if($data['role'] == 'Admin')
			$penggunaSekolah['id_role'] = 2;
		if($data['role'] == 'Wakasek')
			$penggunaSekolah['id_role'] = 5;
		if($data['role'] == 'Kepsek')
			$penggunaSekolah['id_role'] = 6;
		if($data['status'] == 'Tidak Aktif')
			$penggunaSekolah['status'] = 0;
		if($new)
		{
			if(!isset($pengguna['password']))
				$pengguna['password'] = md5($data['nomor_induk']);
			$pengguna['email'] = $data['email'];
			$pengguna['created_at'] = $data['date'];
			$this->db->insert('pengguna', $pengguna);

			$penggunaSekolah['id_pengguna'] = $this->db->insert_id();
			$penggunaSekolah['id_sekolah'] = $id_sekolah;
			$penggunaSekolah['nomor_induk'] = $data['nomor_induk'];
			$penggunaSekolah['created_at'] = $data['date'];
			return $this->db->insert('pengguna_sekolah', $penggunaSekolah);
		}
		else
		{
			if($data['type'] == 'email')
				$penggunaSekolah['nomor_induk'] = $data['nomor_induk'];
			else
				$pengguna['email'] = $data['email'];

			$where = array(
				'id_pengguna' => $data['id_pengguna']
			);
			$this->db->update('pengguna', $pengguna, $where);
			$where['id_sekolah'] = $id_sekolah;
			return $this->db->update('pengguna_sekolah', $penggunaSekolah, $where);
		}
	}
	public function getOwnedPengguna($id_sekolah, $emailPengguna, $nomorIndukPengguna)
	{
		$this->db->select('*');
		$this->db->from('pengguna');
		$this->db->join('pengguna_sekolah', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna', 'left');
		$this->db->where('id_sekolah', $id_sekolah);
		if(count($emailPengguna)>0)
		{
			$this->db->where_in('pengguna.email', $emailPengguna);
			if(count($nomorIndukPengguna)>0)
				$this->db->or_where_in('pengguna_sekolah.nomor_induk', $nomorIndukPengguna);
		}
		elseif(count($nomorIndukPengguna)>0)
			$this->db->where_in('pengguna_sekolah.nomor_induk', $nomorIndukPengguna);
		return $this->db->get();
	}
	public function getRoleWhere($where, $indexId = 0)
	{
		$this->db->select('*');
		$this->db->from('role');
		$this->db->where($where);
		$query = $this->db->get()->result();
		if($indexId = 1)
		{
			$result = array();
			foreach($query as $data)
				$result[$data->id_role] = $data->nm_role;
			return $result;
		}
		else
			return $query;
	}
	function cekLogin($user_login, $password)
	{	
		$query=$this->db->query("SELECT * FROM pengguna WHERE (email='$user_login' OR no_hp='$user_login') AND password='$password' LIMIT 1");
        
        return $query;
	}
	function cekPengguna($user_login){
		$this->db->select('*');
		$this->db->from('pengguna');
		$this->db->where('email', $user_login);
		$this->db->or_where('no_hp', $user_login);
		// return $this->db->get()->result();
		return $this->db->get()->row();
	}

	function checkPassword($id,$password){
		$this->db->select('*');
		$this->db->from('pengguna');
		$this->db->where('password', $password);
		$this->db->where('id_pengguna', $id);
		return $this->db->get()->result_array();
	}

	function updatePassword($id,$password){
		$this->db->set('password', $password);
		$this->db->where('id_pengguna', $id);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->update('pengguna');
	}

	public function updateDefault($id_pengguna, $id_sekolah)
	{
		$this->db->set('default_sekolah', $id_sekolah);
		$this->db->where('id_pengguna', $id_pengguna);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->update('pengguna');
	}
	
	public function insert ($data) {
		$pengguna = array(
	        //'id_role' => $data['role'],
	        'nm_pengguna' => $data['nama'],
	        'alamat' => $data['alamat'],
	        'no_hp' => $data['phone'],
	        'email' => $data['email'],
	        'password' => $data['password'],		        
	        'created_at' => date('Y-m-d H:i:s'),
	        'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->insert('pengguna', $pengguna);
		$id_pengguna = $this->db->insert_id();
		$data = array(
			'id_pengguna' => $id_pengguna,
			'id_role' => $data['role'],
			'id_sekolah' => $data['sekolah'],
			'nomor_induk' => $data['nomor'],
			'status' => 1,
			'created_at' => date('Y-m-d H:i:s'),
	        'updated_at' => date('Y-m-d H:i:s')
		);
		$this->db->insert('pengguna_sekolah', $data);
	}
	public function insertExcel($data) {		
		foreach ($data as $in) {
			$dataInsert = array(
		        'nm_pengguna' 	=> $in['nama'],
		        'alamat' 		=> $in['alamat'],
		        'no_hp' 		=> $in['phone'],
		        'password' 		=> $in['password'],		        
		        'created_at' 	=> date('Y-m-d H:i:s'),
		        'updated_at' 	=> date('Y-m-d H:i:s')
			);
			$this->db->insert('pengguna', $dataInsert);
			$id_pengguna = $this->db->insert_id();
			$temp = array(
				'id_pengguna' => $id_pengguna,
				'id_role' => $in['role'],
				'id_sekolah' => $in['sekolah'],
		        'nomor_induk' 	=> $in['nomor'],
		        'status' => 1,
				'created_at' 	=> date('Y-m-d H:i:s'),
		        'updated_at' 	=> date('Y-m-d H:i:s')
			);
			$this->db->insert('pengguna_sekolah', $temp);
		}
		return true;		
	}

	public function getList ($id_sekolah, $qr = FALSE) {
		$this->db->select('*');
		$this->db->from('pengguna_sekolah');
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('pengguna_sekolah.id_role >=', 2);
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->join('role', 'role.id_role = pengguna_sekolah.id_role');
		$this->db->order_by('pengguna.nm_pengguna', 'ASC');
		$query = $this->db->get();
		if($qr)
			return $query;
		return $query->result();
	}

	public function getIdRoleSekolah ($id_pengguna, $id_sekolah = 0) {
		$this->db->select('*');
		$this->db->from('pengguna_sekolah');
		$this->db->where('id_pengguna', $id_pengguna);
		if($id_sekolah>0)
			$this->db->where('id_sekolah', $id_sekolah);
		$this->db->join('sekolah', 'pengguna_sekolah.id_sekolah = sekolah.id_sekolah');
		$query = $this->db->get();

		return $query->result();
	}

	public function delete ($id, $id_sekolah) {
		$this->db->where('id_pengguna', $id);
		$this->db->where('id_sekolah', $id_sekolah);
		$this->db->delete('pengguna_sekolah');
	}

	public function getKaryawanById ($id, $id_sekolah, $qr = FALSE) {
		$this->db->select('*');
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('pengguna_sekolah.id_pengguna', $id);
		$this->db->where('pengguna_sekolah.id_role > 1');
		$this->db->from('pengguna_sekolah');
		$this->db->join('pengguna', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		if($qr)
			return $this->db->get();
		return $this->db->get()->result();
	}

	public function update ($id, $data) {	
		$this->db->set('nm_pengguna', $data['nama']);
		$this->db->set('alamat', $data['alamat']);
		$this->db->set('no_hp', $data['phone']);
		$this->db->set('email', $data['email']);
		if(isset($data['password']))
			$this->db->set('password', $data['password']);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id_pengguna', $id);
		$this->db->update('pengguna');

		$this->db->set('id_role', $data['role']);
		$this->db->set('nomor_induk', $data['nomor']);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id_pengguna', $id);
		$this->db->where('id_sekolah', $data['sekolah']);
		$this->db->update('pengguna_sekolah');
	}
}