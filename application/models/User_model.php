<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	
	public function create($gmail){
		
		$this->load->database();
		
		$sql = "INSERT IGNORE INTO users(user_gmail) VALUES ('$gmail')";
		
		$this->db->query($sql);
		
		$query = $this->db->query("SELECT user_id FROM users WHERE user_gmail = '$gmail' LIMIT 1");
		$row = $query->row_array();
		
		$this->db->close();
		
		return array('user_id' => $row['user_id']);
		
	}
	
	public function update_info($user_id,$user_info){
		
		$user_id = htmlentities($user_id,ENT_QUOTES);
		
		$user_info = base64_encode(serialize($user_info));
		
		$this->load->database();
		
		$sql = "DELETE FROM users_info WHERE user_id = $user_id";
		
		$this->db->query($sql);
		
		$sql = "INSERT INTO users_info(user_id,user_info)
				VALUES($user_id,'$user_info')";
				
		$this->db->query($sql);
		
		$this->db->close();
		
		/*
		return array('user_id' => $user_id,
					 'user_info' => $user_info);
		*/
	}
	
	public function append_info($user_id,$info){
		
		$user_id = htmlentities($user_id,ENT_QUOTES);
		
		$this->load->database();
		
		$row = $this->get_info($user_id);
		
		$user_info = array_merge($row['user_info'],$info);
		
		$this->update_info($user_id,$user_info);
		
		$this->db->close();
	}
	
	public function get_info($user_id){
		
		
		$user_id = htmlentities($user_id,ENT_QUOTES);
		
		$this->load->database();
		
		$sql = "SELECT user_info FROM users_info WHERE user_id = $user_id LIMIT 1";
		
		$query = $this->db->query($sql);
		$row = $query->row_array();
		
		$this->db->close();
		
		if(!$row) {
			$row['user_info'] = array();
		}else{
			$row['user_info'] = unserialize(base64_decode($row['user_info']));
			
		}
		
		//var_dump($row);
		
		return $row;
	}

	
}