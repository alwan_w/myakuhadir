<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Model {

	public function updatePengguna($update, $where)
	{
		return $this->db->update('pengguna', $update, $where);
	}

	public function getPenggunaSekolah($id_pengguna, $id_sekolah, $id_role) {
		$this->db->select('*');
		$this->db->from('pengguna');
		$this->db->join('pengguna_sekolah', 'pengguna.id_pengguna = pengguna_sekolah.id_pengguna');
		$this->db->where('pengguna.id_pengguna', $id_pengguna);
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('pengguna_sekolah.id_role', $id_role);
		return $this->db->get();
	}

	public function update ($id, $data) {		
		$this->db->set('nm_mata_pelajaran', $data);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id_mata_pelajaran', $id);
		$this->db->update('mata_pelajaran');
	}
}