<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sekolah extends CI_Model {

	public function updateSekolah($update, $where)
	{
		return $this->db->update('sekolah', $update, $where);
	}
	public function getSekolah($id_sekolah)
	{
		$this->db->select('*');
		$this->db->from('sekolah');
		$this->db->where('id_sekolah', $id_sekolah);
		return $this->db->get()->result_array();
	}

	public function insert ($data) 
	{
		$this->db->insert('sekolah', $data);
		return $this->db->insert_id();
	}

	public function addPenggunaSekolah($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['status'] = 1;
		$this->db->insert('pengguna_sekolah', $data);
		return $this->db->insert_id();
	}

	public function getListSekolah ($where = array(1 => 1))
	{
		$this->db->select('*');
		$this->db->where($where);
		$this->db->from('sekolah');
		return $this->db->get()->result();
	}

	public function checkSekolah ($id_sekolah) 
	{
		$res = $this->db->get_where('sekolah', array('id_sekolah' => $id_sekolah));
		if(count($res->result())==1)
			return true;
		else
			return false;
	}

	public function getGuru($id_sekolah){
		$this->db->select('*');
		$this->db->where('pengguna_sekolah.id_sekolah', $id_sekolah);
		$this->db->where('pengguna_sekolah.id_role', 3);
		$this->db->from('pengguna_sekolah');
		$this->db->join('pengguna', 'pengguna_sekolah.id_pengguna = pengguna.id_pengguna','left');
		return $this->db->get()->result();
	}
	public function getWaliKelas($id)
	{
		return $this->db->query("SELECT * FROM pengguna, kelas WHERE kelas.wali_kelas = pengguna.id_pengguna AND kelas.id_kelas = '$id' ");
	}
	public function getKelasByGuru($id)
	{
		return $this->db->query("SELECT * FROM kelas WHERE wali_kelas = '$id' ");
	}

	public function delete ($id) {
		$this->db->where('id_sekolah', $id);
		$this->db->delete('sekolah');
	}

	public function getKelasById ($id) {
		$this->db->where('id_kelas', $id);
		$this->db->from('kelas');
		return $this->db->get()->result();
	}

	public function update ($id, $data) {		
		$this->db->set('nm_kelas', $data['nm_kelas']);
		$this->db->set('nm_ketua_kelas', $data['nm_ketua_kelas']);
		$this->db->set('jumlah_siswa', $data['jumlah_siswa']);
		$this->db->set('wali_kelas', $data['wali_kelas']);
		$this->db->set('id_sekolah', $data['id_sekolah']);
		$this->db->set('updated_at', date('Y-m-d H:i:s'));
		$this->db->where('id_kelas', $id);
		$this->db->update('kelas');
	}
}