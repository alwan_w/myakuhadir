<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['login']['get'] = 'AuthController/signin';
$route['logout']['get'] = 'AuthController/logout';
$route['login']['post'] = 'AuthController/signinAction';
$route['signin-user']['get'] = 'AuthController/signinUser';
$route['registersekolah']['get'] = 'AuthController/registerSekolah';
$route['registersekolah']['post'] = 'AuthController/createSekolah';
$route['pilihsekolah']['get'] = 'AuthController/pilihSekolah';
$route['pilihmode']['get'] = 'AuthController/pilihMode';
$route['dashboard/ganti-password']['post'] = 'AuthController/gantiPassword';

$route['carisekolah']['get'] = 'JurnalController/cariSekolah';
$route['dashboard/jurnal/siswa']['get'] = 'JurnalController/dashboardSiswa';

$route['g-aut']['get'] = 'OAuthController/google';

$route['dashboard']['get'] = 'Home/dashboard';

$route['dashboard/profile']['get'] = 'PenggunaController/index';
$route['dashboard/profile/edit']['get'] = 'PenggunaController/editProfile';
$route['dashboard/profile/edit']['post'] = 'PenggunaController/updateProfile';

$route['dashboard/sekolah']['get'] = 'SekolahController/index';
$route['dashboard/sekolah/edit']['get'] = 'SekolahController/editSekolah';
$route['dashboard/sekolah/edit']['post'] = 'SekolahController/updateSekolah';

$route['dashboard/kelas']['get'] = 'KelasController/index';
$route['dashboard/kelas/upload']['post'] = 'KelasController/uploadKelas';
$route['dashboard/kelas/tambah']['get'] = 'KelasController/create';
$route['dashboard/kelas/tambah']['post'] = 'KelasController/store';
$route['dashboard/kelas/update/(:num)']['post'] = 'KelasController/update/$1';
$route['dashboard/kelas/hapus/(:num)']['get'] = 'KelasController/destroy/$1';
$route['dashboard/kelas/(:num)']['get'] = 'KelasController/show/$1';
$route['dashboard/kelas/tambah-siswa/(:num)']['get'] = 'KelasController/addKelasSiswa/$1';
$route['dashboard/kelas/pilih-ketua/(:num)']['get'] = 'KelasController/pilihKetuaKelas/$1';
$route['dashboard/kelas/remove-siswa/(:num)']['get'] = 'KelasController/deleteFromKelas/$1';

//$route['dashboard/pelajaran']['get'] = 'PelajaranController/index';
//$route['dashboard/pelajaran/tambah']['get'] = 'PelajaranController/create';
$route['dashboard/pelajaran/tambah']['post'] = 'PelajaranController/store';
$route['dashboard/pelajaran/update/(:num)']['post'] = 'PelajaranController/update/$1';
//$route['dashboard/pelajaran/hapus/(:num)']['get'] = 'PelajaranController/destroy/$1';
//$route['dashboard/pelajaran/(:num)']['get'] = 'PelajaranController/show/$1';

$route['dashboard/civitas']['get'] = 'KaryawanController/index';
$route['dashboard/civitas/qr/(:num)']['get'] = 'KaryawanController/showQR/$1';
$route['dashboard/civitas/download-all-qr']['get'] = 'KaryawanController/downloadZip';
$route['dashboard/civitas/tambah']['get'] = 'KaryawanController/create';
$route['dashboard/civitas/tambah']['post'] = 'KaryawanController/store';
$route['dashboard/civitas/upload']['post'] = 'KaryawanController/uploadCivitas';
$route['dashboard/civitas/update/(:num)']['post'] = 'KaryawanController/update/$1';
$route['dashboard/civitas/hapus/(:num)']['get'] = 'KaryawanController/destroy/$1';
$route['dashboard/civitas/(:num)']['get'] = 'KaryawanController/show/$1';

$route['dashboard/siswa']['get'] = 'SiswaController/index';
$route['dashboard/siswa/qr/(:num)']['get'] = 'SiswaController/showQR/$1';
$route['dashboard/siswa/download-all-qr']['get'] = 'SiswaController/downloadZip';
$route['dashboard/siswa/upload']['post'] = 'SiswaController/uploadSiswa';
$route['dashboard/siswa/tambah']= 'SiswaController/create';
$route['dashboard/siswa/edit/(:num)']['get'] = 'SiswaController/edit/$1';

$route['dashboard/waktu']['get'] = 'JamPelajaranController/index';
$route['dashboard/waktu/tambah']['get'] = 'JamPelajaranController/create';
$route['dashboard/waktu/tambah']['post'] = 'JamPelajaranController/store';
$route['dashboard/waktu/update/(:num)']['post'] = 'JamPelajaranController/update/$1';
$route['dashboard/waktu/hapus/(:num)']['get'] = 'JamPelajaranController/destroy/$1';
$route['dashboard/waktu/(:num)']['get'] = 'JamPelajaranController/show/$1';

$route['kehadiran']= 'DataDashboard/kehadiran';
$route['listkelas']= 'DataDashboard/listkelas';
$route['listkelassiswa']= 'DataDashboard/listkelassiswa';
$route['listpengguna']= 'DataDashboard/listpengguna';
$route['listpenggunasekolah']= 'DataDashboard/listpenggunasekolah';
$route['jurnalguru']= 'DataDashboard/jurnalguru';

$route['dashboard/result/izin-siswa']['get'] = 'HasilAbsensiController/izinSiswa';
$route['dashboard/result/izin-siswa/update']['post'] = 'HasilAbsensiController/updateIzinSiswa';
$route['dashboard/result/izin-siswa/delete/(:num)']['get'] = 'HasilAbsensiController/deleteIzinSiswa/$1';

$route['dashboard/result/absensi/report-ortu']['get'] = 'HasilAbsensiController/reportOrtu';
$route['dashboard/result/absensi/report-ortu/kirim']['get'] = 'HasilAbsensiController/kirimReportOrtu';

$route['dashboard/result/absensi/siswa']['get'] = 'HasilAbsensiController/siswa';
$route['dashboard/result/absensi/civitas']['get'] = 'HasilAbsensiController/civitas';
//Versi lama dibawah (pake upload excel)
//$route['dashboard/result/absensi/guru']['get'] = 'HasilAbsensiController/guru';
//$route['dashboard/result/absensi/guru/upload']['post'] = 'HasilAbsensiController/upload_absensi_guru';

//$route['dashboard/result/download/absensi-siswa']['get'] = 'HasilAbsensiController/downloadExcelSiswa';
$route['dashboard/result/download/absensi-siswa-rekap']['post'] = 'HasilAbsensiController/downloadRekapExcelSiswa';

$route['dashboard/jadwal-piket']['get'] = 'JadwalController/showJadwalPiket';
$route['dashboard/jadwal-piket/upload']['post'] = 'JadwalController/uploadJadwalPiket';

$route['dashboard/jadwal-sekolah']['get'] = 'JadwalController/showJadwalSekolah';
$route['dashboard/jadwal-sekolah/setAktif/(:num)']['get'] = 'JadwalController/setJadwalSekolahAktif/$1';

$route['dashboard/jadwal']['get'] = 'JadwalController/index';
$route['dashboard/jadwal/tambah']['get'] = 'JadwalController/create';
$route['dashboard/jadwal/tambah']['post'] = 'JadwalController/store';
$route['dashboard/jadwal/update/(:num)']['post'] = 'JadwalController/update/$1';
$route['dashboard/jadwal/upload']['post'] = 'JadwalController/uploadJadwal';
$route['dashboard/jadwal/hapus/(:num)']['get'] = 'JadwalController/destroy/$1';
$route['dashboard/jadwal/(:num)']['get'] = 'JadwalController/show/$1';

$route['hasil-absensi-siswa-smpia13surabaya/(:num)']['get'] = 'RekapAbsensi/index/$1';

$route['dashboard/setting/jadwal-masuk']['get'] = 'JadwalController/jadwalMasuk';
$route['dashboard/setting/jadwal-masuk/tambah']['get'] = 'JadwalController/createJadwalMasuk';
$route['dashboard/setting/jadwal-masuk/tambah']['post'] = 'JadwalController/storeJadwalMasuk';

//$route['dashboard/kelas-siswa']['get'] = 'KelasSiswaController/index';
//$route['dashboard/kelas-siswa/tambah']['get'] = 'KelasSiswaController/create';
$route['dashboard/kelas-siswa/tambah']['post'] = 'KelasSiswaController/store';
//$route['dashboard/kelas-siswa/update/(:num)']['post'] = 'KelasSiswaController/update/$1';
$route['dashboard/kelas-siswa/hapus/(:num)']['get'] = 'KelasSiswaController/destroy/$1';
//$route['dashboard/kelas-siswa/(:num)']['get'] = 'KelasSiswaController/show/$1';

$route['dashboard/jurnal']['get'] = 'JurnalController/index';
$route['dashboard/jurnal/hadir/(:num)']['post'] = 'JurnalController/hadirAction/$1';
$route['dashboard/jurnal/isi-jurnal/(:any)']['get'] = 'JurnalController/showJurnal/$1';
//$route['dashboard/jurnal/isi-jurnal-guru/(:any)']['get'] = 'JurnalController/showJurnalGuru/$1';
//$route['dashboard/jurnal/tugas-ujian/(:any)']['get'] = 'JurnalController/showTugasUjian/$1';
$route['dashboard/jurnal/isi-jurnal/(:any)']['post'] = 'JurnalController/isiJurnalAction/$1';
//$route['dashboard/jurnal/tugas-ujian/(:any)']['post'] = 'JurnalController/isiTugasUjian/$1';
//$route['dashboard/jurnal/kelas-selesai/(:any)']['post'] = 'JurnalController/exitKelas/$1';
$route['dashboard/jurnal/daftar-hadir/(:any)']['get'] = 'JurnalController/showDaftarHadir/$1';
$route['dashboard/jurnal/daftar-hadir/(:any)']['post'] = 'JurnalController/updateDaftarHadir/$1';
$route['dashboard/jurnal/hadir-siswa/(:any)']['get'] = 'JurnalController/showDaftarHadirSiswa/$1';
$route['dashboard/jurnal/histori-jurnal']['get'] = 'JurnalController/showHistoriJurnal';
$route['dashboard/jurnal/histori-jurnal/detail/(:any)']['get'] = 'JurnalController/showHistoriDetail/$1';
$route['dashboard/jurnal/histori-jurnal/daftar-hadir/(:any)']['get'] = 'JurnalController/showHistoriDaftarHadir/$1';
$route['dashboard/jurnal/ijin-siswa/(:any)']['post'] = 'JurnalController/ijinSiswa/$1';
$route['dashboard/jurnal/piket']['get'] = 'JurnalController/showPiket';