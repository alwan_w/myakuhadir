<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package     CodeIgniter
 * @author      ExpressionEngine Dev Team
 * @copyright   Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license     http://codeigniter.com/user_guide/license.html
 * @link        http://codeigniter.com
 * @since       Version 1.0
 * @filesource
 */
function getTahunAjaran($date = NULL, $yearOnly = FALSE){
    $tahun = date('Y');
    $bulan = date('m');
    if(!is_null($date))
    {
        $date = explode('-', $date);
        $tahun = $date[0];
        $bulan = $date[1];
    }
    $tahunAjaran = $tahun;
    if($bulan >= 7)
    {
        $semester = 'Ganjil';
        $tahunAjaran = $tahun . '/' . ($tahun+1);
    }
    else
    {
        $semester = 'Genap';
        $tahunAjaran = ($tahun-1) . '/' . $tahun;
    }
    if($yearOnly)
        return $tahunAjaran;
    else
        return $semester . ' ' . $tahunAjaran;
    

}
function sanitize($input){
    $input = str_replace('"', "", $input);
    $input = str_replace("'", "", $input);
    $input = str_replace(" ", "", $input);
    return $input;
}

function sanitizeNoSpace($input){
    $input = str_replace('"', "", $input);
    $input = str_replace("'", "", $input);
    return $input;
}

function isDateValid($date) {
  $tempDate = explode('-', explode(' ', $date)[0]);
  // checkdate(month, day, year)
  return checkdate($tempDate[1], $tempDate[2], $tempDate[0]);
}

function humanDateFormat($date, $hari = FALSE)
{
    $str = explode('-',$date);
    $month = getMonthName($str[1]);
    if($hari)
    {
        $hari = getDayName($date, FALSE, TRUE);
        return $hari . ', ' . $str[2] . ' ' . $month . ' ' . $str[0];
    }
    return $str[2] . ' ' . $month . ' ' . $str[0];
}

function changeDateFormat($date, $time = '0')
{
    if($time == '0')
    {
        $str = explode(' ',$date);
        $month = getMonthFromName($str[1]);
        return $str[3] . "-" . $month . "-" . $str[2];
    }
    else
    {
        $str = explode('-',$date);
        $day = date('D', $time);
        $month = getMonthName($str[1]);
        return $day . ' ' . $month . ' ' . $str[2] . " " . $str[0];
    }
}

function getDayName($date, $english = FALSE, $full = FALSE)
{
    $day = date('D', strtotime($date));
    if($english)
    {
        if($full)
        {
            if($day=='Mon') return 'Monday';
            if($day=='Tue') return 'Tuesday';
            if($day=='Wed') return 'Wednesday';
            if($day=='Thu') return 'Thursday';
            if($day=='Fri') return 'Friday';
            if($day=='Sat') return 'Saturday';
            if($day=='Sun') return 'Sunday';
        }
        else
            return $day;
    } 
    else
    {
        if($full)
        {
            if($day=='Mon') return 'Senin';
            if($day=='Tue') return 'Selasa';
            if($day=='Wed') return 'Rabu';
            if($day=='Thu') return 'Kamis';
            if($day=='Fri') return 'Jumat';
            if($day=='Sat') return 'Sabtu';
            if($day=='Sun') return 'Minggu';
        }
        else
        {
            if($day=='Mon') return 'Sen';
            if($day=='Tue') return 'Sel';
            if($day=='Wed') return 'Rab';
            if($day=='Thu') return 'Kam';
            if($day=='Fri') return 'Jum';
            if($day=='Sat') return 'Sab';
            if($day=='Sun') return 'Min';
        }
    }
}
function getMonthName($month)
{
    $month = (int)$month;
    if($month==1) return 'Januari';
    if($month==2) return 'Februari';
    if($month==3) return 'Maret';
    if($month==4) return 'April';
    if($month==5) return 'Mei';
    if($month==6) return 'Juni';
    if($month==7) return 'Juli';
    if($month==8) return 'Agustus';
    if($month==9) return 'September';
    if($month==10) return 'Oktober';
    if($month==11) return 'November';
    if($month==12) return 'Desember';
}
function getMonthNameShort($month)
{
    if($month==1) return 'Jan';
    if($month==2) return 'Feb';
    if($month==3) return 'Mar';
    if($month==4) return 'Apr';
    if($month==5) return 'May';
    if($month==6) return 'Jun';
    if($month==7) return 'Jul';
    if($month==8) return 'Aug';
    if($month==9) return 'Sep';
    if($month==10) return 'Oct';
    if($month==11) return 'Nov';
    if($month==12) return 'Dec';
}
function getMonthFromName($month)
{
    if($month=='Jan') return '01';
    if($month=='Feb') return '02';
    if($month=='Mar') return '03';
    if($month=='Apr') return '04';
    if($month=='May') return '05';
    if($month=='Jun') return '06';
    if($month=='Jul') return '07';
    if($month=='Aug') return '08';
    if($month=='Sep') return '09';
    if($month=='Oct') return '10';
    if($month=='Nov') return '11';
    if($month=='Dec') return '12';
}