<?php
class KelasSiswaController extends CI_Controller {

	public function __construct(){
		
		parent::__construct();
		if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') != 2) {
            $url = base_url() . 'login';
            redirect($url);
        }
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('KelasSiswa');
		$this->load->model('JamPelajaran');
		$this->load->model('JadwalGuru');
	}

    public function index()
    {
    	$data = $this->KelasSiswa->getList();
    	// print_r($data);
        $this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/kelasSiswa/listKelasSiswa', array('data' => $data));
		$this->load->view('layout/footer');
    }

    public function create () {
    	$kelas	= $this->JadwalGuru->getKelas($this->session->userdata('sess_sekolah'));
    	$siswa 	= $this->KelasSiswa->getSiswa($this->session->userdata('sess_sekolah'));
    	  	
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/kelasSiswa/createKelasSiswa', array('kelas' => $kelas, 'siswa'=> $siswa));
		$this->load->view('layout/footer');
    }

    /*public function show ($id) {
    	$data = $this->KelasSiswa->getWaktuById($id);
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/jadwal/showJadwal', array('data' => $data));
		$this->load->view('layout/footer');
    }*/

    public function store () {
    	$this->form_validation->set_rules('id_siswa', 'Siswa', 'required');
	  	if ($this->form_validation->run() == FALSE)
        {
            redirect(base_url().'dashboard/kelas-siswa/tambah');
        }
        else
        {
        	$data = array(
        		'id_siswa'		=> $this->input->post('id_siswa'),
        		'id_kelas'		=> $this->input->post('id_kelas')
        	);
        	$id_list = $this->siswa->getIdSiswaByKelas($data['id_kelas']);
        	if(!in_array($data['id_siswa'], $id_list))
            	$this->KelasSiswa->insert($data);
            redirect(base_url().'dashboard/kelas-siswa');
        }
    }

    /*public function update ($id) {
    	$data = $this->KelasSiswa->update($id, 1);
    	redirect(base_url().'dashboard/jadwal');	
    }*/

    public function destroy ($id) {
    	$this->KelasSiswa->delete($id);
    	redirect(base_url().'dashboard/kelas-siswa');
    }
}