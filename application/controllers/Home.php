<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct(){
		
		parent::__construct();
		
		$this->load->helper('url');
	
		if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') < 1) {
            $url = base_url() . 'login';
            redirect($url);
        }
	}
	
	public function index(){
		$this->_do_view();
	}
	
	public function _do_view(){
		$this->load->view('layout/header');
		$this->load->view('page/home');
		$this->load->view('layout/footer');
	}

	public function dashboard () {
		redirect(base_url().'dashboard/result/absensi/siswa');
		$params = $this->input->get('type', TRUE);
		$this->load->view('layout/header');
		$this->load->view('page/dashboard/header');
		$this->load->view('layout/navigationbar');
		if ($params == null) {
			$this->load->view('page/dashboard/latest/DashboardGuru');
		}elseif($params == 'siswa'){
			$this->load->view('page/dashboard/latest/DashboardSiswa');
		}
		$this->load->view('layout/footer');	
		if ($params == null) {
			$this->load->view('page/dashboard/latest/FooterGuru');
		}elseif($params == 'siswa'){
			$this->load->view('page/dashboard/latest/FooterSiswa');
		}
	}
	
}