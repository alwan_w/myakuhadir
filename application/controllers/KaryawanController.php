<?php
class KaryawanController extends CI_Controller {

	public function __construct(){
		
		parent::__construct();
		if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') != 2) {
            $url = base_url() . 'login';
            redirect($url);
        }
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->library('excel');
        $this->load->model('sekolah');
		$this->load->model('Karyawan');
		$this->load->model('Role');
	}

    public function index()
    {
    	$data = $this->Karyawan->getList($this->session->userdata('sess_sekolah'));
    	// var_dump($data);
    	// die();
        $this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/karyawan/listKaryawan', array('data' => $data));
		$this->load->view('layout/footer');
    }

	public function create () {
		$role = $this->Role->getList();
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/karyawan/createKaryawan', array('role' => $role));
		$this->load->view('layout/footer');
    }

    public function show ($id) {
    	$data = $this->Karyawan->getKaryawanById($id, $this->session->userdata('sess_sekolah'));
    	$role = $this->Role->getList();
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/karyawan/showKaryawan', array('data' => $data, 'role'=>$role));
		$this->load->view('layout/footer');
    }

    private function confirmUpload($pengguna)
    {
        if(count($pengguna)>0)
        {
            $id_sekolah = $this->session->userdata('sess_sekolah');
            $emailPengguna = array();
            $nomorIndukPengguna = array();
            $checkEmail = array();
            $checkNomorInduk = array();
            for($i=0; $i<count($pengguna); $i++)
            {
                $checkEmail[$pengguna[$i]['email']] = $pengguna[$i];
                $checkNomorInduk[$pengguna[$i]['nomor_induk']] = $pengguna[$i];
                $checkEmail[$pengguna[$i]['email']]['indeks'] = $i;
                $checkNomorInduk[$pengguna[$i]['nomor_induk']]['indeks'] = $i;
                array_push($emailPengguna, $pengguna[$i]['email']);
                array_push($nomorIndukPengguna, $pengguna[$i]['nomor_induk']);
            }
            $owned = $this->Karyawan->getOwnedPengguna($id_sekolah, $emailPengguna, $nomorIndukPengguna)->result_array();
            //$change = array('email' => array(), 'nomor_induk' => array());
            $temp = $pengguna;
            //$new = array();
            $date = date('Y-m-d H:i:s');
            #check email
            for($i=0; $i<count($owned); $i++)
            {
                $email = $owned[$i]['email'];
                $nomorInduk = $owned[$i]['nomor_induk'];
                if(isset($checkEmail[$email]))
                {
                    $data = $checkEmail[$email];
                    $data['id_pengguna'] = $owned[$i]['id_pengguna'];
                    $data['date'] = $date;
                    $data['type'] = 'email';
                    $this->Karyawan->addExcel($id_sekolah, $data);
                    /*$checkEmail[$email]['id_pengguna'] = $owned[$i]['id_pengguna'];
                    array_push($change['email'], $checkEmail[$email]);*/
                    unset($temp[$checkEmail[$email]['indeks']]);
                    if($nomorInduk == $checkEmail[$email]['nomor_induk'])
                        unset($checkNomorInduk[$nomorInduk]);
                }
                if(isset($checkNomorInduk[$nomorInduk]))
                {
                    $data = $checkNomorInduk[$nomorInduk];
                    $data['id_pengguna'] = $owned[$i]['id_pengguna'];
                    $data['date'] = $date;
                    $data['type'] = 'nomor_induk';
                    $this->Karyawan->addExcel($id_sekolah, $data);
                    /*$checkNomorInduk[$nomorInduk]['id_pengguna'] = $owned[$i]['id_pengguna'];
                    array_push($change['nomor_induk'], $checkNomorInduk[$nomorInduk]);*/
                    unset($temp[$checkNomorInduk[$nomorInduk]['indeks']]);
                }   
            }
            foreach($temp as $new)
            {
                $data = $new;
                $data['date'] = $date;
                $this->Karyawan->addExcel($id_sekolah, $data, TRUE);
            }

            //$data = array('change' => $change, 'new' => $new);
        }
        redirect(base_url().'dashboard/civitas');
    }

    public function uploadCivitas() 
    {
    	if(isset($_FILES['userfile']['name']) && $_FILES['userfile']['size'] > 0 && FALSE)
        {
            $arr_file = explode('.', $_FILES['userfile']['name']);
            $extension = end($arr_file);
         
            if($extension == 'xls')
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            elseif($extension == 'xlsx')
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            else
                redirect(base_url().'dashboard/siswa');
            
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($_FILES['userfile']['tmp_name']);
             
            $sheet = $spreadsheet->getSheet(0);
            
            $i = 0;
            $row = 2;
            // First Name, Last Name, Email, Nomor Induk, Role, Password, No Telepon, Alamat, Status (Aktif/ Tidak)
            $pengguna = array();
            while($sheet->getCell('A'.$row)->getValue() != null)
            {
                $pengguna[$i]['nm_pengguna'] = $sheet->getCell('A'.$row)->getValue();
                $pengguna[$i]['email'] = $sheet->getCell('B'.$row)->getValue();
                $pengguna[$i]['nomor_induk'] = $sheet->getCell('C'.$row)->getValue();
                $pengguna[$i]['role'] = $sheet->getCell('D'.$row)->getValue();
                $pengguna[$i]['password'] = $sheet->getCell('E'.$row)->getValue();
                $pengguna[$i]['no_hp'] = $sheet->getCell('F'.$row)->getValue();
                $pengguna[$i]['alamat'] = $sheet->getCell('G'.$row)->getValue();
                $pengguna[$i]['status'] = $sheet->getCell('H'.$row)->getValue();

                $row++;
                $i++;
            }
            $this->confirmUpload($pengguna);
        }
        else
            redirect(base_url().'dashboard/civitas');
    }

    public function store () 
    {
    	$this->form_validation->set_rules('nama', 'Nama', 'required');
    	$this->form_validation->set_rules('nomor', 'Nomor Induk', 'required');
    	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[pengguna.email]');
    	if ($this->form_validation->run() == FALSE)
        {
            redirect(base_url().'dashboard/civitas/tambah');
        }
        else
        {
        	$data = array(
        		'nama' => $this->input->post('nama'),
        		'nomor' => $this->input->post('nomor'),
        		'alamat' => $this->input->post('alamat'),
        		'phone' => $this->input->post('phone'),
        		'email' => $this->input->post('email'),
        		'password' => md5($this->input->post('password')),
        		'role' => $this->input->post('role'),
                'sekolah' => $this->session->userdata('sess_sekolah')
        	);
            $this->Karyawan->insert($data);
            redirect(base_url().'dashboard/civitas');
        }
    }

    public function update ($id) {
    	$data = array(
	        		'nama' => $this->input->post('nama'),
	        		'nomor' => $this->input->post('nomor'),
	        		'alamat' => $this->input->post('alamat'),
	        		'phone' => $this->input->post('phone'),
	        		'email' => $this->input->post('email'),
	        		'role' => $this->input->post('role'),
                    'sekolah' => $this->session->userdata('sess_sekolah')
	        	);
        if($this->input->post('password') != '')
            $data['password'] = md5($this->input->post('password'));
    	$this->Karyawan->update($id, $data);
    	redirect(base_url().'dashboard/civitas');	
    }

    public function destroy ($id) {
    	$this->Karyawan->delete($id, $this->session->userdata('sess_sekolah'));
    	redirect(base_url().'dashboard/civitas');
    }

    public function showQR($id_karyawan)
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $read = $this->Karyawan->getKaryawanById($id_karyawan, $id_sekolah, TRUE)->result_array();
        if(count($read)>0)
        {
            $nim = $read[0]['nomor_induk'] . '<-->' . $read[0]['id_pengguna'] . '<-->'. $read[0]['id_pengguna_sekolah'];
            $this->load->library('ciqrcode');
            $params['data'] = $nim;
            $params['level'] = 'H';
            $params['size'] = 10;
            $gambar = $this->ciqrcode->generate($params);
            header("Content-Type: image/png");
            imagepng($gambar);
        }
        else
            redirect(base_url('dashboard/civitas'));
    }
    public function downloadZip()
    {
        $files = FCPATH . 'assets/qr';
        if (!is_dir($files)) {
            mkdir($files, 0777, TRUE);
        }
        $sekolah = $this->sekolah->getSekolah($this->session->userdata('sess_sekolah'))[0];
        $this->generateQR($sekolah);

        $path ='assets/qr/'.$sekolah['id_sekolah'].'/civitas/';
        $this->zip->read_dir($path, FALSE);
        $this->zip->download('All QR Civitas Download.zip');
        $this->session->set_flashdata('msg', '<script>swal("Success !!", "QR Code Berhasil Didownlaod", "success");</script>');
        redirect(base_url('dashboard/civitas'));
    }
    public function generateQR($sekolah)
    {
        $read = $this->Karyawan->getList($sekolah['id_sekolah'], TRUE)->result_array();
        $path = 'assets/qr/'.$sekolah['id_sekolah'].'/civitas/';
        if (!is_dir(FCPATH.$path))
            mkdir(FCPATH.$path, 0777, TRUE);
        else
        {
            $files = glob(FCPATH.$path.'*'); // get all file names
            foreach($files as $file){ // iterate files
                if(is_file($file))
                    unlink($file); // delete file
            }
        }
        foreach ($read as $r) 
        {
            $nim = $r['nomor_induk'] . '<-->' . $r['id_pengguna'] . '<-->'. $r['id_pengguna_sekolah'];

            $name = 'qrcivitas'.$r['nomor_induk'].' '.$r['nm_pengguna'];
            $config['cacheable']    = true; //boolean, the default is true
            $config['cachedir']     = 'assets/'; //string, the default is application/cache/
            $config['errorlog']     = 'assets/'; //string, the default is application/logs/
            $config['imagedir']     = $path; //direktori penyimpanan qr code
            $config['quality']      = true; //boolean, the default is true
            $config['size']         = '1024'; //interger, the default is 1024
            $config['black']        = array(224,255,255); // array, default is array(255,255,255)
            $config['white']        = array(70,130,180); // array, default is array(0,0,0)
            $this->ciqrcode->initialize($config);
     
            $image_name=$name.'.png'; //buat name dari qr code sesuai dengan nim
     
            $params['data'] = $nim; //data yang akan di jadikan QR CODE
            $params['level'] = 'H'; //H=High
            $params['size'] = 10;

            $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder imagedir

            $this->ciqrcode->generate($params);
        }
        
    }
}