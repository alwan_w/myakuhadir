<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardSiswa extends CI_Controller {
	
    private $id_sekolah;
	public function __construct(){
		
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('HasilAbsensi');
		$this->load->database();
	
		// session_start();
		
		// if(!isset($_SESSION['is_login'])){
		
		// 	redirect('/auth/');
		// }
        $this->id_sekolah = $this->session->userdata('sess_sekolah');
	}

	public function listKelas () {
		$listKelas = $this->db->select('id_kelas, nm_kelas')->where('id_sekolah', $this->id_sekolah)->get('kelas')->result();
		$array = array(
    		'list' => $listKelas
    	);
    	$this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
	}

	public function siswa_detailKeterlambatanOrang ($id) {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $query = "SELECT * from kehadiran 
                    JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa
                    JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna
                    WHERE kehadiran_siswa = '".$id."'
                    AND kehadiran.id_sekolah = '".$this->id_sekolah."'
                    AND kehadiran_verification = 3
                    AND kehadiran_timestamp >= '".$start."' 
                    AND kehadiran_timestamp <= '".$end."'";
        $getter = $this->db->query($query)->result();
        $data = [];
        foreach ($getter as $g) {
            $data[] = array(
                'tanggal' => date('d F Y', strtotime($g->kehadiran_timestamp)),
                'waktu' => date('H:i', strtotime($g->kehadiran_timestamp))
            );
        };
        $array = array(
            'nama' => $getter[0]->nm_pengguna,
            'nis' => $getter[0]->kehadiran_siswa,
            'data' => $data
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function siswa_jumlahSiswa ($kelas = 0) {
		$start = $this->input->get('start', TRUE);
    	$end = $this->input->get('end', TRUE);
    	if (isset($start) && isset($end)) {    					
    		$start = $this->input->get('start', TRUE);
    		$end = $this->input->get('end', TRUE);
    	} else {
    		$start = date('Y-m-01');
    		$end = date('Y-m-d');
    	}
    	if ($kelas == 0) {
    		// $siswa = $this->db->query('SELECT DISTINCT kehadiran_siswa from kehadiran WHERE kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result();
    		$siswa = $this->db->query('SELECT * FROM pengguna_sekolah JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna WHERE id_role = 1 AND id_sekolah = '.$this->id_sekolah)->result();
    	} else {
    		// $siswa = $this->db->query('SELECT DISTINCT kehadiran_siswa from kehadiran JOIN pengguna ON pengguna.nomor_induk=kehadiran.kehadiran_siswa LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result();
    		$siswa = $this->db->query('SELECT * from  pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.'')->result();
    	}    	
		$array = array(
			'count' => count($siswa)
		);
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
	}

	 public function siswa_jumlahKeterlambatan ($kelas = 0) {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        if ($kelas == 0 ) {
            $terlambat = $this->db->query('SELECT COUNT(*) as terlambat FROM kehadiran WHERE kehadiran_verification = "3" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->terlambat;
        } else {
            $terlambat = $this->db->query('SELECT COUNT(*) as terlambat FROM kehadiran JOIN pengguna ON pengguna.nomor_induk=kehadiran.kehadiran_siswa LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.' AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_verification = "3" AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->terlambat;
        }
        $array = array(
            'terlambat' => $terlambat
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

     public function siswa_statusKehadiran ($kelas = 0) {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        if ($kelas == 0 ) {
            $siswaTepat = $this->db->query('SELECT COUNT(*) as hadir FROM kehadiran WHERE kehadiran_verification = "0" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->hadir;
            $siswaTelat = $this->db->query('SELECT COUNT(*) as ijin FROM kehadiran WHERE kehadiran_verification = "3" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->ijin;            
        } else {            
            $siswaTepat = $this->db->query('SELECT COUNT(*) as hadir FROM kehadiran JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.' AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_verification = "0" AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->hadir;
            $siswaTelat = $this->db->query('SELECT COUNT(*) as ijin FROM kehadiran JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.' AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_verification = "3"AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->ijin;            
        }
        $statusKehadiran[] = array('y' => (int)$siswaTelat, 'name' => 'Siswa terlambat');
        $statusKehadiran[] = array('y' => (int)$siswaTepat, 'name' => 'siswa tepat waktu');
        $array = array(
            'data' => $statusKehadiran            
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function siswa_pieKehadiran ($kelas = 0) {
		$start = $this->input->get('start', TRUE);
    	$end = $this->input->get('end', TRUE);
    	if (isset($start) && isset($end)) {    					
    		$start = $this->input->get('start', TRUE);
    		$end = $this->input->get('end', TRUE);
    	} else {
    		$start = date('Y-m-01');
    		$end = date('Y-m-d');
    	}
    	if ($kelas == 0 ) {
    		$totalSiswa = count($this->db->where('id_role', 1)->where('id_sekolah', $this->id_sekolah)->get('pengguna_sekolah')->result());
	    	$effectiveDay = count($this->HasilAbsensi->getDistinctDaySiswa($start, $end));
	    	$siswaMasuk = $this->db->query('SELECT COUNT(*) as hadir FROM kehadiran WHERE NOT kehadiran_verification = "1" AND NOT kehadiran_verification = "2" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->hadir;
	    	$siswaIjin = $this->db->query('SELECT COUNT(*) as ijin FROM kehadiran WHERE kehadiran_verification = "2" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->ijin;
	    	$siswaSakit = $this->db->query('SELECT COUNT(*) as sakit FROM kehadiran WHERE kehadiran_verification = "1" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->sakit;
    	} else {
    		$totalSiswa = count($this->db->where('id_role', 1)->where('id_sekolah', $this->id_sekolah)->where('kelas_siswa.id_kelas', $kelas)->join('kelas_siswa', 'kelas_siswa.id_siswa=pengguna.id_pengguna', 'left')->get('pengguna_sekolah')->result());
	    	$effectiveDay = count($this->HasilAbsensi->getDistinctDaySiswa($start, $end)); //CEK LAGI NANTI
	    	$siswaMasuk = $this->db->query('SELECT COUNT(*) as hadir FROM kehadiran JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.' AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND NOT kehadiran_verification = "1" AND NOT kehadiran_verification = "2" AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->hadir;
	    	$siswaIjin = $this->db->query('SELECT COUNT(*) as ijin FROM kehadiran JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.' AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_verification = "2"AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->ijin;
	    	$siswaSakit = $this->db->query('SELECT COUNT(*) as sakit FROM kehadiran JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.' AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_verification = "1"AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->sakit;
    	}
    	// $pie[] = array('name' => 'Siswa hadir', 'y' => (int)$siswaMasuk);
    	$pie[] = array('name' => 'Siswa sakit', 'y' => (int)$siswaSakit);
    	$pie[] = array('name' => 'Siswa izin', 'y' => (int)$siswaIjin);
    	$pie[] = array('name' => 'Siswa tanpa keterangan', 'y' => (int)($totalSiswa * $effectiveDay) - ($siswaMasuk + $siswaIjin + $siswaSakit));
    	$array = array(
			'data' => $pie,
            'siswaMasuk' => (int)$siswaMasuk,
            'presentase' => $effectiveDay == 0 ? 0 : round(($siswaMasuk / ($totalSiswa * $effectiveDay)) * 100, 2)
		);
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
	}

	public function siswa_siswaTidakMasuk ($kelas = 0) {
		$start = $this->input->get('start', TRUE);
    	$end = $this->input->get('end', TRUE);
    	if (isset($start) && isset($end)) {    					
    		$start = $this->input->get('start', TRUE);
    		$end = $this->input->get('end', TRUE);
    	} else {
    		$start = date('Y-m-01');
    		$end = date('Y-m-d');
    	}
    	if ($kelas == 0) {
    		$siswaIjin = $this->db->query('SELECT COUNT(*) as ijin FROM kehadiran WHERE kehadiran_verification = "2" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->ijin;
    		$siswaSakit = $this->db->query('SELECT COUNT(*) as sakit FROM kehadiran WHERE kehadiran_verification = "1" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->sakit;
    	} else {
			$siswaIjin = $this->db->query('SELECT COUNT(*) as ijin FROM kehadiran JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.' AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_verification = "2"AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->ijin;
    		$siswaSakit = $this->db->query('SELECT COUNT(*) as sakit FROM kehadiran JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.' AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_verification = "1"AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'"')->result()[0]->sakit;
    	}    	
    	$array = array(
			'sakit' => $siswaSakit,
			'ijin' => $siswaIjin
		);
		return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
	}

	public function siswa_keterlambatanHarian ($kelas = 0) {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        if ($kelas == 0) {
            $siswaLate = $this->db->query('SELECT SUM(kehadiran_verification = "3") as count, DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d") as day FROM kehadiran WHERE kehadiran_timestamp >= "'.$start.'" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp <= "'.$end.'" GROUP BY day')->result();
            $totalHadir = $this->db->query('SELECT COUNT(*) as count, DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d") as day FROM kehadiran WHERE NOT kehadiran_verification = "1" AND NOT kehadiran_verification = "2" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'" GROUP BY day')->result();
        } else {
            $siswaLate = $this->db->query('SELECT SUM(kehadiran_verification = "3") as count, DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d") as day FROM kehadiran JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.' AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND  kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'" GROUP BY day')->result();
            $totalHadir = $this->db->query('SELECT COUNT(*) as count, DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d") as day FROM kehadiran JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna WHERE kelas_siswa.id_kelas = '.$kelas.' AND NOT kehadiran_verification = "1" AND NOT kehadiran_verification = "2" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND  kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'" GROUP BY day')->result();
        }
        if (count($siswaLate) != 0) {
            foreach ($siswaLate as $key) {
                $day[] = date('d M y', strtotime($key->day));
                $count[] = (int)$key->count;
            }
        } else {
            $day = [];
            $count = [];
        }
        if (count($totalHadir) != 0) {
            foreach ($totalHadir as $key) {
                $day1[] = date('d M y', strtotime($key->day));
                $count1[] = (int)$key->count;
            }
        } else {
            $day1 = [];
            $count1 = [];
        }
        $late = array('count' => $count, 'day' => $day);
        $hadir = array('count' => $count1, 'day' => $day1);
        $array = array(
            'terlambat' => $late,
            'hadir' => $hadir
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function siswa_dailyAbsent ($kelas = 0 ){
		$start = $this->input->get('start', TRUE);
    	$end = $this->input->get('end', TRUE);
    	if (isset($start) && isset($end)) {    					
    		$start = $this->input->get('start', TRUE);
    		$end = $this->input->get('end', TRUE);
    	} else {
    		$start = date('Y-m-01');
    		$end = date('Y-m-d');
    	}		
		if ($kelas == 0) {
			$status = $this->db->select('sum(kehadiran_verification = 1) as sakit, sum(kehadiran_verification = 2) as izin,  DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d") as day, SUM(kehadiran_verification != "1" AND kehadiran_verification != "2") as hadir')
				->where('kehadiran_timestamp >=', $start)
				->where('kehadiran_timestamp <=', $end)
                ->where('kehadiran.id_sekolah', $this->id_sekolah)
				->join('pengguna_sekolah', 'pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa')
                ->join('pengguna', 'pengguna.id_pengguna=pengguna_sekolah.id_pengguna')
				->join('kelas_siswa', 'pengguna.id_pengguna=kelas_siswa.id_siswa')
				->group_by('DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d")')
				->get('kehadiran')->result();			
		} else {
			$status = $this->db->select('sum(kehadiran_verification = 1) as sakit, sum(kehadiran_verification = 2) as izin,  DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d") as day, SUM(kehadiran_verification != "1" AND kehadiran_verification != "2") as hadir')
				->where('kehadiran_timestamp >=', $start)
				->where('kehadiran_timestamp <=', $end)				
				->where('kelas_siswa.id_kelas', $kelas)
                ->where('kehadiran.id_sekolah', $this->id_sekolah)
				->join('pengguna_sekolah', 'pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa')
                ->join('pengguna', 'pengguna.id_pengguna=pengguna_sekolah.id_pengguna')
				->join('kelas_siswa', 'pengguna.id_pengguna=kelas_siswa.id_siswa')
				->group_by('DATE_FORMAT(`kehadiran_timestamp`, "%Y-%m-%d")')
				->get('kehadiran')->result();			
		}
        $jumlah_siswa = $this->db->query('SELECT Count(*) as count from pengguna_sekolah where id_role=1 AND id_sekolah =' . $this->id_sekolah)->result()[0]->count;
		$sakit = [];
		$izin = [];
    	if (count($status) != 0) {
    		foreach ($status as $key) {
	    		$day[] = date('d M y', strtotime($key->day));
	    		$sakit[] = (int)$key->sakit;
	    		$izin[] = (int)$key->izin;
                $tidakHadir[] = (int)($jumlah_siswa - ($key->hadir + $key->izin + $key->sakit));
	    	}
    	} else {
    		$day = [];
    		$sakit = [];
			$izin = [];
            $tidakHadir = [];
    	}
    	$getAbsenPerDay[] = array('data' => $sakit, 'name' => 'Sakit');
    	$getAbsenPerDay[] = array('data' => $izin, 'name' => 'Izin');
        $getAbsenPerDay[] = array('data' => $tidakHadir, 'name' => 'Tanpa keterangan');
    	$array = array(
    		'data' => $getAbsenPerDay,
    		'labels' => $day    		
    	);
    	return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
	}

	public function siswa_keterlambatanPerorang ($kelas = 0) {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        if ($kelas == 0 ) {
            $terlambat = $this->db->query('SELECT COUNT(*) as count, nm_pengguna, kehadiran_siswa as nomor_induk, nm_kelas FROM kehadiran JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna JOIN kelas ON kelas_siswa.id_kelas=kelas.id_kelas WHERE kehadiran_verification = "3" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'" GROUP BY kehadiran_siswa ORDER BY count DESC')->result();
        } else {
            $terlambat = $this->db->query('SELECT COUNT(*) as count, nm_pengguna, kehadiran_siswa as nomor_induk, nm_kelas FROM kehadiran JOIN pengguna_sekolah ON pengguna_sekolah.nomor_induk=kehadiran.kehadiran_siswa JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna LEFT JOIN kelas_siswa ON kelas_siswa.id_siswa=pengguna.id_pengguna JOIN kelas ON kelas_siswa.id_kelas=kelas.id_kelas WHERE kelas_siswa.id_kelas = '.$kelas.' AND kehadiran_verification = "3" AND kehadiran.id_sekolah = '.$this->id_sekolah . ' AND kehadiran_timestamp >= "'.$start.'" AND kehadiran_timestamp <= "'.$end.'" GROUP BY kehadiran_siswa ORDER BY count DESC')->result();
        }
        $array = array(
            'data' => $terlambat
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

}

?>