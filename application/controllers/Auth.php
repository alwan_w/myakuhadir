<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index(){
		$this->load->view('layout/auth/header');
		$this->load->view('page/auth/login');
		$this->load->view('layout/auth/footer');
	}
	
	public function sign_in($token){
		
		session_start();
		
		$this->load->model('user_model');
		
		
		$user = json_decode(file_get_contents("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=".$token),true);
		
		 
		//var_dump($user);
		
		//$data['user_gmail'] = $user['email'];
		
		$user_id = $this->user_model->create($user['email']);
		
		
		$_SESSION['is_login'] = 1;
		$_SESSION['name'] = $user['name'];
		$_SESSION['picture'] = $user['picture'];
		
		
		$user_info['name'] = $user['name'];
		$user_info['picture'] = $user['picture'];
		
		$this->user_model->append_info($user_id['user_id'],$user_info);
		
		
		$this->load->view('layout/auth/header');
		$this->load->view('page/auth/sign_in');
		$this->load->view('layout/auth/footer');
		
	}
	
	
	public function sign_in_done(){
		//$this->load->helper('url');
		redirect('/home');
	}
	
	public function logout(){
		$this->load->view('layout/auth/header');
		$this->load->view('page/auth/logout');
		$this->load->view('layout/auth/footer');
	}
	
	public function sign_out(){
		
		session_start();
		
		//$this->load->helper('url');
		
		session_destroy();
		
		redirect('/auth');
	}
}
