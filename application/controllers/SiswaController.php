<?php
//use PhpOffice\PhpSpreadsheet\IOFactory;
//use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class SiswaController extends CI_Controller {

	public function __construct(){
		
		parent::__construct();
		if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') != 2) {
            $url = base_url() . 'login';
            redirect($url);
        }
		$this->load->helper('url');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
        $this->load->model('sekolah');
        $this->load->model('Kelas');
	}

    private function confirmUpload($pengguna)
    {
        if(count($pengguna)>0)
        {
            $id_sekolah = $this->session->userdata('sess_sekolah');
            $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
            $listKelas = array();
            $emailPengguna = array();
            $nomorIndukPengguna = array();
            $checkEmail = array();
            $checkNomorInduk = array();
            for($i=0; $i<count($pengguna); $i++)
            {
                /*if(!is_null($pengguna[$i]['email']))
                {
                    $checkEmail[$pengguna[$i]['email']] = $pengguna[$i];
                    $checkEmail[$pengguna[$i]['email']]['indeks'] = $i;
                    array_push($emailPengguna, $pengguna[$i]['email']);
                }*/
                $checkNomorInduk[$pengguna[$i]['nomor_induk']] = $pengguna[$i];
                $checkNomorInduk[$pengguna[$i]['nomor_induk']]['indeks'] = $i;
                array_push($nomorIndukPengguna, $pengguna[$i]['nomor_induk']);
            }
            //$this->siswa->nonAktifAll($id_sekolah);
            $owned = $this->siswa->getOwnedPengguna($id_sekolah, $emailPengguna, $nomorIndukPengguna)->result_array();
            //$change = array('email' => array(), 'nomor_induk' => array());
            $temp = $pengguna;
            //$new = array();
            $date = date('Y-m-d H:i:s');
            #check email
            for($i=0; $i<count($owned); $i++)
            {
                $kelas = NULL;
                $id_siswa = NULL;
                $email = $owned[$i]['email'];
                $nomorInduk = $owned[$i]['nomor_induk'];
                if(isset($checkEmail[$email]))
                {
                    $data = $checkEmail[$email];
                    $data['id_pengguna'] = $owned[$i]['id_pengguna'];
                    $data['date'] = $date;
                    $data['type'] = 'email';
                    $id_siswa = $this->siswa->addExcel($id_sekolah, $data);
                    $kelas = $temp[$checkEmail[$email]['indeks']]['kelas'];

                    unset($temp[$checkEmail[$email]['indeks']]);
                    if($nomorInduk == $checkEmail[$email]['nomor_induk'])
                        unset($checkNomorInduk[$nomorInduk]);
                }
                if(isset($checkNomorInduk[$nomorInduk]))
                {
                    $data = $checkNomorInduk[$nomorInduk];
                    $data['id_pengguna'] = $owned[$i]['id_pengguna'];
                    $data['date'] = $date;
                    $data['type'] = 'nomor_induk';
                    $id_siswa = $this->siswa->addExcel($id_sekolah, $data);
                    $kelas = $temp[$checkNomorInduk[$nomorInduk]['indeks']]['kelas'];

                    unset($temp[$checkNomorInduk[$nomorInduk]['indeks']]);
                }   
                if(!is_null($kelas))
                {
                    if(!isset($listKelas[$kelas]))
                        $listKelas[$kelas] = array('kelas' => $kelas, 'id_siswa' => array());
                    if(!is_null($id_siswa))
                        array_push($listKelas[$kelas]['id_siswa'], $id_siswa);
                }
            }
            foreach($temp as $new)
            {
                $kelas = $new['kelas'];
                $data = $new;
                $data['date'] = $date;
                $id_siswa = $this->siswa->addExcel($id_sekolah, $data, TRUE);
                if(!is_null($kelas))
                {
                    if(!isset($listKelas[$kelas]))
                        $listKelas[$kelas] = array('kelas' => $kelas, 'id_siswa' => array());
                    array_push($listKelas[$kelas]['id_siswa'], $id_siswa);
                }
            }
            //check Kelas Siswa
            foreach($listKelas as $kelas)
            {
                $tambahan = 0;
                $kelasrombel = explode('-', $kelas['kelas']);
                $data = array(
                    'nm_kelas' => $kelasrombel[0], 
                    'rombel_kelas' => NULL
                );
                if(count($kelasrombel)>1)
                    $data['rombel_kelas'] = $kelasrombel[1];

                $check = $this->Kelas->checkKelas($id_sekolah, $tahunAjaran, $data)->result_array();
                if(count($check)>0)
                {
                    $id_kelas = $check[0]['id_kelas'];
                    $jumlah_siswa = (int)$check[0]['jumlah_siswa'];
                }
                else
                {
                    $data['id_sekolah'] = $id_sekolah;
                    $data['tahun_ajaran'] = $tahunAjaran;
                    $data['created_at'] = $date;
                    $data['updated_at'] = $date;
                    $id_kelas = $this->Kelas->insert($data);
                    $jumlah_siswa = 0;
                }

                foreach($kelas['id_siswa'] as $id_siswa)
                {
                    $data = array(
                        'id_siswa' => $id_siswa,
                        'id_kelas' => $id_kelas
                    );
                    $check = $this->Kelas->checkKelasSiswa($data)->result_array();
                    if(count($check)<1)
                    {
                        $data['tahun_ajaran'] = $tahunAjaran;
                        $data['created_at'] = $date;
                        $data['updated_at'] = $date;
                        $this->Kelas->addKelasSiswa($data);
                        $tambahan++;
                    }
                }
                $jumlah_siswa = $jumlah_siswa + $tambahan;
                $this->Kelas->update(array('jumlah_siswa' => $jumlah_siswa), array('id_kelas' => $id_kelas));
            }
        }
        redirect(base_url().'dashboard/siswa');
    }

    public function uploadSiswa()
    {
        if(isset($_FILES['userfile']['name']) && $_FILES['userfile']['size'] > 0 && FALSE)
        {
            $arr_file = explode('.', $_FILES['userfile']['name']);
            $extension = end($arr_file);
         
            if($extension == 'xls')
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            elseif($extension == 'xlsx')
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            else
                redirect(base_url().'dashboard/siswa');

            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($_FILES['userfile']['tmp_name']);
             
            $sheet = $spreadsheet->getSheet(0);
            
            $i = 0;
            $row = 2;
            // Nama, Nomor Induk, Kelas, Email, No Telepon, Alamat
            $pengguna = array();
            while($sheet->getCell('A'.$row)->getValue() != null)
            {
                $pengguna[$i]['nm_pengguna'] = $sheet->getCell('A'.$row)->getValue();
                $pengguna[$i]['nomor_induk'] = $sheet->getCell('B'.$row)->getValue();
                $pengguna[$i]['kelas'] = $sheet->getCell('C'.$row)->getValue();
                $pengguna[$i]['email'] = $sheet->getCell('D'.$row)->getValue();
                $pengguna[$i]['no_hp'] = $sheet->getCell('E'.$row)->getValue();
                $pengguna[$i]['alamat'] = $sheet->getCell('F'.$row)->getValue();

                $i++;
                $row++;
            }
            $this->confirmUpload($pengguna);
        }
        redirect(base_url().'dashboard/siswa');
    }
    public function stopWords($num)
    {
        $ds = str_replace(' ', '', str_replace('+', '', $num));
        return $ds;
    }
	public function create () {
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/siswa/createSiswa');
		$this->load->view('layout/footer');
    }
    public function deleteSiswa($id)
    {
        if ($this->siswa->deleteSiswa($id)) {
            $this->session->set_flashdata('msg', "<script> notify('Sukses. Data anda telah terhapus', 'success');</script>");
            redirect(base_url().'dashboard/siswa');
        }
        else{
            $this->session->set_flashdata('msg', "<script> notify('Gagal. Gagal menghapus data', 'warning');</script>");
            redirect(base_url().'dashboard/siswa');

        }
    }
    public function store () {
        $phone = $this->stopWords($this->input->post('phone'));
    	$data = array(
    		'nama' => $this->input->post('nama'),
    		'nomor' => $this->input->post('nomor'),
    		'alamat' => $this->input->post('alamat'),
    		'phone' => $phone,
    		'email' => $this->input->post('email'),
    		'id' => $this->input->post('id'),
    		'role' => '1',
            'status' => 1,
            'sekolah' => $this->session->userdata('sess_sekolah')
    	);
    	$this->siswa->insert($data);
        $this->session->set_flashdata('msg', "<script> notify('Sukses. Data anda telah tersimpan', 'success');</script>");
        redirect(base_url().'dashboard/siswa');
    }
    public function index()
    {
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
    	$read = $this->siswa->getSiswaAndKelas($this->session->userdata('sess_sekolah'), $tahunAjaran)->result_array();
    	$data = array('read' => $read, 'tahunAjaran' => $tahunAjaran);
        $this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/siswa/listSiswa', $data);
		$this->load->view('layout/footer');
    }
    public function edit($id)
    {
    	$read = $this->siswa->readSiswaWhere($id, $this->session->userdata('sess_sekolah'))->result_array();
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$data = array('read' =>$read);
		$this->load->view('page/siswa/createSiswa', $data);
		$this->load->view('layout/footer');
    }
    public function showQR($id_siswa)
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $read = $this->siswa->readSiswaWhere($id_siswa, $id_sekolah)->result_array();
        if(count($read)>0)
        {
            $nim = $read[0]['nomor_induk'];
            if(!$this->session->userdata('sess_old_absen'))
                $nim = $nim . '<-->' . $read[0]['id_pengguna'] . '<-->'. $read[0]['id_pengguna_sekolah'];
            $this->load->library('ciqrcode');
            $params['data'] = $nim;
            $params['level'] = 'H';
            $params['size'] = 10;
            $gambar = $this->ciqrcode->generate($params);
            header("Content-Type: image/png");
            imagepng($gambar);
        }
        else
            redirect(base_url('dashboard/siswa'));
    }
    public function downloadZip()
    {
        $files = FCPATH . 'assets/qr';
        if (!is_dir($files)) {
            mkdir($files, 0777, TRUE);
        }

        $sekolah = $this->sekolah->getSekolah($this->session->userdata('sess_sekolah'))[0];
        $this->generateQR($sekolah);

    	$path ='assets/qr/'.$sekolah['id_sekolah'].'/siswa/';
    	$this->zip->read_dir($path, FALSE);
        $this->zip->download('All QR Siswa Download.zip');
        $this->session->set_flashdata('msg', '<script>swal("Success !!", "QR Code Berhasil Didownlaod", "success");</script>');
        redirect(base_url('dashboard/siswa'));
    }
    public function generateQR($sekolah)
    {
    	$read = $this->siswa->readSiswa($this->session->userdata('sess_sekolah'))->result_array();
        $path = 'assets/qr/'.$sekolah['id_sekolah'].'/siswa/';
        if (!is_dir(FCPATH.$path))
            mkdir(FCPATH.$path, 0777, TRUE);
        else
        {
            $files = glob(FCPATH.$path.'*'); // get all file names
            foreach($files as $file){ // iterate files
                if(is_file($file))
                    unlink($file); // delete file
            }
        }
    	foreach ($read as $r)
        {
            $nim = $r['nomor_induk'];

            if(!$this->session->userdata('sess_old_absen'))
                $nim = $nim . '<-->' . $r['id_pengguna'] . '<-->'. $r['id_pengguna_sekolah'];

			$name = 'qrsiswa'.$nim;
			$config['cacheable']    = true; //boolean, the default is true
	        $config['cachedir']     = 'assets/'; //string, the default is application/cache/
	        $config['errorlog']     = 'assets/'; //string, the default is application/logs/
	        $config['imagedir']     = $path; //direktori penyimpanan qr code
	        $config['quality']      = true; //boolean, the default is true
	        $config['size']         = '1024'; //interger, the default is 1024
	        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
	        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
	        $this->ciqrcode->initialize($config);
	 
	        $image_name=$name.'.png'; //buat name dari qr code sesuai dengan nim
	 
	        $params['data'] = $nim; //data yang akan di jadikan QR CODE
	        $params['level'] = 'H'; //H=High
	        $params['size'] = 10;
	        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/qr/
	        $this->ciqrcode->generate($params);
    	}
    	
    }
}