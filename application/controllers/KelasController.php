<?php
class KelasController extends CI_Controller {

	public function __construct(){
		
		parent::__construct();
        if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') != 2) {
            $url = base_url() . 'login';
            redirect($url);
        }
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('Kelas');

	}

    public function index()
    {
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
    	$data = $this->Kelas->getListKelas($this->session->userdata('sess_sekolah'), $tahunAjaran);  
        $this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/kelas/listKelas', array('data' => $data, 'tahunAjaran' => $tahunAjaran));
		$this->load->view('layout/footer');
    }

    private function confirmUpload($kelas, $detail)
    {
        if(count($kelas)>0)
        {
            $id_sekolah = $this->session->userdata('sess_sekolah');
            $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
            $date = date('Y-m-d H:i:s');
            $update = array();
            $new = array();
            $siswa = array(
                'id_sekolah' => $id_sekolah,
                'date' => $date
            );
            for($i=0; $i<count($kelas); $i++)
            {
                $fullKelas = $kelas[$i]['nm_kelas'] . $kelas[$i]['rombel_kelas'];
                $ketua = null;
                $wali = null;
                $totalSiswa = count($detail[$fullKelas]['siswa']);
                if(isset($detail[$fullKelas]['ketua']['nomor_induk']))
                {
                    $ketua = $detail[$fullKelas]['ketua'];
                    $ketua['id_sekolah'] = $id_sekolah;
                    $ketua['date'] = $date;
                    $ketua = $this->Kelas->checkPengguna($ketua);
                }
                if(isset($detail[$fullKelas]['wali']['nomor_induk']))
                {
                    $wali = $detail[$fullKelas]['wali'];
                    $wali['id_sekolah'] = $id_sekolah;
                    $wali['date'] = $date;
                    $wali = $this->Kelas->checkPengguna($wali, 1);
                }
                //Cek tiap kelas, kalo ada di update, delete semua relasi dari kelas siswa, di cek wali kelas sama siswa kalo gak ada buat baru, ditambahin relasi ke kelas siswa
                $cekKelas = $this->Kelas->checkKelas($id_sekolah, $tahunAjaran, $kelas[$i])->result_array();
                if(count($cekKelas)>0)
                {
                    $data = array(
                        'id_ketua_kelas' => $ketua,
                        'wali_kelas' => $wali,
                        'jumlah_siswa' => $totalSiswa,
                        'updated_at' => $date
                    );
                    $this->Kelas->excelKelas($data, array('id_kelas' => $cekKelas[0]['id_kelas']));
                    $this->Kelas->emptyKelas($cekKelas[0]['id_kelas'], $tahunAjaran);

                    foreach($detail[$fullKelas]['siswa'] as $temp)
                    {
                        $siswa['nm_pengguna'] = $temp['nm_pengguna'];
                        $siswa['nomor_induk'] = $temp['nomor_induk'];
                        $id_siswa = $this->Kelas->checkPengguna($siswa);
                        $data = array(
                            'id_siswa' => $id_siswa,
                            'id_kelas' => $cekKelas[0]['id_kelas'],
                            'tahun_ajaran' => $tahunAjaran,
                            'created_at' => $date,
                            'updated_at' => $date
                        );
                        $this->Kelas->addKelasSiswa($data);
                    }
                }
                else
                {
                    $data = array(
                        'id_sekolah' => $id_sekolah,
                        'tahun_ajaran' => $tahunAjaran,
                        'nm_kelas' => $kelas[$i]['nm_kelas'],
                        'rombel_kelas' => $kelas[$i]['rombel_kelas'],
                        'id_ketua_kelas' => $ketua,
                        'wali_kelas' => $wali,
                        'jumlah_siswa' => count($detail[$fullKelas]['siswa']),
                        'created_at' => $date,
                        'updated_at' => $date
                    );
                    $id_kelas = $this->Kelas->excelKelas($data);

                    foreach($detail[$fullKelas]['siswa'] as $temp)
                    {
                        $siswa['nm_pengguna'] = $temp['nm_pengguna'];
                        $siswa['nomor_induk'] = $temp['nomor_induk'];
                        $id_siswa = $this->Kelas->checkPengguna($siswa);
                        $data = array(
                            'id_siswa' => $id_siswa,
                            'id_kelas' => $id_kelas,
                            'tahun_ajaran' => $tahunAjaran,
                            'created_at' => $date,
                            'updated_at' => $date
                        );
                        $this->Kelas->addKelasSiswa($data);
                    }
                }
            }
        }
        redirect(base_url().'dashboard/kelas');
    }


    public function uploadKelas()
    {
        if(isset($_FILES['userfile']['name']) && $_FILES['userfile']['size'] > 0 && FALSE)
        {
            $arr_file = explode('.', $_FILES['userfile']['name']);
            $extension = end($arr_file);
         
            if($extension == 'xls')
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            elseif($extension == 'xlsx')
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            else
                redirect(base_url().'dashboard/siswa');
            
            $reader->setReadDataOnly(false);
            $spreadsheet = $reader->load($_FILES['userfile']['tmp_name']);
             
            $sheet = $spreadsheet->getSheet(0);
                        
            //kelas : nm_kelas, rombel_kelas, id_ketua_kelas, id_wali_kelas, jumlah_siswa
            //siswa : nomor_induk, nm_pengguna
            //ketua : nomor_induk, nm_pengguna
            //wali : nomor_induk, nm_pengguna
            $col = 'A'; //starting col
            $row = 5; //starting row
            $kelas = array();
            $detail = array();
            $stop = 0; // max 5;
            while($stop<6)
            {
                $limit = 0; // max 5;
                $row = 5;
                $col1 = $col;
                $col1++;
                $col2 = $col1;
                $col2++;
                $wali = array();
                $ketua = array();
                while($limit<6)
                {
                    if(sanitize($sheet->getCell($col.$row)->getValue()) == 'Kelas')
                    {
                        $limit = 0;
                        $stop = 0;
                        $nm_kelas = $sheet->getCell($col1.$row)->getValue();
                        $rombel_kelas = $sheet->getCell($col2.$row)->getValue();
                        $kelas[count($kelas)] = array(
                            'rombel_kelas' => $rombel_kelas,
                            'nm_kelas' => $nm_kelas
                        );
                        $row++;
                        $wali = array(
                            'nm_pengguna' => $sheet->getCell($col1.$row)->getValue(),
                            'nomor_induk' => $sheet->getCell($col2.$row)->getValue()
                        );
                        $row = $row+2;
                        $siswa = array();
                        while($sheet->getCell($col.$row)->getValue() != null &&
                            $sheet->getCell($col1.$row)->getValue() != null &&
                            $sheet->getCell($col2.$row)->getValue() != null)
                        {
                            $siswa[count($siswa)] = array(
                                'nomor_induk' => $sheet->getCell($col1.$row)->getValue(),
                                'nm_pengguna' => $sheet->getCell($col2.$row)->getValue()
                            );
                            if($sheet->getStyle($col2.$row)->getFill()->getStartColor()->getRGB() != '000000')
                                $ketua = $siswa[count($siswa)-1];
                            $row++;
                        }
                        $detail[$nm_kelas.$rombel_kelas] = array(
                            'wali' => $wali,
                            'ketua' => $ketua,
                            'siswa' => $siswa
                        );
                    }
                    else
                    {
                        $row++;
                        $limit++;
                    }
                }
                $col++;
                $stop++;
            }
            $this->confirmUpload($kelas, $detail);
        }
        else
            redirect(base_url().'dashboard/kelas');
    }

    public function create () {
    	$guru 	= $this->Kelas->getGuru($this->session->userdata('sess_sekolah'));
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/kelas/createKelas',array('guru' => $guru));
		$this->load->view('layout/footer');	
    }
    public function checkGuru($id)
    {
        $guru = $this->Kelas->getWaliKelas($id)->result_array();
        foreach ($guru as $g) {
            $idWK = $g['id_pengguna'];
        }
        $kelas = $this->Kelas->getKelasByGuru($idWK)->num_rows();
        if ($kelas!=0) {
            # code...
        }
    }
    public function inpKelasSiswa($id_kelas)
    {
        $kelas = $this->Kelas->getKelasById($id_kelas)->result_array();
        if(count($kelas)>0 && $kelas[0]['id_sekolah'] == $this->session->userdata('sess_sekolah'))
        {
            $id_list = $this->siswa->getIdSiswaByKelas($id_kelas);
            $filesCount = count($_POST['id']);
            $jumlah = 0;
            for ($i=0; $i < $filesCount; $i++) 
            { 
                $id = $_POST['id'][$i];
                if (isset($_POST['idKlS'][$i])) {
                    $idKelas = $_POST['idKlS'][$i];
                }
                else{
                    $idKelas = null;
                }
                
                $data = array(
                    'id_kelas_siswa'=>$idKelas,
                    'id_kelas'=> $id_kelas, 
                    'id_siswa' => $id, 
                    'tahun_ajaran' => $kelas[0]['tahun_ajaran'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                if(!in_array($id, $id_list))
                {
                    $this->siswa->insKelasSiswa($data);
                    $id_list[count($id_list)] = $id;
                    $jumlah++;
                }
            }
            $update = array(
                'jumlah_siswa' => ($jumlah + $kelas[0]['jumlah_siswa']),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->Kelas->update($update, array('id_kelas' => $id_kelas));
            $this->session->set_flashdata('msg', "<script> notify('Sukses. Data anda telah tersimpan', 'success');</script>");
            redirect(base_url().'dashboard/kelas/tambah-siswa/'.$id_kelas);
        }
        redirect(base_url().'dashboard/kelas');
    }

    public function pilihKetuaKelas($id_siswa)
    {
        if((int)$this->input->get('kelas')>0)
        {
            $kelas = $this->Kelas->getKelasById((int)$this->input->get('kelas'))->result_array();
            if(count($kelas)>0 && $kelas[0]['id_sekolah'] == $this->session->userdata('sess_sekolah'))
            {
                $data = array(
                    'id_siswa' => $id_siswa,
                    'id_kelas' => (int)$this->input->get('kelas')
                );
                $checkSiswa = $this->Kelas->checkKelasSiswa($data)->result_array();
                if(count($checkSiswa)>0)
                {
                    $this->Kelas->pilihKetuaKelas($id_siswa, $data['id_kelas']);
                    $this->session->set_flashdata('msg', "<script> notify('Sukses. Ketua Kelas berhasil dipilih', 'success');</script>");
                    redirect(base_url().'dashboard/kelas/tambah-siswa/'.$data['id_kelas']);
                }
                $this->session->set_flashdata('msg', "<script> notify('Gagal. Pemilihan Ketua Kelas gagal', 'danger');</script>");
                redirect(base_url().'dashboard/kelas/tambah-siswa/'.$data['id_kelas']);
            }
        }
        redirect(base_url().'dashboard/kelas');
    }

    public function deleteFromKelas($id_siswa)
    {
        if((int)$this->input->get('kelas')>0)
        {
            $kelas = $this->Kelas->getKelasById((int)$this->input->get('kelas'))->result_array();
            if(count($kelas)>0 && $kelas[0]['id_sekolah'] == $this->session->userdata('sess_sekolah'))
            {
                if($this->siswa->deleteFromKelas($id_siswa, $kelas[0]))
                {
                    $this->session->set_flashdata('msg', "<script> notify('Sukses. Siswa berhasil dikeluarkan', 'success');</script>");
                    redirect(base_url().'dashboard/kelas/tambah-siswa/'.(int)$this->input->get('kelas'));
                }
                $this->session->set_flashdata('msg', "<script> notify('Gagal. Terjadi kesalahan saat mengeluarkan siswa', 'danger');</script>");
                redirect(base_url().'dashboard/kelas/tambah-siswa/'.(int)$this->input->get('kelas'));
            }
        }
        redirect(base_url().'dashboard/kelas');
    }
    public function addKelasSiswa($kelas) {
    	$read = $this->siswa->readSiswa($this->session->userdata('sess_sekolah'))->result_array();
    	$siswa = $this->siswa->getSiswaByKelas($kelas)->result_array();
    	$data = $this->Kelas->getKelasById($kelas)->result();   
    	$pass = array('data' => $data, 'read' => $read, 'siswa' => $siswa); 	
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/kelas/inputKelasSiswa', $pass);
		$this->load->view('layout/footer');
    }

    public function store() {
    	$this->form_validation->set_rules('nm_kelas', 'Kelas', 'required');
    	$this->form_validation->set_rules('rombel_kelas', 'Rombel', 'required');
    	 // $this->form_validation->set_rules('jumlah_siswa', 'Kelas', 'required');

    	if ($this->form_validation->run() == FALSE)
        {
            redirect(base_url().'dashboard/kelas/tambah');
        }
        else
        {
        	$data = array(
        		'nm_kelas' => sanitizeNoSpace($this->input->post('nm_kelas')),
                'rombel_kelas' => sanitizeNoSpace($this->input->post('rombel_kelas')),
        		'jumlah_siswa' => 0,
                'id_ketua_kelas' => NULL,
        		'wali_kelas' => sanitize($this->input->post('wali_kelas')),
                'tahun_ajaran' => $this->session->userdata('sess_tahun_ajaran'),
                'id_sekolah' => $this->session->userdata('sess_sekolah'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	);
            $id = $this->Kelas->insert($data);
            redirect(base_url().'dashboard/kelas/tambah-siswa/'.$id);
        }
    }

    public function show ($id) 
    {
        $kelas = $this->Kelas->getKelasById($id)->result_array();
        if(count($kelas)>0 && $kelas[0]['id_sekolah'] == $this->session->userdata('sess_sekolah'))
        {
            $guru   = $this->Kelas->getGuru($this->session->userdata('sess_sekolah'));
            $data = $this->Kelas->getKelasById($id)->result();      
            $this->load->view('layout/header');
            $this->load->view('layout/navigationbar');
            $this->load->view('page/kelas/showKelas', array('data' => $data, 'guru' => $guru));
            $this->load->view('layout/footer');     
        }
        else
            redirect(base_url().'dashboard/kelas');
    	
    }

    public function update ($id) 
    {
        $kelas = $this->Kelas->getKelasById($id)->result_array();
        if(count($kelas)>0 && $kelas[0]['id_sekolah'] == $this->session->userdata('sess_sekolah'))
        {
            $this->form_validation->set_rules('nm_kelas', 'Kelas', 'required');
            $this->form_validation->set_rules('rombel_kelas', 'Rombel', 'required');

            if ($this->form_validation->run() == TRUE)
            {
                $data = array(
                    'nm_kelas' => sanitizeNoSpace($this->input->post('nm_kelas')),
                    'rombel_kelas' => sanitizeNoSpace($this->input->post('rombel_kelas')),
                    'wali_kelas' => sanitize($this->input->post('wali_kelas')),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $this->Kelas->update($data, array('id_kelas' => $id));
            }
        }
        redirect(base_url().'dashboard/kelas'); 
    }

    public function destroy ($id) 
    {
        $kelas = $this->Kelas->getKelasById($id)->result_array();
        if(count($kelas)>0 && $kelas[0]['id_sekolah'] == $this->session->userdata('sess_sekolah'))
        {
            $this->Kelas->deleteKelas($id);
        }
    	redirect(base_url().'dashboard/kelas');
    }
}