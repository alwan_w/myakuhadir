<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardGuru extends CI_Controller {
	
    private $id_sekolah;
	public function __construct(){
		
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('HasilAbsensi');
		$this->load->database();
	
		// session_start();
		
		// if(!isset($_SESSION['is_login'])){
		
		// 	redirect('/auth/');
		// }
        $this->id_sekolah = $this->session->userdata('sess_sekolah');
	}

    public function guru_addNotes () {
        $note = json_decode(file_get_contents('php://input'),true)['notes'];
        $jenis = json_decode(file_get_contents('php://input'),true)['jenis'];
        $this->db->query('INSERT INTO `dashboard_notes` (jenis, notes, created_at) VALUES ('.$jenis.', "'.$note.'", "'.date('Y-m-d H:i:s').'")');
        $array = array(
            'status' => 200
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_deleteNotes ($id) {
        $this->db->where('id', $id)->delete('dashboard_notes');
        $array = array(
            'status' => 200
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_updateNotes ($id) {
        $note = json_decode(file_get_contents('php://input'),true)['notes'];
        $this->db->set(array('notes' => $note))->where('id', $id)->update('dashboard_notes');
        $array = array(
            'status' => 200
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_viewNotes ($id) {
        $getter = $this->db->from('dashboard_notes')->where('id', $id)->get()->result()[0];
        $array = array(
            'note' => $getter
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_getNotes () {
        $getter = $this->db->from('dashboard_notes')->order_by('created_at', 'DESC')->get()->result();
        $array = array(
            'notes' => $getter
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));  
    }

    public function guru_detailKeterlambatanOrang ($id) {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $query = "SELECT * from absensi_guru 
                    WHERE jenis='in'
                    AND kode_absensi = '".$id."'
                    AND id_sekolah = '".$this->id_sekolah."'
                    AND (max=1 OR min=1) 
                    AND timestamp >= '".$start."' 
                    AND timestamp <= '".$end."'";
        $getter = $this->db->query($query)->result();
        $data = [];
        foreach ($getter as $g) {
            $data[] = array(
                'tanggal' => date('d F Y', strtotime($g->timestamp)),
                'jenis' => $g->min == 1 ? 'min' : 'max',
                'waktu' => date('H:i', strtotime($g->timestamp))
            );
        };
        $array = array(
            'nama' => $getter[0]->nama,
            'kode_absensi' => $getter[0]->kode_absensi,
            'data' => $data
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_presentaseTerlambat () {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $keterlambatan = $this->db->query("SELECT COUNT(*) as late from absensi_guru WHERE id_sekolah = '".$this->id_sekolah."' AND jenis='in' AND (max=1 OR min=1) AND timestamp >= '".$start."' AND timestamp <= '".$end."'")->result();
        $expectedValue_Tepatwaktu = $this->db->query("SELECT COUNT(*) as expect from absensi_guru WHERE id_sekolah = '".$this->id_sekolah."' AND jenis='in' AND timestamp >= '".$start."' AND timestamp <= '".$end."'")->result();
        // var_dump($keterlambatan[0]->late); die();
        $value = ($expectedValue_Tepatwaktu[0]->expect == 0 )? 0 :($keterlambatan[0]->late / $expectedValue_Tepatwaktu[0]->expect) * 100;
        $array = array(
            'percent' => round($value,2)
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

     public function guru_kehadiran () {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $expectedValue_Tepatwaktu = $this->db->query("SELECT COUNT(*) as expect from absensi_guru WHERE id_sekolah = '".$this->id_sekolah."' AND jenis='in' AND timestamp >= '".$start."' AND timestamp <= '".$end."'")->result();
        $array = array(
            'data' => $expectedValue_Tepatwaktu[0]->expect
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_jumlahGuru () {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        // $guru = $this->db->query('SELECT DISTINCT nama from absensi_guru WHERE timestamp >= "'.$start.'" AND timestamp <= "'.$end.'"')->result();
        $guru = $this->db->query('SELECT * from pengguna_sekolah JOIN pengguna ON pengguna.id_pengguna=pengguna_sekolah.id_pengguna WHERE id_sekolah = '."'".$this->id_sekolah."'".' AND id_role = 3 OR id_role = 2')->result();
        $array = array(
            'count' => count($guru)
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_updateData () {
        $tanggal = $this->db->query('SELECT DATE_FORMAT(timestamp, "%Y-%m-%d") as tanggal FROM absensi_guru WHERE id_sekolah = '."'".$this->id_sekolah."'".' ORDER BY timestamp DESC')->result();
        $array = array(
            'date' => $tanggal[0]->tanggal
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_keterlambatanMinMax () {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $min5 = $this->db->query("SELECT COUNT(*) as min from absensi_guru WHERE id_sekolah = '".$this->id_sekolah."' AND jenis='in' AND min=1 AND timestamp >= '".$start."' AND timestamp <= '".$end."'")->result();
        $plus5 = $this->db->query("SELECT COUNT(*) as plus from absensi_guru WHERE id_sekolah = '".$this->id_sekolah."' AND jenis='in' AND max=1 AND timestamp >= '".$start."' AND timestamp <= '".$end."'")->result();
        $array = array(
            'data' => array('min'=>$min5[0]->min, 'plus'=>$plus5[0]->plus)
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_totalKeterlambatan  () {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d', strtotime("+1 days"));
        }
        $getTerlambatNow = count($this->HasilAbsensi->getList($start, $end, 'terlambat'));
        $newstart = date('Y-m-d', strtotime("-1 month", strtotime($start)));
        $newend = date('Y-m-d', strtotime("+1 month", strtotime($newstart)));
        $getTerlambatYesterday = count($this->HasilAbsensi->getList($newstart, $newend, 'terlambat'));
        if ($getTerlambatYesterday == 0) {
            $output = '+ 100';
            $status = 'up';
        }elseif ($getTerlambatYesterday > $getTerlambatNow) {
            $output = '- '.number_format(100 - (($getTerlambatNow / $getTerlambatYesterday) * 100), 2);
            $status = 'down';
        } elseif ($getTerlambatYesterday < $getTerlambatNow) {
            $output = '+ '.number_format((($getTerlambatNow / $getTerlambatYesterday) * 100) - 100, 2);
            $status = 'up';
        } else{
            $output = '- 0';
            $status = 'down';
        }


        $array = array(
            'keterlambatan' => $getTerlambatNow,
            'rate' => $output,
            'status' => $status
        );
        $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_pieKeterlambatan () {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $keterlambatan = $this->db->query("SELECT COUNT(*) as late from absensi_guru WHERE id_sekolah = '".$this->id_sekolah."' AND jenis='in' AND (max=1 OR min=1) AND timestamp >= '".$start."' AND timestamp <= '".$end."'")->result();
        $expectedValue_Tepatwaktu = $this->db->query("SELECT COUNT(*) as expect from absensi_guru WHERE id_sekolah = '".$this->id_sekolah."' AND jenis='in' AND max=0 AND min=0 AND timestamp >= '".$start."' AND timestamp <= '".$end."'")->result();
        $pie[] = array('name' => 'Terlambat', 'y' => (int)$keterlambatan[0]->late);
        $pie[] = array('name' => 'Tidak terlambat', 'y' => (int)$expectedValue_Tepatwaktu[0]->expect);
        $array = array(
            'data' => $pie
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_pieJenisKeterlambatan () {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $min5 = $this->db->query("SELECT COUNT(*) as min from absensi_guru WHERE id_sekolah = '".$this->id_sekolah."' AND jenis='in' AND min=1 AND timestamp >= '".$start."' AND timestamp <= '".$end."'")->result();
        $plus5 = $this->db->query("SELECT COUNT(*) as plus from absensi_guru WHERE id_sekolah = '".$this->id_sekolah."' AND jenis='in' AND max=1 AND timestamp >= '".$start."' AND timestamp <= '".$end."'")->result();
        $keterlambatan = $this->db->query("SELECT COUNT(*) as late from absensi_guru WHERE id_sekolah = '".$this->id_sekolah."' AND jenis='in' AND (max=1 OR min=1) AND timestamp >= '".$start."' AND timestamp <= '".$end."'")->result();
        $expectedValue_Tepatwaktu = $this->db->query("SELECT COUNT(*) as expect from absensi_guru WHERE id_sekolah = '".$this->id_sekolah."' AND jenis='in' AND timestamp >= '".$start."' AND timestamp <= '".$end."'")->result();
        $pie[] = array('name' => 'MIN 5', 'y' => (int)$min5[0]->min, 'color' => '#19b5fe');
        $pie[] = array('name' => 'PLUS 5', 'y' => (int)$plus5[0]->plus, 'color' => '#2574a9');
        // $pie[] = array('name' => 'Tidak terlambat', 'y' => $expectedValue_Tepatwaktu[0]->expect - $keterlambatan[0]->late);
        $array = array(
            'data' => $pie
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_keterlambatanHarian () {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $getLatePerDay = $this->HasilAbsensi->getDailyLate($start, $end);
        // var_dump($getTop10Late); die();
        $count = [];
        if (count($getLatePerDay) != 0) {
            foreach ($getLatePerDay as $key) {
                $day[] = date('d M y', strtotime($key->day));
                $count[] = (int)$key->count;
            }
        } else {
            $day = [];
            $count = [];
        }
        
        $getLatePerDay = array('count' => $count, 'day' => $day);

        $array = array(
            'count' => $count,
            'labels' => $day            
        );
        $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_tablePalingSeringTelat () {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d', strtotime("+1 days"));
        }
        $getTop10Late = $this->HasilAbsensi->getPeopleLate($start, $end);
        $array = array(
            'data' => $getTop10Late
        );
        $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

     public function guru_jurnalJumlahTerlambat ($kelas) {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $keterlambatan = $this->HasilAbsensi->getJurnalLate($start, $end, $kelas);
        $array = array(
            'data' => ($keterlambatan[0]->count == null ) ? '0': $keterlambatan[0]->count
        );
        $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_getJurnal ($kelas) {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $data = $this->db->query('
            SELECT jurnal_guru_ekstra.*, DATE_FORMAT(jurnal_guru_ekstra.created_at, "%Y-%m-%d") as tanggal, mata_pelajaran.nm_mata_pelajaran as mapel, pengguna.nm_pengguna as guru, kelas.nm_kelas, (jadwal_guru.jam_mulai + INTERVAL 11 MINUTE < jurnal_guru_ekstra.jam_hadir) as terlambat, jadwal_guru.* 
            FROM `jurnal_guru_ekstra`
            LEFT JOIN `jadwal_guru` ON jadwal_guru.id_jadwal_guru=jurnal_guru_ekstra.id_jadwal_guru
            LEFT JOIN mata_pelajaran ON mata_pelajaran.id_mata_pelajaran=jadwal_guru.id_mata_pelajaran
            LEFT JOIN pengguna ON pengguna.id_pengguna=jurnal_guru_ekstra.id_guru
            LEFT JOIN kelas ON kelas.id_kelas=jurnal_guru_ekstra.id_kelas
            WHERE jurnal_guru_ekstra.id_kelas = "'.$kelas.'"
            ')->result();
        $dataReady = [];
        foreach ($data as $d) {
            $dataReady[] = array(
                'id' => $d->id_jurnal,
                'terlambat' => $d->terlambat == 1 ? true:false,
                'guru' => $d->guru,
                'mapel' => $d->mapel,
                'mulai' => $d->jam_mulai,
                'selesai' => $d->jam_selesai,
                'hadir' => $d->jam_hadir,
                'keluar' => $d->jam_keluar,
                'date' => $d->tanggal,
                'kelas' => $d->nm_kelas,
                'kompetensi' => $d->kompetensi_dasar,
                'catatan' => $d->catatan,
            );
        }
        $array = array(
            'data' => $dataReady
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

    public function guru_terlambatJurnalPerorang () {
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        if (isset($start) && isset($end)) {                     
            $start = $this->input->get('start', TRUE);
            $end = $this->input->get('end', TRUE);
        } else {
            $start = date('Y-m-01');
            $end = date('Y-m-d');
        }
        $keterlambatan = $this->HasilAbsensi->getJurnalLateByPerson_new($start, $end);

        // var_dump($keterlambatan); die();
        $array = array(
            'data' => $keterlambatan
        );
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($array));
    }

}

?>