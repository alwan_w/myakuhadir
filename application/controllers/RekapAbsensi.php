<?php 

class rekapAbsensi extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') != 2) {
            $url = base_url() . 'login';
            redirect($url);
        }
	}
	public function cob($id)
	{
		echo $id;
		  $msg = '5215100061'; //Plain text 
           $key = 'siits'; //Key 32 character 
           //default menggunakan MCRYPT_RIJNDAEL_256 
           $hasil_enkripsi = $this->encrypt->encode($msg, $key);  
           $hasil_dekripsi = $this->encrypt->decode($id, $key); 
           echo "Pesan yang mau dienkripsi: ".$msg."<br/><br/>"; 
           echo "Hasil enkripsi: ".$hasil_enkripsi."<br/><br/>"; 
           echo "Hasil dekripsi: ".$hasil_dekripsi."<br/><br/>"; 
	}
	public function getMonthName($month)
    {
        if($month==1) return 'Januari';
        if($month==2) return 'Februari';
        if($month==3) return 'Maret';
        if($month==4) return 'April';
        if($month==5) return 'Mei';
        if($month==6) return 'Juni';
        if($month==7) return 'Juli';
        if($month==8) return 'Agustus';
        if($month==9) return 'September';
        if($month==10) return 'Oktober';
        if($month==11) return 'November';
        if($month==12) return 'Desember';
    }
  	public function convTgl($vtgl = '2019-02-29 11:05:20')
	{
        $month = $this->getMonthName(date('n', strtotime($vtgl)));
        $date = explode(" ", $vtgl);
        $str = explode("-", $date[0]);
        $time = explode(":", $date[1]);
        $jam = $time[0] . ":" . $time[1];
        return $str[2] . " " . $month . " " . $str[0] . " " . $jam;
	}
	public function index($token)
	{
		$now = date('Y-m-d');
		$nama = '';
		$kelas = '';
		$current = date('d', time());
		$token = $this->absen_model->readToken($token);
		if(count($token) > 0)
		{
			$id_sekolah = $token[0]['id_sekolah'];
			$tahunAjaran = $token[0]['tahun_ajaran'];
			$nomor_induk = $token[0]['nomor_induk'];
			$exp = $token[0]['tanggal_exp'];
			if($exp>$now)
			{
				$month = date('m', strtotime($token[0]['waktu_report']));
				$year = date('Y', strtotime($token[0]['waktu_report']));
				$teksDate = '(' . getMonthName($month) . ' ' . $year . ')';

				$temp = $this->absen_model->getTanggalEfektif($id_sekolah, $month, $year);
				$listTgl = array();
				foreach($temp as $tgl)
					$listTgl[$tgl['tanggal']] = $tgl['tanggal'];
				
				$rekap = array(
					'hari_efektif' => count($listTgl),
					'alpha' => 0,
					'izin' => 0,
					'sakit' => 0,
					'tepat' => 0,
					'terlambat' => 0
				);
				$read = $this->absen_model->getKehadiranToken($id_sekolah, $tahunAjaran, $nomor_induk, $month, $year);
				for($i=0; $i < count($read); $i++) 
				{
					if($i == 0)
					{
						$nama = $read[$i]['nm_pengguna']; 
						$kelas = $read[$i]['nm_kelas'] . $read[$i]['rombel_kelas'];
					}
					if($read[$i]['kehadiran_verification'] == 0)
					{
						$read[$i]['status_hadir'] = 'Tepat Waktu';
						$rekap['tepat']++;
					}
					elseif($read[$i]['kehadiran_verification'] == 1)
					{
						$read[$i]['status_hadir'] = 'Sakit';
						$rekap['sakit']++;
					}
					elseif($read[$i]['kehadiran_verification'] == 2)
					{
						$read[$i]['status_hadir'] = 'Izin';
						$rekap['izin']++;
					}
					else
					{
						$read[$i]['status_hadir'] = 'Terlambat';
						$rekap['terlambat']++;
					}

					$tglAbsen = date('Y-m-d', strtotime($read[$i]['kehadiran_timestamp']));
					if(isset($listTgl[$tglAbsen]))
						unset($listTgl[$tglAbsen]);	
				}

				foreach($listTgl as $list)
				{
					$temp = array(
						'status_hadir' => 'Alpha',
						'kehadiran_verification' => 4,
						'kehadiran_timestamp' => $list . ' 00:00:00'
					);
					array_push($read, $temp);
					$rekap['alpha']++;
				}

		        $data = array('read' => $read, 'nama_siswa'=> $nama, 'kelas_siswa'=>$kelas, 'teksDate' => $teksDate, 'rekap' => $rekap);
				$this->load->view('layout/header');
		        $this->load->view('page/hasilAbsensi/header');
				$this->load->view('page/hasilAbsensi/rekapOrtu', $data);
				$this->load->view('layout/footer');
				$this->load->view('page/hasilAbsensi/footer');
			}
			else
			{
				$this->load->view('layout/header');
		        $this->load->view('page/hasilAbsensi/header');
				$this->load->view('page/hasilAbsensi/expRekap');
				$this->load->view('layout/footer');
				$this->load->view('page/hasilAbsensi/footer');
			}	
		}
		else
			redirect(base_url().'login');
		/*foreach ($dbToken as $t) {
			$siswa = $t['token_siswa'];
			$expDB = $t['token_exp'];
		}
		if ($current <= 7) {
			$exp = $expDB-30;
		}
		else{
			$exp = $expDB;
		}
		if ($current<$exp) {
			$read = $this->absen_model->getKehadiranById($siswa)->result_array();
			for ($i=0; $i < count($read); $i++) { 
				$read[$i]['kehadiran_timestamp'] = $this->convTgl($read[$i]['kehadiran_timestamp']);
			}
			foreach ($read as $r) {
				$nama = $r['nm_pengguna'];
				$kelas = $r['nm_kelas'];
			}
	        $data = array('read' => $read, 'nama_siswa'=> $nama, 'kelas_siswa'=>$kelas);
			$this->load->view('layout/header');
	        $this->load->view('page/hasilAbsensi/header');
			$this->load->view('page/hasilAbsensi/rekapOrtu', $data);
			$this->load->view('layout/footer');
			$this->load->view('page/hasilAbsensi/footer');
		}
		else{
			$this->load->view('layout/header');
	        $this->load->view('page/hasilAbsensi/header');
			$this->load->view('page/hasilAbsensi/expRekap');
			$this->load->view('layout/footer');
			$this->load->view('page/hasilAbsensi/footer');
		}*/		
	}
	
}
 ?>