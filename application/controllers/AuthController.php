<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller {
	
	public function __construct(){
		
		parent::__construct();
		$this->load->helper(array('form', 'url'));
        $this->load->library('google');
		$this->load->library('form_validation','session');
		$this->load->model('Karyawan');
        $this->load->model('Sekolah');
	}
	
	public function signin(){
        if ((int)$this->session->userdata('sess_id') > 0 && (int)$this->session->userdata('sess_role') > 0 && $this->session->flashdata('message') == '') 
        {
            $id_role = (int)$this->session->userdata('sess_role');
            if($id_role == 1){
                redirect(base_url().'dashboard/jurnal/siswa');
            } 

            //login sebagai admin, redirect ke ....
            elseif($id_role == 2){
                redirect(base_url().'dashboard/result/absensi/siswa');
            }   

            //login sebagai guru, redirect ke jurnal
            elseif($id_role == 3){
                redirect(base_url().'dashboard/jurnal');
            }

            //login sebagai pelatih, redirect ke jurnal
            elseif($id_role == 4){
                redirect(base_url().'dashboard/jurnal');
            }
        }
        $this->load->view('page/auth/custom_login', array('googleLoginURL' => $this->google->loginURL()));
        if($this->session->flashdata('message') != '')
            $this->session->sess_destroy();
    }
    
    public function pilihMode(){
        if(!$this->session->userdata('user'))
            redirect(base_url().'login');
	    $this->load->view('page/auth/pilih_mode');
	}

    public function registerSekolah(){
        if(!$this->session->userdata('user'))
            redirect(base_url().'login');
		$this->load->view('page/auth/registerasi_sekolah');
    }
    public function createSekolah(){
        $this->form_validation->set_rules('nama_sekolah', 'Nama dari Sekolah', 'required');
        if ($this->form_validation->run() == FALSE)
            redirect(base_url().'registersekolah');
        else
        {
            $nama_logo = hash('sha256',$this->input->post('nama_sekolah') . date('Y-m-d H:i:s'));
            $config['upload_path'] = FCPATH.'assets/logos/';
            if (!is_dir($config['upload_path'])) 
            {
                 mkdir($config['upload_path'], 0777, TRUE);
            }
            $config['max_filename'] = 0;
            $config['allowed_types'] = '*';
            $config['max_size']  = '2048';    
            $config['file_name'] = $nama_logo;
            $this->load->library('upload', $config);
            if($this->upload->do_upload('logo', 'false'))
            {
                $data = array(
                    'nama_sekolah' => $this->input->post('nama_sekolah'),
                    'alamat_sekolah' => $this->input->post('alamat'),
                    'telepon_sekolah' => $this->input->post('kontak'),
                    'logo_sekolah' => $nama_logo
                );
                $id_sekolah = $this->Sekolah->insert($data);
                $user = $this->session->userdata('user');
                $data = array(
                    'id_sekolah' => $id_sekolah,
                    'id_pengguna' => $user->id_pengguna,
                    'id_role' => '2'
                );
                $this->Sekolah->addPenggunaSekolah($data);
                $this->signinUser();
            }
            else
            {
                $flash = array(
                    'pesan' => $this->upload->display_errors()
                );
                $this->session->set_flashdata($flash);
                redirect(base_url().'registersekolah');
            }
        }
    }
    
    public function pilihSekolah(){
        if(!$this->session->userdata('user'))
            redirect(base_url().'login');
        $role= $this->Karyawan->getRoleWhere("nm_role != 'admin'", 1);
        $sekolah = $this->Sekolah->getListSekolah();
        if(isset($_GET['mode']) && in_array($_GET['mode'], $role))
        {
            if(isset($_GET['sekolah']) && $this->Sekolah->checkSekolah(floor($_GET['sekolah'])))
            {
                $user = $this->session->userdata('user');
                $data = array(
                    'id_sekolah' => floor($_GET['sekolah']),
                    'id_pengguna' => $user->id_pengguna,
                    'id_role' => array_search($_GET['mode'], $role, true)
                );
                $this->Sekolah->addPenggunaSekolah($data);
                $this->signinUser();
            }
            $this->load->view('page/auth/pilih_sekolah', array('data' => $sekolah, 'mode' => $_GET['mode']));
        }
        else
            redirect(base_url('pilihmode'));
    }

	public function signinAction(){
    	$this->form_validation->set_rules('user', 'User Email/ No HP', 'required');
    	$this->form_validation->set_rules('password', 'Password', 'required');
    	if ($this->form_validation->run() == FALSE){
    		// validasi form login
	       	redirect(base_url().'login');
	    }else{
	    	$password 		= 	md5($this->input->post('password'));
	    	$user_login	=	sanitize($this->input->post('user'));
    		$login 			=	$this->Karyawan->cekLogin($user_login,$password)->num_rows();
    		if($login < 1){
    			// NIP dan password tidak sesuai
                $this->session->set_flashdata('message', 'Mohon maaf. Email atau Password anda salah!');
                redirect(base_url().'login');
    			// echo "Gagal";
    		}else{
    			$user 		=	$this->Karyawan->cekPengguna($user_login);
                $data  = array(
                    'user'       =>  $user
                );
                $this->session->set_userdata($data);
                $this->signinUser();
    		}
	    }
	}

    public function signinUser()
    {
        if(!$this->session->userdata('user'))
            redirect(base_url().'login');

        $user = $this->session->userdata('user');
        $role_sekolah = $this->Karyawan->getIdRoleSekolah($user->id_pengguna);
        if(count($role_sekolah)>1)
        {
            $found = 0;
            foreach($role_sekolah as $rs)
            {
                if($user->default_sekolah == $rs->id_sekolah)
                {
                    $found++;
                    $role_sekolah = $rs;
                    break;
                }
            }
            if($found < 1)
                exit;//redirect ke halaman pilih sekolah buat user yg udah punya lebih dari 1 sekolah
        }
        elseif (count($role_sekolah)<1) 
        {
            redirect(base_url().'pilihmode');
        }
        else
        {
            if($user->default_sekolah != $role_sekolah[0]->id_sekolah)
                $this->Karyawan->updateDefault($user->id_pengguna,$role_sekolah[0]->id_sekolah);
        }
        $tema = $user->tema_pengguna;
        if(is_null($tema) || $tema == '')
            $tema = 'default';
        $data  = array(
            'sess_tema' => $tema,
            'sess_foto' => $user->foto_pengguna,
            'sess_logo_sekolah' => $role_sekolah[0]->logo_sekolah,
            'sess_nama_sekolah' => $role_sekolah[0]->nama_sekolah,
            'sess_id'       =>  $user->id_pengguna,
            'sess_role'     =>  $role_sekolah[0]->id_role,
            'sess_sekolah'     =>  $role_sekolah[0]->id_sekolah,
            'sess_old_absen'     =>  $role_sekolah[0]->old_absen,
            'sess_name'     =>  $user->nm_pengguna,
            'sess_tahun_ajaran' => getTahunAJaran(),
            'logged_in'     =>  TRUE
        );
        $this->session->set_userdata($data);
        $id_role = $role_sekolah[0]->id_role;
        if($id_role == 1 || $id_role == 2 || $id_role == 3 || $id_role == 4){

            //login sebagai siswa, redirect ke ....
            if($id_role == 1){
                redirect(base_url().'dashboard/jurnal/siswa');
            } 

            //login sebagai admin, redirect ke ....
            elseif($id_role == 2){
                redirect(base_url().'dashboard/result/absensi/siswa');
            }   

            //login sebagai guru, redirect ke jurnal
            elseif($id_role == 3){
                redirect(base_url().'dashboard/jurnal');
            }

            //login sebagai pelatih, redirect ke jurnal
            elseif($id_role == 4){
                redirect(base_url().'dashboard/jurnal');
            }
        }else{
            $this->session->set_flashdata('message', 'Mohon maaf. Email atau Password anda salah!');
            redirect(base_url().'login');
        }
    }
	public function logout(){
        $this->session->sess_destroy();
        redirect(base_url().'login');
    }

    public function gantiPassword(){
        // $user   = $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'));
        $id = $this->session->userdata('sess_id');
        if($id == '' || (int)$id<1)
            redirect(base_url().'login');
        $this->form_validation->set_rules('old_password', 'Password lama', 'required');
        $this->form_validation->set_rules('new_password', 'Password baru', 'required');
        $pesan = '';
        if ($this->form_validation->run() == FALSE)
            $pesan = 'Terjadi Kesalahan, Silahkan Coba Lagi';
        else
        {
            $old = md5($this->input->post('old_password'));
            $new = md5($this->input->post('new_password'));
            $check = $this->Karyawan->checkPassword($id,$old);
            if(count($check)>0)
            {
                $user = $this->Karyawan->updatePassword($id,$new);
                $this->session->set_flashdata('message', 'Ganti Password Berhasil. Silahkan login menggunakan password baru');
                redirect(base_url().'login');
            }
            else
                $pesan = 'Password Sekarang Salah, Silahkan Coba Lagi';
        }
        $this->session->set_flashdata('pesan', $pesan);

        $link = $this->input->post('referred_from');
        if($link == '')
            $link = base_url().'login';
        
        redirect($link);
        

        // // $this->form_validation->set_rules('old_password', 'Password Lama', 'required');
        // $this->form_validation->set_rules('new_password', 'Password Baru', 'required');

        // $login          =   $this->Karyawan->cekLogin($user[0]->nomor_induk,md5($this->input->post('old_password')))->num_rows();
        // if($login < 1){    
        //     $this->session->sess_destroy();
        //     redirect(base_url().'login');
        // }     
        // $this->session->sess_destroy();
    }
} 