<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher extends CI_Controller {

	public function index(){
		$this->load->view('layout/auth/header');
		$this->load->view('page/auth/login_teacher');
		$this->load->view('layout/auth/footer');
	}
	
	public function logout(){
		$this->load->view('layout/auth/header');
		$this->load->view('page/auth/logout');
		$this->load->view('layout/auth/footer');
	}
	
	public function sign_out(){
		
		session_start();
		//$this->load->helper('url');
		session_destroy();
		
		redirect('/auth');
	}
}
