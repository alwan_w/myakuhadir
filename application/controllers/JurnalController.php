 <?php
class JurnalController extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation', 'session');
        $this->load->model('Jurnal');
        $this->load->model('Karyawan');

        if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') < 2) {
            $url = base_url() . 'login';
            redirect($url);
        }
        date_default_timezone_set('Asia/Jakarta');
        setlocale(LC_TIME, 'INDONESIA');
    }

    public function showPiket()
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
        $user   = $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));
        $walipiket = $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, TRUE);
        $piket = $walipiket['piket'];
        if(count($piket) > 0 && $piket[0]->hari == date('N'))
        {
            $now     = getdate();
            $date     = $now['wday'];
            
            $data     = array(
                'jurnal'         => $this->Jurnal->getListJadwalGuru($user[0]->id_pengguna),
                'jadwal'        => $this->Jurnal->getJadwalPiket($id_sekolah, $user[0]->id_pengguna, $tahunAjaran, date('Y-m-d')),
                'tanggal'        => date('d-F-Y'),
                'user'          => $user,
                'kelas'         => $walipiket['wali'],
                'tahunAjaran' => $tahunAjaran,
                'teksTanggal' => humanDateFormat(date('Y-m-d'), TRUE)
            );
            $this->load->view('page/jurnal/header');
            $this->load->view('page/jurnal/navbar', array('page' => 'piket', 'user' => $user, 'kelas' => $walipiket));
            $this->load->view('page/jurnal/piket', $data);
            $this->load->view('page/jurnal/footer');
        }
        else
            redirect(base_url() . 'dashboard/jurnal');
    }

    public function index()
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');

        $user   = $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));
        $now     = getdate();
        $date     = $now['wday'];
        $walipiket = $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, TRUE);
        $data     = array(
            'jurnal'         => $this->Jurnal->getListJadwalGuru($user[0]->id_pengguna),
            'jadwal'        => $this->Jurnal->getListJadwal($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, date('Y-m-d')),
            'tanggal'        => date('d-F-Y'),
            'user'          => $user,
            'kelas'         => $walipiket['wali'],
            'tahunAjaran' => $tahunAjaran,
            'teksTanggal' => humanDateFormat(date('Y-m-d'), TRUE)
        );
        $this->load->view('page/jurnal/header');
        $this->load->view('page/jurnal/navbar', array('page' => 'home', 'user' => $user, 'kelas' => $walipiket));
        $this->load->view('page/jurnal/home', $data);
        $this->load->view('page/jurnal/footer');
    }

    /*public function showJurnal($id_jurnal)
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
        $user       =   $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));
        $jurnal     =    $this->Jurnal->getJurnal(null, $id_jurnal);
        $kelas         =    $this->Jurnal->getKelasById($jurnal->id_kelas);
        $data     = array(
            'kelas'        =>     $kelas,
            'id_jurnal'    =>     $id_jurnal,
            'jurnal'    =>    $jurnal,
            'user'      =>  $user
        );

        $this->load->view('page/jurnal/header');
        $this->load->view('page/jurnal/navbar', array('page' => 'home', 'user' => $user, 'kelas' => $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, TRUE)));
        $this->load->view('page/jurnal/isi-jurnal', $data);
        $this->load->view('page/jurnal/footer');
    }*/

    public function showJurnal($id_jurnal)
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
        $user       =   $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));
        $jurnal     =   $this->Jurnal->getJurnal(null, $id_jurnal);
        $kelas      =   $this->Jurnal->getKelasById($jurnal->id_kelas);
        $data   = array(
            'kelas'     =>  $kelas,
            'id_jurnal' =>  $id_jurnal,
            'jurnal'    =>  $jurnal,
            'user'      =>  $user
        );
        $this->load->view('page/jurnal/header');
        $this->load->view('page/jurnal/navbar', array('page' => '', 'user' => $user, 'kelas' => $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, TRUE)));
        $this->load->view('page/jurnal/isi-jurnal', $data);
        $this->load->view('page/jurnal/footer');
    }

    public function showTugasUjian($id_jurnal)
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
        $user       =   $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));
        $jurnal     =   $this->Jurnal->getJurnal(null, $id_jurnal);
        $kelas      =   $this->Jurnal->getKelasById($jurnal->id_kelas);
        // print_r($jurnal);
        $data   = array(
            'kelas'     =>  $kelas,
            'id_jurnal' =>  $id_jurnal,
            'jurnal'    =>  $jurnal,
            'user'      =>  $user
        );

        $this->load->view('page/jurnal/header');
        $this->load->view('page/jurnal/navbar', array('page' => '', 'user' => $user, 'kelas' => $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, TRUE)));
        $this->load->view('page/jurnal/tugas-ujian', $data);
        $this->load->view('page/jurnal/footer');
    }

    public function showHistoriJurnal()
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
        $user       =   $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));

        $jurnal     =   $this->Jurnal->getJurnal(1, $user[0]->id_pengguna, $tahunAjaran);
        $data   = array(
            'jurnal' => $jurnal,
            'user' => $user,
            'tahunAjaran' => $tahunAjaran,
            'teksTanggal' => humanDateFormat(date('Y-m-d'), TRUE)
        );
        $this->load->view('page/jurnal/histori/header');
        $this->load->view('page/jurnal/navbar', array('page' => 'histori', 'user' => $user, 'kelas' => $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, TRUE)));
        $this->load->view('page/jurnal/histori/histori-jurnal', $data);
        $this->load->view('page/jurnal/histori/footer');
    }

    public function showHistoriDetail($id_jurnal)
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
        $user       =   $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));
        $jurnal     =   $this->Jurnal->getJurnal(2, $id_jurnal);
        $data   = array(

            'jurnal'    =>  $jurnal,
            'user'      =>  $user
        );
        $this->load->view('page/jurnal/histori/header');
        $this->load->view('page/jurnal/navbar', array('page' => '', 'user' => $user, 'kelas' => $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, TRUE)));
        $this->load->view('page/jurnal/histori/histori-detail', $data);
        $this->load->view('page/jurnal/histori/footer');
    }

    public function showHistoriDaftarHadir($id_jurnal)
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
        $user = $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));
        $jurnal = $this->Jurnal->getJurnal(2, $id_jurnal);
        $jam = date('H:i:s');
        if(isset($jurnal[0]->tanggal_jurnal)) 
            $tanggal_jurnal = $jurnal[0]->tanggal_jurnal;
        else 
            $tanggal_jurnal = date("Y-m-d",strtotime($jurnal[0]->created_at));
        
        $siswa = $this->Jurnal->getKehadiranKelas($id_sekolah, $jurnal[0]->id_kelas, $tanggal_jurnal);
        $temp = array(
            'id_jurnal' => $id_jurnal,
            'id_kelas' => $jurnal[0]->id_kelas,
            'tanggal' => $tanggal_jurnal,
            'jam_kehadiran' => $jurnal[0]->jam_mulai
        );
        $kehadiranJurnal = $this->Jurnal->getKehadiranJurnalAll($temp);

        $jumlah = array(
            'total' => count($siswa),
            'sakit' => 0,
            'izin' => 0,
            'alpha' => 0
        );
        if(count($kehadiranJurnal['kehadiranJurnal']) > 0)
        {
            $type = $kehadiranJurnal['type'];
            $kehadiranJurnal = $kehadiranJurnal['kehadiranJurnal'];
            $checkKehadiran = array();
            for($i = 0; $i<count($kehadiranJurnal); $i++)
            {
                if(!isset($checkKehadiran[$kehadiranJurnal[$i]->kehadiran_siswa]))
                    $checkKehadiran[$kehadiranJurnal[$i]->kehadiran_siswa] = $kehadiranJurnal[$i];
            }
            for($i = 0; $i<$jumlah['total']; $i++)
            {
                if(isset($checkKehadiran[$siswa[$i]->kehadiran_siswa]))
                {
                    if(is_null($siswa[$i]->kehadiran_verification))
                    {
                        $siswa[$i]->kehadiran_verification = $checkKehadiran[$siswa[$i]->kehadiran_siswa]->kehadiran_verification;
                        $temp = array(
                            'id_jurnal' => $id_jurnal,
                            'id_kelas' => $jurnal[0]->id_kelas,
                            'kehadiran_verification' => $siswa[$i]->kehadiran_verification,
                            'tanggal_kehadiran' => $tanggal_jurnal,
                            'jam_kehadiran' => $jurnal[0]->jam_mulai
                        );
                        $data = array(
                            'kehadiran_siswa' => $siswa[$i]->nomor_induk,
                            'id_sekolah' => $id_sekolah,
                            'kehadiran_timestamp' => $tanggal_jurnal . ' ' . $jam,
                            'kehadiran_verification' => $siswa[$i]->kehadiran_verification,
                            'id_pengguna_pengabsen' => $this->session->userdata('sess_id')
                        );
                        $data = array(
                            'kehadiran' => $data,
                            'kehadiranJurnal' => $temp
                        );
                        $this->Jurnal->addKehadiranJurnal($data, TRUE);
                    }
                    else
                    {
                        $siswa[$i]->kehadiran_verification = $checkKehadiran[$siswa[$i]->kehadiran_siswa]->kehadiran_verification;
                        if($type == 'kehadiran')
                        {
                            $insert = array(
                                'id_jurnal' => $id_jurnal,
                                'id_kelas' => $jurnal[0]->id_kelas,
                                'id_kehadiran' => $siswa[$i]->kehadiran_id,
                                'kehadiran_verification' => $siswa[$i]->kehadiran_verification,
                                'tanggal_kehadiran' => $tanggal_jurnal,
                                'jam_kehadiran' => $jurnal[0]->jam_mulai
                            );
                            $this->Jurnal->addKehadiranJurnal($insert);
                        }
                    }

                    unset($checkKehadiran[$siswa[$i]->kehadiran_siswa]);
                }
                if($siswa[$i]->kehadiran_verification == 1)
                    $jumlah['sakit']++;
                if($siswa[$i]->kehadiran_verification == 2)
                    $jumlah['izin']++;
                if(is_null($siswa[$i]->kehadiran_verification))
                    $jumlah['alpha']++;
            }
        }
        else
        {
            for($i=0; $i<$jumlah['total']; $i++)
            {
                if($siswa[$i]->kehadiran_verification == 1)
                    $jumlah['sakit']++;
                if($siswa[$i]->kehadiran_verification == 2)
                    $jumlah['izin']++;
                if(is_null($siswa[$i]->kehadiran_verification))
                    $jumlah['alpha']++;
            }
        }
        $data = array(
            'siswa' => $siswa,
            'jurnal' => $jurnal,
            'user' => $user,
            'jumlah' => $jumlah
        );
        $this->load->view('page/jurnal/header');
        $this->load->view('page/jurnal/navbar', array('page' => '', 'user' => $user, 'kelas' => $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, TRUE)));
        $this->load->view('page/jurnal/histori/histori-daftar-hadir', $data);
        $this->load->view('page/jurnal/footer');
    }

    public function showDaftarHadir($id_jurnal)
    {
        date_default_timezone_set("Asia/Jakarta");
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
        
        $now = date('Y-m-d');

        $user = $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));
        $jurnal = $this->Jurnal->getJurnal(null, $id_jurnal);
        $kelas = $this->Jurnal->getKelasById($jurnal->id_kelas);
        $jam = date('H:i:s');

        if(isset($jurnal->tanggal_jurnal)) 
            $tanggal_jurnal = $jurnal->tanggal_jurnal;
        else 
            $tanggal_jurnal = date("Y-m-d",strtotime($jurnal->created_at));
        $editable = TRUE;
        if($now != $tanggal_jurnal)
            $editable = FALSE;
        $siswa = $this->Jurnal->showKehadiranSiswa($jurnal->id_kelas, $tanggal_jurnal);

        $siswaMasuk = array();
        foreach ($siswa as $key) {
            array_push($siswaMasuk, $key->kehadiran_siswa);
        }

        $siswaTidakMasuk = $this->Jurnal->siswaTidakAbsen($jurnal->id_kelas, $siswaMasuk);
        $temp = array(
            'id_jurnal' => $id_jurnal,
            'id_kelas' => $jurnal->id_kelas,
            'tanggal' => $tanggal_jurnal,
            'jam_kehadiran' => $jurnal->jam_mulai
        );
        $kehadiranJurnal = $this->Jurnal->getKehadiranJurnalAll($temp);
        if(count($kehadiranJurnal['kehadiranJurnal']) > 0)
        {
            $type = $kehadiranJurnal['type'];
            $kehadiranJurnal = $kehadiranJurnal['kehadiranJurnal'];
            $checkKehadiran = array();
            for($i = 0; $i<count($kehadiranJurnal); $i++)
            {
                if(!isset($checkKehadiran[$kehadiranJurnal[$i]->kehadiran_siswa]))
                    $checkKehadiran[$kehadiranJurnal[$i]->kehadiran_siswa] = $kehadiranJurnal[$i];
            }
            for($i = 0; $i<count($siswa); $i++)
            {
                if(isset($checkKehadiran[$siswa[$i]->kehadiran_siswa]))
                {
                    $siswa[$i]->kehadiran_verification = $checkKehadiran[$siswa[$i]->kehadiran_siswa]->kehadiran_verification;
                    unset($checkKehadiran[$siswa[$i]->kehadiran_siswa]);
                    if($type == 'kehadiran')
                    {
                        $insert = array(
                            'id_jurnal' => $id_jurnal,
                            'id_kelas' => $jurnal->id_kelas,
                            'id_kehadiran' => $siswa[$i]->kehadiran_id,
                            'kehadiran_verification' => $siswa[$i]->kehadiran_verification,
                            'tanggal_kehadiran' => $tanggal_jurnal,
                            'jam_kehadiran' => $jurnal->jam_mulai
                        );
                        $this->Jurnal->addKehadiranJurnal($insert);
                    }
                    if(count($checkKehadiran)<1)
                        break;
                }
            }
            if(count($checkKehadiran)>0)
            {
                for($i=0; $i<count($siswaTidakMasuk); $i++)
                {
                    if(isset($checkKehadiran[$siswaTidakMasuk[$i]->nomor_induk]))
                    {
                        $jumlah = count($siswa);
                        $siswa[$jumlah] = $siswaTidakMasuk[$i];
                        $siswa[$jumlah]->kehadiran_verification = $checkKehadiran[$siswaTidakMasuk[$i]->nomor_induk]->kehadiran_verification;
                        unset($checkKehadiran[$siswaTidakMasuk[$i]->nomor_induk]);

                        $temp = array(
                            'id_jurnal' => $id_jurnal,
                            'id_kelas' => $jurnal->id_kelas,
                            'kehadiran_verification' => $siswa[$jumlah]->kehadiran_verification,
                            'tanggal_kehadiran' => $tanggal_jurnal,
                            'jam_kehadiran' => $jurnal->jam_mulai
                        );
                        $data = array(
                            'kehadiran_siswa'     => $siswa[$jumlah]->nomor_induk,
                            'id_sekolah' => $id_sekolah,
                            'kehadiran_timestamp' => $tanggal_jurnal . ' ' . $jam,
                            'kehadiran_verification' => $siswa[$jumlah]->kehadiran_verification,
                            'id_pengguna_pengabsen' => $this->session->userdata('sess_id')
                        );
                        $data = array(
                            'kehadiran' => $data,
                            'kehadiranJurnal' => $temp
                        );
                        $this->Jurnal->addKehadiranJurnal($data, TRUE);
                        if(count($checkKehadiran)<1)
                            break;
                    }
                }
            }
        }
        $data   =   [
            'id_jurnal' => $id_jurnal,
            'kelas' => $kelas,
            'siswa' => $siswa,
            'jurnal' => $jurnal,
            'siswaTidakMasuk' => $siswaTidakMasuk,
            'tanggal_jurnal' => $tanggal_jurnal,
            'editable' => $editable
        ];

        $this->load->view('page/jurnal/header');
        $this->load->view('page/jurnal/navbar', array('page' => '', 'user' => $user, 'kelas' => $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, TRUE)));
        $this->load->view('page/jurnal/daftar-hadir-kelas', $data);
        $this->load->view('page/jurnal/footer');
    }

    public function updateDaftarHadir($id_jurnal)
    {
        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date('Y-m-d');
        $jam = date('H:i:s');
        $user = $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));

        // cek if jurnal already created or not
        $jurnal = $this->Jurnal->getJurnal(null, $id_jurnal);
        if(count($jurnal)>0)
        {
            $id_sekolah = $this->session->userdata('sess_sekolah');
            $id_pengguna = $this->session->userdata('sess_id');
            if(isset($jurnal->tanggal_jurnal)) 
                $tanggal_jurnal = $jurnal->tanggal_jurnal;
            else 
                $tanggal_jurnal = date("Y-m-d",strtotime($jurnal->created_at));
            $hadir  = $this->input->post('status');
            foreach ($hadir as $key => $value) 
            {
                if($value != 'none')
                {
                    if($value == 'Izin')
                        $status = 2;
                    elseif($value == 'Sakit')
                        $status = 1;
                    else
                        $status = 3;

                    $check = $this->Jurnal->getKehadiran($id_sekolah, $key, $tanggal_jurnal);
                    if(count($check)<1)
                    {
                        $data   = array(
                            'kehadiran_siswa'     => $key,
                            'id_sekolah' => $id_sekolah,
                            'kehadiran_timestamp' => $tanggal_jurnal . ' ' . $jam,
                            'id_pengguna_pengabsen' => $id_pengguna,
                            'kehadiran_verification' => $status
                        );
                        // print_r($value);
                        
                        $id_kehadiran = $this->Jurnal->siswaAbsen($data);
                        $data['kehadiran_id'] = $id_kehadiran;
                    }
                    else
                    {
                        $data = $check[0];
                        $data['kehadiran_verification'] = $status;
                    }

                    $check = $this->Jurnal->getKehadiranJurnal($id_jurnal, $data['kehadiran_id'], $tanggal_jurnal);
                    if(count($check)<1)
                    {
                        $insert = array(
                            'id_jurnal' => $id_jurnal,
                            'id_kelas' => $jurnal->id_kelas,
                            'id_kehadiran' => $data['kehadiran_id'],
                            'kehadiran_verification' => $data['kehadiran_verification'],
                            'tanggal_kehadiran' => $tanggal_jurnal,
                            'jam_kehadiran' => $jurnal->jam_mulai
                        );
                        $this->Jurnal->updateKehadiranJurnal($insert, 1);
                    }
                    else
                    {
                        $update = array(
                            'kehadiran_verification' => $data['kehadiran_verification']
                        );
                        $where = array(
                            'id_kehadiran_jurnal' => $check[0]->id_kehadiran_jurnal
                        );
                        $this->Jurnal->updateKehadiranJurnal(array('update' => $update, 'where' => $where));
                    }
                }
            }
        }
        redirect(base_url() . 'dashboard/jurnal/daftar-hadir/' . $id_jurnal);
    }

    public function showDaftarHadirSiswa($id_kelas)
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
        date_default_timezone_set("Asia/Jakarta");
        $now = date('Y-m-d');

        $user       =   $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));
        $kelas      =   $this->Jurnal->getKelasById($id_kelas);

        $siswa = $this->Jurnal->showKehadiranSiswa($id_kelas, $now);

        $siswaMasuk = array();
        foreach ($siswa as $key) {
            array_push($siswaMasuk, $key->kehadiran_siswa);
        }
        $siswaTidakMasuk = $this->Jurnal->siswaTidakAbsen($id_kelas, $siswaMasuk);

        $data   =   [
            'kelas'             =>  $kelas,
            'siswa'             =>  $siswa,
            'siswaTidakMasuk'   =>  $siswaTidakMasuk
        ];

        $this->load->view('page/jurnal/header');
        $this->load->view('page/jurnal/navbar', array('page' => 'absen', 'user' => $user, 'kelas' => $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, TRUE)));
        $this->load->view('page/jurnal/daftar-hadir', $data);
        $this->load->view('page/jurnal/footer');
    }

    public function dashboardSiswa()
    {
        date_default_timezone_set("Asia/Jakarta");
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
        $now = date('Y-m-d');

        $user       =   $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));
        $kelas      =   $this->Jurnal->getKelasBySiswa($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));
        $id_kelas = $kelas['id_kelas'];

        $siswa = $this->Jurnal->showKehadiranSiswa($id_kelas, $now);

        $siswaMasuk = array();
        foreach ($siswa as $key) {
            array_push($siswaMasuk, $key->kehadiran_siswa);
        }
        $siswaTidakMasuk = $this->Jurnal->siswaTidakAbsen($id_kelas, $siswaMasuk);

        $data   =   [
            'kelas'             =>  $kelas,
            'siswa'             =>  $siswa,
            'siswaTidakMasuk'   =>  $siswaTidakMasuk,
            'waliKelas'         =>  $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran)
        ];

        $this->load->view('page/jurnal/header');
        $this->load->view('page/jurnal/navbar', array('page' => '', 'user' => $user, 'kelas' => $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, $tahunAjaran, TRUE)));
        $this->load->view('page/jurnal/siswa', $data);
        $this->load->view('page/jurnal/footer');
    }
    
    public function cariSekolah(){
		$this->load->view('page/dashboard/cari_sekolah');
    }
    
    /*public function exitKelas($id_jurnal)
    {
        $siswa = $this->Jurnal->exitKelas($id_jurnal);
        $this->session->set_flashdata('message', 'Terima kasih. Anda telah menyelesaikan kelas pada pukul ' . date('H:i'));
        redirect(base_url() . 'dashboard/jurnal/');
    }*/

    public function hadirAction($id_jadwal_guru)
    {
        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date('Y-m-d');
        $user = $this->Karyawan->getKaryawanById($this->session->userdata('sess_id'), $this->session->userdata('sess_sekolah'));

        // cek if jurnal already created or not
        $cekJurnal = $this->Jurnal->cekJurnal($id_jadwal_guru, $tanggal);

        if (count($cekJurnal) < 1) 
        {
            $cekJadwal = $this->Jurnal->cekJadwalGuru($id_jadwal_guru);
            if(count($cekJadwal)<1)
                redirect(base_url() . 'dashboard/jurnal');
            else
            {
                $id_guru = $this->session->userdata('sess_id');
                $id_piket = NULL;
                if($this->input->post('piket') == TRUE)
                {
                    $id_guru = $cekJadwal[0]['id_guru'];   
                    $id_piket = $this->session->userdata('sess_id');
                }
                $id_jurnal = uniqid() . str_shuffle(date("Ymdhs"));
                $data = array(
                    'id_jurnal' => $id_jurnal,
                    'id_guru' => $id_guru,
                    'id_jadwal_guru' => $id_jadwal_guru,
                    'id_kelas' => $this->input->post('id_kelas'),
                    'id_guru_piket' => $id_piket,
                    'tanggal_jurnal' => $tanggal,
                    'minggu_ke' => date('W'),
                    'id_subjek' => $this->input->post('id_subjek'),
                    'jam_hadir' => date('H:i:s'),
                    'status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $this->Jurnal->cekLog($data);
                redirect(base_url() . 'dashboard/jurnal/isi-jurnal/' . $id_jurnal);
            }
        } 
        redirect(base_url() . 'dashboard/jurnal/isi-jurnal/' . $cekJurnal[0]['id_jurnal']);
    }

    public function ijinSiswa($id_kelas)
    {
        $tanggal = date('Y-m-d H:i:s');
        $hadir  = $this->input->post('status');
        foreach ($hadir as $key => $value) 
        {
            if($value != 'none')
            {
                if($value == 'Izin')
                    $status = 2;
                elseif($value == 'Sakit')
                    $status = 1;
                else
                    $status = 3;

                $data   = array(
                    'kehadiran_siswa' => $key,
                    'id_sekolah' => $this->session->userdata('sess_sekolah'),
                    'kehadiran_timestamp' => $tanggal,
                    'kehadiran_verification' => $status,
                    'id_pengguna_pengabsen' => $this->session->userdata('sess_id')
                );
                // print_r($value);
                $this->Jurnal->siswaAbsen($data);
            }
        }
        redirect(base_url() . 'dashboard/jurnal/hadir-siswa/' . $id_kelas);
    }

    public function isiJurnalAction($id_jurnal)
    {
        //input form untuk jurnal
        $data_jurnal     = array(
            'kompetensi_dasar' => $this->input->post('kompetensi_dasar'),
            'catatan' => $this->input->post('catatan'),
            'tugas' => $this->input->post('tugas')
        );

        $this->Jurnal->inputJurnal($id_jurnal, $data_jurnal);
        redirect(base_url() . 'dashboard/jurnal/isi-jurnal/' . $id_jurnal);
    }

    public function isiTugasUjian($id_jurnal)
    {
        //input form untuk tugas
        $data_tugas     = array(
            'deskripsi_tugas'       =>  $this->input->post('deskripsi_tugas'),
            'jenis_tugas'           =>  $this->input->post('jenis_tugas')
        );

        //input form untuk ujian
        $data_ujian     = array(
            'deskripsi_ujian'       =>  $this->input->post('deskripsi_ujian'),
            'jenis_ujian'           =>  $this->input->post('jenis_ujian')
        );
        $this->Jurnal->inputTugas($id_jurnal, $data_tugas);
        $this->Jurnal->inputUjian($id_jurnal, $data_ujian);
        redirect(base_url() . 'dashboard/jurnal/tugas-ujian/' . $id_jurnal);
    }
}
