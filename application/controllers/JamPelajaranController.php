<?php
class JamPelajaranController extends CI_Controller {

	public function __construct(){
		
		parent::__construct();
		if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') != 2) {
            $url = base_url() . 'login';
            redirect($url);
        }
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('JamPelajaran');
	}

    public function index()
    {
    	$data = $this->JamPelajaran->getList($this->session->userdata('sess_sekolah'));
        $this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/jamPelajaran/listJamPelajaran', array('data' => $data));
		$this->load->view('layout/footer');
    }

    public function create () {    	
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/jamPelajaran/createJamPelajaran');
		$this->load->view('layout/footer');
    }

    public function show ($id) {
    	$data = $this->JamPelajaran->getWaktuById($id);
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/jamPelajaran/showJamPelajaran', array('data' => $data));
		$this->load->view('layout/footer');
    }

    public function store () {
    	$this->form_validation->set_rules('jam_ke', 'jam_ke', 'required');
    	$this->form_validation->set_rules('jam_mulai', 'jam_mulai', 'required');
    	$this->form_validation->set_rules('jam_selesai', 'jam_selesai', 'required');
    	  if ($this->form_validation->run() == FALSE)
	        {
	            redirect(base_url().'dashboard/civitas/tambah');
	        }
	        else
	        {
	        	$data = array(
	        		'jam_ke' 		=> $this->input->post('jam_ke'),
	        		'jam_mulai' 		=> $this->input->post('jam_mulai'),
	        		'jam_selesai' 		=> $this->input->post('jam_selesai'),
	        		'id_sekolah'		=> $this->session->userdata('sess_sekolah')
	        	);
	            $this->JamPelajaran->insert($data);
	            redirect(base_url().'dashboard/waktu');
	        }
    }

    public function update ($id) {
    	$this->form_validation->set_rules('jam_ke', 'jam_ke', 'required');
    	$this->form_validation->set_rules('jam_mulai', 'jam_mulai', 'required');
    	$this->form_validation->set_rules('jam_selesai', 'jam_selesai', 'required');
		$waktu = array(
    		'jam_ke' 		=> $this->input->post('jam_ke'),
    		'jam_mulai' 		=> $this->input->post('jam_mulai'),
    		'jam_selesai' 		=> $this->input->post('jam_selesai'),
    		'id_sekolah'		=> $this->session->userdata('sess_sekolah')
    	);
    	$data = $this->JamPelajaran->update($id, $waktu);
    	redirect(base_url().'dashboard/waktu');	
    }

    public function destroy ($id) {
    	$this->JamPelajaran->delete($id);
    	redirect(base_url().'dashboard/waktu');
    }
}