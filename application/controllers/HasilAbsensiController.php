<?php

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class HasilAbsensiController extends CI_Controller {

    private $style_title;
    private $style_col;
    private $style_row;
    private $style_status;

    public function __construct(){
        
        parent::__construct();
        if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') != 2) {
            $url = base_url() . 'login';
            redirect($url);
        }
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('HasilAbsensi', 'absensi');
        $this->load->model('Jurnal');
        $this->load->library('excel');
        $this->load->library('upload');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function kirimReportOrtu()
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = getTahunAjaran();
        $listReport = $this->absensi->getListReportOrtu($id_sekolah, $tahunAjaran);
        $go = 1;
        if(count($listReport)>0 && explode(' ', $listReport[0]['waktu_report'])[0] == date('Y-m-d'))
            $go = 0;
        if($go)
        {
            $siswa = $this->siswa->getSiswaAndKelas($this->session->userdata('sess_sekolah'), $this->session->userdata('sess_tahun_ajaran'))->result_array();
            $id_report_ortu = null;
            $date = date('Y-m-d H:i:s');

            foreach($siswa as $user)
            {
                if(is_null($id_report_ortu))
                {
                    $insert = array(
                        'id_sekolah' => $id_sekolah,
                        'tahun_ajaran' => $tahunAjaran,
                        'nama_report' => 'Report Tanggal ' . humanDateFormat(date('Y-m-d')),
                        'waktu_report' => $date
                    );
                    $id_report_ortu = $this->absensi->addReportOrtu($insert);
                }
                $token = md5($user['nomor_induk'].$user['id_sekolah'].$user['id_pengguna_sekolah'].$date);
                $exp = date('Y-m-d', strtotime('7 day'));
                $data = array(
                    'id_sekolah' => $id_sekolah,
                    'id_report_ortu' => $id_report_ortu,
                    'status' => 1,
                    'token_code' => $token, 
                    'nomor_induk' => $user['nomor_induk'], 
                    'tanggal_exp' => $exp
                );
                $this->absensi->addTokenReport($data);
            }
        }
        redirect(base_url().'dashboard/result/absensi/report-ortu');
        
    }

    public function reportOrtu()
    {
        $id_sekolah = $this->session->userdata('sess_sekolah');
        $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
        $listReport = $this->absensi->getListReportOrtu($id_sekolah, $tahunAjaran);
        $id_report_ortu = NULL;
        if(count($listReport)>0)
            $id_report_ortu = $listReport[0]['id_report_ortu'];
        if((int)$this->input->get('report')>0)
            $id_report_ortu = $this->input->get('report');
        $report = $this->absensi->getReportOrtu($id_sekolah, $tahunAjaran, $id_report_ortu);
        $tanggal = 'Report Orang Tua Tidak Ditemukan';
        if(count($report)>0)
        {
            $tanggal = explode(' ', $report[0]['waktu_report'])[0];
            $tanggal = 'Status Report Orang Tua Siswa (' .humanDateFormat($tanggal, 1) .')';
        }
        $data = array(
            'listReport' => $listReport,
            'report' => $report,
            'tanggal' => $tanggal,
            'id_report_ortu' => $id_report_ortu
        );
        $this->load->view('layout/header');
        $this->load->view('layout/navigationbar');
        $this->load->view('page/hasilAbsensi/reportOrtu', $data);
        $this->load->view('layout/footer');
    }

    public function deleteIzinSiswa($kehadiran_id)
    {
        if($this->session->userdata('sess_role'))
        {
            if($this->session->userdata('sess_role') == 2)
                $this->Jurnal->delete('kehadiran', array('kehadiran_id' => $kehadiran_id));
        }
        redirect(base_url().'dashboard/result/izin-siswa');
    }

    public function updateIzinSiswa()
    {
        if($this->input->post('tanggal'))
        {
            $tanggal = $this->input->post('tanggal');
            $jam = date('H:i:s');
            $absen  = $this->input->post('status');
            foreach ($absen as $key => $value) 
            {
                if($value != 'none')
                {
                    $data   = array(
                        'kehadiran_siswa'     => $key,
                        'id_sekolah' => $this->session->userdata('sess_sekolah'),
                        'kehadiran_timestamp' => $tanggal . ' ' . $jam,
                        'id_pengguna_pengabsen' => $this->session->userdata('sess_id')
                    );
                    // print_r($value);
                    if($value == 'Izin')
                        $data['kehadiran_verification'] = 2;
                    elseif($value == 'Sakit')
                        $data['kehadiran_verification'] = 1;
                    else
                        $data['kehadiran_verification'] = 3;

                    $this->Jurnal->siswaAbsen($data);
                }
            }
        }
        redirect(base_url().'dashboard/result/izin-siswa');
    }
    
    public function izinSiswa()
    {
        $tanggal = date('Y-m-d');
        $kelas = null;
        $rombel = null;
        $status = null;
        $getTanggal = sanitize($this->input->get('tanggal'));
        $getKelas = sanitizeNoSpace($this->input->get('kelas'));
        $getRombel = sanitizeNoSpace($this->input->get('rombel'));
        
        if($getTanggal)
            $tanggal = $getTanggal;
        if($getKelas)
            $kelas = $getKelas;
        if($getRombel)
            $rombel = $getRombel;
        
        $tahunAjaran = getTahunAjaran($tanggal);
        $absen = $this->Jurnal->getSiswaAbsen($this->session->userdata('sess_sekolah'), $tanggal, $tahunAjaran, $kelas, $rombel, $status)->result_array();

        $nisAbsen = array();
        foreach ($absen as $temp)
            array_push($nisAbsen, $temp['kehadiran_siswa']);

        $belum = $this->Jurnal->getSiswaBelumAbsen($this->session->userdata('sess_sekolah'), $tahunAjaran, $nisAbsen, $kelas, $rombel)->result_array();

        $listKelas = $this->Jurnal->getAllKelas($this->session->userdata('sess_sekolah'), $tahunAjaran, 1);

        $listRombel = $listKelas['rombel'];
        $listKelas = $listKelas['kelas'];

        $teksTanggal = 'Tanggal ' . humanDateFormat($tanggal) . ' (' . $tahunAjaran . ')';

        if(is_null($kelas))
        {
            if(!is_null($rombel))
                $teksKelas = 'Semua Kelas dengan Rombel "' . $rombel .'"';
            else
                $teksKelas = 'Semua Kelas';
        }
        else
        {
            if(!is_null($rombel))
                $teksKelas = 'Kelas "' . $kelas . $rombel .'"';
            else
                $teksKelas = 'Semua Kelas "' . $kelas .'"';
        }

        $data = array(
            'absen' => $absen, 
            'belum' => $belum, 
            'tanggal' => $tanggal,
            'teksTanggal' => $teksTanggal, 
            'teksKelas' => $teksKelas,
            'rombel' => $listRombel,
            'kelas' => $listKelas
        );
        $this->load->view('layout/header');
        $this->load->view('layout/navigationbar');
        $this->load->view('page/hasilAbsensi/izinSakit', $data);
        $this->load->view('layout/footer');
    }

    public function convTgl($vtgl = '2019-02-29 11:05:20', $useJam = TRUE)
    {
        //$month = $this->getMonthName(date('n', strtotime($vtgl)));
        $month = getMonthName(date('n', strtotime($vtgl)));
        $date = explode(" ", $vtgl);
        $str = explode("-", $date[0]);
        $time = explode(":", $date[1]);
        $jam = $time[0] . ":" . $time[1];
        if(!$useJam)
            return $str[2] . " " . $month . " " . $str[0];
        return $str[2] . " " . $month . " " . $str[0] . " " . $jam;
    }

    public function siswa()
    {
         // **
        //$read = $this->absen_model->read($this->session->userdata('sess_sekolah'))->result_array();
        $start = date('Y-m-d');
        $end = date('Y-m-d');
        $status = null;
        if(sanitize($this->input->get('start')))
            $start = sanitize($this->input->get('start'));
        if(sanitize($this->input->get('end')))
            $end = sanitize($this->input->get('end'));
        if($this->input->get('status') == 'Terlambat')
            $status = 3;
        if($this->input->get('status') == 'Tepat')
            $status = 0;
        
        $read = $this->absen_model->getKehadiranSiswa($this->session->userdata('sess_sekolah'), $start, $end, $status)->result_array();
        for ($i=0; $i < count($read); $i++) { 
          $read[$i]['kehadiran_tanggal'] = $this->convTgl($read[$i]['kehadiran_timestamp']);
        }
        $data = array('read' => $read);
        $jadwal = $this->db->where('status', 1)->where('id_sekolah', $this->session->userdata('sess_sekolah'))->get('tb_jadwal_masuk')->result_array();
        if(count($jadwal)>0)
        {
            $jadwal = explode(":",$jadwal[0]['jm_masuk']);
            $jadwal = $jadwal[0] . '.' . $jadwal[1];
        }
        else
            $jadwal = '07.05';

        if($start == $end)
            $tanggal = 'List Absensi Siswa (' . humanDateFormat($start, 1) . ')';
        else
            $tanggal = 'List Absensi Siswa (' . humanDateFormat($start) . ' - ' . humanDateFormat($end) . ')'; 

        $data['jadwal'] = $jadwal;
        $data['tanggal'] = $tanggal;

        $this->load->view('layout/header');
        $this->load->view('page/hasilAbsensi/header');
        $this->load->view('layout/navigationbar');
        $this->load->view('page/hasilAbsensi/kedatanganSiswa', $data);
        $this->load->view('layout/footer');
        $this->load->view('page/hasilAbsensi/footer');
    }

    public function downloadRekapExcelSiswa()
    {
        $this->form_validation->set_rules('semester', 'Ganjil/ Genap', 'required');
        $this->form_validation->set_rules('tahun', '2019++', 'required');
        $this->form_validation->set_rules('bulan', 'bulan rekap', 'required');

        if ($this->form_validation->run() == FALSE)
            redirect(base_url().'dashboard/result/absensi/siswa');
        $semester = $this->input->post('semester');
        $year = (int)$this->input->post('tahun');
        $bulan = $this->input->post('bulan');

        if($semester != 'Ganjil' && $semester != 'Genap')
            redirect(base_url().'dashboard/result/absensi/siswa');
        if($bulan != 'all')
        {
            $bulan = (int)$bulan;
            if($bulan<1 || $bulan>12)
                redirect(base_url().'dashboard/result/absensi/siswa');
        }

        $tahunAjaran = $semester.' '.$year.'/'.($year+1);
        //$start = date('Y-m-d 00:00:00');
        //$end = date('Y-m-d 23:59:59');
        //
        /*$bulan = explode('-', $start);
        $year = $bulan[0];
        $bulan = $bulan[1];*/

        $siswa = array();
        $absen = array();

        //ambil data siswa & kelas
        $siswa = $this->siswa->getSiswaAndKelas($this->session->userdata('sess_sekolah'), $tahunAjaran, TRUE, TRUE)->result_array();
        //ambil data absen
        //$absen = $this->absen_model->getKehadiranByDate($this->session->userdata('sess_sekolah'), $start, $end, TRUE)->result_array(); //ini versi lama
        $absen = $this->absen_model->getKehadiranByMonth($this->session->userdata('sess_sekolah'), $bulan, $year, $semester)->result_array();
        //bikin spreadsheet
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        if($bulan == 'all')
            $sheet->setTitle(str_replace('/', '-', $tahunAjaran));
        else
            $sheet->setTitle(getMonthName($bulan).' '. $year);
        $this->formatExcelRekap($sheet);
        //Judul di sheet
        $sheet->setCellValue('B1', 'Rekap Absensi Siswa SMPI Al-Azhar 13 Surabaya');
        $sheet->getStyle('B1')->applyFromArray($this->style_title);
        $dateTitle = "Semua Bulan (" . $tahunAjaran .')';
        if($bulan != 'all')
            $dateTitle = getMonthName($bulan) .' (' . $tahunAjaran .')';
        $sheet->setCellValue('B2', $dateTitle);
        $sheet->getStyle('B2')->applyFromArray($this->style_title);
        
        //bikin array buat rekap absen siswa
        $rekapSiswa = array();
        $checkSiswa = array();
        //lokasi row di excel
        $row = 7;

        foreach($siswa as $data)
        {
            if(!is_null($data['nm_kelas']) && $data['nm_kelas'] != '')
            {
                $rekapSiswa[$data['nomor_induk']] = array(
                    'row' => $row,
                    'tepatwaktu' => 0,
                    'sakit' => 0,
                    'izin' => 0,
                    'terlambat' => 0,
                    'tanpaketerangan' => 0
                );
                $row++;
                //array buat siswa yang gak absen
                $checkSiswa[$data['nomor_induk']] = $data['nomor_induk'];
            }
        }
        //array dari checkSiswa yang bakal dijadiin patokan siswa absen atau nggak
        $tempCheck = $checkSiswa;

        //variabel buat ngecheck apakah bulan di foreach sudah ganti/ belum
        $month = $bulan;
        if($semester == 'Ganjil')
        {
            if($bulan == 'all')
                $month = 7;
        }
        else
        {
            $year = $year+1;
            if($bulan == 'all')
                $month = 1;
        }
        //variabel buat ngecheck apakah tanggal di foreach sudah ganti/ belum
        $tanggal = new DateTime($year.'-'.$month.'-'.'1');
        //inisiasi column excel
        $col = 'K';
        $firstCol = $col;
        //bikin kolom bulan di excel
        $sheet->setCellValue($firstCol.'4', getMonthName($month));
        //bikin kolom tanggal dan hari di excel
        $sheet->setCellValue($firstCol.'5', (int)explode('-', $tanggal->format('Y-m-d'))[2]);
        $sheet->getStyle($firstCol.'5')->applyFromArray($this->style_col);
        $sheet->setCellValue($firstCol.'6', getDayName($tanggal->format('Y-m-d')));
        $sheet->getStyle($firstCol.'6')->applyFromArray($this->style_col);
        $sheet->getColumnDimension($firstCol)->setWidth(4);
        //
        $jumlahAbsen = count($absen);
        $jumlahSiswa = count($checkSiswa);
        for($i = 0; $i<$jumlahAbsen;)
        {
            //check tanggal absen
            $tanggalAbsen = explode(' ', $absen[$i]['kehadiran_timestamp'])[0];
            if($tanggal->format('Y-m-d') != $tanggalAbsen)
            {
                if(count($tempCheck) < $jumlahSiswa)
                {
                    //semua nomor induk yang masih ada di tempCheck dianggap tidak absen
                    foreach($tempCheck as $t)
                    {
                        $row = $rekapSiswa[$t]['row'];
                        $sheet->getStyle($col.$row)->applyFromArray($this->style_status[4]);
                        $rekapSiswa[$t]['tanpaketerangan']++;
                    }
                    $tempCheck = $checkSiswa;
                }
                //tanggal disamain sama tanggal absen
                $tanggal = $tanggal->modify('+1 day');
                $tempMonth = explode('-', $tanggal->format('Y-m-d'))[1];
                if($tempMonth!=$month)
                {
                    $sheet->mergeCells($firstCol.'4:'.$col.'4');
                    $sheet->getStyle($firstCol.'4:'.$col.'4')->applyFromArray($this->style_col);
                    $firstCol = $col;
                    $firstCol++;
                    $month = $tempMonth;
                    $sheet->setCellValue($firstCol.'4', getMonthName($month));
                }
                $col++;
                $sheet->setCellValue($col.'5', (int)explode('-', $tanggal->format('Y-m-d'))[2]);
                $sheet->getStyle($col.'5')->applyFromArray($this->style_col);
                $sheet->setCellValue($col.'6', getDayName($tanggal->format('Y-m-d')));
                $sheet->getStyle($col.'6')->applyFromArray($this->style_col);
                $sheet->getColumnDimension($col)->setWidth(4);
            }
            else
            {
                if(isset($rekapSiswa[$absen[$i]['nomor_induk']]))
                {
                    //check status absen siswa
                    $status = $absen[$i]['kehadiran_verification'];
                    if($status==0)
                        $rekapSiswa[$absen[$i]['nomor_induk']]['tepatwaktu']++;
                    elseif($status==1)
                        $rekapSiswa[$absen[$i]['nomor_induk']]['sakit']++;
                    elseif($status==2)
                        $rekapSiswa[$absen[$i]['nomor_induk']]['izin']++;
                    else
                        $rekapSiswa[$absen[$i]['nomor_induk']]['terlambat']++;

                    $row = $rekapSiswa[$absen[$i]['nomor_induk']]['row'];
                    $sheet->getStyle($col.$row)->applyFromArray($this->style_status[$status]);
                    //unset dari tempCheck sebagai tanda siswa sudah absen
                    unset($tempCheck[$absen[$i]['nomor_induk']]);
                }
                $i++;
                if($i==$jumlahAbsen)
                {
                    $sheet->mergeCells($firstCol.'4:'.$col.'4');
                    $sheet->getStyle($firstCol.'4:'.$col.'4')->applyFromArray($this->style_col);
                    if(count($tempCheck) < $jumlahSiswa)
                    {
                        //semua nomor induk yang masih ada di tempCheck dianggap tidak absen
                        foreach($tempCheck as $t)
                        {
                            $row = $rekapSiswa[$t]['row'];
                            $sheet->getStyle($col.$row)->applyFromArray($this->style_status[4]);
                            $rekapSiswa[$t]['tanpaketerangan']++;
                        }
                        $tempCheck = $checkSiswa;
                    }
                }
            }
        }
        //taruh data rekap siswa ke excel
        $j = 0;
        for($i = 0; $i<count($siswa); $i++)
        {
            if(isset($rekapSiswa[$siswa[$i]['nomor_induk']]))
            {
                $row = $rekapSiswa[$siswa[$i]['nomor_induk']]['row'];
                $sheet->setCellValue('A'.$row, ($j+1));
                $sheet->getStyle('A'.$row)->applyFromArray($this->style_row);
                $sheet->setCellValue('B'.$row, $siswa[$i]['nomor_induk']);
                $sheet->getStyle('B'.$row)->applyFromArray($this->style_row);
                $sheet->setCellValue('C'.$row, $siswa[$i]['nm_pengguna']);
                $sheet->getStyle('C'.$row)->applyFromArray($this->style_row);
                $sheet->setCellValue('D'.$row, $siswa[$i]['nm_kelas'] . $siswa[$i]['rombel_kelas']);
                $sheet->getStyle('D'.$row)->applyFromArray($this->style_row);
                $sheet->setCellValue('E'.$row, $rekapSiswa[$siswa[$i]['nomor_induk']]['tepatwaktu']);
                $sheet->getStyle('E'.$row)->applyFromArray($this->style_row);
                $sheet->setCellValue('F'.$row, $rekapSiswa[$siswa[$i]['nomor_induk']]['sakit']);
                $sheet->getStyle('F'.$row)->applyFromArray($this->style_row);
                $sheet->setCellValue('G'.$row, $rekapSiswa[$siswa[$i]['nomor_induk']]['izin']);
                $sheet->getStyle('G'.$row)->applyFromArray($this->style_row);
                $sheet->setCellValue('H'.$row, $rekapSiswa[$siswa[$i]['nomor_induk']]['terlambat']);
                $sheet->getStyle('H'.$row)->applyFromArray($this->style_row);
                $sheet->setCellValue('I'.$row, $rekapSiswa[$siswa[$i]['nomor_induk']]['tanpaketerangan']);
                $sheet->getStyle('I'.$row)->applyFromArray($this->style_row);
                $sheet->setCellValue('J'.$row, '=E'.$row.'+F'.$row.'+G'.$row.'+H'.$row.'+I'.$row);
                $sheet->getStyle('J'.$row)->applyFromArray($this->style_row);
                unset($rekapSiswa[$siswa[$i]['nomor_induk']]);
                $j++;
            }
        }
        //
        $sheet->getStyle('K7:'.$col.$row)->applyFromArray($this->style_row);
        //penambahan legend
        $row = $row + 2;
        for($i = 0; $i<5; $i++)
        {
            if($i==0)
                $sheet->setCellValue('B'.($row + $i), 'Tepat Waktu');
            elseif($i==1)
                $sheet->setCellValue('B'.($row + $i), 'Sakit');
            elseif($i==2)
                $sheet->setCellValue('B'.($row + $i), 'Izin');
            elseif($i==3)
                $sheet->setCellValue('B'.($row + $i), 'Terlambat');
            else
                $sheet->setCellValue('B'.($row + $i), 'Tanpa Keterangan');
            $sheet->getStyle('A'.($row + $i))->applyFromArray($this->style_status[$i]);
        }

        if($col != 'K')
        {
            $writer = new Xlsx($spreadsheet); // instantiate Xlsx

            $title = "Rekap Absensi Siswa " . str_replace('/','-',$dateTitle);

            header('Content-Type: application/vnd.ms-excel'); // generate excel file
            header('Content-Disposition: attachment;filename="'. $title .'.xlsx"'); 
            header('Cache-Control: max-age=0');
            
            $writer->save('php://output'); // download file 
        }
        else
        {
            $this->session->set_flashdata('pesanRekap', 'Data absensi siswa tidak ditemukan, silahkan coba bulan dan tahun ajaran yang lain.');
            redirect(base_url().'dashboard/result/absensi/siswa');
        }
    }

    public function downloadExcelSiswa()
    {
        $start = date('Y-m-d 00:00:00');
        $end = date('Y-m-d 23:59:59');
        //
        $start = '2020-02-01 00:00:00';
        $end = '2020-02-29 23:59:59';
        $bulan = explode('-', $start);
        $year = $bulan[0];
        $bulan = $bulan[1];

        $absen = array();
        if($start<$end)
        {
            $absen = $this->absen_model->getKehadiranByDate($this->session->userdata('sess_sekolah'), $start, $end)->result_array();
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setTitle(getMonthName($bulan).' '. $year);
            $this->formatExcel($sheet);
        }

        $row = 2;
        foreach($absen as $data)
        {
            $sheet->setCellValue('A'.$row, ($row-1));
            $sheet->getStyle('A'.$row)->applyFromArray($this->style_row);
            $sheet->setCellValue('B'.$row, $data['nomor_induk']);
            $sheet->getStyle('B'.$row)->applyFromArray($this->style_row);
            $sheet->setCellValue('C'.$row, $data['nm_pengguna']);
            $sheet->getStyle('C'.$row)->applyFromArray($this->style_row);
            $sheet->setCellValue('D'.$row, $data['nm_kelas']);
            $sheet->getStyle('D'.$row)->applyFromArray($this->style_row);
            $sheet->setCellValue('E'.$row, $data['kehadiran_timestamp']);
            $sheet->getStyle('E'.$row)->applyFromArray($this->style_row);
            $status = $data['kehadiran_verification'];
            if($status==0)
                $status = 'Tepat Waktu';
            elseif($status==1)
                $status = 'Sakit';
            elseif($status==2)
                $status = 'Ijin';
            else
                $status = 'Terlambat';
            $sheet->setCellValue('F'.$row, $status);
            $sheet->getStyle('F'.$row)->applyFromArray($this->style_row);

            $row++;
        }

        if($row != 2)
        {
            $writer = new Xlsx($spreadsheet); // instantiate Xlsx

            $title = "Hasil Absensi Siswa " . $this->convTgl($start, FALSE) . " - " . $this->convTgl($end, FALSE);

            header('Content-Type: application/vnd.ms-excel'); // generate excel file
            header('Content-Disposition: attachment;filename="'. $title .'.xlsx"'); 
            header('Cache-Control: max-age=0');
            
            $writer->save('php://output'); // download file 
        }
        else
        {
            echo 'Data tidak ditemukan';
        }
    }

    private function formatExcelRekap($sheet)
    {
        //style title
        $style_title = array(
            'font' => array('bold' => true, 'size' => 14),    
            'alignment' => array(
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            )
        );
        //style header
        $style_col = array(
            'font' => array('bold' => true),    
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'right' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'bottom' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'left' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            )
        );
        //style row
        $style_row = array(
            'alignment' => array(
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER 
            ),
            'borders' => array(
                'top' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'right' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'bottom' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'left' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            )
        );
        //style status absen
        $style_status = array(
            0 => array(
                'borders' => array(
                    'top' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'right' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'bottom' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'left' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ),
                'fill' => array(
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'startColor' => array(
                        'argb' => 'FF00B050'
                    )
                )
            ),
            1 => array(
                'borders' => array(
                    'top' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'right' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'bottom' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'left' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ),
                'fill' => array(
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'startColor' => array(
                        'argb' => 'FF00B0F0'
                    )
                )
            ),
            2 => array(
                'borders' => array(
                    'top' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'right' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'bottom' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'left' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ),
                'fill' => array(
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'startColor' => array(
                        'argb' => 'FFFFFF00'
                    )
                )
            ),
            3 => array(
                'borders' => array(
                    'top' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'right' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'bottom' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'left' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ),
                'fill' => array(
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'startColor' => array(
                        'argb' => 'FFFF0000'
                    )
                )
            ),
            4 => array(
                'borders' => array(
                    'top' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'right' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'bottom' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                    'left' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
                ),
                'fill' => array(
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'startColor' => array(
                        'argb' => 'FF0D0D0D'
                    )
                )
            )
        );

        $this->style_title = $style_title;
        $this->style_col = $style_col;
        $this->style_row = $style_row;
        $this->style_status = $style_status;

        $sheet->setCellValue('A4', "No");
        $sheet->setCellValue('B4', "NIS");
        $sheet->setCellValue('C4', "Nama");
        $sheet->setCellValue('D4', "Kelas");
        $sheet->setCellValue('E4', "Jumlah");
        $sheet->setCellValue('E5', "Tepat Waktu");
        $sheet->setCellValue('F5', "Sakit");
        $sheet->setCellValue('G5', "Izin");
        $sheet->setCellValue('H5', "Terlambat");
        $sheet->setCellValue('I5', "Tanpa Keterangan");
        $sheet->setCellValue('J5', "Total");
        //merge
        $sheet->mergeCells('B1:I1');
        $sheet->mergeCells('B2:I2');
        $sheet->mergeCells('A4:A6');
        $sheet->getStyle('A4:A6')->applyFromArray($this->style_col);
        $sheet->mergeCells('B4:B6');
        $sheet->getStyle('B4:B6')->applyFromArray($this->style_col);
        $sheet->mergeCells('C4:C6');
        $sheet->getStyle('C4:C6')->applyFromArray($this->style_col);
        $sheet->mergeCells('D4:D6');
        $sheet->getStyle('D4:D6')->applyFromArray($this->style_col);
        $sheet->mergeCells('E4:J4');
        $sheet->getStyle('E4:J4')->applyFromArray($this->style_col);
        $sheet->mergeCells('E5:E6');
        $sheet->getStyle('E5:E6')->applyFromArray($this->style_col);
        $sheet->mergeCells('F5:F6');
        $sheet->getStyle('F5:F6')->applyFromArray($this->style_col);
        $sheet->mergeCells('G5:G6');
        $sheet->getStyle('G5:G6')->applyFromArray($this->style_col);
        $sheet->mergeCells('H5:H6');
        $sheet->getStyle('H5:H6')->applyFromArray($this->style_col);
        $sheet->mergeCells('I5:I6');
        $sheet->getStyle('I5:I6')->applyFromArray($this->style_col);
        $sheet->mergeCells('J5:J6');
        $sheet->getStyle('J5:J6')->applyFromArray($this->style_col);

        for($i = 'A'; $i!='K'; $i++)
            $sheet->getColumnDimension($i)->setAutoSize(true);
    }

    private function formatExcel($sheet)
    {
        //style title
        $style_title = array(
            'font' => array('bold' => true, 'size' => 16),    
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            )
        );
        //style header
        $style_col = array(
            'font' => array('bold' => true),    
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'right' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'bottom' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'left' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            )
        );
        //style row
        $style_row = array(
            'alignment' => array(
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER 
            ),
            'borders' => array(
                'top' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'right' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'bottom' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN),
                'left' => array('borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN)
            )
        );

        $this->style_col = $style_col;
        $this->style_row = $style_row;

        $sheet->setCellValue('A1', "No");
        $sheet->setCellValue('B1', "NIS");
        $sheet->setCellValue('C1', "Nama");
        $sheet->setCellValue('D1', "Kelas");
        $sheet->setCellValue('E1', "Waktu Absen");
        $sheet->setCellValue('F1', "Status Absen");

        for($i = 'A'; $i!='G'; $i++)
        {
            $sheet->getStyle($i.'1')->applyFromArray($this->style_col);
            $sheet->getColumnDimension($i)->setAutoSize(true);
        }
    }

    public function downloadExcelSiswaOld()
    {
        $start = date('Y-m-d 00:00:00');
        $end = date('Y-m-d 00:00:00');
        $excel = new PHPExcel();
        $title = "Hasil Absensi Siswa " . $this->convTgl($start, FALSE) . " - " . $this->convTgl($end, FALSE);
        $excel->getProperties()->setCreator($this->session->userdata('sess_name'))
            ->setLastModifiedBy($this->session->userdata('sess_name'))
            ->setTitle($title)
            ->setSubject("Siswa")
            ->setDescription("Laporan Absensi Siswa")
            ->setKeywords("Absensi Siswa");
        //style header
        $style_col = array(
            'font' => array('bold' => true),    
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
            )
        );
        //style row
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
            )
        );
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "No");
        $excel->setActiveSheetIndex(0)->setCellValue('B1', "NIS");
        $excel->setActiveSheetIndex(0)->setCellValue('C1', "Nama");
        $excel->setActiveSheetIndex(0)->setCellValue('D1', "Kelas");
        $excel->setActiveSheetIndex(0)->setCellValue('E1', "Waktu Absen");
        $excel->setActiveSheetIndex(0)->setCellValue('F1', "Status Absen");
        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A1')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B1')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C1')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D1')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E1')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F1')->applyFromArray($style_col);

        $absen = $this->absen_model->read($this->session->userdata('sess_sekolah'))->result_array();
        $numrow = 2;
        foreach($absen as $data)
        {
            $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, ($numrow-1));
            $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data['nomor_induk']);
            $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data['nm_pengguna']);
            $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data['nm_kelas']);
            $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data['kehadiran_timestamp']);
            $status = $data['kehadiran_verification'];
            if($status==0)
                $status = 'Tepat Waktu';
            elseif($status==1)
                $status = 'Sakit';
            elseif($status==2)
                $status = 'Ijin';
            else
                $status = 'Terlambat';
            $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $status);
            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
            $numrow++;
        }
        //set width kolom
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        // Set judul sheet excel nya
        $excel->getActiveSheet(0)->setTitle("Laporan Absensi Siswa");
        $excel->setActiveSheetIndex(0);
        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // Set nama file excel nya
        header('Content-Disposition: attachment; filename="'.$title.'.xlsx"'); 
        header('Cache-Control: max-age=0');
        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
    }

    public function civitas()
    {
        $start = date('Y-m-d');
        $end = date('Y-m-d');
        $status = null;
        if(sanitize($this->input->get('start')))
            $start = sanitize($this->input->get('start'));
        if(sanitize($this->input->get('end')))
            $end = sanitize($this->input->get('end'));
        if($this->input->get('status') == 'Belum Pulang')
            $status = 1;
        if($this->input->get('status') == 'Sudah Pulang')
            $status = 2;
        
        $read = $this->absen_model->getKehadiranGuru($this->session->userdata('sess_sekolah'), $start, $end, $status)->result_array();
        for ($i=0; $i < count($read); $i++) { 
          $read[$i]['tanggal_indo'] = humanDateFormat($read[$i]['tanggal']);
        }
        $data = array('read' => $read);

        if($start == $end)
            $tanggal = 'List Absensi Guru & Karyawan (' . humanDateFormat($start, 1) . ')';
        else
            $tanggal = 'List Absensi Guru & Karyawan (' . humanDateFormat($start) . ' - ' . humanDateFormat($end) . ')'; 

        $data['tanggal'] = $tanggal;

        $this->load->view('layout/header');
        $this->load->view('page/hasilAbsensi/header');
        $this->load->view('layout/navigationbar');
        $this->load->view('page/hasilAbsensi/kedatanganCivitas', $data);
        $this->load->view('layout/footer');
        $this->load->view('page/hasilAbsensi/footer');
    }

    public function guru()
    {       
        $start = $this->input->get('start', TRUE);
        $end = $this->input->get('end', TRUE);
        $status = ($this->input->get('status', TRUE))? $this->input->get('status', TRUE) : NULL;
        // var_dump($status);
        // die();
        if (isset($start)) {
            $absensi = $this->absensi->getList($start, date('Y-m-d', strtotime("+1 days", strtotime($end))), $status, $this->session->userdata('sess_sekolah'));
        } else {
            $absensi = $this->absensi->getList(date('Y-m-d', strtotime("-30 days")), date('Y-m-d', strtotime("+1 days")), $status, $this->session->userdata('sess_sekolah'));
        }
        // echo date('Y-m-d');
        // die();
        $this->load->view('layout/header');
        $this->load->view('page/hasilAbsensi/header');
        $this->load->view('layout/navigationbar');
        $this->load->view('page/hasilAbsensi/kedatanganKaryawan', array('absensi' => $absensi));
        $this->load->view('layout/footer');
        $this->load->view('page/hasilAbsensi/footer');
    }

    public function upload_absensi_guru () {
        $path = 'uploads/';
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'xlsx|xls';
        $config['remove_spaces'] = TRUE;
        $this->upload->initialize($config);
        // $this->load->library('upload', $config);
        if (!$this->upload->do_upload('userfile')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            
            if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
            }
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            // var_dump($allDataInSheet);
            // die();
            $flag = true;
                $i=0;
                foreach ($allDataInSheet as $value) {
                  if($flag){
                    $flag =false;
                    continue;
                  }
                  if ($value['B'] != 'Nama') {
                    $date = str_replace('/', '-', $value['C']);
                    // $date = $value['C'];
                    // var_dump($date); die();
                    $jenis = explode('/', $value['D']);
                    $inserdata[$i]['id_sekolah'] = $this->session->userdata('sess_sekolah');
                    $inserdata[$i]['kode_absensi'] = $value['A'];
                    $inserdata[$i]['nama'] = $value['B'];
                    $inserdata[$i]['jenis'] = strtolower($jenis[1]);
                    $inserdata[$i]['timestamp'] = date('Y-m-d H:i:s', strtotime($date));
                    $i++;
                  }                  
                } 
                // var_dump($inserdata);
                // die();
                unlink($inputFileName);
                $result = $this->absensi->insert_batch($inserdata); 
                if($result){
                  redirect(base_url().'dashboard/result/absensi/guru');
                }else{
                  echo "ERROR !";
                }
    }

    public function show ($id) {
        $data = $this->JamPelajaran->getWaktuById($id);
        $this->load->view('layout/header');
        $this->load->view('layout/navigationbar');
        $this->load->view('page/jamPelajaran/showJamPelajaran', array('data' => $data));
        $this->load->view('layout/footer');
    }

    public function store () {
        $this->form_validation->set_rules('kelas', 'Kelas', 'required');
          if ($this->form_validation->run() == FALSE)
            {
                redirect(base_url().'dashboard/civitas/tambah');
            }
            else
            {
                $this->JamPelajaran->insert();
                redirect(base_url().'dashboard/civitas');
            }
    }

    public function update ($id) {
        $data = $this->JamPelajaran->update($id, 'a');
        redirect(base_url().'dashboard/civitas');   
    }

    public function destroy ($id) {
        $this->JamPelajaran->deleteKelas($id);
        redirect(base_url().'dashboard/civitas');
    }

    /*public function getMonthName($month)
    {
        if($month==1) return 'Januari';
        if($month==2) return 'Februari';
        if($month==3) return 'Maret';
        if($month==4) return 'April';
        if($month==5) return 'Mei';
        if($month==6) return 'Juni';
        if($month==7) return 'Juli';
        if($month==8) return 'Agustus';
        if($month==9) return 'September';
        if($month==10) return 'Oktober';
        if($month==11) return 'November';
        if($month==12) return 'Desember';
    }*/
    
    public function kehadiran(){
        $siswa = $this->siswa->dataSiswa()->result_array();
        $absen = $this->siswa->dataHadir()->result_array();
        $checkSiswa = array();
        foreach($siswa as $data)
        {
            //array buat siswa yang gak absen
            $checkSiswa[$data['nomor_induk']] = $data;
        }
        //array dari checkSiswa yang bakal dijadiin patokan siswa absen atau nggak
        $tempCheck = $checkSiswa;
        $curdate = null;
        $totalSiswa = count($tempCheck);
        echo '<table>
        <thead>
                   <tr>
                      <th>Id Pengguna</th>
                      <th>Tahun Ajaran</th>
                      <th>Nomor Induk</th>
                      <th>Waktu Kehadiran</th>
                      <th>Status Kehadiran</th>
                    </tr>
                    </thead>
                    <tbody>
        ';    
        foreach($absen as $data){
            $tgl = explode(' ', $data['kehadiran_timestamp'])[0];
            if(is_null($curdate) || $tgl != $curdate){
                if(count($tempCheck) < $totalSiswa){
                    if(is_null($curdate)){
                    $curdate = $tgl;
                    }    
                    foreach($tempCheck as $temp){
                         echo '<tr>';
                         echo '<td>'.$temp['id_pengguna'].'</td>';
                         echo '<td>'.$tahunAjaran.'</td>';
                         echo '<td>'.$temp['nomor_induk'].'</td>';
                         echo '<td>'.$curdate.' 16:00:00'.'</td>';   
                         echo '<td>'.'4'.'</td>';     
                         echo '</tr>'; 
                    }
                }
                $tahunAjaran=getTahunAjaran($tgl);
                $curdate=$tgl;
                $tempCheck = $checkSiswa;
            }
            if(isset($tempCheck[$data['kehadiran_siswa']])){
             echo '<tr>';
             echo '<td>'.$tempCheck[$data['kehadiran_siswa']]['id_pengguna'].'</td>';
             echo '<td>'.$tahunAjaran.'</td>';
             echo '<td>'.$tempCheck[$data['kehadiran_siswa']]['nomor_induk'].'</td>';
             echo '<td>'.$data['kehadiran_timestamp'].'</td>';   
             echo '<td>'.$data['kehadiran_verification'].'</td>';     
             echo '</tr>';    
             unset($tempCheck[$data['kehadiran_siswa']]);
            }
        }
        echo '</tbody>
        </table>';
    }
    
    public function listkelas(){
        $kelas = $this->siswa->dataKelas()->result_array();
        echo '<table>
        <thead>
            <tr>
                <th>Id Kelas</th>
                <th>Kelas</th>
                <th>Rombel</th>
                <th>Id Ketua Kelas</th>
                <th>Id Wali Kelas</th>
                <th>Tahun Ajaran</th>
            </tr>
        </thead>
        <tbody>
        ';      
        foreach($kelas as $data){
            echo '<tr>';
            echo '<td>'.$data['id_kelas'].'</td>';
            echo '<td>'.$data['nm_kelas'].'</td>';
            echo '<td>'.$data['rombel_kelas'].'</td>';   
            echo '<td>'.$data['id_ketua_kelas'].'</td>'; 
            echo '<td>'.$data['wali_kelas'].'</td>';
            echo '<td>'.$data['tahun_ajaran'].'</td>'; 
            echo '</tr>'; 
        }
        echo '</tbody>
        </table>';   
    }
    
    public function listkelassiswa(){
        $kelassiswa = $this->siswa->dataKelasSiswa()->result_array();
        echo '<table>
        <thead>
            <tr>
                <th>Id Kelas Siswa</th>
                <th>Id Kelas</th>
                <th>Id Siswa</th>
                <th>Tahun Ajaran</th>
            </tr>
        </thead>
        <tbody>
        ';      
        foreach($kelassiswa as $data){
            echo '<tr>';
            echo '<td>'.$data['id_kelas_siswa'].'</td>';
            echo '<td>'.$data['id_kelas'].'</td>';
            echo '<td>'.$data['id_siswa'].'</td>';
            echo '<td>'.$data['tahun_ajaran'].'</td>'; 
            echo '</tr>'; 
        }
        echo '</tbody>
        </table>';   
    }
    
    public function listpengguna(){
        $pengguna = $this->siswa->dataPengguna()->result_array();
        echo '<table>
        <thead>
            <tr>
                <th>Id Pengguna</th>
                <th>Nama Pengguna</th>
                <th>Alamat</th>
                <th>No HP</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
        ';      
        foreach($pengguna as $data){
            echo '<tr>';
            echo '<td>'.$data['id_pengguna'].'</td>';
            echo '<td>'.$data['nm_pengguna'].'</td>';
            echo '<td>'.$data['alamat'].'</td>';   
            echo '<td>'.$data['no_hp'].'</td>'; 
            echo '<td>'.$data['email'].'</td>';
            echo '</tr>'; 
        }
        echo '</tbody>
        </table>';   
    }
    
    public function listpenggunasekolah(){
        $penggunasekolah = $this->siswa->dataPenggunaSekolah()->result_array();
        echo '<table>
        <thead>
            <tr>
                <th>Id Pengguna Sekolah</th>
                <th>Id Sekolah</th>
                <th>Id Pengguna</th>
                <th>Id Role</th>
                <th>Nomor Induk</th>
            </tr>
        </thead>
        <tbody>
        ';      
        foreach($penggunasekolah as $data){
            echo '<tr>';
            echo '<td>'.$data['id_pengguna_sekolah'].'</td>';
            echo '<td>'.$data['id_sekolah'].'</td>';
            echo '<td>'.$data['id_pengguna'].'</td>';   
            echo '<td>'.$data['id_role'].'</td>'; 
            echo '<td>'.$data['nomor_induk'].'</td>';
            echo '</tr>'; 
        }
        echo '</tbody>
        </table>';   
    }
    
    public function jurnalguru(){
        $jurnalguru = $this->siswa->jurnalGuru()->result_array();
        echo '<table>
        <thead>
            <tr>
                <th>Id Jurnal</th>
                <th>Id Jadwal Guru</th>
                <th>Id Guru</th>
                <th>Id Kelas</th>
                <th>Minggu Ke-</th>
                <th>Id Subjek</th>
                <th>Jam Hadir</th>
                <th>Status</th>
                <th>Kompetensi Dasar</th>
                <th>Catatan</th>
            </tr>
        </thead>
        <tbody>
        ';      
        foreach($jurnalguru as $data){
            echo '<tr>';
            echo '<td>'.$data['id_jurnal'].'</td>';
            echo '<td>'.$data['id_jadwal_guru'].'</td>';
            echo '<td>'.$data['id_guru'].'</td>';   
            echo '<td>'.$data['id_kelas'].'</td>'; 
            echo '<td>'.$data['minggu_ke'].'</td>';
            echo '<td>'.$data['id_subjek'].'</td>';
            echo '<td>'.$data['jam_hadir'].'</td>';
            echo '<td>'.$data['status'].'</td>';
            echo '<td>'.$data['kompetensi_dasar'].'</td>';
            echo '<td>'.$data['catatan'].'</td>';
            echo '</tr>'; 
        }
        echo '</tbody>
        </table>';   
    }
}
