<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataDashboard extends CI_Controller{
    public function __construct(){
        parent::__construct();
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('M_DataDashboard');
        date_default_timezone_set("Asia/Jakarta");
    }
    
    public function kehadiran(){
        $siswa = $this->M_DataDashboard->dataSiswa()->result_array();
        $absen = $this->M_DataDashboard->dataHadir()->result_array();
        $checkSiswa = array();
        foreach($siswa as $data)
        {
            //array buat siswa yang gak absen
            $checkSiswa[$data['nomor_induk']] = $data;
        }
        //array dari checkSiswa yang bakal dijadiin patokan siswa absen atau nggak
        $tempCheck = $checkSiswa;
        $curdate = null;
        $totalSiswa = count($tempCheck);
        echo '<table>
        <thead>
                   <tr>
                      <th>Id Pengguna</th>
                      <th>Tahun Ajaran</th>
                      <th>Nomor Induk</th>
                      <th>Waktu Kehadiran</th>
                      <th>Status Kehadiran</th>
                    </tr>
                    </thead>
                    <tbody>
        ';    
        foreach($absen as $data){
            $tgl = explode(' ', $data['kehadiran_timestamp'])[0];
            if(is_null($curdate) || $tgl != $curdate){
                if(count($tempCheck) < $totalSiswa){
                    if(is_null($curdate)){
                    $curdate = $tgl;
                    }    
                    foreach($tempCheck as $temp){
                         echo '<tr>';
                         echo '<td>'.$temp['id_pengguna'].'</td>';
                         echo '<td>'.$tahunAjaran.'</td>';
                         echo '<td>'.$temp['nomor_induk'].'</td>';
                         echo '<td>'.$curdate.' 16:00:00'.'</td>';   
                         echo '<td>'.'4'.'</td>';     
                         echo '</tr>'; 
                    }
                }
                $tahunAjaran=getTahunAjaran($tgl);
                $curdate=$tgl;
                $tempCheck = $checkSiswa;
            }
            if(isset($tempCheck[$data['kehadiran_siswa']])){
             echo '<tr>';
             echo '<td>'.$tempCheck[$data['kehadiran_siswa']]['id_pengguna'].'</td>';
             echo '<td>'.$tahunAjaran.'</td>';
             echo '<td>'.$tempCheck[$data['kehadiran_siswa']]['nomor_induk'].'</td>';
             echo '<td>'.$data['kehadiran_timestamp'].'</td>';   
             echo '<td>'.$data['kehadiran_verification'].'</td>';     
             echo '</tr>';    
             unset($tempCheck[$data['kehadiran_siswa']]);
            }
        }
        echo '</tbody>
        </table>';
    }
    
    public function listkelas(){
        $kelas = $this->M_DataDashboard->dataKelas()->result_array();
        echo '<table>
        <thead>
            <tr>
                <th>Id Kelas</th>
                <th>Kelas</th>
                <th>Rombel</th>
                <th>Id Ketua Kelas</th>
                <th>Id Wali Kelas</th>
                <th>Tahun Ajaran</th>
            </tr>
        </thead>
        <tbody>
        ';      
        foreach($kelas as $data){
            echo '<tr>';
            echo '<td>'.$data['id_kelas'].'</td>';
            echo '<td>'.$data['nm_kelas'].'</td>';
            echo '<td>'.$data['rombel_kelas'].'</td>';   
            echo '<td>'.$data['id_ketua_kelas'].'</td>'; 
            echo '<td>'.$data['wali_kelas'].'</td>';
            echo '<td>'.$data['tahun_ajaran'].'</td>'; 
            echo '</tr>'; 
        }
        echo '</tbody>
        </table>';   
    }
    
    public function listkelassiswa(){
        $kelassiswa = $this->M_DataDashboard->dataKelasSiswa()->result_array();
        echo '<table>
        <thead>
            <tr>
                <th>Id Kelas Siswa</th>
                <th>Id Kelas</th>
                <th>Id Siswa</th>
                <th>Tahun Ajaran</th>
            </tr>
        </thead>
        <tbody>
        ';      
        foreach($kelassiswa as $data){
            echo '<tr>';
            echo '<td>'.$data['id_kelas_siswa'].'</td>';
            echo '<td>'.$data['id_kelas'].'</td>';
            echo '<td>'.$data['id_siswa'].'</td>';
            echo '<td>'.$data['tahun_ajaran'].'</td>'; 
            echo '</tr>'; 
        }
        echo '</tbody>
        </table>';   
    }
    
    public function listpengguna(){
        $pengguna = $this->M_DataDashboard->dataPengguna()->result_array();
        echo '<table>
        <thead>
            <tr>
                <th>Id Pengguna</th>
                <th>Nama Pengguna</th>
                <th>Alamat</th>
                <th>No HP</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
        ';      
        foreach($pengguna as $data){
            echo '<tr>';
            echo '<td>'.$data['id_pengguna'].'</td>';
            echo '<td>'.$data['nm_pengguna'].'</td>';
            echo '<td>'.$data['alamat'].'</td>';   
            echo '<td>'.$data['no_hp'].'</td>'; 
            echo '<td>'.$data['email'].'</td>';
            echo '</tr>'; 
        }
        echo '</tbody>
        </table>';   
    }
    
    public function listpenggunasekolah(){
        $penggunasekolah = $this->M_DataDashboard->dataPenggunaSekolah()->result_array();
        echo '<table>
        <thead>
            <tr>
                <th>Id Pengguna Sekolah</th>
                <th>Id Sekolah</th>
                <th>Id Pengguna</th>
                <th>Id Role</th>
                <th>Nomor Induk</th>
            </tr>
        </thead>
        <tbody>
        ';      
        foreach($penggunasekolah as $data){
            echo '<tr>';
            echo '<td>'.$data['id_pengguna_sekolah'].'</td>';
            echo '<td>'.$data['id_sekolah'].'</td>';
            echo '<td>'.$data['id_pengguna'].'</td>';   
            echo '<td>'.$data['id_role'].'</td>'; 
            echo '<td>'.$data['nomor_induk'].'</td>';
            echo '</tr>'; 
        }
        echo '</tbody>
        </table>';   
    }
    
    public function jurnalguru(){
        $jurnalguru = $this->M_DataDashboard->jurnalGuru()->result_array();
        echo '<table>
        <thead>
            <tr>
                <th>Id Jurnal</th>
                <th>Id Jadwal Guru</th>
                <th>Id Guru</th>
                <th>Id Kelas</th>
                <th>Minggu Ke-</th>
                <th>Id Subjek</th>
                <th>Jam Hadir</th>
                <th>Status</th>
                <th>Kompetensi Dasar</th>
                <th>Catatan</th>
            </tr>
        </thead>
        <tbody>
        ';      
        foreach($jurnalguru as $data){
            echo '<td>'.$data['id_jurnal'].'</td>';
            echo '<td>'.$data['id_jadwal_guru'].'</td>';
            echo '<td>'.$data['id_guru'].'</td>';   
            echo '<td>'.$data['id_kelas'].'</td>'; 
            echo '<td>'.$data['minggu_ke'].'</td>';
            echo '<td>'.$data['id_subjek'].'</td>';
            echo '<td>'.$data['jam_hadir'].'</td>';
            echo '<td>'.$data['status'].'</td>';
            echo '<td>'.$data['kompetensi_dasar'].'</td>';
            echo '<td>'.$data['catatan'].'</td>';
            echo '</tr>'; 
        }
        echo '</tbody>
        </table>';
}
}
?>