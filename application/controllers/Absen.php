<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class absen extends CI_Controller {
	
	public function __construct(){
		
		parent::__construct();
		
		$this->load->helper('url');
		$this->load->model('JamPelajaran');
	}

	public function manual()
	{
		$this->load->view('page/ceklogManual');
	}

	public function index(){
		//echo 'sasa';
		$this->load->view('page/ceklog');
	}
	public function guru(){
		//echo 'sasa';
		$this->load->view('page/ceklogGuru');
	}
	public function today()
	{
		$hari = date ("D");
 
		switch($hari){
			case 'Sun':
				$hari_ini = "Minggu";
			break;
	 
			case 'Mon':			
				$hari_ini = "Senin";
			break;
	 
			case 'Tue':
				$hari_ini = "Selasa";
			break;
	 
			case 'Wed':
				$hari_ini = "Rabu";
			break;
	 
			case 'Thu':
				$hari_ini = "Kamis";
			break;
	 
			case 'Fri':
				$hari_ini = "Jumat";
			break;
	 
			case 'Sat':
				$hari_ini = "Sabtu";
			break;
			
			default:
				$hari_ini = "Tidak di ketahui";		
			break;
		}

		$date = date('d-m-Y H:i');
		return $hari_ini.', '.$date;
	}

	public function insertGuru()
	{
		date_default_timezone_set("Asia/Jakarta");
		if(!$this->session->userdata('logged_in'))
		{
			$data  = array(
	            'sess_sekolah'     =>  1
	        );
	        $this->session->set_userdata($data);
		}
		$guru = explode("<-->",$this->input->post('content'));
		$var = $this->absen_model->readWhereGuru($guru);
		if (count($guru) == 3 && count($var)>0) 
		{
			$id_sekolah = $var[0]['id_sekolah'];
			$id_pengguna = $var[0]['id_pengguna'];
			$date = date('Y-m-d');
			$time = date('H:i:s');
			$check = $this->absen_model->checkKehadiranGuru($id_sekolah, $id_pengguna, $date);
			if (count($check)>0) 
			{
				if($check[0]['status'] == 1)
				{
					$update = array(
						'status' => 2,
						'jam_pulang' => $time
					);
					$where = array('id_kehadiran_guru' => $check[0]['id_kehadiran_guru']);
					echo json_encode($this->absen_model->updateKehadiranGuru($update, $where));
				}
				else
					echo json_encode('Sukses !!');
			}
			else
			{
				$data = array(
					'id_sekolah' => $id_sekolah, 
					'id_pengguna' => $id_pengguna,
					'status' => 1,
					'tanggal' => $date,
					'jam_datang' => $time,
					'jam_pulang' => NULL
				);
				echo json_encode($this->absen_model->simpanKehadiranGuru($data));
			}
		}
		else
			die(header("HTTP/1.0 404 Not Found"));
	}

	public function insertManual()
	{
		date_default_timezone_set("Asia/Jakarta");
		if(!$this->session->userdata('logged_in') && !$this->session->userdata('sess_old_absen'))
		{
			$data  = array(
	            'sess_sekolah'     =>  1,
	            'sess_old_absen'     =>  1
	        );
	        $this->session->set_userdata($data);
		}
		$siswa = array(sanitizeNoSpace($this->input->post('nomor')), $this->session->userdata('sess_sekolah'), 0);
		$timestamp = date('Y-m-d H:i:s');
		$var  = $this->absen_model->readWhereSiswa($siswa, $this->session->userdata('sess_old_absen'))->result_array();
		if (count($siswa) == 3 && count($var)>0) 
		{

			if ($this->isExist($siswa)) 
			{
				$tgl = $this->today();
				$status = $this->getStatus();
				$data = array('kehadiran_timestamp' => $timestamp, 'kehadiran_siswa'=>$siswa[0], 'kehadiran_verification'=>$status, 'id_sekolah' => $var[0]['id_sekolah'], 'id_pengguna_pengabsen' => 0);
				echo json_encode($this->absen_model->simpanKehadiran($data));
			}
			else
				echo json_encode('Sukses !!');
		}
		else
			die(header("HTTP/1.0 404 Not Found"));
	}

	public function insert()
	{
		date_default_timezone_set("Asia/Jakarta");
		if(!$this->session->userdata('logged_in') && !$this->session->userdata('sess_old_absen'))
		{
			$data  = array(
	            'sess_sekolah'     =>  1,
	            'sess_old_absen'     =>  1
	        );
	        $this->session->set_userdata($data);
		}
		if($this->session->userdata('sess_old_absen'))
			$siswa = array($this->input->post('content'), $this->session->userdata('sess_sekolah'), 0);
		else
			$siswa = explode("<-->",$this->input->post('content'));
		$timestamp = date('Y-m-d H:i:s');
		if (count($siswa) == 3 && $this->isSiswa($siswa)) {
			if ($this->isExist($siswa)) {
				$var  = $this->absen_model->readWhereSiswa($siswa, $this->session->userdata('sess_old_absen'))->result_array();
				foreach ($var as $v) {
					$phone = $v['no_hp'];
					$nama = $v['nm_pengguna'];
				}
				$tgl = $this->today();
				$status = $this->getStatus();
				$data = array('kehadiran_timestamp' => $timestamp, 'kehadiran_siswa'=>$siswa[0], 'kehadiran_verification'=>$status, 'id_sekolah' => $var[0]['id_sekolah'], 'id_pengguna_pengabsen' => 0);
				echo json_encode($this->absen_model->simpanKehadiran($data));
			}
			else{
				echo json_encode('Sukses !!');
			}
		}
		else{
			die(header("HTTP/1.0 404 Not Found"));
		}
	}

	public function isSiswa($id)
	{
		$read = $this->absen_model->readWhereSiswa($id, $this->session->userdata('sess_old_absen'));
		if ($read->num_rows()==0) {
			return false;
		}
		else{
			return true;
		}
	}
	public function getStatus()
	{
		$readJamMasuk = $this->absen_model->getJamMasuk()->result_array();
		foreach ($readJamMasuk as $jm) {
			$jam = explode(':', $jm['jm_masuk']);
			$jamMasuk = $jam[0] . '.' . $jam[1];
			//$jamMasuk = 07.00+0.05;
		}
		date_default_timezone_set("Asia/Jakarta");
		/*$jam = date('H');
		$min = date('i')*0.01;
		$curr = $jam+$min;*/
		$curr = date('H.i');
		//$curr = '07.36';
		if ($curr>$jamMasuk) {
			return 3;
			
		}
		else{
			return 0;
			
		}
	}
	public function isExist($id)
	{
		$today = date('Y-m-d');
		$query = $this->absen_model->readWhere($today, $id);
		$read = $this->absen_model->readWhereSiswa($id, $this->session->userdata('sess_old_absen'));
		if ($read->num_rows()==0) {
			return false;
		}
		else{
			if ($query->num_rows()>0) {
				return false;
			}
			else{
				return true;				
			}
		}
	}

	public function kirimOrtu()
	{
		$getSiswa = $this->siswa->readSiswa($this->session->userdata('sess_sekolah'))->result_array();
		foreach ($getSiswa as $gs) {
			$token = $this->storeToken($gs);
			$hp = $gs['no_hp'];
			$nama = $gs['nm_pengguna'];
			$tgl = $this->today();
			$status = $this->getStatus();
			$amtTelat = $this->getJumlahTelat($gs['nomor_induk']);


			if ($token == FALSE) {
				$this->session->set_flashdata('msg', "<script>swal('Gagal!', 'Terjadi kesalahan', 'warning')</script>");
				echo '<script>window.history.back()</script>';
			}
			else{
				if ($this->sendRekap($hp, $nama, $token, $amtTelat)==0) {
					$this->session->set_flashdata('msg', "<script>swal('Sukses!', 'Rekap absensi telah terkirim ke orang tua',
					 'success')</script>");
					echo '<script>window.history.back()</script>';
				}
			}

		}
	}

	public function menuSetJam()
	{
		$data = $this->JamPelajaran->getJamMasuk()->result_array();
		$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/jadwal/jm', array('data' => $data));
		$this->load->view('layout/footer');
	}
	public function sendRekap($des, $nama, $link)
	{ 
		$destination = $des; 
		$my_apikey = "JJML5ZLWSU7U5GII87AV"; 
		$message = "[REKAP ABSENSI SISWA SMPIA AL AZHAR 13 SURABAYA] Diberitahukan kepada orang tua siswa SMPIA 13 Surabaya. Berikut rekap absensi ananda ".$nama." anda bisa klik link berikut : ".$link; 
		$api_url = "http://panel.apiwha.com/send_message.php"; 
		$api_url .= "?apikey=". urlencode ($my_apikey); 
		$api_url .= "&number=". urlencode ($destination); 
		$api_url .= "&text=". urlencode ($message); 
		$my_result_object = json_decode(file_get_contents($api_url, false)); 
		return $my_result_object->result_code;
	}
	
	public function sendRekapBaru($des, $nama, $link, $amtTelat)
	{ 
		$destination = $des; 
		$my_apikey = "JJML5ZLWSU7U5GII87AV"; 
		$message = "[REKAP ABSENSI SISWA SMPIA AL AZHAR 13 SURABAYA] Diberitahukan kepada orang tua siswa SMPIA 13 Surabaya. 
		Berikut rekap absensi ananda ".$nama." dengan jumlah keterlambatan = ".$amtTelat." anda bisa klik link berikut : ".$link; 
		$api_url = "http://panel.apiwha.com/send_message.php"; 
		$api_url .= "?apikey=". urlencode ($my_apikey); 
		$api_url .= "&number=". urlencode ($destination); 
		$api_url .= "&text=". urlencode ($message); 
		$my_result_object = json_decode(file_get_contents($api_url, false)); 
		return $my_result_object->result_code;
	}

	public function sendWhatsapp($des, $nama, $date, $status)
	{ 
		$destination = $des; 
		$my_apikey = "JJML5ZLWSU7U5GII87AV"; 
		$message = "[PEMBERITAHUAN ABSENSI SMPIA AL AZHAR 13 SURABAYA] Diberitahukan kepada orang tua dari peserta didik atas nama ".$nama." telah hadir di Sekolah pada ".$date."  dengan Status ".$status." Terima kasih"; 
		$api_url = "http://panel.apiwha.com/send_message.php"; 
		$api_url .= "?apikey=". urlencode ($my_apikey); 
		$api_url .= "&number=". urlencode ($destination); 
		$api_url .= "&text=". urlencode ($message); 
		$my_result_object = json_decode(file_get_contents($api_url, false)); 
		//echo "<br>Result: ". $my_result_object->success; 
		//echo "<br>Description: ". $my_result_object->description; 
		//echo "<br>Code: ". $my_result_object->result_code;
		return $my_result_object->result_code;
	}
	public function coba($token)
	{
		$current = date('d', time())+3;
		$read = $this->absen_model->readToken($token)->result_array();
		foreach ($read as $t) {
			$siswa = $t['token_siswa'];
			$exp = $t['token_exp'];
		}
		if ($current<$exp) {
			echo 'belum exp';
			echo $siswa;
		}
		else{
			echo 'exp';
		}
	}
	public function storeToken($siswa)
	{
		//echo random_string('alnum', 50);
//		echo md5('muhammad khotib').'/11001';
		$token = md5($siswa['nomor_induk'].$siswa['id_sekolah'].$siswa['id_pengguna_sekolah'].date('Y-m-d'));
		$exp = date('d', time())+7;
		$data = array('token_code' => $token, 'token_siswa' =>$siswa['nomor_induk'], 'token_exp' =>$exp, 'id_sekolah' => $siswa['id_sekolah']);
		if ($this->absen_model->insertToken($data)) {
			return base_url('/rekapAbsensi/index/').$token;
		}
		else{
			return false;
		}

	}

	public function sendEmailReport()
	{
		//$token = $this->absen_model->getToken();
		if(isset($token['token_code']))
		{
			$update = array('status' => 0);
			$where = array('token_id' => $token['token_id'], 'token_code' => $token['token_code']);
			if(!is_null($token['email']) && strpos($token['email'], '@'))
			{
				$tanggal = explode(' ', $token['waktu_report'])[0];
				$title = "REKAP ABSENSI SISWA SMPI AL AZHAR 13 SURABAYA (" .strtoupper(humanDateFormat($tanggal)).")"; 
				$link = base_url('/rekapAbsensi/index/').$token['token_code'];
				$from_email = 'portallomba.hmsi@gmail.com';
				$to_email = $token['email'];
				$to_email = 'lu.pikk99@gmail.com';
				$message = "[".$title."]".
					"<br>"."Diberitahukan kepada orang tua siswa SMP Islam Al-Azhar 13 Surabaya.	Berikut rekap absensi ananda ".$token['nm_pengguna'].". Klik link berikut untuk melihat rekap : <a href='".$link."'>".$link."</a>".
					"<br>"."Link tersebut berlaku sampai dengan hari ".humanDateFormat($token['tanggal_exp'], 1);
				$config = array(
		                'protocol' => 'smtp',
		                'smtp_host' => 'ssl://smtp.googlemail.com',
		                'smtp_port' => 465,
		                'smtp_user' => 'portallomba.hmsi@gmail.com',
		                'smtp_pass' => 'portallomba1',
		                'mailtype'  => 'html', 
		                'charset'   => 'iso-8859-1'
		        );

		        $this->load->library('email', $config); 
		        $this->email->set_newline("\r\n");

		        $this->email->from($from_email, 'SMPI AL AZHAR 13 Surabaya'); 
		        $this->email->to($to_email);
		        $this->email->subject($title); 
		        $this->email->message($message);
		        if (!$this->email->send())
			    	show_error($this->email->print_debugger());
			    else
			    {
			    	$update['status'] = 2;
			    	$update['waktu_kirim'] = date('Y-m-d H:i:s');
			    }
			}
			$this->absen_model->updateToken($update, $where);
		}
	}

	public function _do_view($page){
		$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/'.$page);
		$this->load->view('layout/footer');	
	}

	public function getJumlahTelat($id)
	{
		$amtTelat = $this->absen_model->getTelat($id)->num_rows();
		return $amtTelat;
	}

}