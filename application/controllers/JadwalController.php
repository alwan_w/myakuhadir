<?php
class JadwalController extends CI_Controller {

	public function __construct(){
		
		parent::__construct();
		if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') != 2) {
            $url = base_url() . 'login';
            redirect($url);
        }
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
        $this->load->model('Jadwal');
		$this->load->model('JadwalGuru');
		$this->load->model('JamPelajaran');
	}
	public function updateJamMasuk()
	{
		$id = $this->input->post('id');
		$data = array('status' => 1);
		$this->JamPelajaran->updOther($id);
		echo json_encode($this->JamPelajaran->updateJam($data, $id));
	}

	public function updateJam()
	{
		$id = $this->input->post('id');
		$jam = $this->input->post('jam');
		$data = array('jm_masuk' => $jam);
		echo json_encode($this->JamPelajaran->updateJam($data, $id));

	}	

    public function index()
    {
    	$data = $this->JadwalGuru->getList($this->session->userdata('sess_sekolah'), $this->session->userdata('sess_tahun_ajaran'));
        $this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/jadwal/listJadwal', array('data' => $data, 'tahunAjaran' => $this->session->userdata('sess_tahun_ajaran')));
		$this->load->view('layout/footer');
    }

    public function confirmUpload($nama_jadwal, $kelas, $guru)
    {
        $jumlahGuru = count($guru);
        if(count($kelas) > 0 && $jumlahGuru > 0)
        {
            $id_sekolah = $this->session->userdata('sess_sekolah');
            $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
            $temp = $guru;
            $guru = array();
            $mapel = array();
            //check guru
            for($i = 0; $i<$jumlahGuru; $i++)
            {
                if(!is_null($temp[$i]['nomor_induk']))
                {
                    $id_pengguna = $this->Jadwal->checkGuru($id_sekolah, $temp[$i]);
                    $guru[$temp[$i]['code']] = array(
                        'id_pengguna' => $id_pengguna,
                        'mata_pelajaran' => $temp[$i]['mata_pelajaran']
                    );
                    //$guru[$temp[$i]['code']] = $id_pengguna;
                    array_push($mapel, $temp[$i]['mata_pelajaran']);
                }
            }
            $listMapel = $this->Jadwal->getAllMapel($id_sekolah);
            $temp = $mapel;
            $mapel = array();
            //check mapel
            for($i = 0; $i<count($temp); $i++)
            {
                $found = 0;
                foreach($listMapel as $list)
                {
                    if($temp[$i] == $list['nm_mata_pelajaran'])
                    {
                        $mapel[$temp[$i]] = $list['id_mata_pelajaran'];
                        $found++;
                        break;
                    }
                }
                if($found < 1 && !is_null($temp[$i]))
                {
                    $id_mapel = $this->Jadwal->addMapel($id_sekolah, $temp[$i]);
                    $mapel[$temp[$i]] = $id_mapel;
                }
            }
            //buat jadwal sekolah baru
            $id_jadwal_sekolah = $this->Jadwal->addJadwalSekolah($id_sekolah, $tahunAjaran, $nama_jadwal);
            //insert jadwal guru
            $date = date('Y-m-d H:i:s');
            $temp = array();
            $check = 0;
            $insert = array(
                'id_sekolah' => $id_sekolah,
                'id_jadwal_sekolah' => $id_jadwal_sekolah,
                'tahun_ajaran' => $tahunAjaran,
                'created_at' => $date,
                'updated_at' => $date
            );
            foreach($kelas as $kls)
            {
                $id_kelas = $this->Jadwal->checkKelas($id_sekolah, $tahunAjaran, $kls['nm_kelas'], $kls['rombel_kelas']);
                $insert['id_kelas'] = $id_kelas;
                foreach($kls['jadwal'] as $jadwal)
                {
                    if(isset($temp['pelajaran']))
                    {
                        if($temp['pelajaran'] == $jadwal['pelajaran'] && $temp['hari'] == $jadwal['hari'])
                        {
                            $temp['lastPeriode'] = $jadwal['periode'];
                            $temp['jam_selesai'] = $jadwal['jam_selesai'];
                            $check = 1;
                        }
                        else
                        {
                            $pelajaran = explode('/', $temp['pelajaran']);
                            if(count($pelajaran)>1)
                            {
                                if(isset($guru[$pelajaran[0]]))
                                {
                                    $insert['id_guru'] = $guru[$pelajaran[0]]['id_pengguna'];
                                    $insert['id_mata_pelajaran'] = $mapel[$guru[$pelajaran[0]]['mata_pelajaran']];
                                    $insert['hari'] = $temp['hari'];
                                    $insert['jam_mulai'] = $temp['jam_mulai'];
                                    $insert['jam_selesai'] = $temp['jam_selesai'];
                                    $insert['periode'] = $temp['firstPeriode'];
                                    if($check > 0)
                                        $insert['periode'] = $temp['firstPeriode']. ' - ' .$temp['lastPeriode'];

                                    $this->Jadwal->addJadwalGuru($insert);
                                }
                            }
                            $check = 0;
                            $temp = array();
                        }
                    }
                    if(!isset($temp['pelajaran']))
                    {
                        $temp = array(
                            'hari' => $jadwal['hari'],
                            'firstPeriode' => $jadwal['periode'],
                            'lastPeriode' => $jadwal['periode'],
                            'jam_mulai' => $jadwal['jam_mulai'],
                            'jam_selesai' => $jadwal['jam_selesai'],
                            'pelajaran' => $jadwal['pelajaran']
                        );
                    }
                }
                if(isset($temp['pelajaran']))
                {
                    $pelajaran = explode('/', $temp['pelajaran']);
                    if(count($pelajaran)>1)
                    {
                        if(isset($guru[$pelajaran[0]]))
                        {
                            $insert['id_guru'] = $guru[$pelajaran[0]]['id_pengguna'];
                            $insert['id_mata_pelajaran'] = $mapel[$guru[$pelajaran[0]]['mata_pelajaran']];
                            $insert['hari'] = $temp['hari'];
                            $insert['jam_mulai'] = $temp['jam_mulai'];
                            $insert['jam_selesai'] = $temp['jam_selesai'];
                            $insert['periode'] = $temp['firstPeriode'];
                            if($check > 0)
                                $insert['periode'] = $temp['firstPeriode']. ' - ' .$temp['lastPeriode'];

                            $this->Jadwal->addJadwalGuru($insert);
                        }
                    }
                    $check = 0;
                    $temp = array();
                }
            }
            $this->Jadwal->setStausJadwalSekolah($id_sekolah, $tahunAjaran, $id_jadwal_sekolah);
        }
    }

    public function uploadJadwal()
    {
        if(isset($_FILES['userfile']['name']) && $_FILES['userfile']['size'] > 0 && FALSE)
        {
            $nama_jadwal = sanitizeNoSpace($this->input->post('nama'));
            $arr_file = explode('.', $_FILES['userfile']['name']);
            $extension = end($arr_file);
         
            if($extension == 'xls')
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            elseif($extension == 'xlsx')
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            else
                redirect(base_url().'dashboard/jadwal');

            //$reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($_FILES['userfile']['tmp_name']);
             
            $sheet = $spreadsheet->getSheet(0);
            /* array yang dibuat
            $kelas = array(
                $col => array(
                    'nm_kelas' => 7,
                    'rombel_kelas' => A,
                    'jadwal' => array(
                        0 => array(
                            'hari' => 1,
                            'periode' => 0,
                            'jam_mulai' => 06.55,
                            'jam_selesai' => 07.30,
                            'pelajaran' => 'TESTIMONY, DHUHA PRAYER'
                        )
                        1 => array(
                            'hari' => 1,
                            'periode' => 1,
                            'jam_mulai' => 07.30,
                            'jam_selesai' => 08.10,
                            'pelajaran' => '13/English'
                        )
                    )
                )
            );
            $guru = array(
                'CODE 1' => array(
                    'nama' => 'Syfa Al Huda,S.Ag',
                    'nomor_induk' => 'NIP2',
                    'mata_pelajaran' => 
                ),
                'CODE 2' => array(
                    'nama' => 'Agung Yuliana,S.Pd',
                    'nomor_induk' => '107012294',
                    'mata_pelajaran' => 'Civics'
                )
            )
            */
            
            //Ambil Kelas Dulu

            $kelas = array();
            $col = 'D';
            $row = 5;
            $merged = array();
            while($sheet->getCell($col.($row+1))->getValue() != null)
            {
                $temp = array(
                    'nm_kelas' => $sheet->getCell($col.$row)->getValue(),
                    'rombel_kelas' => $sheet->getCell($col.($row+1))->getValue(),
                    'jadwal' => array()
                );
                if($sheet->getCell($col.$row)->isInMergeRange())
                {
                    $range = $sheet->getCell($col.$row)->getMergeRange();
                    if(!isset($merged[$range]))
                        $merged[$range] = $sheet->getCell($col.$row)->getValue();
                    $temp['nm_kelas'] = $merged[$range];
                }
                $kelas[$col] = $temp;
                $col++;
                $lastCol = $col;
            }
            //Ambil jadwal
            $stop = 0;
            $hari = 1; //mulai dari senin
            $firstCol = 'D';
            $col = $firstCol;
            $row = 7;
            while($stop < 2)
            {
                if($sheet->getCell('C'.$row)->getValue() != null)
                {
                    if($stop > 0)
                    {
                        $stop = 0;
                        $hari++;
                    }
                    $val = sanitize($sheet->getCell('C'.$row)->getFormattedValue());
                    $val = str_replace('.', ':', $val);
                    $jam = explode('-', $val);
                    $mulai = $jam[0];
                    if(strlen($mulai) == 4)
                        $mulai = '0'.$mulai;

                    $selesai = $jam[0];
                    if(count($jam)>1)
                        $selesai = $jam[1];
                    for($col = $firstCol; $col != $lastCol; $col++)
                    {
                        $jadwal = array(
                            'hari' => $hari,
                            'periode' => $sheet->getCell('B'.$row)->getValue(),
                            'jam_mulai' => $mulai.':00',
                            'jam_selesai' => $selesai.':00',
                            'pelajaran' => sanitize($sheet->getCell($col.$row)->getValue())
                        );
                        if($sheet->getCell($col.$row)->isInMergeRange())
                        {
                            $range = $sheet->getCell($col.$row)->getMergeRange();
                            if(!isset($merged[$range]))
                                $merged[$range] = $sheet->getCell($col.$row)->getValue();
                            $jadwal['pelajaran'] = $merged[$range];
                        }
                        array_push($kelas[$col]['jadwal'], $jadwal);
                    }
                }
                else
                    $stop++;
                $row++;
            }
            //Ambil Guru yang ngajar
            //inisiasi Col yang bakal dipake
            $col = $lastCol;
            $col++;
            $codeCol = $col;
            $col++;
            $nameCol = $col;
            $col++;
            $nomorIndukCol = $col;
            $col++;
            $mapelCol = $col;
            $row = 7;
            $guru = array();
            if($sheet->getCell($codeCol.'6')->getValue() == 'CODE')
            {
                while((int)$sheet->getCell($codeCol.$row)->getValue() > 0)
                {
                    $code = (int)$sheet->getCell($codeCol.$row)->getValue();
                    $guru[count($guru)] = array(
                        'code' => $code,
                        'nama' => $sheet->getCell($nameCol.$row)->getValue(),
                        'nomor_induk' => $sheet->getCell($nomorIndukCol.$row)->getValue(),
                        'mata_pelajaran' => $sheet->getCell($mapelCol.$row)->getValue()
                    );
                    $row++;
                }
            }
            $this->confirmUpload($nama_jadwal, $kelas, $guru);
        }
        redirect(base_url().'dashboard/jadwal');
    }

    public function showJadwalSekolah()
    {
        $data = $this->Jadwal->getListJadwalSekolah($this->session->userdata('sess_sekolah'), $this->session->userdata('sess_tahun_ajaran'));
        $this->load->view('layout/header');
        $this->load->view('layout/navigationbar');
        $this->load->view('page/jadwal/listJadwalSekolah', array('data' => $data, 'tahunAjaran' => $this->session->userdata('sess_tahun_ajaran')));
        $this->load->view('layout/footer');
    }

    public function setJadwalSekolahAktif($id_jadwal_sekolah)
    {
        $check = $this->Jadwal->getJadwalSekolah($this->session->userdata('sess_sekolah'), $this->session->userdata('sess_tahun_ajaran'), $id_jadwal_sekolah);
        if(count($check)>0)
            $this->Jadwal->setStausJadwalSekolah($this->session->userdata('sess_sekolah'), $this->session->userdata('sess_tahun_ajaran'), $id_jadwal_sekolah);
        redirect(base_url().'dashboard/jadwal-sekolah');
    }

	public function jadwalMasuk()
	{
		$data = $this->JamPelajaran->getJamMasuk($this->session->userdata('sess_sekolah'))->result_array();
  		$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/jadwal/jadwalMasuk', array('data' => $data));
		$this->load->view('layout/footer');
	}

    public function create () 
    {
    	$guru 	= $this->JadwalGuru->getGuru($this->session->userdata('sess_sekolah'));
    	$mapel	= $this->JadwalGuru->getMapel($this->session->userdata('sess_sekolah'));
    	$kelas	= $this->JadwalGuru->getKelas($this->session->userdata('sess_sekolah'));
    	$jam 	= $this->JadwalGuru->getJam($this->session->userdata('sess_sekolah'));
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/jadwal/createJadwal', array('guru' => $guru, 'mapel' => $mapel, 'kelas' => $kelas, 'jam' => $jam));
		$this->load->view('layout/footer');
    }

    public function createJadwalMasuk () 
    {
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/jadwal/createJadwalMasuk');
		$this->load->view('layout/footer');
    }

    public function storeJadwalMasuk () 
    {
    	$this->form_validation->set_rules('jm_judul', 'Judul Jadwal Masuk', 'required');
    	$this->form_validation->set_rules('jm_masuk', 'Jam Masuk', 'required');
	  	if ($this->form_validation->run() == FALSE)
        {
            redirect(base_url().'dashboard/setting/jadwal-masuk/tambah');
        }
        else
        {
        	$data = array(
        		'jm_masuk' 			=> $this->input->post('jm_masuk'),
        		'jm_judul'			=> $this->input->post('jm_judul'),
        		'id_sekolah'		=> $this->session->userdata('sess_sekolah')
        	);
            $this->JamPelajaran->insertJadwalMasuk($data);
            redirect(base_url().'dashboard/setting/jadwal-masuk');
        }
    }

    public function show ($id) {
    	$guru 	= $this->JadwalGuru->getGuru($this->session->userdata('sess_sekolah'));
    	$mapel	= $this->JadwalGuru->getMapel($this->session->userdata('sess_sekolah'));
    	$kelas	= $this->JadwalGuru->getKelas($this->session->userdata('sess_sekolah'));
    	$jam 	= $this->JadwalGuru->getJam($this->session->userdata('sess_sekolah'));
    	$data = $this->JadwalGuru->getJadwalById($id);
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/jadwal/showJadwal', array('data' => $data, 'guru' => $guru, 'mapel' => $mapel, 'kelas' => $kelas, 'jam' => $jam));
		$this->load->view('layout/footer');
    }

    public function store () {
    	$this->form_validation->set_rules('hari', 'Hari', 'required');
	  	if ($this->form_validation->run() == FALSE)
        {
            redirect(base_url().'dashboard/civitas/tambah');
        }
        else
        {
        	$jam_mulai 		= $this->JamPelajaran->getWaktuById($this->input->post('jam_mulai'));
        	$jam_selesai	= $this->JamPelajaran->getWaktuById($this->input->post('jam_selesai'));
        	
        	$data = array(
        		'id_guru' 			=> $this->input->post('id_guru'),
        		'id_kelas'			=> $this->input->post('id_kelas'),
        		'hari'				=> $this->input->post('hari'),
        		'jam_mulai' 		=> $jam_mulai[0]->jam_mulai,
        		'jam_selesai'		=> $jam_selesai[0]->jam_selesai,
        		'id_mata_pelajaran'	=> $this->input->post('id_mata_pelajaran'),
        		'id_sekolah'		=> $this->session->userdata('sess_sekolah')
        	);
            $this->JadwalGuru->insert($data);
            redirect(base_url().'dashboard/jadwal');
        }	
    }

    public function update ($id) {
    	$this->form_validation->set_rules('hari', 'Hari', 'required');
	  	if ($this->form_validation->run() == FALSE)
        {
            redirect(base_url().'dashboard/jadwal/'.$id);
        }
        else
        {
        	$jam_mulai 		= $this->JamPelajaran->getWaktuById($this->input->post('jam_mulai'));
        	$jam_selesai	= $this->JamPelajaran->getWaktuById($this->input->post('jam_selesai'));
        	
        	$data = array(
        		'id_guru' 			=> $this->input->post('id_guru'),
        		'id_kelas'			=> $this->input->post('id_kelas'),
        		'hari'				=> $this->input->post('hari'),
        		'jam_mulai' 		=> $jam_mulai[0]->jam_mulai,
        		'jam_selesai'		=> $jam_selesai[0]->jam_selesai,
        		'id_mata_pelajaran'	=> $this->input->post('id_mata_pelajaran')
        	);
            $this->JadwalGuru->update($id, $data);
            redirect(base_url().'dashboard/jadwal');
        }	
    }

    public function destroy ($id) 
    {
    	$this->JadwalGuru->delete($id);
    	redirect(base_url().'dashboard/jadwal');
    }

    public function showJadwalPiket()
    {
        $data = $this->Jadwal->getListJadwalPiket($this->session->userdata('sess_sekolah'), $this->session->userdata('sess_tahun_ajaran'));
        $this->load->view('layout/header');
        $this->load->view('layout/navigationbar');
        $this->load->view('page/jadwal/listJadwalPiket', array('data' => $data, 'tahunAjaran' => $this->session->userdata('sess_tahun_ajaran')));
        $this->load->view('layout/footer');
    }

    public function uploadJadwalPiket()
    {
        if(isset($_FILES['userfile']['name']) && $_FILES['userfile']['size'] > 0 && FALSE)
        {
            $nama_jadwal = sanitizeNoSpace($this->input->post('nama'));
            $arr_file = explode('.', $_FILES['userfile']['name']);
            $extension = end($arr_file);
         
            if($extension == 'xls')
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            elseif($extension == 'xlsx')
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            else
                redirect(base_url().'dashboard/jadwal-piket');

            //$reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($_FILES['userfile']['tmp_name']);
             
            $sheet = $spreadsheet->getSheet(0);

            $i = 0;
            $row = 2;
            // Nama, Nomor Induk, Hari
            $guru = array();
            
            while($sheet->getCell('C'.$row)->getValue() != null)
            {
                $guru[$i]['nm_pengguna'] = $sheet->getCell('B'.$row)->getValue();
                $guru[$i]['nomor_induk'] = $sheet->getCell('C'.$row)->getValue();
                $guru[$i]['hari'] = strtolower(sanitize($sheet->getCell('D'.$row)->getValue()));

                $row++;
                $i++;
            }

            $id_sekolah = $this->session->userdata('sess_sekolah');
            $tahunAjaran = $this->session->userdata('sess_tahun_ajaran');
            $date = date('Y-m-d H:i:s');
            //check
            $listGuru = array();
            for($i = 0; $i<count($guru); $i++)
            {
                if(!is_null($guru[$i]['nomor_induk']))
                {
                    if(!isset($listGuru[$guru[$i]['nomor_induk']]))
                    {
                        $check = array(
                            'nm_pengguna' => $guru[$i]['nm_pengguna'],
                            'nomor_induk' => $guru[$i]['nomor_induk']
                        );
                        $id_guru = $this->Jadwal->checkGuru($id_sekolah, $check);
                        $listGuru[$guru[$i]['nomor_induk']] = $id_guru;
                    }
                }
                else
                    unset($guru[$i]);
            }
            //check guru yang lolos seleksi nomor induk
            if(count($guru)>0)
            {
                //hapus semua jadwal piket saat ini
                $data = array(
                    'id_sekolah' => $id_sekolah,
                    'tahun_ajaran' => $tahunAjaran
                );
                $this->Jadwal->deleteJadwalPiket($data);
                $data['created_at'] = $date;
                $data['updated_at'] = $date;
                foreach($guru as $user)
                {
                    if($user['hari'] == 'senin' || $user['hari'] == 'monday')
                        $hari = 1;
                    elseif($user['hari'] == 'selasa' || $user['hari'] == 'tuesday')
                        $hari = 2;
                    elseif($user['hari'] == 'rabu' || $user['hari'] == 'wednesday')
                        $hari = 3;
                    elseif($user['hari'] == 'kamis' || $user['hari'] == 'thursday')
                        $hari = 4;
                    elseif($user['hari'] == 'jumat' || $user['hari'] == 'friday')
                        $hari = 5;
                    elseif($user['hari'] == 'sabtu' || $user['hari'] == 'saturday')
                        $hari = 6;
                    else
                        $hari = 7;
                    $data['hari'] = $hari;
                    $data['id_guru'] = $listGuru[$user['nomor_induk']];
                    $this->Jadwal->addJadwalPiket($data);
                }
            }
        }
        redirect(base_url().'dashboard/jadwal-piket');
    }
}