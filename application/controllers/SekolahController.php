<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SekolahController extends CI_Controller {

	public function __construct(){
		
		parent::__construct();
		if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') != 2) {
            $url = base_url() . 'login';
            redirect($url);
        }
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('Sekolah');

	}

	public function index()
	{
		$id_sekolah = $this->session->userdata('sess_sekolah');
		$sekolah = $this->Sekolah->getSekolah($id_sekolah);
		if(count($sekolah)>0)
		{
			$data = array(
				'sekolah' => $sekolah[0]
			);
			$this->load->view('layout/header');
			$this->load->view('layout/navigationbar');
			$this->load->view('page/sekolah/showSekolah', $data);
			$this->load->view('layout/footer');
		}
	}

	public function editSekolah()
	{
		$id_sekolah = $this->session->userdata('sess_sekolah');
		$sekolah = $this->Sekolah->getSekolah($id_sekolah);
		if(count($sekolah)>0)
		{
			$data = array(
				'sekolah' => $sekolah[0]
			);
			$this->load->view('layout/header');
			$this->load->view('layout/navigationbar');
			$this->load->view('page/sekolah/editSekolah', $data);
			$this->load->view('layout/footer');
		}
	}

	public function updateSekolah()
	{
		$id_sekolah = $this->session->userdata('sess_sekolah');
		$id = sanitize($this->input->post('id'));
		$sekolah = $this->Sekolah->getSekolah($id_sekolah);
		$this->form_validation->set_rules('nama', 'Nama dari Sekolah', 'required');
        if ($this->form_validation->run() == FALSE || $id_sekolah != $id || count($sekolah)<1)
            redirect(base_url().'dashboard/sekolah/edit');
        else
        {
        	$date = date('Y-m-d H:i:s');
        	$update = array(
        		'nama_sekolah' => sanitizeNoSpace($this->input->post('nama')),
        		'alamat_sekolah' => sanitizeNoSpace($this->input->post('alamat')),
        		'email_sekolah' => sanitize($this->input->post('email')),
        		'telepon_sekolah' => sanitizeNoSpace($this->input->post('telepon')),
        		'website_sekolah' => sanitizeNoSpace($this->input->post('website')),
        		'updated_at' => $date
        	);
        	$where = array('id_sekolah' => $id_sekolah);
        	$updateLogo = FALSE;
        	//upload
        	if(isset($_FILES['logo']['name']) && !empty($_FILES['logo']['name']))
        	{
	            $nama_logo = hash('sha256',$this->input->post('nama') . $date);
	            $config['upload_path'] = FCPATH.'assets/logos/';
	            if (!is_dir($config['upload_path'])) 
	            {
	                 mkdir($config['upload_path'], 0777, TRUE);
	            }
	            $config['max_filename'] = 0;
	            $config['allowed_types'] = '*';
	            $config['max_size']  = '2048';    
	            $config['file_name'] = $nama_logo;
	            $this->load->library('upload', $config);
	            if($this->upload->do_upload('logo', 'false'))
	            {
	                $update['logo_sekolah'] = $nama_logo;
	                $file_to_unlink = $config['upload_path'].$sekolah[0]['logo_sekolah'];
	                $updateLogo = TRUE;
	            }
	            else
	            {
	            	$flash = array(
	                    'pesan' => $this->upload->display_errors()
	                );
	                $this->session->set_flashdata($flash);
		            redirect(base_url().'dashboard/sekolah/edit');
	            }
        	}
        	//update
        	$this->Sekolah->updateSekolah($update, $where);
        	if($updateLogo)
        	{
                $this->session->set_userdata('sess_logo_sekolah', $nama_logo);
                if(file_exists($file_to_unlink))
            	    unlink($file_to_unlink);
        	}
        	redirect(base_url().'dashboard/sekolah');
        }
	}

}

/* End of file SekolahController.php */
/* Location: ./application/controllers/SekolahController.php */