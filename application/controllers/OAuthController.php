<?php defined('BASEPATH') OR exit('No direct script access allowed');
class OAuthController extends CI_Controller
{
    function __construct(){
        parent::__construct();

        //load google login library
        $this->load->library('google');
        
        //load user model
        $this->load->model('OAuth_model', 'm_oauth');
    }
    
    public function google(){
        //redirect to profile page if user already logged in
        if($this->session->userdata('sess_role'))
            redirect('dashboard');

        if(isset($_GET['code']))
        {
            //authenticate user
            if($this->google->getAuthenticate())
		    {
            	//get user info from google
            	$gpInfo = $this->google->getUserInfo();
            
            	//preparing data for database insertion
            	$userData['oauth_provider'] = 'google';
            	$userData['oauth_uid']      = $gpInfo['id'];
            	$userData['first_name']     = $gpInfo['given_name'];
            	$userData['last_name']      = $gpInfo['family_name'];
            	$userData['email']          = $gpInfo['email'];
            	$userData['gender']         = !empty($gpInfo['gender'])?$gpInfo['gender']:'';
            	$userData['locale']         = !empty($gpInfo['locale'])?$gpInfo['locale']:'';
            	$userData['profile_url']    = !empty($gpInfo['link'])?$gpInfo['link']:'';
            	$userData['picture_url']    = !empty($gpInfo['picture'])?$gpInfo['picture']:'';
            
            	//insert or update user data to the database
            	$userID = $this->m_oauth->checkGoogleUser($userData);
            	$user = $this->m_oauth->getUserWhere(array('email' => $userData['email']));
            	//store status & user info in session
            	$data  = array(
                    'user'       =>  $user
                );
                $this->session->set_userdata($data);
            	redirect('signin-user');
		    }
        }
		redirect('login');
    }
}