<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PenggunaController extends CI_Controller {

	public function __construct(){
		
		parent::__construct();
		if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') < 1) {
            $url = base_url() . 'login';
            redirect($url);
        }
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('Pengguna');
		$this->load->model('Jurnal');


	}

	public function index()
	{
		$id_sekolah = $this->session->userdata('sess_sekolah');
		$id_pengguna = $this->session->userdata('sess_id');
		$id_role = $this->session->userdata('sess_role');
		$check = $this->Pengguna->getPenggunaSekolah($id_pengguna, $id_sekolah, $id_role);
		$pengguna = $check->result_array();
		if(count($pengguna)==1)
		{
			$data = array(
				'pengguna' => $pengguna[0]
			);
			if($id_role == 2)
			{
				$this->load->view('layout/header');
				$this->load->view('layout/navigationbar');
				$this->load->view('page/pengguna/showProfile', $data);
				$this->load->view('layout/footer');
			}
			elseif($id_role == 3 || $id_role == 4)
			{
				$user   = $check->result();
        		$walipiket = $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, getTahunAjaran(), TRUE);

				$this->load->view('page/jurnal/histori/header');
	            $this->load->view('page/jurnal/navbar', array('user' => $user, 'kelas' => $walipiket));
	            $this->load->view('page/pengguna/showProfile', $data);
	            $this->load->view('page/jurnal/footer');
			}
		}
	}

	public function editProfile()
	{
		$id_sekolah = $this->session->userdata('sess_sekolah');
		$id_pengguna = $this->session->userdata('sess_id');
		$id_role = $this->session->userdata('sess_role');
		$check = $this->Pengguna->getPenggunaSekolah($id_pengguna, $id_sekolah, $id_role);
		$pengguna = $check->result_array();
		if(count($pengguna)==1)
		{
			$data = array(
				'pengguna' => $pengguna[0]
			);
			if($id_role == 2)
			{
				$this->load->view('layout/header');
				$this->load->view('layout/navigationbar');
				$this->load->view('page/pengguna/editProfile', $data);
				$this->load->view('layout/footer');
			}
			elseif($id_role == 3 || $id_role == 4)
			{
				$user   = $check->result();
        		$walipiket = $this->Jurnal->cekWaliKelas($user[0]->id_pengguna, $id_sekolah, getTahunAjaran(), TRUE);

				$this->load->view('page/jurnal/header');
	            $this->load->view('page/jurnal/navbar', array('user' => $user, 'kelas' => $walipiket));
	            $this->load->view('page/pengguna/editProfile', $data);
	            $this->load->view('page/jurnal/footer');
			}
		}
	}

	public function updateProfile()
	{
		$id_sekolah = $this->session->userdata('sess_sekolah');
		$id_pengguna = $this->session->userdata('sess_id');
		$id_role = $this->session->userdata('sess_role');
		$pengguna = $this->Pengguna->getPenggunaSekolah($id_pengguna, $id_sekolah, $id_role)->result_array();
		$id = sanitize($this->input->post('id'));
		$this->form_validation->set_rules('nama', 'Nama dari Pengguna', 'required');
		$this->form_validation->set_rules('email', 'Email dari Pengguna', 'required');
        if ($this->form_validation->run() == FALSE || $id_pengguna != $id || count($pengguna)<1)
            redirect(base_url().'dashboard/profile/edit');
        else
        {
        	$date = date('Y-m-d H:i:s');
        	$update = array(
        		'nm_pengguna' => sanitizeNoSpace($this->input->post('nama')),
        		'alamat' => sanitizeNoSpace($this->input->post('alamat')),
        		'email' => sanitize($this->input->post('email')),
        		'no_hp' => sanitizeNoSpace($this->input->post('telepon')),
        		'tema_pengguna' => sanitize($this->input->post('tema')),
        		'updated_at' => $date
        	);
        	$where = array('id_pengguna' => $id_pengguna);
        	$updateFoto = FALSE;
        	//upload
        	if(isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name']))
        	{
	            $nama_foto = hash('sha256',$this->input->post('email') . $date);
	            $config['upload_path'] = FCPATH.'assets/photos/';
	            if (!is_dir($config['upload_path'])) 
	            {
	                 mkdir($config['upload_path'], 0777, TRUE);
	            }
	            $config['max_filename'] = 0;
	            $config['allowed_types'] = '*';
	            $config['max_size']  = '2048';    
	            $config['file_name'] = $nama_foto;
	            $this->load->library('upload', $config);
	            if($this->upload->do_upload('foto', 'false'))
	            {
	                $update['foto_pengguna'] = $nama_foto;
	                $file_to_unlink = $config['upload_path'].$pengguna[0]['foto_pengguna'];
	                $updateFoto = TRUE;
	            }
	            else
	            {
	            	$flash = array(
	                    'pesan' => $this->upload->display_errors()
	                );
	                $this->session->set_flashdata($flash);
		            redirect(base_url().'dashboard/profile/edit');
	            }
        	}
        	//update
        	$this->Pengguna->updatePengguna($update, $where);
        	$this->session->set_userdata('sess_tema', $update['tema_pengguna']);
        	if($updateFoto)
        	{
                $this->session->set_userdata('sess_foto', $nama_foto);
                if(file_exists($file_to_unlink))
            	    unlink($file_to_unlink);
        	}
        	redirect(base_url().'dashboard/profile');
        }
	}

}

/* End of file PenggunaController.php */
/* Location: ./application/controllers/PenggunaController.php */