<?php
class PelajaranController extends CI_Controller {

	public function __construct(){
		
		parent::__construct();
		if ((int)$this->session->userdata('sess_id') < 1 || (int)$this->session->userdata('sess_role') != 2) {
            $url = base_url() . 'login';
            redirect($url);
        }
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->model('Pelajaran');
	}

    public function index()
    {
    	$data = $this->Pelajaran->getList($this->session->userdata('sess_sekolah'));
        $this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/pelajaran/listPelajaran', array('data' => $data));
		$this->load->view('layout/footer');
    }

    public function create () {
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/pelajaran/createPelajaran');
		$this->load->view('layout/footer');
    }

    public function show ($id) {
    	$data = $this->Pelajaran->getPelajaranById($id);
    	$this->load->view('layout/header');
		$this->load->view('layout/navigationbar');
		$this->load->view('page/pelajaran/showPelajaran', array('data' => $data));
		$this->load->view('layout/footer');
    }

    public function store () {
    	$this->form_validation->set_rules('nm_mata_pelajaran', 'Mata Pelajaran', 'required');
    	$this->form_validation->set_rules('jenis_mata_pelajaran', 'Mata Pelajaran', 'required');
    	  if ($this->form_validation->run() == FALSE)
	        {
	            redirect(base_url().'dashboard/pelajaran/tambah');
	        }
	        else
	        {	
	        	$data = array(
	        		'nm_mata_pelajaran' 	=> $this->input->post('nm_mata_pelajaran'),
	        		'jenis_mata_pelajaran'	=> $this->input->post('jenis_mata_pelajaran'),
	        		'id_sekolah'			=> $this->session->userdata('sess_sekolah')
	        	);
	            $this->Pelajaran->insert($data);
	            redirect(base_url().'dashboard/pelajaran');
	        }
    }

    public function update ($id) {
    	$this->form_validation->set_rules('nm_mata_pelajaran', 'Mata Pelajaran', 'required');
    	$this->form_validation->set_rules('jenis_mata_pelajaran', 'Mata Pelajaran', 'required');
    	$pelajaran = array(
	        		'nm_mata_pelajaran' 	=> $this->input->post('nm_mata_pelajaran'),
	        		'jenis_mata_pelajaran'	=> $this->input->post('jenis_mata_pelajaran'),
	        		'id_sekolah'			=> $this->session->userdata('sess_sekolah')
	        	);
    	$data = $this->Pelajaran->update($id, $pelajaran);
    	redirect(base_url().'dashboard/pelajaran');	
    }

    public function destroy ($id) {
    	$this->Pelajaran->delete($id);
    	redirect(base_url().'dashboard/pelajaran');
    }
}