<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_kehadiran extends CI_Migration {

	public function up() {
		$sql = "CREATE TABLE IF NOT EXISTS `kehadiran` (
			`kehadiran_id` INT NOT NULL auto_increment PRIMARY KEY,
			`kehadiran_siswa` varchar(128) NOT NULL,
			`kehadiran_timestamp` varchar(255),
			`kehadiran_verification` tinyint(1)
		);";
		$this->db->query($sql);
	}

	public function down(){
		$this->dbforge->drop_table('kehadiran');
	}

}

