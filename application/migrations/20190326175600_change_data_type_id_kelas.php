<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_change_data_type_id_kelas extends CI_Migration {

	public function up(){
		$sql_kelas 			= "ALTER TABLE `kelas` MODIFY COLUMN `id_kelas` INT auto_increment";
		$sql_jam			= "ALTER TABLE `jam_pelajaran` MODIFY COLUMN `id_jam_pelajaran` INT auto_increment";
		$sql_kelas_siswa	= "ALTER TABLE `kelas_siswa` MODIFY COLUMN `id_kelas_siswa` INT auto_increment";
		$sql_jadwal_guru	= "ALTER TABLE `jadwal_guru` MODIFY COLUMN `id_jadwal_guru` INT auto_increment";

		$this->db->query($sql_kelas);
		$this->db->query($sql_jam);
		$this->db->query($sql_kelas_siswa);
		$this->db->query($sql_jadwal_guru);

	}
}

