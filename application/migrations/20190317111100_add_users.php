<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_users extends CI_Migration {

	public function up(){

		$this->dbforge->add_field('id');

		$fields = array(
			'name'  => array(
				'type' => 'varchar',
				'constraint' => '255'
			),
			'google_account' => array(
				'type' => 'varchar',
				'constraint' => '255',
				'unique' => true
			)

		);

		$this->dbforge->add_field($fields);

		
		$this->dbforge->create_table('users');
    }

    public function down(){
        $this->dbforge->drop_table('users');
    }
}

