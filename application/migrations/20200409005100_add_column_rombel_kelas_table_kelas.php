
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_rombel_kelas_table_kelas extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `kelas` 
		 ADD `rombel_kelas` VARCHAR(255) NULL AFTER `nm_kelas`;";
		$this->db->query($sql_up);

		$sql_up		 	= "ALTER TABLE `kelas` CHANGE `nm_ketua_kelas` `id_ketua_kelas` INT NULL DEFAULT NULL;";

		$this->db->query($sql_up);
	}
}