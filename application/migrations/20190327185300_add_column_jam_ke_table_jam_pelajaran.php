<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_jam_ke_table_jam_pelajaran extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE jam_pelajaran ADD COLUMN jam_ke int AFTER id_jam_pelajaran;";

		$this->db->query($sql_up);
	}
}

