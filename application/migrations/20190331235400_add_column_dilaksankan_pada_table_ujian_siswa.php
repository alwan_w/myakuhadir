<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_dilaksankan_pada_table_ujian_siswa extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE ujian_siswa ADD COLUMN dilaksanakan_pada int AFTER deskripsi_ujian;";

		$this->db->query($sql_up);
	}
}

