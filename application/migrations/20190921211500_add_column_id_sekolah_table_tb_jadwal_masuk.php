<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_id_sekolah_table_tb_jadwal_masuk extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `tb_jadwal_masuk` 
		ADD `status` int NOT NULL AFTER `jm_judul`;";

		$sql_check = "SHOW COLUMNS FROM `tb_jadwal_masuk` LIKE 'status'";
		$check = $this->db->query($sql_check)->result();
		if(count($check) < 1)
			$this->db->query($sql_up);

		$sql_up		 	= "ALTER TABLE `tb_jadwal_masuk` 
		ADD `id_sekolah` int NOT NULL AFTER `id_jm`;";

		$this->db->query($sql_up);

		$sql_up			= "UPDATE `tb_jadwal_masuk` SET jm_masuk = CONCAT(REPLACE(jm_masuk,'.',''), '00');";
		$this->db->query($sql_up);

		$sql_up			= "UPDATE `tb_jadwal_masuk` SET `id_sekolah` = 1;";
		$this->db->query($sql_up);

		$sql_up			= "ALTER TABLE `tb_jadwal_masuk` 
		CHANGE `jm_masuk` `jm_masuk` TIME NOT NULL;";
		$this->db->query($sql_up);
	}
}