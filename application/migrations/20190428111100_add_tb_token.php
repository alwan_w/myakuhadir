<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_tb_token extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `tb_token` (
			`toke_id` INT NOT NULL auto_increment PRIMARY KEY,
			`token_code` varchar(100) NOT NULL,
			`token_siswa` varchar(100),
			`token_exp` varchar(100)
		);";
	$this->db->query($sql);
}

public function down(){
	$this->dbforge->drop_table('tb_token');
}

}

