<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_change_column_hari_to_jadwal_guru extends CI_Migration {

	public function up(){
		$sql_drop 		= "ALTER TABLE jam_pelajaran DROP hari;";
		$sql_up		 	= "ALTER TABLE jadwal_guru ADD COLUMN hari varchar(100) AFTER id_kelas;";

		$this->db->query($sql_up);
		$this->db->query($sql_drop);

	}
}

