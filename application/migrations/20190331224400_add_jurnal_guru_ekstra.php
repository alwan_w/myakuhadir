<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_jurnal_guru_ekstra extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `jurnal_guru_ekstra` (
			`id_jurnal` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`id_guru` int NOT NULL,
			`id_jadwal_guru` int NOT NULL,
			`id_subjek` int NOT NULL,
			`jam_hadir` datetime NOT NULL,
			`status` int NOT NULL,
			`kompetensi_dasar` text,
			`catatan` text,
			`dokumentasi` varchar(255),
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
		$this->db->query($sql);
	}

	public function down(){
		$this->dbforge->drop_table('jurnal_guru_ekstra');
	}

}

