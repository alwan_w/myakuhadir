<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_old_absen_table_sekolah extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `sekolah` 
		 ADD `old_absen` boolean AFTER `logo_sekolah`;";

		$this->db->query($sql_up);

		$sql_up		 	= "UPDATE `sekolah` SET `old_absen` = 1 WHERE `id_sekolah` = 1;";

		$this->db->query($sql_up);
	}
}