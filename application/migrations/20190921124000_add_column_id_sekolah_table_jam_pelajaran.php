<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_id_sekolah_table_jam_pelajaran extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `jam_pelajaran` 
		ADD `id_sekolah` int NOT NULL AFTER `id_jam_pelajaran`;";

		$this->db->query($sql_up);

		$sql_up		 	= "UPDATE `jam_pelajaran` SET `id_sekolah` = 1;";

		$this->db->query($sql_up);
	}
}

