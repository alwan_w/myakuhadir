<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_change_data_type_id_jurnal_table_ujian_tugas_siswa extends CI_Migration {

	public function up(){
		$sql_kelas 		= "ALTER TABLE `ujian_siswa` MODIFY COLUMN `id_jurnal` varchar(100)";
		$sql 			= "ALTER TABLE `tugas_siswa` MODIFY COLUMN `id_jurnal` varchar(100)";

		$this->db->query($sql_kelas);
		$this->db->query($sql);
	}
}
