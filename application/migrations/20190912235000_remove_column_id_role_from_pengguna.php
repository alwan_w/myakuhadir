<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_remove_column_id_role_from_pengguna extends CI_Migration {

	public function up(){
		$sql = "ALTER TABLE pengguna DROP id_role;";
		$this->db->query($sql);

	}
}
