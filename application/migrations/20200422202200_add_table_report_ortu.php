<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_table_report_ortu extends CI_Migration {
	public function up()
	{
		$sql = "CREATE TABLE IF NOT EXISTS `report_ortu` ( 
			`id_report_ortu` INT NOT NULL PRIMARY KEY AUTO_INCREMENT ,  
			`id_sekolah` INT NOT NULL ,  
			`tahun_ajaran` VARCHAR(255) NULL ,  
			`nama_report` VARCHAR(255) NULL ,  
			`waktu_report` DATETIME NOT NULL
		);";
		$this->db->query($sql);

		$sql = "ALTER TABLE `tb_token` CHANGE `token_siswa` `nomor_induk` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `token_exp` `tanggal_exp` DATE NULL DEFAULT NULL, CHANGE `toke_id` `token_id` INT(11) NOT NULL AUTO_INCREMENT;";
		$this->db->query($sql);

		$sql = "ALTER TABLE `tb_token` ADD `id_report_ortu` INT NULL AFTER `id_sekolah`, ADD `status` INT NOT NULL COMMENT '0 = Gagal dikirim, 1 = Menunggu Dikirim, 2 = Sudah Dikirim' AFTER `id_report_ortu`, ADD `waktu_kirim` DATETIME NULL AFTER `status`;";
		$this->db->query($sql);

		$sql = "ALTER TABLE `pengguna_sekolah`  ADD `status` INT NOT NULL  AFTER `nomor_induk`;";
		$this->db->query($sql);

		$sql = "UPDATE pengguna_sekolah set status = 1 WHERE id_sekolah = 1;";
		$this->db->query($sql);
    }

    public function down()
    {
        $this->dbforge->drop_table('report_ortu');
    }
}