<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_jam_keluar_table_jurnal_guru_ekstra extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE jurnal_guru_ekstra ADD COLUMN jam_keluar time AFTER jam_hadir
				;";

		$this->db->query($sql_up);
	}
}

