<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_id_sekolah_table_kelas extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `kelas` 
		ADD `id_sekolah` int NOT NULL AFTER `id_kelas`;";

		$this->db->query($sql_up);
		$sql_up		 	= "UPDATE `kelas` SET `id_sekolah` = 1;";
		$this->db->query($sql_up);
	}
}

