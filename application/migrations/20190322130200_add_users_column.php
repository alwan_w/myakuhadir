<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_users_column extends CI_Migration {

	public function up(){

		$fields = array(
			'username'  => array(
				'type' => 'varchar',
				'constraint' => '255',
				'unique' => true
			),
			'password' => array(
				'type' => 'varchar',
				'constraint' => '255'
			)

		);

		$this->dbforge->add_column('users',$fields);

		
    }

    public function down(){
        $this->dbforge->drop_table('users');
    }
}

