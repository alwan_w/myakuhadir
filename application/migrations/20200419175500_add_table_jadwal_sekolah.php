<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_table_jadwal_sekolah extends CI_Migration {
	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `jadwal_sekolah` ( 
			`id_jadwal_sekolah` INT NOT NULL PRIMARY KEY AUTO_INCREMENT ,  
			`id_sekolah` INT NOT NULL ,  
			`nama_jadwal` VARCHAR(255) NULL ,  
			`tahun_ajaran` VARCHAR(255) NULL ,  
			`status` INT NOT NULL ,  
			`created_at` DATETIME NULL ,  
			`updated_at` DATETIME NULL ,  
			`deleted_at` DATETIME NULL
		);";
		$this->db->query($sql);

		$sql = "ALTER TABLE `jadwal_guru` ADD `id_jadwal_sekolah` INT NULL AFTER `id_sekolah`;";
		$this->db->query($sql);
    }

    public function down(){
        $this->dbforge->drop_table('jadwal_sekolah');
    }
}