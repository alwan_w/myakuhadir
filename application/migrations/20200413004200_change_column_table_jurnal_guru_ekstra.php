<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_change_column_table_jurnal_guru_ekstra extends CI_Migration {

	public function up(){
		$date = date('Y-m-d H:i:s');
		$sql_up = "ALTER TABLE `jurnal_guru_ekstra` ADD `id_guru_piket` INT NULL AFTER `id_kelas`, ADD `tanggal_jurnal` DATE NULL AFTER `id_guru_piket`;";
		$this->db->query($sql_up);

		$sql_up = "ALTER TABLE `jurnal_guru_ekstra` DROP `jam_keluar`;";
		$this->db->query($sql_up);

		$sql_up = "ALTER TABLE `jurnal_guru_ekstra` CHANGE `dokumentasi` `tugas` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;";
		$this->db->query($sql_up);

		$sql_up = "ALTER TABLE `jadwal_guru` CHANGE `id_jam_pelajaran` `periode` VARCHAR(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;";
		$this->db->query($sql_up);
	}
}