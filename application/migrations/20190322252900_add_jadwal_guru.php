<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_jadwal_guru extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `jadwal_guru` (
			`id_jadwal_guru` varchar(128) NOT NULL PRIMARY KEY,
			`id_guru` varchar(128) NOT NULL,
			`id_kelas` varchar(128) NOT NULL,
			`id_jam_pelajaran` varchar(128) NOT NULL,
			`id_mata_pelajaran` varchar(128) NOT NULL,
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
		$this->db->query($sql);
	}

	public function down(){
		$this->dbforge->drop_table('jadwal_guru');
	}

}

