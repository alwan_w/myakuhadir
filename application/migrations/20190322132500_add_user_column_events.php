<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_user_column_events extends CI_Migration {

	public function up(){

		$fields = array(
			'config' => array(
				'type' => 'text'
			)
		);
		$this->dbforge->add_column('users',$fields);
		
    }

    public function down(){
        $this->dbforge->drop_table('events');
    }
}

