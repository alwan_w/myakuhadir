<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_table_kehadiran_guru extends CI_Migration {
	public function up()
	{
		$sql = "CREATE TABLE IF NOT EXISTS `kehadiran_guru` ( 
			`id_kehadiran_guru` INT NOT NULL PRIMARY KEY AUTO_INCREMENT ,  
			`id_sekolah` INT NOT NULL ,  
			`id_pengguna` INT NOT NULL ,  
			`status` INT(10) NOT NULL COMMENT '1 = belum pulang, 2 = sudah pulang' ,  
			`tanggal` DATE NOT NULL ,  
			`jam_datang` TIME NOT NULL ,  
			`jam_pulang` TIME NULL
		);";

		$this->db->query($sql);
    }

    public function down()
    {
        $this->dbforge->drop_table('kehadiran_guru');
    }
}