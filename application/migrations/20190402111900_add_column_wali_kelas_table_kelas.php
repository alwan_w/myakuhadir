<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_wali_kelas_table_kelas extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE kelas ADD COLUMN wali_kelas varchar(255) AFTER nm_ketua_kelas;";

		$this->db->query($sql_up);
	}
}

