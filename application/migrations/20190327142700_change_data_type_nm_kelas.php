<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_change_data_type_nm_kelas extends CI_Migration {

	public function up(){
		$sql_kelas 			= "ALTER TABLE `kelas` MODIFY COLUMN `nm_kelas` varchar(100)";

		$this->db->query($sql_kelas);
	}
}
