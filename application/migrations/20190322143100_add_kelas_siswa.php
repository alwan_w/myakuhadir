<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_kelas_siswa extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `kelas_siswa` (
			`id_kelas_siswa` varchar(128) NOT NULL PRIMARY KEY,
			`id_kelas` varchar(128) NOT NULL,
			`id_siswa` varchar(128) NOT NULL,
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
		$this->db->query($sql);
	}

	public function down(){
		$this->dbforge->drop_table('kelas_siswa');
	}

}

