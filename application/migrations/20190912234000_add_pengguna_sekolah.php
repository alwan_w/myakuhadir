<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_pengguna_sekolah extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `pengguna_sekolah` (
			`id_pengguna_sekolah` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`id_sekolah` int NOT NULL,
			`id_pengguna` int NOT NULL,
			`id_role` int NOT NULL,
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
		$exist = 0;
		if($this->db->table_exists('pengguna_sekolah'))
			$exist++;
		$this->db->query($sql);
		if($exist == 0)
		{
			$sql = "INSERT INTO `pengguna_sekolah` (id_pengguna, id_role, id_sekolah)
					SELECT id_pengguna,id_role,'1'
					FROM `pengguna`";
			$this->db->query($sql);
		}
    }

    public function down(){
        $this->dbforge->drop_table('pengguna_sekolah');
    }
}
