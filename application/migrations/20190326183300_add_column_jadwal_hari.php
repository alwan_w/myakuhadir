<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_jadwal_hari extends CI_Migration {

	public function up(){
		$sql = "ALTER TABLE jam_pelajaran ADD COLUMN hari VARCHAR(15) AFTER id_jam_pelajaran;";

		$this->db->query($sql);

	}
}

