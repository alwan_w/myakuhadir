<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_id_sekolah_table_kehadiran extends CI_Migration {

	public function up(){
		$sql_up		 	= "UPDATE `kehadiran` SET `kehadiran_verification` = 0 WHERE TIME(`kehadiran_timestamp`) <= '07:05:00' AND `kehadiran_verification` = 3;";

		$this->db->query($sql_up);

		$sql_up		 	= "ALTER TABLE `kehadiran` 
		ADD `id_sekolah` int NOT NULL AFTER `kehadiran_id`;";

		$this->db->query($sql_up);

		$sql_up		 	= "UPDATE `kehadiran` SET `id_sekolah` = 1;";

		$this->db->query($sql_up);
	}
}