<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_role extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `role` (
			`id_role` varchar(128) NOT NULL PRIMARY KEY,
			`nm_role` varchar(100) NOT NULL, 
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
		$sql_insert = "INSERT INTO role (`id_role`, `nm_role`, 	`created_at`)
				VALUES (1, 'siswa', '2019-03-21 16:25:15'),
					(2, 'admin', '2019-03-21 16:25:15'),
					(3, 'guru', '2019-03-21 16:25:15'),
					(4, 'pelatih', '2019-03-21 16:25:15');";
	$this->db->query($sql);
	$this->db->query($sql_insert);

}

public function down(){
	$this->dbforge->drop_table('role');
}

}

