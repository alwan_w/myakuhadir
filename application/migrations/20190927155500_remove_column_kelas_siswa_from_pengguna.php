<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_remove_column_kelas_siswa_from_pengguna extends CI_Migration {

	public function up(){
		$sql = "ALTER TABLE pengguna DROP kelas_siswa;";
		$this->db->query($sql);
	}
}