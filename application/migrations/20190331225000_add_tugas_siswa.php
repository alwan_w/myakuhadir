<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_tugas_siswa extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `tugas_siswa` (
			`id_tugas` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`id_jurnal` int NOT NULL,
			`jenis_tugas` int,
			`deskripsi_tugas` varchar(255),
			`status` int,
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
		$this->db->query($sql);
	}

	public function down(){
		$this->dbforge->drop_table('tugas_siswa');
	}

}

