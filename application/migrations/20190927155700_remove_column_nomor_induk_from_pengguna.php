<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_remove_column_nomor_induk_from_pengguna extends CI_Migration {

	public function up(){
		$sql_check = "SHOW COLUMNS FROM `pengguna_sekolah` LIKE 'nomor_induk'";
		$check = $this->db->query($sql_check)->result();
		if(count($check) < 1)
		{
			$sql = "ALTER TABLE pengguna_sekolah
				ADD `nomor_induk` varchar(20) AFTER `id_role`;";
			$this->db->query($sql);

			$sql = "UPDATE `pengguna_sekolah`, `pengguna`
				SET pengguna_sekolah.nomor_induk = pengguna.nomor_induk
				WHERE pengguna_sekolah.id_pengguna = pengguna.id_pengguna";
			$this->db->query($sql);

			$sql_check = "SHOW COLUMNS FROM `pengguna` LIKE 'nomor_induk'";
			$check = $this->db->query($sql_check)->result();
			if(count($check) > 0)
			{
				$sql = "ALTER TABLE pengguna DROP nomor_induk;";
				$this->db->query($sql);
			}
		}
	}
}