<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_id_kelas_table_jurnal_guru_ekstra extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE jurnal_guru_ekstra ADD COLUMN id_kelas int AFTER id_jadwal_guru;";

		$this->db->query($sql_up);
	}
}

