<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_kelas extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `kelas` (
			`id_kelas` int NOT NULL PRIMARY KEY,
			`nm_kelas` varchar(255) NOT NULL,
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
	$this->db->query($sql);
}

public function down(){
	$this->dbforge->drop_table('kelas');
}

}

