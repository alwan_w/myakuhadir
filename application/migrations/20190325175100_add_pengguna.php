<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_pengguna extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `pengguna` (
		`id_pengguna` varchar(128) NOT NULL PRIMARY KEY,
		`id_role` int NOT NULL,
		`kelas_siswa` int NOT NULL,
		`nm_pengguna` varchar(255) NOT NULL,
		`nomor_induk` varchar(20),
		`alamat` varchar (255),
		`no_hp` varchar (20),
		`email` varchar (100),
		`password` varchar(100),
		`created_at` datetime,
		`updated_at` datetime,
		`deleted_at` datetime
	);";
	$this->db->query($sql);
}

public function down(){
	$this->dbforge->drop_table('pengguna');
}

}

