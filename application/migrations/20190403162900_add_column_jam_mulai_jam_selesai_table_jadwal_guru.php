<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_jam_mulai_jam_selesai_table_jadwal_guru extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `jadwal_guru` 
		ADD `jam_mulai` TIME AFTER `id_jam_pelajaran`, 
		ADD `jam_selesai` TIME AFTER `jam_mulai`;


				;";

		$this->db->query($sql_up);
	}
}

