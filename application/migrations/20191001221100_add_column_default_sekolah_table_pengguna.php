<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_default_sekolah_table_pengguna extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `pengguna` 
		 ADD `default_sekolah` INT NULL AFTER `password`;";

		$this->db->query($sql_up);
	}
}