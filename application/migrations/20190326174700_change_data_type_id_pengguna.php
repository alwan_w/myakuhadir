<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_change_data_type_id_pengguna extends CI_Migration {

	public function up(){
		$sql = "ALTER TABLE `pengguna` MODIFY COLUMN `id_pengguna` INT auto_increment";
		$this->db->query($sql);
	}

	// public function down(){
	// 	$this->dbforge->drop_table('pengguna');
	// }

}

