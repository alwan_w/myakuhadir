<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_change_data_type_jam_hadir extends CI_Migration {

	public function up(){
		$sql			= "ALTER TABLE `jurnal_guru_ekstra` MODIFY COLUMN `jam_hadir` time ";
		
		$this->db->query($sql);

	}
}

