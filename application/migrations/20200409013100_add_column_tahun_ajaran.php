
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_tahun_ajaran extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `kelas` 
		 ADD `tahun_ajaran` VARCHAR(255) NULL AFTER `jumlah_siswa`;";
		$this->db->query($sql_up);

		$sql_up		 	= "UPDATE `kelas` SET `tahun_ajaran` = 'Ganjil 2019/2020' WHERE `kelas`.`tahun_ajaran` IS NULL;";

		$this->db->query($sql_up);

		$sql_up		 	= "ALTER TABLE `kelas_siswa` 
		 ADD `tahun_ajaran` VARCHAR(255) NULL AFTER `id_siswa`;";
		$this->db->query($sql_up);

		$sql_up		 	= "UPDATE `kelas_siswa` SET `tahun_ajaran` = 'Ganjil 2019/2020' WHERE `kelas_siswa`.`tahun_ajaran` IS NULL;";

		$this->db->query($sql_up);

		$sql_up		 	= "ALTER TABLE `jadwal_guru` 
		 ADD `tahun_ajaran` VARCHAR(255) NULL AFTER `id_mata_pelajaran`;";
		$this->db->query($sql_up);

		$sql_up		 	= "UPDATE `jadwal_guru` SET `tahun_ajaran` = 'Ganjil 2019/2020' WHERE `jadwal_guru`.`tahun_ajaran` IS NULL;";

		$this->db->query($sql_up);
	}
}