<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_id_pengabsen_table_kehadiran extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `kehadiran` 
		 ADD `id_pengguna_pengabsen` INT AFTER `kehadiran_verification`;";

		$this->db->query($sql_up);

		$sql_up		 	= "INSERT INTO `pengguna` (`id_pengguna`, `nm_pengguna`, `alamat`, `no_hp`, `email`, `password`, `default_sekolah`, `created_at`, `updated_at`, `deleted_at`) VALUES (0, 'Akuhadir System', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);";

		$this->db->query($sql_up);
		

		$sql_up		 	= "UPDATE `pengguna` SET `id_pengguna` = '0' WHERE `pengguna`.`id_pengguna` = ". $this->db->insert_id() .";";

		$this->db->query($sql_up);

		$sql_up		 	= "UPDATE `kehadiran` SET `id_pengguna_pengabsen` = 0 WHERE `id_sekolah` = 1;";

		$this->db->query($sql_up);
	}
}