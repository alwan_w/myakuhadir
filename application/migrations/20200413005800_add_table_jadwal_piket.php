<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_table_jadwal_piket extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS  `jadwal_piket` ( 
			`id_jadwal_piket` INT NOT NULL PRIMARY KEY AUTO_INCREMENT ,
			`id_sekolah` INT NOT NULL ,  
			`id_guru` INT NOT NULL ,  
			`hari` VARCHAR(10) NULL ,  
			`tahun_ajaran` VARCHAR(255) NULL ,  
			`created_at` DATETIME NULL ,  
			`updated_at` DATETIME NULL ,  
			`deleted_at` DATETIME NULL
		);";
		$this->db->query($sql);
    }

    public function down(){
        $this->dbforge->drop_table('jadwal_piket');
    }
}