<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_sekolah extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `sekolah` (
			`id_sekolah` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`nama_sekolah` varchar(255) NOT NULL,
		  	`logo_sekolah` varchar(200) NOT NULL,
		  	`alamat_sekolah` text NOT NULL,
		  	`website_sekolah` varchar (255),
		  	`email_sekolah` varchar (100),
		  	`telepon_sekolah` varchar (20),			
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
		$this->db->query($sql);
		$sql = "INSERT INTO `sekolah` (`id_sekolah`, `nama_sekolah`, `logo_sekolah`, `alamat_sekolah`, `website_sekolah`, `email_sekolah`, `telepon_sekolah`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'SMP Islam Al-Azhar 13 Surabaya', '', 'Surabaya', NULL, NULL, NULL, NULL, NULL, NULL);";
		$this->db->query($sql);
    }

    public function down(){
        $this->dbforge->drop_table('sekolah');
    }
}
