<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_jam_pelajaran extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `jam_pelajaran` (
			`id_jam_pelajaran` varchar(128) NOT NULL PRIMARY KEY,
			`jam_mulai` time,
			`jam_selesai` time,
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
		$this->db->query($sql);
	}

	public function down(){
		$this->dbforge->drop_table('jam_pelajaran');
	}

}

