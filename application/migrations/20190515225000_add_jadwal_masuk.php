<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_jadwal_masuk  extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS  `tb_jadwal_masuk` (
			`id_jm` INT NOT NULL AUTO_INCREMENT , `jm_masuk` VARCHAR(10) NOT NULL,`jm_judul` VARCHAR(50) NOT NULL , `status` INT NOT NULL , PRIMARY KEY (`id_jm`)
            );";
        $this->db->query($sql);
	}

	public function down(){
		$this->dbforge->drop_table('tb_jadwal_masuk');
	}

}

