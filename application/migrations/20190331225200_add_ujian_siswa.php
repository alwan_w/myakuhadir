<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_ujian_siswa extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `ujian_siswa` (
			`id_ujian` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`id_jurnal` int NOT NULL,
			`jenis_ujian` int,
			`deskripsi_ujian` varchar(255),
			`status` int,
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
		$this->db->query($sql);
	}

	public function down(){
		$this->dbforge->drop_table('ujian_siswa');
	}

}

