<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_id_sekolah_table_absensi_guru extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `absensi_guru` 
		ADD `id_sekolah` int NOT NULL AFTER `id_absensi_guru`;";

		$this->db->query($sql_up);

		$sql_up		 	= "UPDATE `absensi_guru` SET `id_sekolah` = 1;";

		$this->db->query($sql_up);
	}
}