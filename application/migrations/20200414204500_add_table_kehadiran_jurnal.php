<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_table_kehadiran_jurnal extends CI_Migration {
	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `kehadiran_jurnal` ( 
			`id_kehadiran_jurnal` INT NOT NULL PRIMARY KEY AUTO_INCREMENT ,  
			`id_jurnal` VARCHAR(100) NOT NULL , 
			`id_kelas` INT NOT NULL , 
			`id_kehadiran` INT NOT NULL ,  
			`kehadiran_verification` INT NULL ,  
			`tanggal_kehadiran` DATETIME NULL ,
			`jam_kehadiran` TIME NULL
		);";
		$this->db->query($sql);

		$sql = "ALTER TABLE `kehadiran` CHANGE `kehadiran_verification` `kehadiran_verification` INT NULL DEFAULT NULL COMMENT '0 = tepat, 1 = sakit, 2 = ijin, 3 = telat';";
		$this->db->query($sql);
    }

    public function down(){
        $this->dbforge->drop_table('kehadiran_jurnal');
    }
}