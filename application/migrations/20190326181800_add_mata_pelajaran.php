<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_mata_pelajaran extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `mata_pelajaran` (
			`id_mata_pelajaran` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`nm_mata_pelajaran` varchar(255) NOT NULL,
			`jenis_mata_pelajaran` int,
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
		$this->db->query($sql);
	}

	public function down(){
		$this->dbforge->drop_table('mata_pelajaran');
	}

}

