<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_table_google_user extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `google_user` (
			`id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`id_pengguna` int NOT NULL,
			`oauth_provider` varchar(255) NOT NULL,
		  	`oauth_uid` varchar(255) NOT NULL,
		  	`first_name` varchar (255) NOT NULL,
		  	`last_name` varchar (255) NOT NULL,
		  	`email` varchar (255) NOT NULL,
		  	`gender` varchar (10) NOT NULL,
		  	`locale` varchar (10) NOT NULL,
		  	`picture_url` varchar (255) NOT NULL,
		  	`profile_url` varchar (255) NOT NULL,
			`created_at` datetime,
			`updated_at` datetime,
			`deleted_at` datetime
		);";
		$this->db->query($sql);
    }

    public function down(){
        $this->dbforge->drop_table('google_user');
    }
}