<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_id_sekolah_table_tb_token extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `tb_token` 
		ADD `id_sekolah` int NOT NULL AFTER `toke_id`;";

		$this->db->query($sql_up);

		$sql_up		 	= "UPDATE `tb_token` SET `id_sekolah` = 1;";

		$this->db->query($sql_up);
	}
}