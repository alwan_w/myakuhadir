<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_foto_tema_table_pengguna extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `pengguna` ADD `foto_pengguna` VARCHAR(200) NULL AFTER `password`, ADD `tema_pengguna` VARCHAR(50) NULL AFTER `foto_pengguna`;";
		$this->db->query($sql_up);
	}
}