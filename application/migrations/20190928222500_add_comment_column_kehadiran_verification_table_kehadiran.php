<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_comment_column_kehadiran_verification_table_kehadiran extends CI_Migration {

	public function up(){
		$sql_up		 	= "ALTER TABLE `kehadiran` CHANGE `kehadiran_verification` `kehadiran_verification` TINYINT(1) NULL DEFAULT NULL COMMENT '0 = tepat, 1 = sakit, 2 = ijin, 3 = telat';";

		$this->db->query($sql_up);
	}
}
