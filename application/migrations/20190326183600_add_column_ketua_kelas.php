<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_ketua_kelas extends CI_Migration {

	public function up(){
		$sql = "ALTER TABLE kelas ADD COLUMN nm_ketua_kelas VARCHAR(255) AFTER nm_kelas;";

		$this->db->query($sql);

	}
}

