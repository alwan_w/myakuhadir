<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_dashboard_notes extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `dashboard_notes` (
			`id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`jenis` varchar(10) NOT NULL,
		  `notes` varchar(500) DEFAULT NULL,			
			`created_at` datetime,
			`updated_at` datetime			
		);";
		$this->db->query($sql);
    }

    public function down(){
        $this->dbforge->drop_table('dashboard_notes');
    }
}
