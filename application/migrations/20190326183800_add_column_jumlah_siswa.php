<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_column_jumlah_siswa extends CI_Migration {

	public function up(){
		$sql = "ALTER TABLE kelas ADD COLUMN jumlah_siswa int AFTER nm_ketua_kelas;";

		$this->db->query($sql);

	}
}

