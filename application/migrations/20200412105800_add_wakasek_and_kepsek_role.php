<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_wakasek_and_kepsek_role extends CI_Migration {

	public function up(){
		$date = date('Y-m-d H:i:s');
		$sql_up		 	= "INSERT INTO `role` (`id_role`, `nm_role`, `created_at`, `updated_at`, `deleted_at`) VALUES ('5', 'wakasek', '$date', NULL, NULL), ('6', 'kepsek', '$date', NULL, NULL);";

		$this->db->query($sql_up);
	}
}