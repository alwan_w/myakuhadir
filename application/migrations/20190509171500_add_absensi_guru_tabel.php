<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_absensi_guru_tabel extends CI_Migration {

	public function up(){
		/*$sql = "CREATE TABLE IF NOT EXISTS `absensi_guru` (
			`id_absensi_guru` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`kode_absensi` varchar(100) NOT NULL,
		  `nama` varchar(255) DEFAULT NULL,
		  `jenis` varchar(100) DEFAULT NULL,
			`timestamp` datetime,
			`min` int NOT NULL,
			`max` int NOT NULL,
			`created_at` datetime,
			`updated_at` datetime			
		);";*/
		$sql_max = "ALTER TABLE `absensi_guru` 
			ADD `max` int NOT NULL AFTER `timestamp`;";
		$sql_check = "SHOW COLUMNS FROM `absensi_guru` LIKE 'max'";
		$check = $this->db->query($sql_check)->result();
		if(count($check) < 1)
			$this->db->query($sql_max);
		
		$sql_min = "ALTER TABLE `absensi_guru`  
			ADD `min` int NOT NULL AFTER `timestamp`;";
		$sql_check = "SHOW COLUMNS FROM `absensi_guru` LIKE 'min'";
		$check = $this->db->query($sql_check)->result();
		if(count($check) < 1)
			$this->db->query($sql_min);
    }

    /*public function down(){
        $this->dbforge->drop_table('absensi_guru');
    }*/
}
