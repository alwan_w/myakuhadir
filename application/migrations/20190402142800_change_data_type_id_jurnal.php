<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_change_data_type_id_jurnal extends CI_Migration {

	public function up(){
		$sql_kelas 			= "ALTER TABLE `jurnal_guru_ekstra` MODIFY COLUMN `id_jurnal` varchar(100)";

		$this->db->query($sql_kelas);
	}
}
