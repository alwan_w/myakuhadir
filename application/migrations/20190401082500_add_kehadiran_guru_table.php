<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_kehadiran_guru_table extends CI_Migration {

	public function up(){
		$sql = "CREATE TABLE IF NOT EXISTS `absensi_guru` (
			`id_absensi_guru` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`kode_absensi` varchar(100) NOT NULL,
			`nama` varchar(255),
			`jenis` varchar(100),
			`timestamp` datetime,
			`updated_at` datetime,
			`created_at` datetime
		);";
		$this->db->query($sql);
	}

	public function down(){
		$this->dbforge->drop_table('absensi_guru');
	}

}