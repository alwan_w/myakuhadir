function notify(msg, type){
	    	switch(type) {
			  case 'success':
			    var icon = 'fa fa-check'
			    break;
			  case 'warning':
			    var icon = 'fa fa-warning'
			    break;
			  case 'info':
			    var icon = 'fa fa-info-circle'
			    break;
			  default:
			    var icon = 'fa fa-check'
			}
	        $.growl({
	            icon: icon,
	            message: msg,
	            url: ''
	        },{
	            element: 'body',
	            type: type,
	            allow_dismiss: true,
	            placement: {
	                from: 'top',
	                align: 'center'
	            },
	            offset: {
	                x: 30,
	                y: 30
	            },
	            spacing: 10,
	            z_index: 999999,
	            delay: 2500,
	            timer: 1000,
	            url_target: '_blank',
	            mouse_over: false,
	            animate: {
	                enter: 'animated fadeInDown',
	                exit: 'animated fadeInDown'
	            },
	            icon_type: 'class',
	            template: '<div data-growl="container" class="alert" role="alert">' +
	            '<button type="button" class="close" data-growl="dismiss">' +
	            '<span aria-hidden="true">&times;</span>' +
	            '<span class="sr-only">Close</span>' +
	            '</button>' +
	            '<span data-growl="icon"></span>' +
	            '<span data-growl="title"></span>' +
	            '<span data-growl="message"></span>' +
	            '<a href="#" data-growl="url"></a>' +
	            '</div>'
	        });
	    };