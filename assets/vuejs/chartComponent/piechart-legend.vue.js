Vue.component('pie-chart-legend', { 
  props: ['series', 'title'],
  data () {
    return {
      option: {
        chart: {
          colors: ['red', 'blue'],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
          },
          title: {
              text: this.title
          },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
            pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: false
                  },
                  showInLegend: true
                }
          },
          series: this.series
          // [{
          //     name: 'Brands',
          //     colorByPoint: true,
          //     data: [{
          //         name: 'Chrome',
          //         y: 61.41,
          //         sliced: true,
          //         selected: true
          //     }, {
          //         name: 'Internet Explorer',
          //         y: 11.84
          //     }]
          // }]
      }
    }
  },
  template: '<highcharts :options="option"></highcharts>'
})