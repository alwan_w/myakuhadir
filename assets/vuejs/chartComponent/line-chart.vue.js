Vue.component('line-chart', { 
  props: ['series', 'title'],
  data () {
    return {
      option: {        
        chart: {
            type: 'line'
        },
        title: {
            text: this.title
        },
        subtitle: {
            text: ''
        },
        xAxis: this.series.x,
        // {
        //     categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        // },
        yAxis: this.series.yAxis,
        // {
        //     title: {
        //         text: 'Jumlah'
        //     }
        // },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: this.series.series
        // [{
        //     name: 'Tokyo',
        //     data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6, 12, 121, 2, 31, 21, 31, 21, 31, 2, 1, 55, 7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6, 12, 121, 2, 31, 21, 31, 21, 31, 2, 1, 55]
        // }]
      }
    }
  },
  template: '<highcharts :options="option"></highcharts>'
})