VueRangedatePicker.default.install(Vue)
var vm = new Vue({
	el: '#guruapp',
	data () {
		return{
			baseurl: BASEURL,
			selectedDate: {
		        start: '',
		        end: '',
			},
			datestart: moment(new Date()).format('YYYY-MM-01'),
      		modal1: false,
      		dateend: moment(new Date()).format('YYYY-MM-DD'),
      		dateDisplay: moment(new Date()).format('MMMM'),
      		active: {
				listKelas: null
			},
      		listKelas: [],
      		topBar: {
      			jumlahGuru: 0,
      			updateData: '2000-01-01',
      			keterlambatan: 0,
      			presentase: 0,
      			kehadiran: 0
      		},      					
			pieKeterlambatan: [
				{
					name: 'Presentase', 
					colorByPoint: true,
					data: [],
				}
			],
			ringkasanPerorang: {
				min: 0, 
				plus: 0
			},
			pieJenisTerlambat: [
				{
					name: 'Presentase', 
					colorByPoint: true,
					data: [],
				}
			],
			keterlambatanHarian: {
				x: {
					categories: []
				},
				series: [{
					name: null,
					data: []
				}],
				yAxis: {
					title:{
						text: ''
					}
				}
			},
			pagination: {
		      rowsPerPage: 10
		    },
			keterlambatanPerorang: [],
			detailKeterlambatanPerorang: {
				dialog: false,
				nama: null,
				kode: null,
				data: []
			},
			headers: [
	          {
	            text: '#',
	            align: 'left',
	            sortable: false,
	            value: 'name'
	          },
	          { text: 'Kode', value: 'kode_absensi', sortable: false, },
	          { text: 'Nama', value: 'nama', sortable: false, },
	          { text: 'Min 5', value: 'mins', sortable: false, },
	          { text: 'Plus 5', value: 'pluss', sortable: false, },
	          { text: 'Total Keterlambatan', value: 'count', sortable: false, }
	        ],	        
			jurnal: [],
			jurnalKeterlambatan: 0, //keterlambatan jurnal perkelas
			jurnalKeterlambatanPerorang: {
				keterlambatanPerorang: [],
				headers: [
		          {
		            text: '#',
		            align: 'left',
		            sortable: false,		            
		          },
		          { text: 'Nama', value: 'kode_absensi', sortable: false, },
		          { text: 'Terlambat', value: 'nama', sortable: false, }
		        ]
			},
			notes: {
				catatan: [{
					tanggal: '2019-05-04',
					catatan: 'catatan di minggu ke 1 bulan 5'
				},{
					tanggal: '2019-05-06',
					catatan: 'catatan ke 2 '
				}],
				headers: [
		          {
		            text: 'tanggal',
		            align: 'left',
		            sortable: false,		            
		          },
		          { text: 'Catatan', value: 'kode_absensi', sortable: false, },
		          { text: 'Aksi', value: 'nama', sortable: false, }
		        ],
		        txtNotes: '',
		        dialog: false,
		        selectedNotes: '',
		        selectedId: 0,
		        selectedDate: ''
			},
			now: moment(new Date()).format('DD MMMM YYYY'),
			today: moment(new Date()).format('YYYY-MM-DD'),
			start: moment(new Date()).format('YYYY-MM-01'),
      		end: moment(new Date()).endOf('month').format('YYYY-MM-DD'),
      		newStart: moment(new Date()).startOf('month').format('YYYY-MM-DD'),
      		newEnd: moment(new Date()).endOf('month').add(1, 'd').format('YYYY-MM-DD'),
			events: [
				{
				  title: 'Weekly Meeting',
				  date: '2019-01-07',
				  time: '09:00',
				  duration: 45
				},
				{
				  title: 'Thomas\' Birthday',
				  date: '2019-01-10'
				},				
				{
				  title: 'Mash Potatoes',
				  date: '2019-01-10',
				  time: '12:30',
				  duration: 180
				}
			]
		}
	},
	watch: {
		start (val) {
			this.end = moment(val).endOf('month').format('YYYY-MM-DD')
		},
		end (val) {
			this.newStart = moment(val).startOf('month').format('YYYY-MM-DD')
			this.newEnd = moment(val).endOf('month').add(1, 'd').format('YYYY-MM-DD')
		},
		'selectedDate.start': function(val) {			
				this.datestart = moment(val).format('YYYY-MM-DD')
		},
		'selectedDate.end': function(val) {
				this.dateend = moment(val).format('YYYY-MM-DD')
		},
		newStart (val) {
			this.getGuruTerlambatJurnal (this.newStart, this.newEnd)
		}
	},
	filters: {
  		indonesianDate: function (date) {
	    	return moment(date).format('DD MMMM YYYY');
		},
		bulan: function (date) {
	    	return moment(date).format('MMMM');
		}
	},
	computed: {
	    // convert the list of events into a map of lists keyed by date
	    eventsMap () {
	      const map = {}
	      this.events.forEach(e => (map[e.date] = map[e.date] || []).push(e))
	      return map
	    }
	  },
	mounted() {		
		axios.get(this.baseurl+'DashboardSiswa/listKelas')
		.then(res=>{
			for(i in res.data.list){
				this.listKelas.push({
					'id_kelas': res.data.list[i].id_kelas,
					'nm_kelas': res.data.list[i].nm_kelas
				})
			}
			this.active.listKelas = res.data.list[0].id_kelas
			this.index(null, null, res.data.list[0].id_kelas)
			this.getGuruTerlambatJurnal (this.newStart, this.newEnd)
		})
		const date = new Date()
		this.getNotes()
		// console.log(this.listKelas)		
	},
	methods:{
		updateNote (id) {
			axios.post(this.baseurl+'DashboardGuru/guru_updateNotes/'+id, {
				notes: this.notes.selectedNotes
			})
			.then((res) => {
				this.notes.dialog = false
				this.getNotes()
			})
		},
		deleteNote (id) {			
			var r = confirm("Yakin akan menghapus?");
			if (r == true) {
				axios.get(this.baseurl+'DashboardGuru/guru_deleteNotes/'+id)
				.then(res => {
					this.notes.dialog = false
					this.getNotes()
				})
			} else {
				
			}
		},
		handlerNote(id) {
			this.notes.dialog = true
			axios.get(this.baseurl+'DashboardGuru/guru_viewNotes/'+id)
			.then(res=> {
				this.notes.selectedId = res.data.note.id
				this.notes.selectedNotes = res.data.note.notes
				this.notes.selectedDate = res.data.note.created_at
			})
		},
		createNotes () {
			axios.post(this.baseurl+ 'DashboardGuru/guru_addNotes', {
				jenis: 1,
				notes: this.notes.txtNotes
			})
			.then(res=> {
				this.getNotes()
				notes: ''
			})
		},
		getNotes() {
			axios.get(this.baseurl+ 'DashboardGuru/guru_getNotes')
			.then(res=> {
				this.notes.catatan = res.data.notes
			})
		},
		detailTerlambatHandle (id, start, end) {
			this.detailKeterlambatanPerorang.dialog = true
			if (start === null || end === null) {
				var query = ''
			}else {				
				var query = '/?start='+start+'&'+'end='+end
			}
			axios.get(this.baseurl+'DashboardGuru/guru_detailKeterlambatanOrang/'+id+'/'+query).then( res => {
				this.detailKeterlambatanPerorang.nama = res.data.nama
				this.detailKeterlambatanPerorang.kode = res.data.kode_absensi
				this.detailKeterlambatanPerorang.data = res.data.data
			})
		},
		index (start, end, kelas) {
			if (start === null || end === null) {
				var query = ''
			}else {				
				var query = '/?start='+start+'&'+'end='+end
			}
			axios.get(this.baseurl+'DashboardGuru/guru_presentaseTerlambat/'+query)
			.then(res=>{
				this.topBar.presentase = res.data.percent
			})

			axios.get(this.baseurl+'DashboardGuru/guru_kehadiran/'+query)
			.then(res=>{
				this.topBar.kehadiran = res.data.data
			})

			axios.get(this.baseurl+'DashboardGuru/guru_jumlahGuru/'+query)
			.then(res=>{
				this.topBar.jumlahGuru = res.data.count				
			})

			axios.get(this.baseurl+'DashboardGuru/guru_updateData/'+query)
			.then(res=>{
				this.topBar.updateData = res.data.date
			})

			axios.get(this.baseurl+'DashboardGuru/guru_keterlambatanMinMax/'+query)
			.then(res=>{
				this.ringkasanPerorang.min = res.data.data.min
				this.ringkasanPerorang.plus = res.data.data.plus
			})

			axios.get(this.baseurl+'DashboardGuru/guru_totalKeterlambatan'+query)
			.then(res=>{
				this.topBar.keterlambatan = res.data.keterlambatan
			})

			axios.get(this.baseurl+'DashboardGuru/guru_pieKeterlambatan'+query)
			.then(res=>{
				this.pieKeterlambatan[0].data = res.data.data
			})

			axios.get(this.baseurl+'DashboardGuru/guru_pieJenisKeterlambatan'+query)
			.then(res=>{
				this.pieJenisTerlambat[0].data = res.data.data
			})

			axios.get(this.baseurl+'DashboardGuru/guru_keterlambatanHarian'+query)
			.then(res=>{
				this.keterlambatanHarian.x.categories = res.data.labels
				this.keterlambatanHarian.series[0].data = res.data.count
				this.keterlambatanHarian.series[0].name = 'Keterlambatan'
				this.keterlambatanHarian.yAxis.title.text = 'Jumlah Keterlambatan (Orang)'
			})

			axios.get(this.baseurl+'DashboardGuru/guru_tablePalingSeringTelat'+query)
			.then(res=>{
				this.keterlambatanPerorang = res.data.data				
			})

			axios.get(this.baseurl+'DashboardGuru/guru_getJurnal/'+ kelas +'/'+query)
			.then(res=>{
				this.events = res.data.data	
			})

			axios.get(this.baseurl+'DashboardGuru/guru_jurnalJumlahTerlambat/'+ kelas +'/'+query)
			.then(res=>{
				this.jurnalKeterlambatan = res.data.data
			})
		},
		filterdate () {
			this.index(this.datestart, this.dateend, this.active.listKelas)			
		},
		filterKelas (start=null, end=null, id) {
			if (start === null || end === null) {
				var query = ''
			}else {				
				var query = '/?start='+start+'&'+'end='+end
			}
			this.active.listKelas = id;
			axios.get(this.baseurl+'DashboardGuru/guru_getJurnal/'+ id +'/'+query)
			.then(res=>{
				this.events = res.data.data	
			})
		},
		getGuruTerlambatJurnal (start=null, end=null) {
			if (start === null || end === null) {
				var query = ''
			}else {				
				var query = '/?start='+start+'&'+'end='+end
			}
			axios.get(this.baseurl+'DashboardGuru/guru_terlambatJurnalPerorang/'+query)
			.then(res=>{
				this.jurnalKeterlambatanPerorang.keterlambatanPerorang = res.data.data
			})
		},
		onDateSelected: function (daterange) {
		      this.selectedDate = daterange
		      this.filterdate()
		      self = this
		      setTimeout(function(args) {
		      	self.filterdate()
		      }, 600)
		}
	}
})