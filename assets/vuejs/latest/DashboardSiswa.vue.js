VueRangedatePicker.default.install(Vue)
var app = new Vue({
	el: '#siswaapp',
	data () {
		return {
			baseurl: BASEURL,
			selectedDate: {
		        start: '',
		        end: ''
			},
			datestart: moment(new Date()).format('YYYY-MM-01'),
      		modal1: false,
      		dateend: moment(new Date()).format('YYYY-MM-DD'),
      		dateDisplay: moment(new Date()).format('MMMM'),
			active: {
				listKelas: 0
			},
			listKelas: [{
				'id_kelas': 0,
				'nm_kelas': 'Keseluruhan'
			}],			
			topBar: {
				keterlambatan: '0',
				izin: '0',
				sakit: '0',
				jumlahSiswa: '0',
				presentase: '0',
				kehadiran: '0',
				ketidakhadiran: '0'
			},
			pieKeterlambatan: [
				{
					name: 'Presentase', 
					colorByPoint: true,
					data: [],
				}
			],
			pieKehadiran: [
				{
					name: 'Presentase', 
					colorByPoint: true,
					data: [],
				}
			],
			keterlambatanHarian: {
				x: {
					categories: []
				},
				series: [{
					name: null,
					data: []
				},{
					name: null,
					data: []
				}],
				yAxis: {
					title:{
						text: 'Jumlah keterlambatan kelas'
					}
				}
			},
			pagination: {
		      rowsPerPage: 5
		    },
			keterlambatanPerorang: {
				keterlambatanPerorang: [],
				headers: [
		          {
		            text: '#',
		            align: 'left',
		            sortable: false,		            
		          },
		          { text: 'Nomor Induk', value: 'nomor_induk', sortable: false, },
		          { text: 'Nama', value: 'nm_pengguna', sortable: false, },
		          { text: 'Kelas', value: 'nm_kelas', sortable: false, },
		          { text: 'Jumlah Keterlambatan', value: 'count', sortable: false, }
		        ]
			},
			detailKeterlambatanPerorang: {
				dialog: false,
				nama: null,
				kode: null,
				data: []
			},
			alasanTidakHadirHarian: {
				x: {
					categories: [],
					crosshair: true
				},
				series: [{
					name: null,
					data: []
				},{
					name: null,
					data: []
				},{
					name: null,
					data: []
				}],
				yAxis: [{
					title:{
						text: 'Kali'
					}
				},{
					tickInterval: 1,
		            minRange: 1,
		            allowDecimals: false,
		            startOnTick: true,
		            endOnTick: true
				}]
			}
		}
	},
	watch: {
		'selectedDate.start': function(val) {
				this.datestart = moment(val).format('YYYY-MM-DD')
		},
		'selectedDate.end': function(val) {
				this.dateend = moment(val).format('YYYY-MM-DD')
		}
	},
	filters: {
  		indonesianDate: function (date) {
	    	return moment(date).format('DD MMMM YYYY');
		}
	},
	mounted() {
		this.index(null, null)

		axios.get(this.baseurl+'DashboardSiswa/listKelas')
		.then(res=>{
			for(i in res.data.list){
				this.listKelas.push({
					'id_kelas': res.data.list[i].id_kelas,
					'nm_kelas': res.data.list[i].nm_kelas
				})
			}
		})
	},
	methods: {
		detailTerlambatHandle (id, start, end) {
			this.detailKeterlambatanPerorang.dialog = true
			if (start === null || end === null) {
				var query = ''
			}else {				
				var query = '/?start='+start+'&'+'end='+end
			}
			axios.get(this.baseurl+'DashboardSiswa/siswa_detailKeterlambatanOrang/'+id+'/'+query).then( res => {
				this.detailKeterlambatanPerorang.nama = res.data.nama
				this.detailKeterlambatanPerorang.kode = res.data.kode_absensi
				this.detailKeterlambatanPerorang.data = res.data.data
			})
		},
		index (start, end) {
			if (start === null || end === null) {
				var query = ''
			}else {				
				var query = '/?start='+start+'&'+'end='+end
			}

			axios.get(this.baseurl+'DashboardSiswa/siswa_jumlahSiswa/'+query)
			.then(res=>{
				this.topBar.jumlahSiswa = res.data.count
			})

			axios.get(this.baseurl+'DashboardSiswa/siswa_jumlahKeterlambatan'+query)
			.then(res=>{
				this.topBar.keterlambatan = res.data.terlambat
			})

			axios.get(this.baseurl+'DashboardSiswa/siswa_statusKehadiran'+query)
			.then(res=>{
				this.pieKeterlambatan[0].data = res.data.data
			})

			axios.get(this.baseurl+'DashboardSiswa/siswa_pieKehadiran'+query)
			.then(res=>{
				this.pieKehadiran[0].data = res.data.data
				this.topBar.presentase = res.data.presentase
				this.topBar.kehadiran = res.data.siswaMasuk
				this.topBar.ketidakhadiran = res.data.data[0].y + res.data.data[1].y + res.data.data[2].y
			})

			axios.get(this.baseurl+'DashboardSiswa/siswa_siswaTidakMasuk'+query)
			.then(res=>{
				this.topBar.izin = res.data.ijin
				this.topBar.sakit = res.data.sakit
			})

			axios.get(this.baseurl+'DashboardSiswa/siswa_keterlambatanHarian'+query)
			.then(res=>{				
				this.keterlambatanHarian.x.categories = res.data.terlambat.day
				this.keterlambatanHarian.series[0].data = res.data.terlambat.count
				this.keterlambatanHarian.series[0].name = 'Keterlambatan'
				this.keterlambatanHarian.series[1].data = res.data.hadir.count
				this.keterlambatanHarian.series[1].name = 'Kehadiran'
				this.keterlambatanHarian.yAxis.title.text = 'Orang'
			})

			axios.get(this.baseurl+'DashboardSiswa/siswa_dailyAbsent'+query)
			.then(res=>{
				this.alasanTidakHadirHarian.x.categories = res.data.labels
				this.alasanTidakHadirHarian.series[0].data = res.data.data[0].data
				this.alasanTidakHadirHarian.series[1].data = res.data.data[1].data
				this.alasanTidakHadirHarian.series[2].data = res.data.data[2].data
				this.alasanTidakHadirHarian.series[0].name = res.data.data[0].name
				this.alasanTidakHadirHarian.series[1].name = res.data.data[1].name
				this.alasanTidakHadirHarian.series[2].name = res.data.data[2].name
			})

			axios.get(this.baseurl+'DashboardSiswa/siswa_keterlambatanPerorang'+query)
			.then(res=>{
				this.keterlambatanPerorang.keterlambatanPerorang = res.data.data
			})
		},
		filterKelas (start=null, end=null, id) {
			if (start === null || end === null) {
				var query = ''
			}else {				
				var query = '/?start='+start+'&'+'end='+end
			}
			this.active.listKelas = id;
			if (id === 0) {
				this.index(start, end)
			} else {
				axios.get(this.baseurl+'DashboardSiswa/siswa_keterlambatanPerorang/'+id+'/'+query)
				.then(res=>{
					this.keterlambatanPerorang.keterlambatanPerorang = res.data.data
				})
				
				axios.get(this.baseurl+'DashboardSiswa/siswa_jumlahKeterlambatan/'+id+'/'+query)
				.then(res=>{
					this.topBar.keterlambatan = res.data.terlambat
				})
				axios.get(this.baseurl+'DashboardSiswa/siswa_statusKehadiran/'+id+'/'+query)
				.then(res=>{
					this.pieKeterlambatan[0].data = res.data.data
				})
				axios.get(this.baseurl+'DashboardSiswa/siswa_jumlahSiswa/'+id+'/'+query)
				.then(res=>{
					this.topBar.jumlahSiswa = res.data.count
				})
				axios.get(this.baseurl+'DashboardSiswa/siswa_pieKehadiran/'+id+'/'+query)
				.then(res=>{
					this.pieKehadiran[0].data = res.data.data
					this.topBar.presentase = res.data.presentase
					this.topBar.kehadiran = res.data.siswaMasuk
					this.topBar.ketidakhadiran = res.data.data[0].y + res.data.data[1].y + res.data.data[2].y
				})
				axios.get(this.baseurl+'DashboardSiswa/siswa_siswaTidakMasuk/'+id+'/'+query)
				.then(res=>{
					this.topBar.izin = res.data.ijin
					this.topBar.sakit = res.data.sakit
				})
				axios.get(this.baseurl+'DashboardSiswa/siswa_keterlambatanHarian/'+id+'/'+query)
				.then(res=>{
					this.keterlambatanHarian.x.categories = res.data.terlambat.day
					this.keterlambatanHarian.series[0].data = res.data.terlambat.count
					this.keterlambatanHarian.series[0].name = 'Keterlambatan'
					this.keterlambatanHarian.series[1].data = res.data.hadir.count
					this.keterlambatanHarian.series[1].name = 'Kehadiran'
					this.keterlambatanHarian.yAxis.title.text = 'Orang'
				})
				axios.get(this.baseurl+'DashboardSiswa/siswa_dailyAbsent/'+id+'/'+query)
				.then(res=>{
					this.alasanTidakHadirHarian.x.categories = res.data.labels
					this.alasanTidakHadirHarian.series[0].data = res.data.data[0].data
					this.alasanTidakHadirHarian.series[1].data = res.data.data[1].data
					this.alasanTidakHadirHarian.series[2].data = res.data.data[2].data
					this.alasanTidakHadirHarian.series[0].name = res.data.data[0].name
					this.alasanTidakHadirHarian.series[1].name = res.data.data[1].name
					this.alasanTidakHadirHarian.series[2].name = res.data.data[2].name
				})
			}
		},
		filterdate () {
			this.index(this.datestart, this.dateend)
			this.active.listKelas = 0
		},
		onDateSelected: function (daterange) {
		      this.selectedDate = daterange
		      this.filterdate()
		      self = this
		      setTimeout(function(args) {
		      	self.filterdate()
		      }, 600)
		}
	}
})